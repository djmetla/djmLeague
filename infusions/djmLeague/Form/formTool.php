<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: formTool.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class formTool extends djmLeague {

	/* Search opponent create */
	function actionCreateSearchOpponent($data) { 
		global $userdata;
		
			if (!iMEMBER) { 
				$this->location(BASEDIR."index.php");
			}

			djmDB::Select("SELECT * 
									FROM ".dbTeam." as t1
									LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league
									LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game
									WHERE team_id='%d'
									", $data['parent']);
			if (!djmDB::Num()) { $this->setError($this->__("FOTO_1")); }
			$team = djmDB::Data();
			
			$myTeam = $this->isTeamPlayer($team['team_id']);
			if (!$myTeam) { $this->location(urlDefault); }
			
			if ($myTeam == "PL") { 
				$this->setError($this->__("FOTO_2"));
			}
			
			$match_time_array = array(
				$data['search_time_day'],
				$data['search_time_month'],
				$data['search_time_year'],
				$data['search_time_hour'],
				$data['search_time_minute']
			);
			
			$match_time = SelectTime::CreateTime($match_time_array);			
			
			if ($match_time < (time()+600)) {
				$this->setError($this->__("FOTO_3"));
			}
			
			if ($team['league_match_use_maps'] == "YES") {
				$use_maps = true;
				if (!empty($data['maps'])) { 
					$maps = "";
					for($i=0; $i< count($data['maps']); $i++) {
						$maps .= ($maps != "" ? ".":"").stripinput($data['maps'][$i]);
					}
				} else {
					$this->setError($this->__("FOTO_4"));
				}
			} else {
				$use_maps = false;
			}
			
			// Positive points
			if (isset($data['limit_positive'])) {
				$limit_positive = true;
				$limit_positive_rules = array(100,50,25);
				if (in_array($data['limit_positive_points'], $limit_positive_rules)) {
					$limit_positive_points = $team['team_points'] + $data['limit_positive_points'];
				} else {
					$this->setError($this->__("FOTO_5"));
				}
			} else {
				$limit_positive = false;
			}
			
			// Positive points
			if (isset($data['limit_negative'])) {
				$limit_negative = true;
				$limit_negative_rules = array(100,50,25);
				if (in_array($data['limit_negative_points'], $limit_negative_rules)) {
					$limit_negative_points = $team['team_points'] - $data['limit_negative_points'];
				} else {
					$this->setError($this->__("FOTO_5"));
				}
			} else {
				$limit_negative = false;
			}
			
			if ($team['league_match_use_server'] == "YES") {
				$use_server = true;
				$use_server_choice = $data['match_server'];
			} else {
				$use_server = false;
			}
		
			djmDB::Select("SELECT search_id FROM ".dbSearch." WHERE search_team='%d' AND search_league='%d'", array($data['parent'], $team['league_id']));
			if (djmDB::Num()) {
				$temp = djmDB::Data();
				djmDB::Update(dbSearch, array(
					'search_team' => $data['parent'],
					'search_league' => $team['league_id'],
					'search_time' => $match_time,
					'search_server' => ($use_server ?  $use_server_choice : "NULL"),
					'search_map' => ($use_maps ? $maps : "NULL"),
					'search_limit_positive' => ($limit_positive ? $limit_positive_points : "NULL"),
					'search_limit_negative' => ($limit_negative ? $limit_negative_points : "NULL")
				), array('search_id' => $temp['search_id']));				
			} else {
				djmDB::Insert(dbSearch, array(
					'search_team' => $data['parent'],
					'search_league' => $team['league_id'],
					'search_time' => $match_time,
					'search_server' => ($use_server ?  $use_server_choice : "NULL"),
					'search_map' => ($use_maps ? $maps : "NULL"),
					'search_limit_positive' => ($limit_positive ? $limit_positive_points : "NULL"),
					'search_limit_negative' => ($limit_negative ? $limit_negative_points : "NULL")
				));
			}
			
			$this->setLog($this->__("FOTO_6"), $data['parent'], "team");
			$this->setDone($this->__("FOTO_7"), urlTeam.$data['parent']);
	}

	/* Redirect search opponent creating */
	function actionRedirectCreateSearchOpponent($data) {
		global $userdata;
		
			if (!iMEMBER) { 
				$this->location(BASEDIR."index.php");
			}
			
			if (empty($data['my_team']) OR !isset($data['my_team'])) {
				$this->setError($this->__("FOTO_8"));
			}
			
			$mteam = $this->isTeamPlayer($data['my_team']);
			if (!$mteam) { 
				$this->setError($this->__("FOTO_9"));
			}
			
			if ($mteam == "PL") {
				$this->setError($this->__("FOTO_10"));
			}
			
			$this->setDone($this->__("FOTO_11"), urlTool."opponent&amp;task=create&amp;myteam=".$data['my_team']);
	}

	/* Accept Search Opponent */
	function actionAcceptSearchOpponent($data) {
		global $userdata;
		
			if (!iMEMBER) { 
				$this->location(BASEDIR."index.php");
			}

			djmDB::Select("SELECT *
									FROM ".dbSearch." as t1
									LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.search_team
									LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.search_league
									LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
									WHERE search_id='%d'", $data['parent']);
			if(!djmDB::Num()) { $this->setError($this->__("FOTO_12"), urlTool."opponent&amp;task=all"); }
			$search = djmDB::Data();
			
			if ($search['league_enable'] == "NO") {
				$this->setError($this->__("FOTO_13"), urlTool."opponent&amp;task=all");
			}
			
			if ($search['search_time'] < time()) {
				$this->setError($this->__("FOTO_14"),  urlTool."opponent&amp;task=all");
			}
			
			djmDB::Select("SELECT t1.*, t2.*, t3.league_id 
									FROM ".dbTeamPlayer." as t1 
									LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team
									LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.player_team_league
									WHERE player_user='%d' AND player_position != 'PL' AND player_team_league='%d'
			", array($userdata['user_id'], $search['league_id']));
			$myteam = djmDB::Data();
			
			if (!djmDB::Num()) { 
				$this->setError($this->__("FOTO_15"), urlTool."opponent&amp;task=all");
			}			
	
			$iAmPlayer = $this->isTeamPlayer($search['team_id']);
			if ($iAmPlayer) { 
				$this->setError($this->__("FOTO_16"), urlTool."opponent&amp;task=all");
			}	
			
			if ($search['league_match_use_maps'] == "YES" AND (!isset($data['match_map']) OR empty($data['match_map']))) {
				$this->setError($this->__("FOTO_17"));
			}
			
			if ($search['league_match_use_maps'] == "YES") {
				$lmaps = explode(".", $search['league_match_maps']);
				if (in_array($data['match_map'], $lmaps)) {
					$map = $data['match_map'];
				} else {
					$this->setError($this->__("FOTO_18"));
				}
			} else {
				$map = "";
			}
			
			djmDB::Insert(dbMatch, array(
				'match_league' => $search['league_id'],
				'match_time' => $search['search_time'],
				'match_t1' => $search['team_id'],
				'match_t2' => $myteam['team_id'],
				'match_t1_points' => $search['team_points'],
				'match_t2_points' => $myteam['team_points'],
				'match_map' => $map,
				'match_status' => 0
			));
			
			$id = djmDB::ID();
			
			djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $search['team_id']);
			foreach(djmDB::fullData() as $player) { 
				$this->setEvent($this->__("FOTO_19"), $this->__("FOTO_19")." ".$search['team_name']." vs. ".$myteam['team_name']." ".$this->__("FOTO_20"), $player['player_user']);
			}	

			djmDB::Delete(dbSearch, array('search_id' => $data['parent']));
			$this->setLog($this->__("FOTO_21")." ".$search['team_name'], $myteam['team_id'], "team");
			$this->setLog($this->__("FOTO_22"), $id, "match");
			$this->setDone($this->__("FOTO_23"), urlMatch.$id);
	}



	/* Accept challenge */
	function actionAcceptChallenge($data)  {
		global $userdata;
		
			if (!iMEMBER) { 
				$this->location(BASEDIR."index.php");
			}

			djmDB::Select("SELECT * FROM ".dbChallenge." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.challenge_league WHERE challenge_id='%d'", $data['parent']);
			if (!djmDB::Num()) { $this->location(urlDefault); }
			$challenge = djmDB::Data();
			
			djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $challenge['challenge_from']);
			$t1 = djmDB::Data();
			
			djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $challenge['challenge_to']);
			$t2 = djmDB::Data();
			
			$isMember = $this->isTeamPlayer($t2['team_id']);
			if (!$isMember) { 
				$this->location(urlDefault);
			}
			
			if ((time()+300) > $challenge['challenge_time_match']) {
				$this->setError($this->__("FOTO_24"));
			}
			
			
			if (!isset($data['challenge_map'])) {
				$map = "";
			} else { 
				$map = htmlspecialchars($data['challenge_map']);
			}
			
			if ($isMember == "PL") {
				$this->setError($this->__("FOTO_25"));
			}
			
			djmDB::Insert(dbMatch, array(
				'match_league' => $challenge['challenge_league'],
				'match_time' => $challenge['challenge_time_match'],
				'match_t1' => $t1['team_id'],
				'match_t2' => $t2['team_id'],
				'match_t1_points' => $t1['team_points'],
				'match_t2_points' => $t2['team_points'],
				'match_map' => $map,
				'match_status' => 0
			));
			
			$id = djmDB::ID();
			
			djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $t1['team_id']);
			foreach(djmDB::fullData() as $player) { 
				$this->setEvent($this->__("FOTO_19"), $this->__("FOTO_19")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("FOTO_26"), $player['player_user']);
			}
			
			djmDB::Delete(dbChallenge, array('challenge_id' => $challenge['challenge_id']));
			$this->setLog($this->__("FOTO_62"), $id, "match");
			$this->setDone($this->__("FOTO_23"), urlMatch.$id);
	}

	/* Create challenge */
	function actionCreateChallenge($data) {
		global $userdata;
		
			if (!iMEMBER) { 
				$this->location(BASEDIR."index.php");
			}
			
			djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $data['parent']);
			if (!djmDB::Num()) { 
				$this->setError($this->__("FOTO_27",$data['parent']), urlDefault); 
			}
			$oponent = djmDB::Data();
		
			djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $oponent['team_league']);
			$league = djmDB::Data();
			
			if ($league['league_enable'] == "NO") {
				$this->setError($this->__("FOTO_28", $league['league_name']), urlTeam.$oponent['team_id']);
			}
			
			djmDB::Select("SELECT * FROM ".dbTeamPlayer." as t1 LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team WHERE player_user='%d' AND player_team_league='%d'", array($userdata['user_id'], $league['league_id']));
			if (!djmDB::Num()) { $this->setError($this->__("FOTO_29")." ".$league['league_name'], urlTeam.$oponent['team_id']); } 
			$me = djmDB::Data();	

			
			if ($oponent['team_status'] == "INACTIVE") {
				$this->setError($this->__("FOTO_30", $oponent['team_name']), urlTeam.$oponent['team_id']);
			}
			
			if ($me['team_status'] == "INACTIVE") {
				$this->setError($this->__("FOTO_31", $me['team_name']), urlTeam.$oponent['team_id']);
			} 

			if ($me['player_position'] == "PL") {
				$this->setError($this->__("FOTO_32", $oponent['team_name'])." ".$me['team_name'], urlTeam.$oponent['team_id']);
			}			
			
			if ($me['team_id'] == $oponent['team_id']) {
				$this->setError($this->__("FOTO_33"), urlTeam.$me['team_id']);
			}			
			
			$match_time_array = array(
				$data['challenge_time_day'],
				$data['challenge_time_month'],
				$data['challenge_time_year'],
				$data['challenge_time_hour'],
				$data['challenge_time_minute']
			);
			
			$match_time = SelectTime::CreateTime($match_time_array);
	
			if ((time()+500) > $match_time) {
				$this->setError($this->__("FOTO_34"));
			}
				
			if ($league['league_match_use_maps'] == "YES") {
				$use_maps = true;
				if (!empty($data['challenge_maps'])) { 
					$maps = "";
					for($i=0; $i< count($data['challenge_maps']); $i++) {
						$maps .= ($maps != "" ? ".":"").stripinput($data['challenge_maps'][$i]);
					}
				} else {
					$this->setError($this->__("FOTO_35"));
				}
			} else {
				$use_maps = false;
			}
			
			if ($league['league_match_use_server'] == "YES") {
				$use_server = true;
			} else {
				$use_server = false;
			}
	
		
			djmDB::Insert(dbChallenge, array(
				'challenge_league' => $league['league_id'],
				'challenge_from' => $me['team_id'],
				'challenge_to' => $oponent['team_id'],
				'challenge_time' => time(),
				'challenge_map' => ($use_maps ? $maps:""),
				'challenge_time_match' => $match_time,
				'challenge_server' => ($use_server ? $data['challenge_server']:"")
			));
			
			djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $oponent['team_id']);
			$players_count = djmDB::Num();
			foreach (djmDB::fullData() as $player) {
				$this->setEvent($this->__("FOTO_19"), $this->__("FOTO_36", $me['team_name'])." ".$this->__("FOTO_37", $oponent['team_name'])." ".$this->__("FOTO_38", $league['league_name'])." ".strftime("%d.%m.%Y %H:%M", $match_time), $player['player_user']);
			}
			
			$this->setLog($this->__("FOTO_39", $me['team_name']), $oponent['team_id'], "team");
			$this->setLog($this->__("FOTO_40")." ".$oponent['team_name']."", $me['team_id'], "team");
			$this->setDone($this->__("FOTO_41"), urlTeam.$oponent['team_id']);
	}

	/* Join to team */
	function actionJoinTeam($data) {
		global $userdata; 
		
		if (!iMEMBER) { 
			$this->location(BASEDIR."index.php");
		}	
		
		if (empty($data['team_id']) || !isnum($data['team_id'])) {
			$this->setError($this->__("FOTO_42"));
		}
		
		if (empty($data['team_password'])) {
			$this->setError($this->__("FOTO_43"));
		}
		
		// Check team exist
		djmDB::Select("SELECT * FROM ".dbTeam." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league WHERE team_id='%d'", $data['team_id']);
		if (!djmDB::Num()) {
			$this->setError($this->__("FOTO_44", $data['team_id']));
		}
		
		$team = djmDB::Data();
		
		// Check my team exist
		djmDB::Select("SELECT player_id FROM ".dbTeamPlayer." WHERE player_user='%d' AND player_team_league='%d'", array($userdata['user_id'], $team['league_id']));
		if (djmDB::Num()) {
			$this->setError($this->__("FOTO_45")." ".$team['league_name']);
		}
		
		// Check gameID
		$gameID = $this->checkGameID($userdata['user_id'], $team['team_league']);
		if (!$gameID) { 
			$this->setError($this->__("FOTO_46", $team['team_name']));
		}		
		
		// Check team max players
		$players = dbcount("(*)", dbTeamPlayer, "player_team='".$team['team_id']."'");
		if ($players >= $team['league_team_max']) {
			$this->setError($this->__("FOTO_47", $team['team_name'])." ".$team['league_team_max'].")");
		}
		
		// Check League active
		if ($team['league_enable'] == 'NO') {
			$this->setError($this->__("FOTO_48", $team['league_name']));
		}
		
		
		// Check password
		if ($team['team_password'] != $data['team_password']) {
			$this->setError($this->__("FOTO_49"));
		}
		
		djmDB::Insert(dbTeamPlayer, array(
			'player_user' => $userdata['user_id'],
			'player_team' => $team['team_id'],
			'player_team_league' => $team['team_league'],
			'player_time' => time(),
			'player_position' => 'PL'
		));
		$this->setLog($this->__("FOTO_50")." ".$team['team_name'], $userdata['user_id'], "player");
		$this->setLog($this->__("FOTO_50"), $team['team_id'], "team");
		$this->setDone($this->__("FOTO_51")." ".$team['team_name'], urlTeam.$team['team_id']);		
	}


	/* Create new team */
	function actionCreateTeam($data) {
		global $userdata; 
		
		if (!iMEMBER) { 
			$this->location(BASEDIR."index.php");
		}

		if (!isset($data['team_league'])) {
			$this->setError($this->__("FOTO_52"));
		}
		
		if (empty($data['team_name'])) {
			$this->setError($this->__("FOTO_53"));
		}
		
		if (empty($data['team_flag'])) {
			$this->setError($this->__("FOTO_54"));
		}
		
		if (empty($data['team_tag'])) { 
			$this->setError($this->__("FOTO_55"));
		}
		
		if (empty($data['team_password'])) {
			$this->setError($this->__("FOTO_56"));
		}
		

		$gameID = $this->checkGameID($userdata['user_id'], $data['team_league']);
		if (!$gameID) { 
			$this->setError($this->__("FOTO_57"));
		}
		
		
		djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $data['team_league']);
		if (!djmDB::Num()) {
			$this->setError($this->__("FOTO_58"));
		}
		$league = djmDB::Data();
		
		if ($league['league_enable'] == "NO") {
			$this->setError($this->__("FOTO_59", $league['league_name']));
		}		
		
		djmDB::Insert(dbTeam, array(
			'team_league' => $league['league_id'],
			'team_points' => $league['league_default_points'],
			'team_name' => htmlspecialchars($data['team_name']),
			'team_tag' => htmlspecialchars($data['team_tag']),
			'team_password' => htmlspecialchars($data['team_password']),
			'team_flag' => htmlspecialchars($data['team_flag']),
			'team_founder' => $userdata['user_id'],
			'team_created' => time(),
			'team_status' => 'inactive'
		));
		$id = djmDB::ID();
		djmDB::Insert(dbTeamPlayer, array(
			'player_user' => $userdata['user_id'],
			'player_team' => $id,
			'player_team_league' => $league['league_id'],
			'player_time' => time(),
			'player_position' => 'CR'
		));
		
		$this->setLog($this->__("FOTO_60")." ".$data['team_name'], $id, "team");
		$this->setDone($this->__("FOTO_61", $data['team_name']), urlTeam.$id);
	}
}