<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: formMatch.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class formMatch extends djmLeague {

	/* Close match */
	function actionCloseMatch($data) {
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$fromTeam = $t1['team_id'];
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$fromTeam = $t2['team_id'];
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_1"), urlMatch.$data['parent']); 
		}	

		if ($position == "PL") {
			$this->setError($this->__("FOM_1"), urlMatch.$data['parent']);
		}	
		
		if ($match['match_status'] != 1) {
			$this->setError($this->__("FOM_2")); 
		}
		 
		// ELO & Score
		if ($match['league_match_elo_type'] == "default") {
			$elo = new Elo("default");
			$score = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
		} elseif ($match['league_match_elo_type'] == "classic") {
			$elo = new Elo("classic");
			$score = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();				
		} elseif ($match['league_match_elo_type'] == "custom") {
			djmDB::Select("SELECT elo_value FROM ".dbElo." WHERE elo_id='%d'", $match['league_match_elo']);
			$elod = djmDB::Data();
			$elo = new Elo("default");
			$score = $elo->Constant($elod['elo_value'])->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
		}		
	
		$np_1  = $match['match_t1_points'] + $score[0];
		$np_2  = $match['match_t2_points'] + $score[1];
		
		if ($match['match_t1_score'] > $match['match_t2_score']) { 
			$winner = $match['match_t1']; $loser = $match['match_t2']; $draw = "NO";
		} elseif ($match['match_t1_score'] < $match['match_t2_score']) { 
			$winner = $match['match_t2']; $loser = $match['match_t1']; $draw = "NO";
		} elseif ($match['match_t1_score'] == $match['match_t2_score']) { 
			$winner = NULL; $loser = NULL; $draw = "YES";
		}
		
		djmDB::Update(dbTeam, array('team_points' => $np_1), array('team_id' => $match['match_t1']));
		djmDB::Update(dbTeam, array('team_points' => $np_2), array('team_id' => $match['match_t2']));
		
		djmDB::Update(dbMatch, array(
			'match_winner' => $winner,
			'match_losser' => $loser,
			'match_draw' => $draw,
			'match_status' => 2
		), array('match_id' => $match['match_id']));

		$this->setLog($this->__("FOM_3"), $data['parent']);
		$this->setDone($this->__("FOM_4"), urlMatch.$data['parent']);
	}

	/* Change Server */
	function actionEditServer($data) {
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$fromTeam = $t1['team_id'];
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$fromTeam = $t2['team_id'];
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_5"), urlMatch.$data['parent']); 
		}	

		if ($position == "PL") {
			$this->setError($this->__("FOM_5"), urlMatch.$data['parent']);
		}

		if (($match['match_time']+$match['league_match_server_time']) < time()) {
			$this->setError($this->__("FOM_6"), urlMatch.$_GET['match']);
		}

		if (empty($data['match_server'])) {
			$this->setError($this->__("FOM_7"));
		}

		djmDB::Update(dbMatch, array('match_server' => htmlspecialchars($data['match_server'])), array('match_id' => $data['parent']));
		$this->setLog($this->__("FOM_8"), $data['parent']);
		$this->setDone($this->__("FOM_9"));
	}

	/* Make Protest */
	function actionCreateProtest($data) {
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$fromTeam = $t1['team_id'];
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$fromTeam = $t2['team_id'];
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_10"), urlMatch.$data['parent']); 
		}	

		if ($position == "PL") {
			$this->setError($this->__("FOM_10"), urlMatch.$data['parent']);
		}	
		
		if (($match['match_time'] + $match['league_match_protest_delay']) > time() || ($match['match_time'] + $match['league_match_protest_delay_stop']) < time()) {
			$this->setError($this->__("FOM_11"));
		} 		
		
		if (empty($data['protest_type'])) {
			$this->setError($this->__("FOM_12"));
		}
		
		djmDB::Select("SELECT * FROM ".dbProtestPattern." WHERE pattern_id='%d'", $data['protest_type']);
		if (!djmDB::Num()) { $this->setError($this->__("FOM_13")); }
		$pattern = djmDB::Data();
		
		if ($pattern['pattern_use_custom_description'] == "1" && empty($data['protest_custom'])) {
			$this->setError($this->__("FOM_14", $pattern['pattern_name']));
		}
		
		djmDB::Insert(dbProtest, array(
			'protest_pattern' => $pattern['pattern_id'],
			'protest_match' => $match['match_id'],
			'protest_time' => time(),
			'protest_from' => $userdata['user_id'],
			'protest_from_team' => $fromTeam,
			'protest_description' => (!empty($data['protest_custom']) ? htmlspecialchars($data['protest_custom']):""),
			'protest_status' => 0
		));
		
			$team = ($fromTeam == $t1['team_id'] ? $t2['team_id'] : $t1['team_id']);
			djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $team);
			foreach(djmDB::fullData() as $player) { 
				$this->setEvent($this->__("FOM_15"), $this->__("FOM_16")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("FOM_17"), $player['player_user']);
			}		
		
		$this->setLog($this->__("FOM_18"), $match['match_id']);
		$this->setDone($this->__("FOM_19"));		
	}

	/* Make Request */
	function actionCreateRequest($data) {
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$fromTeam = $t1['team_id'];
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$fromTeam = $t2['team_id'];
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_20"), urlMatch.$data['parent']);
		}	

		if ($position == "PL") {
			$this->setError($this->__("FOM_20"), urlMatch.$data['parent']);
		}	
		
		if (($match['match_time'] + $match['league_match_request_delay']) > time() || ($match['match_time'] + $match['league_match_request_delay_stop']) < time()) {
			$this->setError($this->__("FOM_21"));
		} 

		if (empty($data['request_type'])) { 
			$this->setError($this->__("FOM_22"));
		}
		
		if (empty($data['request_user'])) { 
			$this->setError($this->__("FOM_23"));
		}

		djmDB::Select("SELECT * FROM ".dbRequestPattern." WHERE pattern_id='%d'", $data['request_type']);
		if (!djmDB::Num()) {	$this->setError($this->__("FOM_24")); }
		$pattern = djmDB::Data();
		
		djmDB::Insert(dbRequest, array(
			'request_match' => $match['match_id'], 
			'request_pattern' => $pattern['pattern_id'], 
			'request_author' =>  $userdata['user_id'],
			'request_author_team' => $fromTeam,
			'request_to' => $data['request_user'],
			'request_time' => time()
		));
		
		$team = ($fromTeam == $t1['team_id'] ? $t2['team_id'] : $t1['team_id']);
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $team);
		foreach(djmDB::fullData() as $player) { 
			$this->setEvent($this->__("FOM_25"), $this->__("FOM_26")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("FOM_27")." <a href='".urlMatch.$data['parent']."'>".$this->__("FOM_29")."</a>", $player['player_user']);
		}			
		
		$this->setEvent($this->__("FOM_25"), $this->__("FOM_26")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("FOM_28")." <a href='".urlMatch.$data['parent']."'>".$this->__("FOM_29")."</a>", $data['request_user']);
		
		$this->setLog($this->__("FOM_30"), $match['match_id']);
		$this->setDone($this->__("FOM_31"));
	}


	/* Request host */
	function actionRequestHost($data) {
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$fromTeam = $t1['team_id'];
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$fromTeam = $t2['team_id'];
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_32"), urlMatch.$data['parent']);
		}	

		if ($position == "PL") {
			$this->setError($this->__("FOM_32"), urlMatch.$data['parent']);
		}	
		
		if (dbcount("(*)", dbMatchRequest, "request_type='guest' AND request_from_team='".$fromTeam."' AND request_status!='REJECT'") >= $match['league_match_host_num']) {
			$this->setError($this->__("FOM_33"));
		}
		
		if (empty($data['match_host']) OR !isnum($data['match_host'])) {
			$this->setError($this->__("FOM_34"));
		}
		
		djmDB::Select("SELECT * FROM ".dbMatchRequest." WHERE request_type='guest' AND request_from_team='%d' AND request_status!='REJECT'", $fromTeam);
		if (djmDB::Num()) {
			foreach(djmDB::fullData() as $re) {
				if ($re['request_type_guest'] == $data['match_host']) {
					$this->setError($this->__("FOM_35"));
				}
			}
		}
		
		djmDB::Insert(dbMatchRequest, array(
			'request_match' => $match['match_id'],
			'request_from' => $userdata['user_id'],
			'request_from_team' => $fromTeam,
			'request_type' => 'guest',
			'request_type_guest' => $data['match_host'],
			'request_time' => time(),
			'request_status' => 'WAIT'
		));
		
		$team = ($fromTeam == $t1['team_id'] ? $t2['team_id'] : $t1['team_id']);
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $team);
		foreach(djmDB::fullData() as $player) { 
			$this->setEvent($this->__("FOM_36"), $this->__("FOM_26")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("FOM_37")." <a href='".urlMatch.$data['parent']."'>".$this->__("FOM_29")."</a>", $player['player_user']);
		}				
		
		$this->setLog($this->__("FOM_38"), $data['parent']);
		$this->setDone($this->__("FOM_39"));		
	}

	/* Request Delete */
	function actionRequestDelete($data) {
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$fromTeam = $t1['team_id'];
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$fromTeam = $t2['team_id'];
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_40"), urlMatch.$data['parent']);
		}	

		if ($position == "PL") {
			$this->setError($this->__("FOM_40"), urlMatch.$data['parent']);
		}
		
		if (dbcount("(*)", dbMatchRequest, "request_type='delete' AND request_status!='REJECT' AND request_match='".$match['match_id']."'")) {
			$this->setError($this->__("FOM_41"));
		}
		
		djmDB::Insert(dbMatchRequest, array(
			'request_match' => $match['match_id'],
			'request_from' => $userdata['user_id'],
			'request_from_team' => $fromTeam,
			'request_type' => 'delete',
			'request_time' => time(),
			'request_status' => 'WAIT'
		));
		
		$team = ($fromTeam == $t1['team_id'] ? $t2['team_id'] : $t1['team_id']);
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $team);
		foreach(djmDB::fullData() as $player) { 
			$this->setEvent($this->__("FOM_42"), $this->__("FOM_26")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("FOM_43")." <a href='".urlMatch.$data['parent']."'>".$this->__("FOM_29")."</a>", $player['player_user']);
		}				
		
		$this->setLog($this->__("FOM_44"), $data['parent']);
		$this->setDone($this->__("FOM_45"));
	}

	/* Add Result */
	function actionAddResult($data) { 
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_46"), urlMatch.$data['parent']);
		}	
		
		if (!dbcount("(*)", dbMatchMedia, "media_match='".$data['parent']."'")) {
			$this->setError($this->__("FOM_47"));
		}
		
		
		if ($data['t1_score'] == "") {
			$this->setError($this->__("FOM_48"));
		}
		
		if ($data['t2_score'] == "") {
			$this->setError($this->__("FOM_49"));
		}		
		
		if (!isnum($data['t1_score']) OR !isnum($data['t2_score'])) {
			$this->setError($this->__("FOM_50"));
		}
		
		if ($data['t1_score'] > $match['league_match_max_score'] || $data['t2_score'] > $match['league_match_max_score']) {
			$this->setError($this->__("FOM_51"));
		}
		
		djmDB::Update(dbMatch, array(
			'match_t1_score' => $data['t1_score'],
			'match_t2_score' => $data['t2_score'],
			'match_status' => 1
		), array('match_id' => $data['parent']));
		
		if ($match['match_status'] == 1) {
			$this->setLog($this->__("FOM_52"), $data['parent']);
		} else {
			$this->setLog($this->__("FOM_53"), $data['parent']);
		}
		$this->setDone($this->__("FOM_54"));
		
	}


	/* Add Extern Link */
	function actionAddExternLink($data) { 
		global $userdata;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_55"), urlMatch.$data['parent']);
		}
		
		if (empty($data['link_type'])) {
			$this->setError($this->__("FOM_56"));
		}
		
		if (empty($data['link_link'])) {
			$this->setError($this->__("FOM_57"));
		}		
		
		if (!filter_var($data['link_link'], FILTER_VALIDATE_URL)) {
			$this->setError($this->__("FOM_58"));
		}
		
		djmDB::Insert(dbMatchLinks, array(
			'link_match' => $data['parent'],
			'link_user' => $userdata['user_id'],
			'link_time' => time(),
			'link_target' => htmlspecialchars($data['link_link']),
			'link_type' => $data['link_type']
		));
		
		$this->setLog($this->__("FOM_59"), $data['parent']);
		$this->setDone($this->__("FOM_60"));		
	}

	/* Upload media */
	function actionUploadMedia($data) {
		global $userdata, $dbConfig;
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}

		djmDB::Select("SELECT * FROM ".dbMatch." WHERE match_id='%d'", $data['parent']);
		if (!djmDB::Num()) { 
			$this->location(urlDefault);
		}
		$match = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();	
		
		if ($this->isTeamPlayer($t1['team_id'])) {
			$position = $this->isTeamPlayer($t1['team_id']);
		} elseif ($this->isTeamPlayer($t2['team_id'])) {
			$position = $this->isTeamPlayer($t2['team_id']);
		} else {
			$this->setError($this->__("FOM_61"), urlMatch.$data['parent']);
		}
		
		if ($position == "PL") {
			$this->setError($this->__("FOM_61"), urlMatch.$data['parent']);
		}
		
		
		if (!is_dir(pathMedia."Match/".$match['match_league']."/".$data['parent'])) {
			mkdir(pathMedia."Match/".$match['match_league']."/".$data['parent']);
		}
		
		if (!is_uploaded_file($_FILES['media']['tmp_name'][0])) { 
			$this->setError($this->__("FOM_62"));
		}
		
		$max_size = $dbConfig['settings_upload_size'] * (1024 * 1024);
		$upload = new djmUpload('media');
		$image = $upload	->setPath(pathMedia."Match/".$match['match_league']."/".$data['parent']."/")
									->setSize($max_size) 
									->setRes(1920,1080)
									->setExt(array('.jpg','.png','.bmp'))
									->Multiple()
									->Thumbnail(120,70)
									->Upload();
		
		for($i=0; $i < count($image['images']); $i++) {
			djmDB::Insert(dbMatchMedia, array(
				'media_match' => $data['parent'],
				'media_user' => $userdata['user_id'],
				'media_time' => time(),
				'media_item' => $image['images'][$i],
				'media_type' => $image['type'][$i]
			));
		}
		
		$this->setLog($this->__("FOM_63", count($image['images'])), $data['parent']);
		$this->setDone($this->__("FOM_64"));
	}
}