<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: formAdmin.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); } 

class formAdmin extends djmLeague {

	/* New Ban */
	function actionNewBan($data) {
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_BANLIST')) {
			$this->location(urlAdminIndex);
		}		
		
		$url = urlAdmin."a_banlist#NewBan";
		
		if (empty($data['ban_user'])) {
			$this->setError($this->__("FORM_1"), $url);
		}
		
		$exp_s = array(
			$data['ban_expiration_day'],
			$data['ban_expiration_month'],
			$data['ban_expiration_year'],
			$data['ban_expiration_hour'],
			$data['ban_expiration_minute']
		);

		$expiration = SelectTime::CreateTime($exp_s);	
	
		if ($expiration < time()) {
			$this->setError($this->__("FORM_2"), $url);
		}
		
		if (dbcount("(*)", dbBan, "ban_user='".$data['ban_user']."' AND (ban_expiration>".$expiration." OR ban_permanent='YES')")) {
			$this->setError($this->__("FORM_3"), $url);
		}
		
		if (isset($data['ban_permanent'])) {
			$permanent = "YES";
		} else {
			$permanent = "NO";
		}
		
		if (empty($data['ban_reason'])) {
			$this->setError($this->__("FORM_4"), $url);
		}
		
		djmDB::Insert(dbBan, array(
			'ban_user' => $data['ban_user'],
			'ban_time' => time(),
			'ban_expiration' => $expiration,
			'ban_permanent' => $permanent,
			'ban_admin' => $userdata['user_id'],
			'ban_reason' => htmlspecialchars($data['ban_reason'])
		));
		
		$this->setAdminLog($this->__("FORM_5")." #".$data['ban_user']);
		$this->setDone($this->__("FORM_6"), $url);
	}


	/* Make Penalties */
	function actionMakePenalties($data) {
		global $userdata;
		if (!$this->isAdmin('ACCESS_PENALTIES_AND_POINTS')) {
			$this->location(urlAdminIndex);
		}		
	
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $data['parent']);
		
		if(!djmDB::Num()) {
			$this->setError($this->__("FORM_7"));
		}
		
		$team = djmDB::Data();
		
		if (empty($data['complaint_value'])) {
			$this->setError($this->__("FORM_8"));
		}
		
		if (empty($data['complaint_message'])) {
			$this->setError($this->__("FORM_9"));
		}
		
		if (!isnum($data['complaint_value'])) {
			$this->setError($this->__("FORM_10"));
		}
		
		if ($data['complaint_value'] > 100) {
			$this->setError($this->__("FORM_11"));
		}
		
		if ($data['complaint_value'] > $team['team_points']) {
			$this->setError($this->__("FORM_12"));
		}
		
		if ($data['complaint_type'] == "+") {
			$newPts = $team['team_points'] + $data['complaint_value'];
		} elseif ($data['complaint_type'] == "-") {
			$newPts = $team['team_points'] - $data['complaint_value'];
		} else {
			$this->setError($this->__("FORM_13"));
		}
		
		djmDB::Update(dbTeam, array('team_points' => $newPts), array('team_id' => $team['team_id']));
		djmDB::Insert(dbComplaint, array(
			'complaint_time' => time(),
			'complaint_team' => $team['team_id'],
			'complaint_type' => $data['complaint_type'],
			'complaint_value' => $data['complaint_value'],
			'complaint_admin' => $userdata['user_id'],
			'complaint_reason' => $data['complaint_message']
		));
		
		$this->setLog(($data['complaint_type'] == "+" ? $this->__("FORM_14"):$this->__("FORM_15"))." ".$data['complaint_type']." ".$data['complaint_value']." ".$this->__("FORM_16"), $team['team_id'], "team");
		$this->setAdminLog(($data['complaint_type'] == "+" ? $this->__("FORM_14"):$this->__("FORM_15"))." ".$this->__("FORM_17")." ".$team['team_name']." (".$data['complaint_type']." ".$data['complaint_value'].")");
		$this->setDone($this->__("FORM_18"), $url);
	}

	/* Go Penalties */
	function actiongoPenalties($data) {
		global $userdata;
		if (!$this->isAdmin('ACCESS_PENALTIES_AND_POINTS')) {
			$this->location(urlAdminIndex);
		}		
		
		if (empty($data['team_id'])) { 
			$this->setError($this->__("FORM_19"));
		}
		
		if (!isnum($data['team_id'])) {
			$this->setError($this->__("FORM_20"));
		}
		
		djmDB::Select("SELECT team_id, team_name FROM ".dbTeam." WHERE team_id='%d'", $data['team_id']);
		if (!djmDB::Num()) {
			$this->setError($this->__("FORM_21", $data['team_id']));
		}
		
		$team = djmDB::Data();
		
		$this->setAdminLog($this->__("FORM_22")." #".$team['team_id']." (".$team['team_name'].")");
		$this->setDone($this->__("FORM_23")." ".$team['team_name'], urlAdmin."a_penalties_and_points&amp;id=".$team['team_id']);
	}

	/* Cloud Registration */
	function actionCloudRegistration($data) {
		global $userdata;
		if (!$this->isAdmin('ACCESS_M_MODIFICATION')) {
			$this->location(urlAdminIndex);
		}
		
		require_once(pathLibs."Classes/Update.php");
		$update = new djmUpdate();
		$update->cloudRegister();
		unset($update);
		
		$this->setAdminLog($this->__("FORM_24"));
		$this->setDone($this->__("FORM_25"));
	}


	/* Close Protest */
	function actionCloseProtest($data) {
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_PROTESTS')) {
			$this->location(urlAdminIndex);
		}		

		djmDB::Select("SELECT * FROM ".dbProtest." WHERE protest_admin='%d' AND protest_status!='2' AND protest_id='%d'", array($userdata['user_id'], $data['parent']));
		
		if(!djmDB::Num()) {
			$this->setError($this->__("FORM_26"));
		}
		
		$protest = djmDB::Data();
		
		if (empty($data['protest_admin_description'])) {
			$this->setError($this->__("FORM_27"));
		}
		
		if (!dbcount("(match_id)", dbMatch, "match_id='".$protest['protest_match']."' AND match_status='2'")) {
			$this->setError($this->__("FORM_28"));
		}
		
		
		djmDB::Update(dbProtest, array(
			'protest_admin_description' => htmlspecialchars($data['protest_admin_description']),
			'protest_status' => 2
		), array('protest_id' => $protest['protest_id']));
		
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND player_position!='PL'", $protest['protest_match']);
		if (djmDB::Num()) {
			foreach(djmDB::fullData() as $p) {
				$this->setEvent($this->__("FORM_29"), $this->__("FORM_30")." <a href='".urlMatch.$protest['protest_match']."'>#".$protest['protest_match']." ".$this->__("FORM_31")." #".$protest['protest_id']." ".$this->__("FORM_32")." ".$userdata['user_name'], $p['player_user']);
			}
		}
		
		$this->setLog($this->__("FORM_33")." #".$protest['protest_id'], $protest['protest_match'], "match");
		$this->setAdminLog($this->__("FORM_34")." #".$protest['protest_id']);
		$this->setDone($this->__("FORM_35"), urlAdmin."a_protests&amp;page=my");
	}

	/* Close match in protest */
	function actionCloseMatch($data) {
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_PROTESTS')) {
			$this->location(urlAdminIndex);
		}		
	
		$url = urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent'];
	
		djmDB::Select("SELECT * FROM ".dbProtest." WHERE protest_status!='2' AND protest_id='%d'", $data['parent']);
		
		if(!djmDB::Num()) {
			$this->setError($this->__("FORM_13"), $url);
		}
		
		$protest = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent2']);
		$match = djmDB::Data();
		
		if ($protest['protest_match'] != $match['match_id']) { 
			$this->setError($this->__("FORM_13"), $url);
		}
		
		if ($match['match_status'] != 1) {
			$this->setError($this->__("FORM_36"), $url);
		}
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1 = djmDB::Data();

		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2 = djmDB::Data();		
		
		// ELO & Score
		if ($match['league_match_elo_type'] == "default") {
			$elo = new Elo("default");
			$score = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
		} elseif ($match['league_match_elo_type'] == "classic") {
			$elo = new Elo("classic");
			$score = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();				
		} elseif ($match['league_match_elo_type'] == "custom") {
			djmDB::Select("SELECT elo_value FROM ".dbElo." WHERE elo_id='%d'", $match['league_match_elo']);
			$elod = djmDB::Data();
			$elo = new Elo("default");
			$score = $elo->Constant($elod['elo_value'])->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
		}	

		$np_1 = $match['match_t1_points'] + $score[0];
		$np_2 = $match['match_t2_points'] + $score[1];
		
		if ($match['match_t1_score'] > $match['match_t2_score']) { 
			$winner = $match['match_t1']; $loser = $match['match_t2']; $draw = "NO";
		} elseif ($match['match_t1_score'] < $match['match_t2_score']) { 
			$winner = $match['match_t2']; $loser = $match['match_t1']; $draw = "NO";
		} elseif ($match['match_t1_score'] == $match['match_t2_score']) { 
			$winner = NULL; $loser = NULL; $draw = "YES";
		}

		djmDB::Update(dbTeam, array("team_points" => $np_1), array('team_id' => $t1['team_id']));
		djmDB::Update(dbTeam, array("team_points" => $np_2), array('team_id' => $t2['team_id']));
		
		djmDB::Update(dbMatch, array(
			'match_winner' => $winner,
			'match_losser' => $loser,
			'match_draw' => $draw,
			'match_status' => 2
		), array('match_id' => $match['match_id'])); 
		
		$this->setLog($this->__("FORM_37"), $match['match_id'], "match");
		$this->setAdminLog($this->__("FORM_38", $data['parent2'])." #".$data['parent']);
		$this->setDone($this->__("FORM_39"), $url);	
	}

	/* New complaint in protest */
	function actionNewComplaint($data) {
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_PROTESTS')) {
			$this->location(urlAdminIndex);
		}		
	
		$url = urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent'];
		
		if (empty($data['complaint_team'])) {
			$this->setError($this->__("FORM_40"), $url);
		}
		
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $data['complaint_team']);
		
		if(!djmDB::Num()) {
			$this->setError($this->__("FORM_41"), $url);
		}
		
		$team = djmDB::Data();
		
		if (empty($data['complaint_value'])) {
			$this->setError($this->__("FORM_42"), $url);
		}
		
		if (empty($data['complaint_message'])) {
			$this->setError($this->__("FORM_43"), $url);
		}
		
		if (!isnum($data['complaint_value'])) {
			$this->setError($this->__("FORM_44"), $url);
		}
		
		if ($data['complaint_value'] > 100) {
			$this->setError($this->__("FORM_45"), $url);
		}
		
		if ($data['complaint_value'] > $team['team_points']) {
			$this->setError($this->__("FORM_46"), $url);
		}
		
		if ($data['complaint_type'] == "+") {
			$newPts = $team['team_points'] + $data['complaint_value'];
		} elseif ($data['complaint_type'] == "-") {
			$newPts = $team['team_points'] - $data['complaint_value'];
		} else {
			$this->setError($this->__("FORM_13"), $url);
		}
		
		djmDB::Update(dbTeam, array('team_points' => $newPts), array('team_id' => $team['team_id']));
		djmDB::Insert(dbComplaint, array(
			'complaint_time' => time(),
			'complaint_team' => $team['team_id'],
			'complaint_type' => $data['complaint_type'],
			'complaint_value' => $data['complaint_value'],
			'complaint_admin' => $userdata['user_id'],
			'complaint_reason' => $data['complaint_message']
		));
		
		$this->setLog($this->__("FORM_47")." ".$data['complaint_type']." ".$data['complaint_value']." ".$this->__("FORM_48").".", $team['team_id'], "team");
		$this->setAdminLog ($this->__("FORM_49")." ".$team['team_name']." (".$data['complaint_type']." ".$data['complaint_value'].")");
		$this->setDone($this->__("FORM_50"), $url);
	}
	
	


	/* Edit score in protest */
	function actionEditScore($data) {
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_PROTESTS')) {
			$this->location(urlAdminIndex);
		}	

		djmDB::Select("SELECT * FROM ".dbProtest." WHERE protest_id='%d'", $data['parent']);
		$protest = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent2']);
		$match = djmDB::Data();
		
		if (!isnum($data['t1_score']) || !isnum($data['t2_score'])) {
			$this->setError($this->__("FORM_13"),  urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
		}
		
		if ($data['t1_score'] == "") {
			$this->setError($this->__("FORM_51"),  urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
		}
		
		if ($data['t2_score'] == "") {
			$this->setError($this->__("FORM_52"),  urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
		}		
		
		if ($data['t1_score'] > $match['league_match_max_score'] || $data['t2_score'] > $match['league_match_max_score']) {
			$this->setError($this->__("FORM_13"),  urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
		}
		
		djmDB::Update(dbMatch, array(
			'match_t1_score' => $data['t1_score'],
			'match_t2_score' => $data['t2_score'],
			'match_status' => 1
		), array('match_id' => $match['match_id']));
		
		$this->setLog($this->__("FORM_53"), $data['parent2'], "match");
		$this->setAdminLog($this->__("FORM_54", $data['parent'])." #".$data['parent2']);
		$this->setDone($this->__("FORM_55"),  urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);	
	}
	
	/* Refresh match in protest */
	function actionRefreshMatch($data) { 
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_PROTESTS')) {
			$this->location(urlAdminIndex);
		}	

		djmDB::Select("SELECT * FROM ".dbProtest." WHERE protest_id='%d'", $data['parent']);
		$protest = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbMatch." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league WHERE match_id='%d'", $data['parent2']);
		$match = djmDB::Data();
		
		djmDB::Select("SELECT team_points FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
		$t1Pts = djmDB::Data('team_points');
		
		djmDB::Select("SELECT team_points FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
		$t2Pts = djmDB::Data('team_points');
		
		if ($match['match_status'] != 2) {
			$this->setError($this->__("FORM_56"), urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
		}
		
		if ($protest['protest_admin'] != $userdata['user_id']) {
			$this->setError($this->__("FORM_57"), urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
		}

		if ($match['league_match_elo_type'] == "default") {
			$elo = new Elo("default");
			$calc = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
		} elseif ($match['league_match_elo_type'] == "classic") {
			$elo = new Elo("classic");
			$calc = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();				
		} elseif ($match['league_match_elo_type'] == "custom") {
			djmDB::Select("SELECT elo_value FROM ".dbElo." WHERE elo_id='%d'", $match['league_match_elo']);
			$elod = djmDB::Data();
			$elo = new Elo("default");
			$calc = $elo->Constant($elod['elo_value'])->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
		}	

		$t1 = $calc[0];
		$t2 = $calc[1];		
		$t1_new = $t1Pts - $t1;
		$t2_new = $t2Pts - $t2;
		
		djmDB::Update(dbTeam, array('team_points' => $t1_new), array('team_id' => $match['match_t1']));
		djmDB::Update(dbTeam, array('team_points' => $t2_new), array('team_id' => $match['match_t2']));
		djmDB::Update(dbMatch, array(
			'match_t1_score' => '',
			'match_t2_score' => '',
			'match_winner' => 'NULL',
			'match_losser' => 'NULL',
			'match_draw' => 'NULL',
			'match_status' => 0
		), array('match_id' => $match['match_id']));
		
		$this->setLog($this->__("FORM_58"), $match['match_id'], "match");
		$this->setAdminLog($this->__("FORM_59", $data['parent'])." #".$data['parent2']);
		$this->setDone($this->__("FORM_60"), urlAdmin."a_protests&amp;page=protest&amp;id=".$data['parent']);
	}


	/* Take selected protests */
	function actionTakeProtests($data) {
		global $userdata;
		
		if (!$this->isAdmin('ACCESS_PROTESTS')) {
			$this->location(urlAdminIndex);
		}	
		
		if (empty($data['take_protest'])) {
			$this->setError($this->__("FORM_61"));
		}
		
	
		for ($i=0; $i<=count($data['take_protest']); $i++) {
			djmDB::Select("SELECT * FROM ".dbProtest." WHERE protest_id='%d' AND protest_status='0'", $data['take_protest'][$i]);
			if (djmDB::Num()) {
				$protest = djmDB::Data();
				
				djmDB::Update(dbProtest, array(
					'protest_admin' => $userdata['user_id'],
					'protest_status' => 1
				), array('protest_id' => $data['take_protest'][$i]));

				$this->setAdminLog($this->__("FORM_62")." #".$data['take_protest'][$i]);
				$this->setLog($this->__("FORM_62")." #".$protest['protest_id'], $protest['protest_match'], "match");
				$this->setEvent($this->__("FORM_29"), $this->__("FORM_63", $userdata['user_name'])." <a href='".urlMatch.$protest['protest_match']."'>#".$protest['protest_match']."</a>", $protest['protest_from']);
			}
		}
		$this->setDone($this->__("FORM_64"));
	}
	


	/* Edit admin credentials */
	function actionAdminVisitCard($data) {
		global $userdata;
		
		if (!$this->isAdmin()) { $this->location(urlDefault); } 
		
		if (!dbcount("(admin_id)", dbAdmin, "admin_id='".$data['parent']."' AND admin_user='".$userdata['user_id']."'")) {
			$this->setError($this->__("FORM_65")); 
		}
		
		djmDB::Update(dbAdmin, array(
			'admin_name' => htmlspecialchars($data['admin_name']),
			'admin_surname' => htmlspecialchars($data['admin_surname']),
			'admin_description' => htmlspecialchars($data['admin_description'])
		), array('admin_id' => $data['parent']));
		
		$this->setAdminLog($this->__("FORM_66"));
		$this->setDone($this->__("FORM_67"));
	}

	/* Edit ELO */
	function actionEditElo($data) {

		if (!$this->isAdmin('ACCESS_M_ELO')) {
			$this->location(urlAdminIndex);
		}	
	
		if (empty($data['elo_name'])) {
			$this->setError($this->__("FORM_68"));
		}
		
		if (!isnum($data['elo_constant'])) {
			$this->setError($this->__("FORM_69"));
		}
		
		if ($data['elo_constant'] > 90) {
			$this->setError($this->__("FORM_70"));
		}
		
		if ($data['elo_constant'] < 30) { 
			$this->setError($this->__("FORM_71"));
		}
		
		djmDB::Update(dbElo, array(
			'elo_name' => htmlspecialchars($data['elo_name']),
			'elo_value' => htmlspecialchars($data['elo_constant'])
		), array('elo_id' => $data['parent']));
	
		$this->setAdminLog($this->__("FORM_72")." ".$data['elo_name']);
		$this->setDone($this->__("FORM_73"));
	}

	/* Create ELO */
	function actionCreateElo($data) {

		if (!$this->isAdmin('ACCESS_M_ELO')) {
			$this->location(urlAdminIndex);
		}	
	
		if (empty($data['elo_name'])) {
			$this->setError($this->__("FORM_68"));
		}
		
		if (!isnum($data['elo_constant'])) {
			$this->setError($this->__("FORM_69"));
		}
		
		if ($data['elo_constant'] > 90) {
			$this->setError($this->__("FORM_70"));
		}
		
		if ($data['elo_constant'] < 30) { 
			$this->setError($this->__("FORM_71"));
		}
		
		djmDB::Insert(dbElo, array(
			'elo_name' => htmlspecialchars($data['elo_name']),
			'elo_value' => htmlspecialchars($data['elo_constant'])
		));
	
		$this->setAdminLog($this->__("FORM_74")." ".$data['elo_name']);
		$this->setDone($this->__("FORM_75"));
	}

	/* Edit Request Pattern */
	function actionEditRequestPattern($data) {
	
		if (!$this->isAdmin('ACCESS_M_REQUESTS')) {
			$this->location(urlAdminIndex);
		}	
		
		if (empty($data['pattern_name'])) {
			$this->setError($this->__("FORM_76"));
		}
		
		if (empty($data['pattern_text'])) {
			$this->setError($this->__("FORM_77"));
		}
		
		if (isset($data['pattern_league_all'])) {
			$leagues = "";
		} else {
			if (!empty($data['pattern_league'])) {
				$leagues = "";
				for($i=0; $i < count($data['pattern_league']); $i++) {
					$leagues .= ($leagues != "" ? ".":"").stripinput($data['pattern_league'][$i]); 
				}				
			} else {
				$leagues = "";
			}
		}

		djmDB::Update(dbRequestPattern, array(
			'pattern_name' => $data['pattern_name'],
			'pattern_preddefined' => $data['pattern_text'],
			'pattern_league' => $leagues		
		), array('pattern_id' => $data['parent']));

		$this->setAdminLog($this->__("FORM_78")." ".$data['pattern_name']);
		$this->setDone($this->__("FORM_79", $data['pattern_name']));
	}
	
	/* Create Request Pattern */
	function actionCreateRequestPattern($data) {
	
		if (!$this->isAdmin('ACCESS_M_REQUESTS')) {
			$this->location(urlAdminIndex);
		}	
		
		if (empty($data['pattern_name'])) {
			$this->setError($this->__("FORM_80"));
		}
		
		if (empty($data['pattern_text'])) {
			$this->setError($this->__("FORM_81"));
		}
		
		if (isset($data['pattern_league_all'])) {
			$leagues = "";
		} else {
			if (!empty($data['pattern_league'])) {
				$leagues = "";
				for($i=0; $i < count($data['pattern_league']); $i++) {
					$leagues .= ($leagues != "" ? ".":"").stripinput($data['pattern_league'][$i]); 
				}				
			} else {
				$leagues = "";
			}
		}

		djmDB::Insert(dbRequestPattern, array(
			'pattern_name' => $data['pattern_name'],
			'pattern_preddefined' => $data['pattern_text'],
			'pattern_league' => $leagues
		));
		
		$this->setAdminLog($this->__("FORM_82")." ".$data['pattern_name']);
		$this->setDone($this->__("FORM_83", $data['pattern_name']));
	}

	/* Edit Protest Pattern */
	function actionEditProtestPattern($data) {
	
		if (!$this->isAdmin('ACCESS_M_PROTESTS')) {
			$this->location(urlAdminIndex);
		}			
	
		if (empty($data['pattern_name'])) {
			$this->setError($this->__("FORM_84"));
		}
		
		if (empty($data['pattern_preddefined'])) {
			$preddefined = "";
		} else {
			$preddefined = $data['pattern_preddefined'];
		}
		
		if (isset($data['pattern_custom_desc'])) {
			$custom = 1;
		} else {
			if (empty($data['pattern_preddefined'])) {
				$custom = 1;
			} else {
				$custom = 0;
			}
		}
		
		if (isset($data['pattern_league_all'])) {
			$leagues = "";
		} else {
			if (!empty($data['pattern_league'])) {
				$leagues = "";
				for($i=0; $i < count($data['pattern_league']); $i++) {
					$leagues .= ($leagues != "" ? ".":"").stripinput($data['pattern_league'][$i]); 
				}				
			} else {
				$leagues = "";
			}
		}
		
		djmDB::Update(dbProtestPattern, array(
			'pattern_name' => $data['pattern_name'],
			'pattern_preddefined' => $preddefined,
			'pattern_use_custom_description' => $custom,
			'pattern_league' => $leagues		
		), array('pattern_id' => $data['parent']));
		
		$this->setAdminLog($this->__("FORM_85")." ".$data['pattern_name']);
		$this->setDone($this->__("FORM_86"));
	}

	/* Create Protest Pattern */
	function actionCreateProtestPattern($data) {
	
		if (!$this->isAdmin('ACCESS_M_PROTESTS')) {
			$this->location(urlAdminIndex);
		}			
	
		if (empty($data['pattern_name'])) {
			$this->setError($this->__("FORM_87"));
		}
		
		if (empty($data['pattern_preddefined'])) {
			$preddefined = "";
		} else {
			$preddefined = $data['pattern_preddefined'];
		}
		
		if (isset($data['pattern_custom_desc'])) {
			$custom = 1;
		} else {
			if (empty($data['pattern_preddefined'])) {
				$custom = 1;
			} else {
				$custom = 0;
			}
		}
		
		if (isset($data['pattern_league_all'])) {
			$leagues = "";
		} else {
			if (!empty($data['pattern_league'])) {
				$leagues = "";
				for($i=0; $i < count($data['pattern_league']); $i++) {
					$leagues .= ($leagues != "" ? ".":"").stripinput($data['pattern_league'][$i]); 
				}				
			} else {
				$leagues = "";
			}
		}
		
		djmDB::Insert(dbProtestPattern, array(
			'pattern_name' => $data['pattern_name'],
			'pattern_preddefined' => $preddefined,
			'pattern_use_custom_description' => $custom,
			'pattern_league' => $leagues
		));
	
		$this->setAdminLog($this->__("FORM_88")." ".$data['pattern_name']);
		$this->setDone($this->__("FORM_89"));
	}

	/* Panel Event */
	function actionPanelEvent($data) {
	
		if (!$this->isAdmin('ACCESS_M_MODIFICATION')) {
			$this->location(urlAdminIndex);
		}	
		$url = urlAdmin."m_modification#Panels";		
		
		if (!isnum($data['event_interval'])) {
			$this->setError($this->__("FORM_90"), $url);
		}
		
		if ($data['event_interval'] < 10) {
			$this->setError($this->__("FORM_91"), $url);
		}
		
		$interval = $data['event_interval']*1000;
		
		djmDB::Update(dbSettings, array(
			'panel_event_position' => $data['event_pos'],
			'panel_event_refresh' => $interval
		));
		
		$this->setAdminLog($this->__("FORM_92"));
		$this->setDone($this->__("FORM_93"), $url);
	}
	
	/* Panel Search */
	function actionPanelSearch($data) {
	
		if (!$this->isAdmin('ACCESS_M_MODIFICATION')) {
			$this->location(urlAdminIndex);
		}	
		$url = urlAdmin."m_modification#Panels";
		
		if (!isnum($data['panel_search_limit'])) {
			$this->setError($this->__("FORM_94"), $url);
		}
		
		if ($data['panel_search_limit'] < 5) {
			$this->setError($this->__("FORM_95"), $url);
		}
		
		$type = array('OPENTABLE', 'OPENSIDE');
		if (!in_array($data['search_panel_type'], $type)) {
			$this->setError($this->__("FORM_65"), $url);
		}
		
		djmDB::Update(dbSettings, array(
			'panel_search_limit' => $data['panel_search_limit'],
			'panel_search_type' => $data['search_panel_type']
		));
		$this->setAdminLog($this->__("FORM_96"));
		$this->setDone($this->__("FORM_97"), $url);
	}

	/* Edit Modification Settings */
	function actionEditModificationSettings($data) {
	
		if (!$this->isAdmin('ACCESS_M_MODIFICATION')) {
			$this->location(urlAdminIndex);
		}			
		
		if (!in_array($data['modification_link'], array('full','simple'))) {
			$this->setError($this->__("FORM_98"));
		}
		
		if (!isnum($data['modification_team_per_page'])) {
			$this->setError($this->__("FORM_99"));
		}
		
		if ($data['modification_team_per_page'] < 10) {
			$this->setError($this->__("FORM_100"));
		}
		 
		djmDB::Update(dbSettings, array(
				'settings_link' => $data['modification_link'],
				'settings_upload_size' => $data['modification_upload'],
				'settings_profile_link' => $data['modification_profile_link'],
				'settings_team_per_page' => htmlspecialchars($data['modification_team_per_page'])	
		));
		
		$this->setAdminLog($this->__("FORM_101"));
		$this->setDone($this->__("FORM_102"));	
	}

	/* Create league */
	function actionCreateLeague($data) { 

		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->location(urlAdminIndex);
		}	
		
		if (empty($data['league_name'])) {
			$this->setError($this->__("FORM_103"));
		}
		
		if (empty($data['league_game'])) {
			$this->setError($this->__("FORM_104"));
		}
		
		$session_array['start'] = array(
			$data['league_session_start_day'],
			$data['league_session_start_month'],
			$data['league_session_start_year']
		);
		
		$session_array['end'] = array(
			$data['league_session_end_day'],
			$data['league_session_end_month'],
			$data['league_session_end_year']
		);		
		
		$session['start'] = SelectTime::CreateTime($session_array['start']);
		$session['end'] = SelectTime::CreateTime($session_array['end']);
		
		if ($session['end'] < $session['start']) {
			$this->setError($this->__("FORM_105"));
		}
		
		if (!isnum($data['league_default_points'])) {
			$this->setError($this->__("FORM_106"));
		}
		
		if ($data['league_default_points'] < 500) { 
			$this->setError($this->__("FORM_107"));
		}
		
		if (!isnum($data['league_max_player'])) {
			$this->setError($this->__("FORM_108"));
		}
		
		if (!isnum($data['league_min_player'])) {
			$this->setError($this->__("FORM_109"));
		}		
		
		if ($data['league_min_player'] > $data['league_max_player']) {
			$this->setError($this->__("FORM_110"));
		}
		
		// Block status
		if ($data['league_block_status'] != "" OR $data['league_block_status'] != 0) {
			if (isnum($data['league_block_status'])) { 
				$set['block_status'] = (($data['league_block_status'] * 24) *60) *60;
			} else {
				$this->setError($this->__("FORM_111"));
			}
		} else {
			$set['block_status'] = 0;
		}
		
		// Use maps
		if ($data['league_use_maps'] == "YES") {
			$set['use_maps'] = true;
		} else {
			$set['use_maps'] = false;
		}
		
		// Parse maps
		if ($set['use_maps'] == true && empty($data['league_maps'])) {
			$this->setError($this->__("FORM_112"));
		}
		
		if ($set['use_maps'] == true && !empty($data['league_maps'])) {
			$maps = explode(",", $data['league_maps']);
			$set['maps'] = "";
			for($i=0; $i<count($maps); $i++) { $set['maps'] .= ($set['maps'] != "" ? ".":"").stripinput($maps[$i]); }
		} else {
			$set['maps'] = "";
		}
		
		// Match max score
		if (empty($data['league_match_max_score']) OR !isnum($data['league_match_max_score']) OR $data['league_match_max_score'] == 0) {
			$this->setError($this->__("FORM_113"));
		} else {
			$set['max_score'] = $data['league_match_max_score'];
		}
		
		// Elo 
		if (!empty($data['league_elo'])) {
			
			if ($data['league_elo'] == "default") { 
				$set['elo_type'] = "default";
			} elseif ($data['league_elo'] == "classic") {
				$set['elo_type'] = "classic";
			} elseif (isnum($data['league_elo'])) {
				$set['elo_type'] = "custom";
				$set['elo'] = $data['league_elo'];
			}
			
		} else {
			$this->setError($this->__("FORM_114"));
		}
		
		
		// Game server
		if ($data['league_match_use_server'] == "YES") {
			$set['use_server'] = true;
		} else {
			$set['use_server'] = false;
		}
		
		if ($set['use_server'] == true) {
			
			if (!empty($data['league_match_server_time']) AND isnum($data['league_match_server_time'])) {
				$server_time = $data['league_match_server_time'];
				if ($data['league_match_server_time_type'] == "min") { 
					$set['server_time'] = $server_time * 60; // mins
				} else {
					$set['server_time'] = ($server_time * 60) * 60; //hours
				}
			} else {	
				$this->setError($this->__("FORM_115"));
			}
		} else {
			$set['server_time'] = 0;
		}
		
		//Match hosts
		if ($data['league_match_use_host'] == "YES") {
			$set['use_host'] = true;
		} else {
			$set['use_host'] = false;
		}
		
		if ($set['use_host'] == true) { 
		
			if (!empty($data['league_match_host_time']) && isnum($data['league_match_host_time']) && $data['league_match_host_time'] != 0) {
				$set['host_time'] = $data['league_match_host_time'] * 60;
			} else {
				$this->setError($this->__("FORM_116"));
			}
		
			if (isnum($data['league_match_host_num']) && $data['league_match_host_num'] > 0 && $data['league_match_host_num'] < 11) {
				$set['host_num'] = $data['league_match_host_num'];
			} else {
				$this->setError($this->__("FORM_117"));
			}
	
		} else {
			$set['host_time'] = 0;
			$set['host_num'] = 0;
		}
		
		
		// Match delete
		if ($data['league_match_delete'] == "YES") { 
			$set['match_delete'] = true;
		} else {
			$set['match_delete'] = false;
		}
		
		if ($set['match_delete'] == true) { 
			if (isnum($data['league_match_delete_time'])) {
				$set['delete_time'] = $data['league_match_delete_time'] * 60;
			} else {
				$this->setError($this->__("FORM_118"));
			}
		} else {
			$set['delete_time'] = 0;
		}
		
		// Request
		if (!empty($data['league_match_request_delay']) AND isnum($data['league_match_request_delay'])) {
			$request_delay = $data['league_match_request_delay'];
			if ($data['league_match_request_delay_type'] == "min") { 
				$set['request_delay'] = $request_delay * 60; // mins
			} else {
				$set['request_delay'] = ($request_delay * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_119"));
		}		
		
		if (!empty($data['league_match_request_delay_stop']) AND isnum($data['league_match_request_delay_stop'])) {
			$request_delay_stop = $data['league_match_request_delay_stop'];
			if ($data['league_match_request_delay_stop_type'] == "min") { 
				$set['request_delay_stop'] = $request_delay_stop * 60; // mins
			} else {
				$set['request_delay_stop'] = ($request_delay_stop * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_120"));
		}			

		if ($set['request_delay_stop'] < $set['request_delay']) {
			$this->setError($this->__("FORM_121"));
		}
		
		// Protest
		if (!empty($data['league_match_protest_delay']) AND isnum($data['league_match_protest_delay'])) {
			$protest_delay = $data['league_match_protest_delay'];
			if ($data['league_match_protest_delay_type'] == "min") { 
				$set['protest_delay'] = $protest_delay * 60; // mins
			} else {
				$set['protest_delay'] = ($protest_delay * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_122"));
		}	
		
		if (!empty($data['league_match_protest_delay_stop']) AND isnum($data['league_match_protest_delay_stop'])) {
			$protest_delay_stop = $data['league_match_protest_delay_stop'];
			if ($data['league_match_protest_delay_stop_type'] == "min") { 
				$set['protest_delay_stop'] = $protest_delay_stop * 60; // mins
			} else {
				$set['protest_delay_stop'] = ($protest_delay_stop * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_123"));
		}	
		
		if ($set['protest_delay_stop'] < $set['protest_delay']) {
			$this->setError($this->__("FORM_124"));
		}

		djmDB::Insert(dbLeague, array(
			'league_name' => $data['league_name'],
			'league_game' => $data['league_game'],
			'league_session' => $session['start']."|".$session['end'],
			'league_account' => ($data['league_account'] == "NO" ? "NO":"YES"),
			'league_account_key' => ($data['league_account'] == "NO" ? "":$data['league_account']),
			'league_default_points' => $data['league_default_points'],
			'league_team_max' => $data['league_max_player'],
			'league_team_min' => $data['league_min_player'],
			'league_block_status' => $set['block_status'],
			'league_match_use_maps' => ($set['use_maps'] ? "YES":"NO"),
			'league_match_maps' => $set['maps'],
			'league_match_max_score' => $set['max_score'],
			'league_match_elo_type' => $set['elo_type'],
			'league_match_elo' => ($set['elo_type'] == "custom" ? $set['elo']:""),
			'league_match_use_server' => ($set['use_server'] ? "YES":"NO"),
			'league_match_server_time' => $set['server_time'],
			'league_match_host' => ($set['use_host'] ? "YES":"NO"),
			'league_match_host_time' => $set['host_time'],
			'league_match_host_num' => $set['host_num'],
			'league_match_delete' => ($set['match_delete'] ? "YES":"NO"),
			'league_match_delete_time' => $set['delete_time'],
			'league_match_request_delay' => $set['request_delay'],
			'league_match_request_delay_stop' => $set['request_delay_stop'],
			'league_match_protest_delay' => $set['protest_delay'],
			'league_match_protest_delay_stop' => $set['protest_delay_stop'],
			'league_enable' => 'NO'
		));
		$id = djmDB::ID();
		
		mkdir(pathMedia."Match/".$id, 0777);
		$this->setAdminLog($this->__("FORM_125")." ".$data['league_name']);
		$this->setDone($this->__("FORM_126")." ".$data['league_name']." ".$this->__("FORM_127"));
	}

	/* Edit League */
	function actionEditLeague($data) {
	
		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->location(urlAdminIndex);
		}	
		
		if (empty($data['league_name'])) {
			$this->setError($this->__("FORM_128"));
		}
		
		if (empty($data['league_game'])) {
			$this->setError($this->__("FORM_129"));
		}		
		
		if (!isnum($data['league_default_points'])) {
			$this->setError($this->__("FORM_130"));
		}
		
		if ($data['league_default_points'] < 500) { 
			$this->setError($this->__("FORM_131"));
		}
		
		if (!isnum($data['league_max_player'])) {
			$this->setError($this->__("FORM_132"));
		}
		
		if (!isnum($data['league_min_player'])) {
			$this->setError($this->__("FORM_133"));
		}		
		
		if ($data['league_min_player'] > $data['league_max_player']) {
			$this->setError($this->__("FORM_134"));
		}		
		
		// Block status
		if ($data['league_block_status'] != "" OR $data['league_block_status'] != 0) {
			if (isnum($data['league_block_status'])) { 
				$set['block_status'] = (($data['league_block_status'] * 24) *60) *60;
			} else {
				$this->setError($this->__("FORM_135"));
			}
		} else {
			$set['block_status'] = 0;
		}
		
		// Use maps
		if ($data['league_use_maps'] == "YES") {
			$set['use_maps'] = true;
		} else {
			$set['use_maps'] = false;
		}
		
		// Parse maps
		if ($set['use_maps'] == true && empty($data['league_maps'])) {
			$this->setError($this->__("FORM_136"));
		}
		
		if ($set['use_maps'] == true && !empty($data['league_maps'])) {
			$maps = explode(",", $data['league_maps']);
			$set['maps'] = "";
			for($i=0; $i<count($maps); $i++) { $set['maps'] .= ($set['maps'] != "" ? ".":"").stripinput($maps[$i]); }
		} else {
			$set['maps'] = "";
		}
		
		// Match max score
		if (empty($data['league_match_max_score']) OR !isnum($data['league_match_max_score']) OR $data['league_match_max_score'] == 0) {
			$this->setError($this->__("FORM_137"));
		} else {
			$set['max_score'] = $data['league_match_max_score'];
		}
		
		// Elo 
		if (!empty($data['league_elo'])) {
			
			if ($data['league_elo'] == "default") { 
				$set['elo_type'] = "default";
			} elseif ($data['league_elo'] == "classic") {
				$set['elo_type'] = "classic";
			} elseif (isnum($data['league_elo'])) {
				$set['elo_type'] = "custom";
				$set['elo'] = $data['league_elo'];
			}
			
		} else {
			$this->setError($this->__("FORM_138"));
		}		
		
		// Game server
		if ($data['league_match_use_server'] == "YES") {
			$set['use_server'] = true;
		} else {
			$set['use_server'] = false;
		}
		
		if ($set['use_server'] == true) {
			
			if (!empty($data['league_match_server_time']) AND isnum($data['league_match_server_time'])) {
				$server_time = $data['league_match_server_time'];
				if ($data['league_match_server_time_type'] == "min") { 
					$set['server_time'] = $server_time * 60; // mins
				} else {
					$set['server_time'] = ($server_time * 60) * 60; //hours
				}
			} else {	
				$this->setError($this->__("FORM_139"));
			}
		} else {
			$set['server_time'] = 0;
		}
		
		//Match hosts
		if ($data['league_match_use_host'] == "YES") {
			$set['use_host'] = true;
		} else {
			$set['use_host'] = false;
		}
		
		if ($set['use_host'] == true) { 
		
			if (!empty($data['league_match_host_time']) && isnum($data['league_match_host_time']) && $data['league_match_host_time'] != 0) {
				$set['host_time'] = $data['league_match_host_time'] * 60;
			} else {
				$this->setError($this->__("FORM_140"));
			}
		
			if (isnum($data['league_match_host_num']) && $data['league_match_host_num'] > 0 && $data['league_match_host_num'] < 11) {
				$set['host_num'] = $data['league_match_host_num'];
			} else {
				$this->setError($this->__("FORM_141"));
			}
	
		} else {
			$set['host_time'] = 0;
			$set['host_num'] = 0;
		}
		
		
		// Match delete
		if ($data['league_match_delete'] == "YES") { 
			$set['match_delete'] = true;
		} else {
			$set['match_delete'] = false;
		}
		
		if ($set['match_delete'] == true) { 
			if (isnum($data['league_match_delete_time'])) {
				$set['delete_time'] = $data['league_match_delete_time'] * 60;
			} else {
				$this->setError($this->__("FORM_142"));
			}
		} else {
			$set['delete_time'] = 0;
		}
		
		// Request
		if (!empty($data['league_match_request_delay']) AND isnum($data['league_match_request_delay'])) {
			$request_delay = $data['league_match_request_delay'];
			if ($data['league_match_request_delay_type'] == "min") { 
				$set['request_delay'] = $request_delay * 60; // mins
			} else {
				$set['request_delay'] = ($request_delay * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_143"));
		}		
		
		if (!empty($data['league_match_request_delay_stop']) AND isnum($data['league_match_request_delay_stop'])) {
			$request_delay_stop = $data['league_match_request_delay_stop'];
			if ($data['league_match_request_delay_stop_type'] == "min") { 
				$set['request_delay_stop'] = $request_delay_stop * 60; // mins
			} else {
				$set['request_delay_stop'] = ($request_delay_stop * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_144"));
		}			

		if ($set['request_delay_stop'] < $set['request_delay']) {
			$this->setError($this->__("FORM_145"));
		}
		
		// Protest
		if (!empty($data['league_match_protest_delay']) AND isnum($data['league_match_protest_delay'])) {
			$protest_delay = $data['league_match_protest_delay'];
			if ($data['league_match_protest_delay_type'] == "min") { 
				$set['protest_delay'] = $protest_delay * 60; // mins
			} else {
				$set['protest_delay'] = ($protest_delay * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_146"));
		}	
		
		if (!empty($data['league_match_protest_delay_stop']) AND isnum($data['league_match_protest_delay_stop'])) {
			$protest_delay_stop = $data['league_match_protest_delay_stop'];
			if ($data['league_match_protest_delay_stop_type'] == "min") { 
				$set['protest_delay_stop'] = $protest_delay_stop * 60; // mins
			} else {
				$set['protest_delay_stop'] = ($protest_delay_stop * 60) * 60; //hours
			}
		} else {	
			$this->setError($this->__("FORM_147"));
		}	
		
		if ($set['protest_delay_stop'] < $set['protest_delay']) {
			$this->setError($this->__("FORM_148"));
		}

		djmDB::Update(dbLeague, array(
			'league_name' => $data['league_name'],
			'league_game' => $data['league_game'],
			'league_default_points' => $data['league_default_points'],
			'league_team_max' => $data['league_max_player'],
			'league_team_min' => $data['league_min_player'],
			'league_block_status' => $set['block_status'],
			'league_match_use_maps' => ($set['use_maps'] ? "YES":"NO"),
			'league_match_maps' => $set['maps'],
			'league_match_max_score' => $set['max_score'],
			'league_match_elo_type' => $set['elo_type'],
			'league_match_elo' => ($set['elo_type'] == "custom" ? $set['elo']:""),			
			'league_match_use_server' => ($set['use_server'] ? "YES":"NO"),
			'league_match_server_time' => $set['server_time'],
			'league_match_host' => ($set['use_host'] ? "YES":"NO"),
			'league_match_host_time' => $set['host_time'],
			'league_match_host_num' => $set['host_num'],
			'league_match_delete' => ($set['match_delete'] ? "YES":"NO"),
			'league_match_delete_time' => $set['delete_time'],
			'league_match_request_delay' => $set['request_delay'],
			'league_match_request_delay_stop' => $set['request_delay_stop'],
			'league_match_protest_delay' => $set['protest_delay'],
			'league_match_protest_delay_stop' => $set['protest_delay_stop']			
		), array('league_id' => $data['parent']));	

		if (!is_dir(pathMedia."Match/".$data['parent'])) {
			mkdir(pathMedia."Match/".$data['parent'], 0777);
		}
		
		$this->setAdminLog($this->__("FORM_149")." ".$data['league_name']);
		$this->setDone($this->__("FORM_150", $data['league_name']));
	}
	
	/* Delete League */
	function actionDeleteLeague($data) {
	
		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->lcoation(urlAdminIndex);
		}		
		
		// Get League
		djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $data['parent']);
		$league = djmDB::Data();
	
		// Delete media
		$dir = pathMedia."Match/".$league['league_id'];		
		$scan = scandir($dir);
		foreach ($scan as $file) {
			if($file != "." && $file != "..") {
				if (is_dir($dir."/".$file)) {
					$scan2 = scandir($dir."/".$file);
					foreach ($scan2 as $file2) {
						unlink($dir."/".$file."/".$file2);
					}
					rmdir($dir."/".$file);
				} else {
					unlink($dir."/".$file);
				}
			}
		}		

		// Team items delete
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_league='%d'", $league['league_id']);
		if (djmDB::Num()) {
			foreach (djmDB::fullData() as $t) {
				// Complaints
				djmDB::Delete(dbComplaint, array('complaint_team' => $t['team_id']));
				// Logs
				djmDB::Delete(dbLog, array('log_type' => 'team', 'log_parent' => $t['team_id']));
				// Players
				djmDB::Delete(dbTeamPlayer, array('player_team' => $t['team_id']));
			}
			// Teams
			djmDB::Delete(dbTeam, array('team_league' => $league['league_id']));
		}
		
		// Match delete
		djmDB::Select("SELECT * FROM ".dbMatch." WHERE match_league='%d'", $league['league_id']);
		if (djmDB::Num()) {
			foreach (djmDB::fullData() as $m) {
				// Protests
				djmDB::Delete(dbProtest, array('protest_match' => $m['match_id']));
				// Requests
				djmDB::Delete(dbRequest, array('request_match' => $m['match_id']));	
				// Match media
				djmDB::Delete(dbMatchMedia, array('media_match' => $m['match_id']));		
				// Match links
				djmDB::Delete(dbMatchLinks, array('link_match' => $m['match_id']));
				// Match requests
				djmDB::Delete(dbMatchRequest, array('request_match' => $m['match_id']));
				// Logs
				djmDB::Delete(dbLog, array('log_type' => 'match', 'log_parent' => $m['match_id']));
			}
			// Match
			djmDB::Delete(dbMatch, array('match_league' => $league['league_id']));
		}
		
		rmdir($dir);
		djmDB::Delete(dbLeague, array('league_id' => $league['league_id']));
		$this->setAdminLog($this->__("FORM_151")." <strong>".$league['league_name']."</strong>");
		$this->setDone($this->__("FORM_152", $league['league_name']), urlAdmin."m_league");
	}
	
	/* Restart league */
	function actionRestartLeague($data) {

		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->lcoation(urlAdminIndex);
		}	
		
		$url = urlAdmin."m_league&amp;manage_league=".$data['parent']."#Tools";

		// Get League
		djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $data['parent']);
		$league = djmDB::Data();
	
		// Delete media
		$dir = pathMedia."Match/".$league['league_id'];		
		$scan = scandir($dir);
		foreach ($scan as $file) {
			if($file != "." && $file != "..") {
				if (is_dir($dir."/".$file)) {
					$scan2 = scandir($dir."/".$file);
					foreach ($scan2 as $file2) {
						unlink($dir."/".$file."/".$file2);
					}
					rmdir($dir."/".$file);
				} else {
					unlink($dir."/".$file);
				}
			}
		}		

		// Team items delete
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_league='%d'", $league['league_id']);
		if (djmDB::Num()) {
			foreach (djmDB::fullData() as $t) {
				// Complaints
				djmDB::Delete(dbComplaint, array('complaint_team' => $t['team_id']));
				// Logs
				djmDB::Delete(dbLog, array('log_type' => 'team', 'log_parent' => $t['team_id']));
				// Edit team
				djmDB::Update(dbTeam, array('team_points' => $league['league_default_points'], 'team_status' => 'INACTIVE'), array('team_id' => $t['team_id']));
			}
		}
		
		// Match delete
		djmDB::Select("SELECT * FROM ".dbMatch." WHERE match_league='%d'", $league['league_id']);
		if (djmDB::Num()) {
			foreach (djmDB::fullData() as $m) {
				// Protests
				djmDB::Delete(dbProtest, array('protest_match' => $m['match_id']));
				// Requests
				djmDB::Delete(dbRequest, array('request_match' => $m['match_id']));	
				// Match media
				djmDB::Delete(dbMatchMedia, array('media_match' => $m['match_id']));		
				// Match links
				djmDB::Delete(dbMatchLinks, array('link_match' => $m['match_id']));
				// Match requests
				djmDB::Delete(dbMatchRequest, array('request_match' => $m['match_id']));
				// Logs
				djmDB::Delete(dbLog, array('log_type' => 'match', 'log_parent' => $m['match_id']));
			}
			// Match
			djmDB::Delete(dbMatch, array('match_league' => $league['league_id']));
		}
		
		djmDB::Update(dbLeague, array('league_enable' => 'NO'), array('league_id' => $league['league_id']));
		$this->setAdminLog($this->__("FORM_153")." <strong>".$league['league_name']."</strong>");
		$this->setDone($this->__("FORM_154"), $url);
	}
	
	
	/* Change game ID in League */
	function actionChangeGameID($data) { 
	
		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->lcoation(urlAdminIndex);
		}	
	
		$url = urlAdmin."m_league&amp;manage_league=".$data['parent']."#Tools";
		
		if (empty($data['gameID_id'])) {
			$this->setError($this->__("FORM_155"), $url);
		}
		
		if (empty($data['gameID_message'])) {
			$this->setError($this->__("FORM_156"), $url);
		}
		
		djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $data['parent']);
		$league = djmDB::Data();
		
		djmDB::Select("SELECT * FROM ".dbAccountsPattern." WHERE pattern_id='%d'", $data['gameID_id']);
		$gid = djmDB::Data();
		
		if ($gid['pattern_key'] == $league['league_account_key']) {
			$this->setError($this->__("FORM_157"), $url);
		}	
		
		djmDB::Update(dbLeague, array(
			'league_account_key' => $gid['pattern_key']
		), array('league_id' => $league['league_id']));
		
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team_league='%d'", $league['league_id']);
		if (djmDB::Num()) {
			foreach(djmDB::fullData() as $p) {
				$this->setEvent($this->__("FORM_158"), htmlspecialchars($data['gameID_message']), $p['player_user']);
			}	
		}
		
		$this->setAdminLog($this->__("FORM_159")." <strong>".$league['league_name']."</strong>");
		$this->setDone($this->__("FORM_160"), $url);
	}
	
	
	
	/* Clear media in league */
	function actionClearMedia($data) { 
		
		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->lcoation(urlAdminIndex);
		}

		$league = $data['parent'];
		$dir = pathMedia."Match/".$league;		
		$scan = scandir($dir);
		foreach ($scan as $file) {
			if($file != "." && $file != "..") {
				if (is_dir($dir."/".$file)) {
					$scan2 = scandir($dir."/".$file);
					foreach ($scan2 as $file2) {
						unlink($dir."/".$file."/".$file2);
					}
					rmdir($dir."/".$file);
				} else {
					unlink($dir."/".$file);
				}
			}
		}
		
		djmDB::Select("SELECT match_id FROM ".dbMatch." WHERE match_league='%d'", $league);
		if (djmDB::Num()) {
			foreach (djmDB::fullData() as $m) {
				djmDB::Delete(dbMatchMedia, array('media_match' => $m['match_id']));
			}
		}

		$this->setAdminLog($this->__("FORM_161"));
		$this->setDone($this->__("FORM_162"), urlAdmin."m_league&amp;manage_league=".$league."#Tools");
	}
	
	/* Activate League */
	function actionActivateLeague($data) {

		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->lcoation(urlAdminIndex);
		}

		djmDB::Update(dbLeague, array('league_enable' => 'YES'), array('league_id' => $data['parent']));
		$this->setAdminLog($this->__("FORM_163")." ".$data['name']);
		$this->setDone($this->__("FORM_164"));	
	}
	
	/* Deactivate League */
	function actionDeactivateLeague($data) {

		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->lcoation(urlAdminIndex);
		}

		djmDB::Update(dbLeague, array('league_enable' => 'NO'), array('league_id' => $data['parent']));
		$this->setAdminLog($this->__("FORM_165")." ".$data['name']);
		$this->setDone($this->__("FORM_166"));
	}
	
	
	/* Edit League Rules */
	function actionEditRules($data) {
	
		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->location(urlAdminIndex);
		}	
		
		djmDB::Update(dbLeague, array('league_rules' => $data['league_rules'], 'league_rules_time' => time()), array('league_id' => $data['parent']));
		$this->setAdminLog($this->__("FORM_167")." ".$data['league_name']);
		$this->setDone($this->__("FORM_168"));
	}	
	
	/* Edit League Rules */
	function actionEditInformation($data) {
	
		if (!$this->isAdmin('ACCESS_M_LEAGUE')) {
			$this->location(urlAdminIndex);
		}	
		
		djmDB::Update(dbLeague, array('league_information' => $data['league_information'], 'league_information_time' => time()), array('league_id' => $data['parent']));
		$this->setAdminLog($this->__("FORM_169")." ".$data['league_name']);
		$this->setDone($this->__("FORM_170"));
	}		

	/* Create game */
	function actionCreateGame($data) {
	
		if (!$this->isAdmin('ACCESS_M_GAME')) {
			$this->location(urlAdminIndex);
		}	

		if (empty($data['game_name'])) {
			$this->setError($this->__("FORM_171"));
		}
		
		if (!is_uploaded_file($_FILES['game_icon']['tmp_name'])) {
			$this->setError($this->__("FORM_172"));
		} 
		
		$image_name = strtolower(str_replace(" ", "-", $data['game_name']));
		$upload = new djmUpload('game_icon');
		$image = $upload	->setPath(pathMedia."Game/")
									->setName($image_name)
									->setSize(153600)
									->setRes(16, 16)
									->setExt(array('.jpg', '.png', '.gif'))
									->Upload();
									
		
		djmDB::Insert(dbGame, array('game_name' => $data['game_name'], 'game_icon' => $image['name']));
		$this->setAdminLog($this->__("FORM_173")." ".$data['game_name']);
		$this->setDone($this->__("FORM_174", $data['game_name']));
	}
	
	
	/* Edit game */
	function actionEditGame($data) {
	
		if (!$this->isAdmin('ACCESS_M_GAME')) {
			$this->location(urlAdminIndex);
		}		
		
		if (empty($data['game_name'])) {
			$this->setError($this->__("FORM_175"));
		}
		
		djmDB::Select("SELECT * FROM ".dbGame." WHERE game_id='%d'", $data['parent']);
		$game = djmDB::Data();
		
		
		if (is_uploaded_file($_FILES['game_icon']['tmp_name'])) {
			$image_name = strtolower(str_replace(" ", "-", $data['game_name']));
			$upload = new djmUpload('game_icon');
			$image = $upload	->setPath(pathMedia."Game/")
										->setName($image_name)
										->setSize(153600)
										->setRes(16, 16)
										->setExt(array('.jpg', '.png', '.gif'))
										->Upload();			
			
			djmDB::Update(dbGame, array('game_name' => $data['game_name'], 'game_icon' => $image['name']), array('game_id' => $data['parent']));
		}  else {
			djmDB::Update(dbGame, array('game_name' => $data['game_name']), array('game_id' => $data['parent']));
		}
		
		$this->setAdminLog($this->__("FORM_176")." ".$game['game_name']);
		$this->setDone($this->__("FORM_177", $game['game_name']));	
	}
	

	/* Create account pattern */
	function actionCreateAccount($data) {
	
		if (!$this->isAdmin('ACCESS_M_ACCOUNTS')) {
			$this->location(urlAdminIndex);
		}

		if (empty($data['acc_name'])) { 
			$this->setError($this->__("FORM_178"));
		}
		
		if (empty($data['acc_limit'])) {
			$this->setError($this->__("FORM_179"));
		}
		
		djmDB::Insert(dbAccountsPattern, array(
			'pattern_name' => $data['acc_name'],
			'pattern_limit' => $data['acc_limit'],
			'pattern_key' => $this->randString(20),
			'pattern_information' => $data['acc_tutorial']
		));
		
		$this->setAdminLog($this->__("FORM_180")." ".$data['acc_name']);
		$this->setDone($this->__("FORM_181"));
	}

	/* Edit account pattern */
	function actionEditAccount($data) { 
	
		if (!$this->isAdmin('ACCESS_M_ACCOUNTS')) {
			$this->location(urlAdminIndex);
		}		
		
		if (empty($data['acc_name'])) { 
			$this->setError($this->__("FORM_182"));
		}		
		
		if (empty($data['acc_limit'])) {
			$this->setError($this->__("FORM_183"));
		}		
		
		djmDB::Update(dbAccountsPattern, array('pattern_name' => $data['acc_name'], 'pattern_limit' => $data['acc_limit'], 'pattern_information' => $data['acc_tutorial']), array('pattern_id' => $data['parent']));	
		$this->setAdminLog($this->__("FORM_184")." #".$data['parent']);
		$this->setDone($this->__("FORM_185"));		
	}
	

	/* Create admin category */
	function actionCreateAdminCategory($data) {
		
		if (!$this->isAdmin('ACCESS_M_ADMINS')) {
			$this->location(urlAdminIndex);
		}
		
		if (empty($data['category_name'])) {
			$this->setError($this->__("FORM_188"));
		}
		
		djmDB::Insert(dbAdminCategory, array('category_name' => $data['category_name']));
		$this->setAdminLog($this->__("FORM_186")." ".$data['category_name']);
		$this->setDone($this->__("FORM_187"));
	}
	
	/* Edit admin category */
	function actionEditAdminCategory($data) { 
	
		if (!$this->isAdmin('ACCESS_M_ADMINS')) {
			$this->location(urlAdminIndex);
		}	
	
		if (empty($data['category_name'])) {
			$this->setError($this->__("FORM_188"));
		}
		
		djmDB::Update(dbAdminCategory, array('category_name' => $data['category_name']), array('category_id' => $data['parent']));
		$this->setAdminLog($this->__("FORM_189"));
		$this->setDone($this->__("FORM_190"));
	}
	
	/* Create admin */
	function actionAddAdmin($data) {
	
		if (!$this->isAdmin('ACCESS_M_ADMINS')) {
			$this->location(urlAdminIndex);
		}	
	
		if (empty($data['admin_user'])) {
			$this->setError($this->__("FORM_191"));
		}
		
		if (!isset($data['admin_category'])) {
			$this->setError($this->__("FORM_192"));
		}
		
		if (empty($data['admin_category'])) {
			$this->setError($this->__("FORM_193"));
		}
		
		if (empty($data['admin_flag'])) {
			$this->setError($this->__("FORM_194"));
		}
		
		if (isset($data['admin_league_all'])) {
			$leagues = "";
		} else {
			if (!empty($data['admin_league'])) {
				$leagues = "";
				for($i=0; $i < count($data['admin_league']); $i++) {
					$leagues .= ($leagues != "" ? ".":"").stripinput($data['admin_league'][$i]); 
				}				
			} else {
				$leagues = "";
			}
		}
		
		if (isset($data['admin_account_all'])) {
			$account = "";
		} else {
			if (!empty($data['admin_account'])) {
				$account = "";
				for($i=0; $i < count($data['admin_account']); $i++) {
					$account .= ($account != "" ? ".":"").stripinput($data['admin_account'][$i]); 
				}				
			} else {
				$account = "";
			}
		}		
		
		
		djmDB::Select("SELECT * FROM ".dbAdmin." WHERE admin_user='%d'", $data['admin_user']);
		if (djmDB::Num()) {
			$this->setError($this->__("FORM_195"));
		}
			
		djmDB::Select("SELECT * FROM ".DB_USERS."  WHERE user_id='%d'", $data['admin_user']);
		$user = djmDB::Data();
			
		$flags = "";
		for($i=0; $i < count($data['admin_flag']); $i++) {
			$flags .= ($flags != "" ? ".":"").stripinput($data['admin_flag'][$i]); 
		}
	
		djmDB::Insert(dbAdmin, array('admin_user' => $data['admin_user'], 'admin_category' => $data['admin_category'], 'admin_flag' => $flags, 'admin_league' => $leagues, 'admin_account' => $account ));
		$this->setAdminLog($this->__("FORM_196")." ".$user['user_name']);
		$this->setDone($this->__("FORM_197"));
	}

	/* Edit admin */
	function actionEditAdmin($data) { 

		if (!$this->isAdmin('ACCESS_M_ADMINS')) {
			$this->location(urlAdminIndex);
		}	
		
		if (empty($data['admin_flag'])) {
			$this->setError($this->__("FORM_198"));
		}
		
		if (isset($data['admin_league_all'])) {
			$leagues = "";
		} else {
			if (!empty($data['admin_league'])) {
				$leagues = "";
				for($i=0; $i < count($data['admin_league']); $i++) {
					$leagues .= ($leagues != "" ? ".":"").stripinput($data['admin_league'][$i]); 
				}				
			} else {
				$leagues = "";
			}
		}
		
		if (isset($data['admin_account_all'])) {
			$account = "";
		} else {
			if (!empty($data['admin_account'])) {
				$account = "";
				for($i=0; $i < count($data['admin_account']); $i++) {
					$account .= ($account != "" ? ".":"").stripinput($data['admin_account'][$i]); 
				}				
			} else {
				$account = "";
			}
		}				
		
		$flags = "";
		for($i=0; $i < count($data['admin_flag']); $i++) {
			$flags .= ($flags != "" ? ".":"").stripinput($data['admin_flag'][$i]); 
		}

		djmDB::Select("SELECT * FROM ".dbAdmin." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.admin_user  WHERE admin_id='%d'", $data['parent']);
		$user = djmDB::Data();
		
		djmDB::Update(dbAdmin, array('admin_category' => $data['admin_category'], 'admin_flag' => $flags, 'admin_league' => $leagues, 'admin_account' => $account), array('admin_id' => $data['parent']));
		$this->setAdminLog($this->__("FORM_199")." ".$user['user_name']);
		$this->setDone($this->__("FORM_200"));
	}
}
