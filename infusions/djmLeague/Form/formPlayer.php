<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: formTeam.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class formPlayer extends djmLeague {

	/* Change Sound */
	function actionChangeSound($data) {
		global $userdata;
		
		if (!iMEMBER) { 
			$this->location(BASEDIR."login.php");
		}
		
		if (in_array($data['player_sound'], array('1.wav','2.wav','3.wav'))) {
			djmDB::Update(dbPlayer, array('player_sound' => $data['player_sound']), array('player_user' => $userdata['user_id']));		
		}
		
		$this->setDone($this->__("FOP_5"));
	}

	/* Change National */
	function actionNational($data) {
		global $userdata;

		if (!iMEMBER) { 
			$this->location(BASEDIR."login.php");
		}
		
		djmDB::Update(dbPlayer, array('player_flag' => $data['player_national']), array('player_user' => $userdata['user_id']));
		$this->setLog($this->__("FOP_1"), $userdata['user_id'], "player");
		$this->setDone($this->__("FOP_2"));
	}

	/* Game ID */
	function actionGameID($data) { 
		global $userdata;

		if (!iMEMBER) { 
			$this->location(BASEDIR."login.php");
		}
		
		if (!empty($data['game_ids'])) {
			for($i=0; $i<=count($data['game_ids']); $i++) {
				$game_key = $data['game_ids'][$i];
				if (!empty($data['id_'.$game_key])) {
					djmDB::Select("SELECT * FROM ".dbAccountsPattern." WHERE pattern_key='%s'", $game_key);
					if (djmDB::Num()) {
						$pattern = djmDB::Data();
						djmDB::Select("SELECT * FROM ".dbAccounts." WHERE account_user='%d' AND account_key='%s'", array($userdata['user_id'], $game_key));
						if (djmDB::Num()) {
							$acc = djmDB::Data();
							if ($acc['account_new_value'] == "") {
								if ($data['id_'.$game_key] != $acc['account_value']) {
									if (($acc['account_last_change']+$pattern['pattern_limit']) < time()) {
										djmDB::Update(dbAccounts, array(
											'account_new_value' => $data['id_'.$game_key],
											'account_post_change' => time()
										), array('account_id' => $acc['account_id']));
										$this->setLog($this->__("FOP_3")." ".$pattern['pattern_name'], $userdata['user_id'], "player");
									}
								}
							} 
						} else {
							djmDB::Insert(dbAccounts, array(
								'account_user' => $userdata['user_id'],
								'account_key' => $game_key,
								'account_new_value' => $data['id_'.$game_key],
								'account_post_change' => time()
							));
							$this->setLog($this->__("FOP_3")." ".$pattern['pattern_name'], $userdata['user_id'], "player");
						}
					} 
				}
			}
			$this->setDone($this->__("FOP_4"));
		}
	}

}