<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: formTeam.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class formTeam extends djmLeague {

	function actionLeaveTeam($data) {
		global $userdata;
	
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}	
		
		$teamMember = $this->isTeamPlayer($data['parent']);
		if (!$teamMember) {
			$this->location(urlDefault);
		}
		
		$count_players = dbcount("(*)", dbTeamPlayer, "player_team='".$data['parent']."'");
		
		if ($teamMember == "CR" && $count_players > 1) {
			$this->setError($this->__("FOT_1"));
		}
	
		djmDB::Select("SELECT * FROM ".dbTeam." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league WHERE team_id='%d'", $data['parent']);
		if (!djmDB::Num()) { $this->location(urlDefault); }
		$team = djmDB::Data();
		
		if ($team['league_enable'] == "NO") {
			$this->setError($this->__("FOT_2", $team['league_name']));
		}
		
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_user='%d' AND player_team='%d'", array($userdata['user_id'], $team['team_id']));
		if (!djmDB::Num()) { $this->location(urlDefault); }
		$player = djmDB::Data();
		
		$status = func_UserStatus($player['player_time'], $team['league_block_status']);
		
		if ($status['status'] == FALSE) {
			$this->setError($this->__("FOT_3")." ".$team['team_name']." ".$this->__("FOT_4", $status['day']));
		}
	
		
		if (($count_players-1) < $team['league_team_min']) {
			djmDB::Update(dbTeam, array('team_status' => 'INACTIVE'), array('team_id' => $data['parent']));
			$this->setLog($this->__("FOT_5"), $data['parent']);
		}
		
		djmDB::Delete(dbTeamPlayer, array('player_user' => $userdata['user_id'], 'player_team' => $data['parent']));
		$this->setLog($this->__("FOT_6", $userdata['user_name']), $data['parent']);
		$this->setDone($this->__("FOT_7")." ".$team['team_name'], urlLeague.$team['league_id']);
	}

	/* Deactivate */
	function actionTeamDeactivate($data) {
		
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}
		
		$teamMember = $this->isTeamPlayer($data['parent']);
		if (!$teamMember) {
			$this->location(urlDefault);
		}
		
		if ($teamMember != "CR") {
			$this->location(urlDefault);
		}	

		djmDB::Update(dbTeam, array('team_status' => 'INACTIVE'), array('team_id' => $data['parent']));
		$this->setLog($this->__("FOT_8"), $data['parent']);
		$this->setDone($this->__("FOT_9"), urlTeam.$data['parent']);
	}

	/* Activate */
	function actionTeamActivate($data) {
		
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}
		
		$teamMember = $this->isTeamPlayer($data['parent']);
		if (!$teamMember) {
			$this->location(urlDefault);
		}
		
		if ($teamMember != "CR") {
			$this->location(urlDefault);
		}	

		$team_players = dbcount("(*)", dbTeamPlayer, "player_team='".$data['parent']."'");
		
		djmDB::Select("SELECT * FROM ".dbTeam." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league WHERE team_id='%d'", $data['parent']);
		if (!djmDB::Num()) { $this->location(urlDefault); }
		$league = djmDB::Data();
		
		if ($league['league_enable'] == "NO") {
			$this->setError($this->__("FOT_2", $league['league_name']));
		}		
		
		if ($team_players < $league['league_team_min']) { 
			$this->setError($this->__("FOT_10"));
		}
		
		djmDB::Update(dbTeam, array('team_status' => 'ACTIVE'), array('team_id' => $data['parent']));
		$this->setLog($this->__("FOT_11"), $data['parent']);
		$this->setDone($this->__("FOT_12"), urlTeam.$data['parent']);
	}
	
	/* Team Owner */
	function actionChangeTeamOwner($data) {
	
		if (!iMEMBER) {
			$this->location(urlDefault);
		}
		
		$teamMember = $this->isTeamPlayer($data['parent']);
		if (!$teamMember) {
			$this->location(urlDefault);
		}
		
		if ($teamMember != "CR") {
			$this->location(urlDefault);
		}		
		
		if (empty($data['player'])) {
			$this->setError($this->__("FOT_13"));
		}
		
		djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_id='%d' AND player_team='%d'", array($data['player'], $data['parent']));
		if (djmDB::Num()) {
			djmDB::Update(dbTeamPlayer, array('player_position' => 'CL'), array('player_team' => $data['parent'], 'player_position' => 'CR'));
			djmDB::Update(dbTeamPlayer, array('player_position' => 'CR'), array('player_id' => $data['player'], 'player_team' => $data['parent']));
			$this->setLog($this->__("FOT_14"), $data['parent']);
			$this->setDone($this->__("FOT_15"), urlTeam.$data['parent']);
		} else {
			$this->setError($this->__("FOT_16"));
		}
	}

	/* Squad positions*/
	function actionSquadPosition($data) {
		
		if (!iMEMBER) { 
			$this->location(urlDefault);
		}
		
		$teamMember = $this->isTeamPlayer($data['parent']);
		if (!$teamMember) {
			$this->location(urlDefault);
		}
		
		if ($teamMember != "CR") {
			$this->location(urlDefault);
		}
		
		for($i=0; $i<=count($data['player']); $i++) {
			$id = $data['player'][$i];
			$value = $data['player_'.$id];
			if (in_array($value, array('CL', 'PL'))) {
				djmDB::Update(dbTeamPlayer, array('player_position' => $value), array('player_id' => $id));
			}
		}
		
		$this->setLog($this->__("FOT_17"), $data['parent']);
		$this->setDone($this->__("FOT_18"));
	}
	
	/* Team Settings */
	function actionTeamSettings($data) {
	
		if (!iMEMBER) { 
			$this->location(urlDefault); 
		}
		
		$teamMember = $this->isTeamPlayer($data['parent']);	
		if (!$teamMember) {
			$this->location(urlDefault);
		}
		
		if ($teamMember == "PL") {
			$this->location(urlDefault);
		}
		
		if (empty($data['team_name'])) {
			$this->setError($this->__("FOT_19"));
		}
		
		if (empty($data['team_tag'])) {
			$this->setError($this->__("FOT_20"));
		}
		
		if (empty($data['team_password'])) {
			$this->setError($this->__("FOT_21"));
		}
		
		if (!empty($data['team_web'])) { 
			$team_web_1 = str_replace("http://", "", $data['team_web']);
			$team_web = "http://".$team_web_1;
		}
		

		if (isset($data['team_avatar_del'])) {
			@unlink(pathMedia."Team/".$data['team_avatar_del']);
			djmDB::Update(dbTeam, array('team_avatar' => ''), array('team_id' => $data['parent']));
		}

		if (is_uploaded_file($_FILES['team_avatar']['tmp_name'])) {
			$image_name = $data['parent'];
			$upload = new djmUpload('team_avatar');
			$image = $upload	->setPath(pathMedia."Team/")
										->setName($image_name)
										->setSize(153600)
										->setRes(100,100)
										->setExt(array('.jpg','.png','.gif'))
										->Upload();
			djmDB::Update(dbTeam, array('team_avatar' => $image['name']), array('team_id' => $data['parent']));
		} 
		
		djmDB::Update(dbTeam, array(
			'team_name' => htmlspecialchars($data['team_name']),
			'team_tag' => htmlspecialchars($data['team_tag']),
			'team_password' => htmlspecialchars($data['team_password']),
			'team_flag' => htmlspecialchars($data['team_national']),
			'team_web' => htmlspecialchars($team_web)
		), array('team_id' => $data['parent']));
		
		$this->setLog($this->__("FOT_22"), $data['parent']);
		$this->setDone($this->__("FOT_23"));
	}
}