<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: SelectTime.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class SelectTime extends djmLeague {
	static private $Time;
	static private $Name;
	static private $Style = "style='margin-bottom: 1px;'";

	
	private static function Day() {
		$format = "<select name='".self::$Name."_day' class='textbox' ".self::$Style.">";
		for ($i=1; $i<=31; $i++) {
			$format .= "<option value='".$i."' ".(date("d", self::$Time) == $i ? "selected='selected'":"").">".$i."</option>";
		}
		$format .= "</select>";
		return $format;
	}

	private static function Month() {
		global $config, $months;
		$format = "<select name='".self::$Name."_month' class='textbox' ".self::$Style.">";
			for ($i = 1; $i<=12; $i++) {
				$format .= "<option value='".$i."' ".($i == date("m", self::$Time) ? "selected='selected'":"").">".($i < 10 ? "0".$i:$i)."</option>";
			}
		$format .= "</select>";
		return $format;
	}
	
	private static function Year() {
		$format = "<select name='".self::$Name."_year' class='textbox' ".self::$Style.">";
			for($i=date("Y"); $i<=date("Y")+2; $i++) {
				$format .= "<option value='".$i."'>".$i."</option>";
			}
		$format .= "</select>";
		return $format;
	}
	
	
	private static function Hour() {
		$format = "<select name='".self::$Name."_hour' class='textbox' ".self::$Style.">";
			for($i=0; $i<=23; $i++) {
				$format .= "<option value='".$i."' ".(date("H", self::$Time) == $i ? "selected='selected'":"").">".($i < 10 ? "0".$i:$i)."</option>";
			}
		$format .= "</select>";
		return $format;
	}	
	
	private static function Minute() {
		$format .= "<select name='".self::$Name."_minute' class='textbox' ".self::$Style.">";
			for($i=0; $i<=59; $i++) {
				$format .= "<option value='".$i."' ".(date("i", self::$Time) == $i ? "selected='selected'":"").">".($i < 10 ? "0".$i: $i)."</option>";
			}
		$format .= "</select>";
		return $format;
	}

	public static function CreateTime($Input) {
		$Day = $Input[0]; 
		$Month = $Input[1];
		$Year = $Input[2];
		$Hour = (isset($Input[3]) ? $Input[3] : 0);
		$Minute = (isset($Input[4]) ? $Input[4] : 0);
		return mktime($Hour, $Minute, 0, $Month, $Day, $Year);
	}
	
	public static function Create($Name, $face="d|m|y", $time_plus=false) {
		self::$Name = $Name;
		self::$Time = ($time_plus ? time() + $time_plus : time());
		
			$pattern = explode("|", $face);
			$return = "";
			if (in_array("d", $pattern)) { $return .= self::Day()."."; }
			if (in_array("m", $pattern)) { $return .= self::Month()."."; }
			if (in_array("y", $pattern)) { $return .= self::Year()." "; }
			if (in_array("h", $pattern)) { $return .= self::Hour().":"; }
			if (in_array("i", $pattern)) { $return .= self::Minute(); }
		echo $return;
	}
	



}