<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Upload.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class djmUpload extends djmLeague { 

	 private $File;
	 private $Name = NULL;
	 private $Path;
	 private $ruleWidth = NULL;
	 private $ruleHeight = NULL;
	 private $ruleSize = 150000;
	 private $ruleExt = NULL;
	 private $Extension = "";
	 private $Width;
	 private $Height;
	 private $Multi = FALSE;
	 private $Thumbnail = FALSE;
	 private $Thumb_Width = NULL;
	 private $Thumb_Height = NULL;
	 
	function __construct($File) {
		$this->File = $File;
		return $this;
	}

	/*
	* Path rule
	*/
	public function setPath($Path) {
		$this->Path = $Path;
		return $this;
	}
	
	/*
	* Name rule
	*/
	public function setName($Name) {
		$this->Name = $Name;
		return $this;
	}
	
	/*
	* Extension rule
	*/
	public function setExt($Array) {
		$this->ruleExt = $Array;
		return $this;
	}
	
	/*
	* Size rule
	*/
	public function setSize($s) {
		$this->ruleSize = $s;
		return $this;
	}
	
	/*
	* Resolution rule
	*/
	public function setRes($w, $h) {
		$this->ruleWidth = $w;
		$this->ruleHeight = $h;
		return $this;
	}

	/*
	* MultiUpload
	*/
	public function Multiple() {
		$this->Multi = TRUE;
		return $this;
	}
	
	/*
	* Create Thumbs
	*/
	public function Thumbnail($w, $h) {
		$this->Thumbnail = TRUE;
		$this->Thumb_Width = $w;
		$this->Thumb_Height = $h;
		return $this;
	}
	
	
	
	 /* 
	 * Check SIZE 
	 */
	 private function checkSize($Val) {
			if ($Val > $this->ruleSize) {
				$this->setError($this->__("LCU_1"));
			}
	 }
	
	
	/*
	*	 Check Resolution
	*/
	private function checkResolution($Val) {
			$resolution = getimagesize($Val);
			if ($resolution[0] > $this->ruleWidth || $resolution[1] > $this->ruleHeight) {
				$this->setError($this->__("LCU_2")." ".$this->ruleWidth." x ".$this->ruleHeight." ".$this->__("LCU_3"));
			} else {
				$this->Width = $resolution[0];
				$this->Height = $resolution[1];
				return $this;
			}	
	}
	
	/*
	*	Check Extension
	*/
	private function checkExtension($Val) {
			$ext = strtolower(strchr($Val, "."));
			if(!in_array($ext, $this->ruleExt)) {
				$this->setError($this->__("LCU_4"));
			} else {
				$this->Extension = $ext;
				return $this;
			}	
	}
	
	/*
	* Create from BMP
	*/
	 private function imagecreatefrombmp($filename) {
	 
	   if (! $f1 = fopen($filename,"rb")) return FALSE;

	   $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", fread($f1,14));
	   if ($FILE['file_type'] != 19778) return FALSE;

	   $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'.
					 '/Vcompression/Vsize_bitmap/Vhoriz_resolution'.
					 '/Vvert_resolution/Vcolors_used/Vcolors_important', fread($f1,40));
	   $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
	   if ($BMP['size_bitmap'] == 0) $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
	   $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
	   $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
	   $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
	   $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
	   $BMP['decal'] = 4-(4*$BMP['decal']);
	   if ($BMP['decal'] == 4) $BMP['decal'] = 0;

	   $PALETTE = array();
	   if ($BMP['colors'] < 16777216)
	   {
		$PALETTE = unpack('V'.$BMP['colors'], fread($f1,$BMP['colors']*4));
	   }

	   $IMG = fread($f1,$BMP['size_bitmap']);
	   $VIDE = chr(0);

	   $res = imagecreatetruecolor($BMP['width'],$BMP['height']);
	   $P = 0;
	   $Y = $BMP['height']-1;
	   while ($Y >= 0)
	   {
		$X=0;
		while ($X < $BMP['width'])
		{
		 if ($BMP['bits_per_pixel'] == 24)
			$COLOR = unpack("V",substr($IMG,$P,3).$VIDE);
		 elseif ($BMP['bits_per_pixel'] == 16)
		 { 
			$COLOR = unpack("n",substr($IMG,$P,2));
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 elseif ($BMP['bits_per_pixel'] == 8)
		 { 
			$COLOR = unpack("n",$VIDE.substr($IMG,$P,1));
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 elseif ($BMP['bits_per_pixel'] == 4)
		 {
			$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
			if (($P*2)%2 == 0) $COLOR[1] = ($COLOR[1] >> 4) ; else $COLOR[1] = ($COLOR[1] & 0x0F);
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 elseif ($BMP['bits_per_pixel'] == 1)
		 {
			$COLOR = unpack("n",$VIDE.substr($IMG,floor($P),1));
			if     (($P*8)%8 == 0) $COLOR[1] =  $COLOR[1]        >>7;
			elseif (($P*8)%8 == 1) $COLOR[1] = ($COLOR[1] & 0x40)>>6;
			elseif (($P*8)%8 == 2) $COLOR[1] = ($COLOR[1] & 0x20)>>5;
			elseif (($P*8)%8 == 3) $COLOR[1] = ($COLOR[1] & 0x10)>>4;
			elseif (($P*8)%8 == 4) $COLOR[1] = ($COLOR[1] & 0x8)>>3;
			elseif (($P*8)%8 == 5) $COLOR[1] = ($COLOR[1] & 0x4)>>2;
			elseif (($P*8)%8 == 6) $COLOR[1] = ($COLOR[1] & 0x2)>>1;
			elseif (($P*8)%8 == 7) $COLOR[1] = ($COLOR[1] & 0x1);
			$COLOR[1] = $PALETTE[$COLOR[1]+1];
		 }
		 else
			return FALSE;
		 imagesetpixel($res,$X,$Y,$COLOR[1]);
		 $X++;
		 $P += $BMP['bytes_per_pixel'];
		}
		$Y--;
		$P+=$BMP['decal'];
	   }


	   fclose($f1);

	 return $res;
	}
	
	
	/*
	* Create Thumbnail
	*/
	private function createThumbnail($fullFile, $Name, $type) {
	
		if ($this->Extension == ".jpg") {
			$image = imagecreatefromjpeg($fullFile);
		} elseif ($this->Extension == ".png") {
			$image = imagecreatefrompng($fullFile);
		} elseif ($this->Extension == ".gif") {
			$image = imagecreatefromgif($fullFile);
		} elseif ($this->Extension == ".bmp") {
			$image = $this->imagecreatefrombmp($fullFile);
		}
		
		$old_x = imagesx($image);
		$old_y = imagesy($image);
	
		$thumb_width = $this->Thumb_Width;
		$thumb_height = $this->Thumb_Height;

		$thumb_image = imagecreate($thumb_width, $thumb_height);
		$result = imagecopyresized($thumb_image, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $old_x, $old_y);
		
		touch($Name);
		imagejpeg($thumb_image, $Name);	
		imagedestroy($image);
		imagedestroy($thumb_image);		
		
		
	}

	 /*
	 * Set Output data
	 */
	 public function Upload() {
		if ($this->Multi == TRUE) {
			
			$return = array("images" => array(), "type" => array());
			for($i=0; $i < count($_FILES[$this->File]['tmp_name']); $i++) {

				if (is_uploaded_file($_FILES[$this->File]['tmp_name'][$i])) {
				
					$item = $_FILES[$this->File];
					
					// Skontroluj velkos�
					$this->checkSize($item['size'][$i]);
			
					// Skontroluj rozmery
					$this->checkResolution($item['tmp_name'][$i]);
					
					// Skontroluj typ
					$this->checkExtension($item['name'][$i]);

					// Check path
					if (!$this->Path) {
						$this->setError($this->__("LCU_5"));
					}
				
					$upload['name'] = $this->randString(10)."_".time().$this->Extension;
					move_uploaded_file($item['tmp_name'][$i], $this->Path.$upload['name']);
					if (function_exists("chmod")) { chmod($this->Path.$upload['name'], 0644); }
					
					if ($this->Thumbnail) {
						$this->createThumbnail($this->Path.$upload['name'], $this->Path."thumb_".$upload['name']);
					}
					

					array_push($return['images'], $upload['name']);
					array_push($return['type'], $this->Extension);

				} else {
					$this->setError("Uploadin failed! Please try again.");
				}				
			}
			return $return;
			
		} else {
	 
			if (is_uploaded_file($_FILES[$this->File]['tmp_name'])) {
			
				$item = $_FILES[$this->File];
				
				// Skontroluj velkos�
				$this->checkSize($item['size']);
		
				// Skontroluj rozmery
				$this->checkResolution($item['tmp_name']);
				
				// Skontroluj typ
				$this->checkExtension($item['name']);

				// Check path
				if (!$this->Path) {
					$this->setError($this->__("LCU_5"));
				}
				
				// Change name if Required
				if ($this->Name) {
					$upload['name'] = $this->Name.$this->Extension;
				} else {
					$upload['name'] = stripfilename(substr($item['name'], 0, strrpos($item['name'], "."))).$this->Extension;
				}
				
				// File exist
				if (file_exists($this->Path.$upload['name'])) {
					@unlink($this->Path.$upload['name']);
				}
				
				move_uploaded_file($item['tmp_name'], $this->Path.$upload['name']);
				if (function_exists("chmod")) { chmod($this->Path.$upload['name'], 0644); }
		
				return array(
					'name' => $upload['name'],
					'size' => $item['size'],
					'width' => $this->Width,
					'height' => $this->Height,
					'extension' => $this->Extension
				);
		
			} else {
				$this->setError($this->__("LCU_6"));
			}
		}
	 }
}