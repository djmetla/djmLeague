<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Elo.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class Elo extends djmLeague {

	private $t1_points = NULL;
	private $t2_points = NULL;
	private $t1Score = NULL;
	private $t2Score = NULL;
	private $type = "default";
	private $Constant = 30;
	
	public function __construct($Type) {
		$this->type = $Type;
		return $this;
	}
	
	/* Set Constant */
	public function Constant($Val) {
		$this->Constant = $Val;
		return $this;
	}
	
	/* Set t1 points */
	public function t1Pts($pts) { 
		$this->t1_points = $pts;
		return $this;
	}
	
	/* Set t2 points */
	public function t2Pts($pts) {
		$this->t2_points = $pts;
		return $this;
	}
	
	/* Set t1 score */
	public function t1Score($scr) {
		$this->t1Score = $scr;
		return $this;
	}
	
	/* Set t2 score */
	public function t2Score($scr) {
		$this->t2Score = $scr;
		return $this;
	}
	
	
	private function _classic($b1, $b2) {
		if ($b1 > $b2) {
			return array(2, -2);
		} elseif ($b1 < $b2) {
			return array(-2, 2);
		} else {
			return array(1,1);
		}
	}
	
	
	private function _elo($b1, $b2, $s1, $s2) {
	
		$k = $this->Constant; 

		if (($s1 + $s2) == 0) {
			$_s1 = 0;
			$_s2 = 0;
		} else {
			$_s1 = $s1 / ($s1 + $s2);
			$_s2 = $s2 / ($s1 + $s2);
		}

		$c1 = 1 / (1 + pow(10, (($b2 - $b1) / 400)));
		$c2 = 1 / (1 + pow(10, (($b1 - $b2) / 400)));

		if ($s1 > $s2) {
			$body1 = round($b1 + $k * ($_s1 - $c1));
			$body2 = round($b2 + $k * ($c2 * (atan($_s2 + M_PI))) * ($_s2 - $c2));
			if (($body1 - $b1) <= 0) {
				$body1 = $b1 + 1;
			}
		} elseif ($s1 < $s2) {
			$body1 = round($b1 + $k * ($c1 * (atan($_s1 + M_PI))) * ($_s1 - $c1));
			$body2 = round($b2 + $k * ($_s2 - $c2));
			if (($body2 - $b2) <= 0) {
				$body2 = $b2 + 1;
			}
		} else {
			$body1 = round($b1 + $k * ($_s1 - $c1) + 1);
			$body2 = round($b2 + $k * ($_s2 - $c2) + 1);
		}

		return array(($body1 - $b1), ($body2 - $b2));	
	}	
	
	
	public function Calculate() { 
	
		if ($this->type == "default") {
			$elo = $this->_elo($this->t1_points, $this->t2_points, $this->t1Score, $this->t2Score);	
		} else { 
			$elo = $this->_classic($this->t1Score, $this->t2Score);
		}
		
		return $elo;
	}
	

	
}
?>
