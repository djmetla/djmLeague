<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Update.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

class djmUpdate extends djmLeague {

	private $Server_Update = "http://www.djmetla.eu/djmLeague/api";
	private $Cache;

	function __construct() {
		if ($this->checkPHPSettings()) {
			$curlInit = curl_init($this->Server_Update);           
			curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);   
			$this->Cache = json_decode(curl_exec($curlInit), true);    
			return $this;
		} else {
			return NULL;
		}
	}
	
	public function checkPHPSettings() {
		if (ini_get('allow_url_fopen') == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getPckg() {
		return $this->Cache['version'];
	}
	
	public function getName() { 
		return $this->Cache['name'];
	}
	
	public function getVersion() {
		return $this->Cache['version'];
	}
	
	public function getDownload() {
		return $this->Cache['download'];
	}
	
	public function getChangelog() {
		return $this->Cache['changelog'];
	}
	
	public function getRelease() {
		return " - Release date: ".$this->Cache['date']['date'];
	}

	public function showInstallation() {
		$installation = @file_get_contents($this->Cache['installation']);
		return $installation;
	}
	
	public function showChangelog() {
		$changeLog = @file_get_contents($this->Cache['changelog']);
		return $changeLog;
	}
	

	

}
?>