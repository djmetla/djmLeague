<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Application.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 
class djmLeague {

    private $args;
    private $page;
	private $lang = array();

    public function __construct() {
        $this->args = array_keys($_GET);
        $this->page = $this->args[0];
        return $this;
    }
	
   /*
    *  @var isPlayer return player data with table fusion_user 
    */ 
    public function isPlayer($user=NULL) {
        global $userdata;
        if ($user) {
            djmDB::Select("SELECT * FROM ".dbPlayer." WHERE player_user='%d' ", $user);
            if (djmDB::Num()) {
                djmDB::Select("SELECT t1.*, t2.user_id, t2.user_name FROM ".dbPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user WHERE player_user='%d' ", $user);
                return djmDB::Data();
            } else {
                return FALSE; 
            }
        } else {
            if (iMEMBER) { 
                djmDB::Select("SELECT * FROM ".dbPlayer." WHERE player_user='%d'", $userdata['user_id']);
               if (djmDB::Num()) { 
                    djmDB::Select("SELECT t1.*, t2.user_id, t2.user_name FROM ".dbPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user WHERE player_user='%d'", $userdata['user_id']);
                    return djmDB::Data();
               } else {
                   return FALSE;
               }
            } else {
                return false;
            }
        }
    }
  
	/*
	* @var isTeamPlayer
	*/
	public function isTeamPlayer($team, $user=false) {
		if ($user) {
			djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_user='%d' AND player_team='%d'", array($user, $team));
			if (djmDB::Num()) {
				$data = djmDB::Data();
				return $data['player_position'];
			} else {
				return FALSE;
			}	
		} else {
			global $userdata;
			if (iMEMBER) {
				djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_user='%d' AND player_team='%d'", array($userdata['user_id'], $team));
				if (djmDB::Num()) {
					$data = djmDB::Data();
					return $data['player_position'];
				} else {
					return FALSE;
				}
			} else {
				return FALSE;
			}
		}
	}
  
    /*
     *  @var isAdmin return true or false
     */
    public function isAdmin($Flag=NULL) {
		global $userdata;
			if ($Flag) {
				if (iMEMBER) { 
					if (iSUPERADMIN && $userdata['user_id'] == 1) {
						return true;
					} else {				
						djmDB::Select("SELECT * FROM ".dbAdmin." WHERE admin_user='%d'", $userdata['user_id']);
						if (djmDB::Num()) { 
							$data = djmDB::Data(); $flags = explode(".", $data['admin_flag']);
							if (in_array($Flag, $flags)) { 
								return true;
							} else {
								return false;
							}
						} else {
							return false;
						}
					}
				} else {
					return false;
				}
			} else {
				if (iMEMBER) {
					if (iSUPERADMIN && $userdata['user_id'] == 1) {
						return true;
					} else {
						djmDB::Select("SELECT * FROM ".dbAdmin." WHERE admin_user='%d'", $userdata['user_id']);
						if (djmDB::Num()) {
							return djmDB::Data();
						} else{
							return false;
						}
					}
				} else {
					return false;
				}
			}
    }
    
	/* 
	* Profile link
	*/
	public function Profile($Player, $user_name, $add="") {
		return "<a href='".urlProfile.$Player."' ".($add ? $add:"").">".$user_name."</a>";
	}
	
	/*
	* GameID Tool
	*/
	public function GameID($Player, $League) {
		djmDB::Select("SELECT league_account_key FROM ".dbLeague." WHERE league_id='%d' AND league_account='YES' ", $League);
		if (djmDB::Num()) {
			$data = djmDB::Data();
			djmDB::Select("SELECT * FROM ".dbAccounts." WHERE account_user='%d' AND account_key='%s'", array($Player, $data['league_account_key']));
			if (djmDB::Num()) {
				$data2 = djmDB::Data();
				if ($data2['account_value'] != "") {
					echo $data2['account_value']; $this->Tooltip($this->__("APP_1")." ".strftime("%d.%m.%Y %H:%M", $data2['account_last_change']));
				} else {
					if ($data2['account_new_value'] != "") {
					  echo $this->__("APP_2"); $this->Tooltip($this->__("APP_3"));
					} else {
					  echo $this->__("APP_4"); $this->Tooltip($this->__("APP_5")); 
					}
				}
			} else {
				echo $this->__("APP_4"); $this->Tooltip($this->__("APP_5")); 
			}
		} else { echo "--- "; $this->Tooltip($this->__("APP_6")); }
	}
	
	/* 
	*	GameID Checking 
	*/
	public function checkGameID($Player, $League) {
		djmDB::Select("SELECT league_account_key FROM ".dbLeague." WHERE league_account='YES'");
		if (djmDB::Num()) {
				$key = djmDB::Data();
				djmDB::Select("SELECT * FROM ".dbAccounts." WHERE account_key='%s' AND account_user='%d' AND account_value!='' ", array($key['league_account_key'], $Player));
				if (djmDB::Num()) {
					return true;
				} else {
					return false;
				}
		} else {
			return true;
		}
	}
	
	/* 
	* Team points progress
	*/
	public function TeamPoints($id, $points) {
			djmDB::Select("SELECT * FROM ".dbMatch." WHERE (match_t1='%d' OR match_t2='%d') AND match_status='2' ORDER BY match_id DESC LIMIT 1", array($id, $id));
			if (djmDB::Num()) {
				$match = djmDB::Data();
				if ($match['match_winner'] == $id) {
					echo "<span class='green'>".$points." <span style='position:relative; top: -2px;'>&uarr;</span></span>";
				} elseif ($match['match_losser'] == $id) {
					echo "<span class='red'>".$points." <span style='position:relative; top: -2px;'>&darr;</span></span>";
				} else {
					echo "<span class='blue'>".$points." <span style='position:relative; top: -2px;'>&asymp;</span></span>";
				}
			} else {
				echo $points;
			}
	}
	
	/*
	* Team Activity
	*/
	public function TeamActivity($id) {
			
			$week = time() - 604800;
			$matches = dbcount("(match_id)", dbMatch, "match_status !='3' AND(match_t1='".$id."' OR match_t2='".$id."') AND match_time BETWEEN ".$week." AND ".time()."");
			
			$percent = (100 / 20) * $matches;
			if ($percent > 100) {
				$percent = 100;
			}
			
			if ($percent >= 80) {
				$color = "#8fc400"; $text = "#ffffff";
			} elseif ($percent >= 50) {
				$color = "#fff800";
			} elseif ($percent >= 25) {
				$color = "#ff9900";
			} elseif ($percent >= 0) {
				$color=  "#ff0000";
			}
				
			echo "
				<div class='activity'> 
					<div class='activity_text' style='color: ".$text.";'>".$percent."%</div>  
					<div style='float: left; height: 10px; width: ".$percent."%; background-color: ".$color.";'>&nbsp;</div> 
				</div>
			";
	}
	
	
	/*
	* Team position
	*/	
	public function teamRank($Team, $League) {
	//echo $League;
    $sql_rank = djmDB::Select("SELECT x.* 
												 FROM (
													SELECT t.team_id, t.team_league, t.team_status, @rownum := @rownum + 1 AS position 
													FROM " . dbTeam . " as t 
													JOIN (SELECT @rownum := 0) r
													WHERE t.team_status='ACTIVE' AND t.team_league='" . $League . "'
													ORDER BY t.team_points DESC) as x 
													WHERE x.team_id = '" . $Team . "' AND x.team_league='" . $League . "'  AND x.team_status='ACTIVE'");
    $rank = djmDB::Data();
		echo ($rank['position'] != "" ? $rank['position']."." : $this->Tooltip($this->__("APP_7")));	
	}
	
    /*
    *   @var setError return redirect and set error
     */
    public function setError($String, $url="") {
        $_SESSION['league_error'] = $String;
        $this->location(($url ? $url:FUSION_REQUEST));
    }
  
    
    /*
     *  @var setDone return message is done
     */
    public function setDone($String, $url="") {
        $_SESSION['league_done'] = $String;
		$this->location(($url ? $url:FUSION_REQUEST));
    }
    
	/*
	*	@var setEvent
	*/
	public function setEvent($title, $String, $user) {
		global $userdata;
		djmDB::Insert(dbEvent, array('event_user' => $user, 'event_time' => time(), 'event_subject' => $title, 'event_text' => $String));
	}
	
    /*
     *  @var setLog
     */
    public function setLog($String, $parent, $target=FALSE) {
        global $Logs, $userdata; 
		if ($target) { 
			djmDB::Insert(dbLog, array('log_type'=> $target, 'log_user' => $userdata['user_id'], 'log_parent' => $parent, 'log_time' => time(), 'log_value' => $String ));		
		} else {
			if (iMEMBER && in_array($this->page, $Logs)) {
				djmDB::Insert(dbLog, array('log_type'=> $this->page, 'log_user' => $userdata['user_id'], 'log_parent' => $parent, 'log_time' => time(), 'log_value' => $String ));
			}
		}
    }
    
	/*
	*	@var setAdminLog
	*/
	public function setAdminLog($String) {
		global $userdata;
		djmDB::Insert(dbAdminLog, array('log_page' => $_GET['option'], 'log_user' => $userdata['user_id'], 'log_time' => time(), 'log_value' => $String));
	}
	
	
    /*
     * @var Redirect
     */
    public function location($url) {
        redirect($url);
    }

	/*
	*	 Help system
	*/
	public function Help($File) {
		if( file_exists(pathDocs.$File)) {
			$content = file_get_contents(pathDocs.$File);
		} else {
			$content = "Error loading content!";
		}
		echo "<div id='dialog' class='hide' title='".$this->__("APP_8")."'>".$content."</div>";
	}
	
	/*
	*	Icon
	*/
	public function Icon($File, $Folder="System", $align=false) {
		if (file_exists(pathMedia.$Folder."/".$File)) {
		echo "<img src='".pathMedia.$Folder."/".$File."' alt='".$File."' style='position: relative; top: 2px; ".($align ? "float: right;":"")."' />";
		} 
	}
		
	/*
	*	Tooltip
	*/
	public function Tooltip($String, $Align=FALSE) {
		echo "<span class='tooltip' title='".$String."' style='cursor: help; ".($Align ? "float: right":"")."'> <img src='".pathMedia."System/tooltip.png' alt='?' style='position: relative; top: 3px;' /> </span>";
	}
	
	/* 
	* Flags
	*/
	public function Flags($Name, $Selected=false) {
		echo "<select name='".$Name."' style='width: 232px;' class='textbox'>";
			echo "<option value=''></option>";
			$dir = pathMedia."Flags/";
			foreach (scandir($dir) as $file) {
				if ($file != "." && $file != "..") {
					$name_1 = explode(".", $file);
					$name = str_replace("-", " ", $name_1[0]);
					 echo "<option value='".$file."' ".($Selected && $Selected==$file ? "selected='selected'":"").">".$name."</option>"; 
				 }
			}
		echo "</select>&nbsp;";
	}
	
	/*
	*	Form head
	*/
	public function Form($Target, $Method="POST", $Name=NULL, $Data=FALSE) {
		echo "<form action='' method='".$Method."' ".($Name ? "name='".$Name."'":"")." ".($Data ? "enctype='multipart/form-data'":"")." autocomplete='off'>";
		echo "<input type='hidden' name='form' value='".$Target."' />";
	}
	
	/*
	*	Input
	*/
	public function Input($Type, $Name, $Value="", $Placeholder=NULL, $Checked=FALSE) {
		$Restriction = array("radio", "checkbox", "submit", "hidden");
		if ($Type == "checkbox") { $CheckType = "checked='checked'"; } elseif ($Type == "radio") { $CheckType = "checked='checked'"; } else { $checkType = ""; } /* UPRAV TO MOC COMPLICATED */
		echo "<input 
						type='".$Type."' 
						name='".$Name."' 
						".($Value ? "value='".$Value."'":"")." 
						".($Placeholder ? "placeholder='".$Placeholder."'":"")." 
						class='".($Type == "submit" ? "button":"textbox")."' 
						".(!in_array($Type, $Restriction) ? "style='width: 230px;'":"")." 
						".($Checked ? $CheckType:"")."
					/>".($Type != "hidden" ? "&nbsp;":"")."";
	}
	
	
	/*
	* RandString function
	*/
	public function randString($limit) {
		$result = "";
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTVUXYZ0123456789";
		$charArray = str_split($chars);
		for($i = 0; $i < $limit; $i++) {  $randItem = array_rand($charArray); $result .= $charArray[$randItem]; }
		return $result;
	}

	
	/*
	*	Translate
	*/
	private function splitStrings($str) {
        return explode('=',trim($str));
    }	
	public function __($String, $Additional="") {
			global $settings;
			if (file_exists(pathLocale.$settings['locale'].".ini")) { 	
				$file = pathLocale.$settings['locale'].($this->page == "admin" ? "_admin":"").".ini";
			} else {
				$file = pathLocale."Slovak".($this->page == "admin" ? "_admin":"").".ini";
			}	

			$trans_value = array_map(array($this,'splitStrings'),file($file));
			
			foreach ($trans_value as $k => $v) {
				$this->lang[$v[0]] = $v[1];
			}
			
			if (array_key_exists($String, $this->lang)) {
				$return = sprintf($this->lang[$String], $Additional);
			} else {
				$return = "TRANSLATE_ERROR";
			}	
					
			return $return;	
	}
	
	
	/* 
	* Get Admin log
	*/
	private function getAdminLog($Page="") {
		if ($Page) { 
			djmDB::Select("SELECT * FROM ".dbAdminLog." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.log_user WHERE log_page='%s' ORDER BY log_id DESC LIMIT 100", $Page);
		} else {
			djmDB::Select("SELECT * FROM ".dbAdminLog." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.log_user ORDER BY log_id DESC LIMIT 100");
		} 
			if (djmDB::Num()) {
				return djmDB::fullData();
			} else {
				return FALSE; 
			}
	}
	
	
	/*
	*	 Copyright
	*/
	private function Copyright() {
		echo "<div class='right'>djmLeague by <a href='http://www.djmetla.eu' target='_blank'>djmetla</a> &copy; 2013 ".(date("Y") != "2013" ?  " - ".date("Y") :"")."</div>";
	}
	
	
    /*
     *  Parse
     */
    private function Parse() {
        global $Pages, $Page, $config, $update, $dbConfig, $locale, $settings, $userdata;
        $page = array();
		$page['comment'] = false;
		$page['help'] = false;
		
		// isAdmin
		if ($this->page == "admin") {
			if (!$this->isAdmin()) { $this->location(urlDefault); }
			
			// Update message
			if ($update->checkPHPSettings() && $update->getPckg() > $config['pckgVersion']) {
				opentable($this->__("APP_9"));
					echo "<div style='margin: 0 auto; width: 450px;'>";
						echo "<img src='".pathMedia."System/update.png' alt='Update' align='left' style='margin: 5px 15px 0 0'/> <span style='font-weight: bold; font-size: 17px;'>".$this->__("APP_10")."</span>
								  <br/> ".$this->__("APP_11")." <a href='#'>".$this->__("APP_12")."</a>.
						";
					echo "</div>";
				closetable();
			}
			
		}
		
        if (in_array($this->page, $Pages)) {
            $file = ucfirst($this->page);

            if (file_exists(pathPages . $file . ".php")) {
                
                /* Include PHP File */
                include pathPages . $file . ".php";
				
				/* Include Form files & system */
			   if (isset($_POST) && isset($_POST['form'])) {
					include pathForms ."form". $file .".php";
					$form_class = "form".$file;
					$form_method = "action".ucfirst($_POST['form']);
					$form = new $form_class();
						if (method_exists($form, $form_method)) {
							$form->$form_method($_POST);
						} else {
							$this->setError("Core Error ## Form method no exist!");
						}
			   }      				
                
                /* Make Errors & done */
                include pathLibs."Error.php";
                
				/* Check active bans */
				if (iMEMBER && dbcount("(*)", dbBan, "ban_user='".$userdata['user_id']."' AND (ban_expiration>".time()." OR ban_permanent='YES')")) {
					$banned = TRUE;
				} else {
					$banned = FALSE;
				}

				/* Check Installation settings */
				if ($this->page == "admin") {
				$f['media'] = substr(sprintf('%o', fileperms(pathMedia)), -4);
				$f['Game'] = substr(sprintf('%o', fileperms(pathMedia."Game/")), -4);
				$f['Match'] = substr(sprintf('%o', fileperms(pathMedia."Match/")), -4);
				$f['Team'] = substr(sprintf('%o', fileperms(pathMedia."Team/")), -4);
				}
				
                /* Parse page */
				add_to_title("&nbsp;-&nbsp;".$page['title']." - djmLeague");
				if ($this->page == "admin" && ($f['media'] != "0777" OR $f['Game'] != "0777" OR $f['Match'] != "0777" OR $f['Team'] != "0777")) {
					opentable($this->__("APP_19"));
						echo "<div style='padding: 10px;'>";
						echo $this->__("APP_16")."<br/><br/>
							<table border='0' width='50%'>
							<tr> <td>djmLeague/Media</td><td>".($folder['media'] != "0777" ? "<span class='red'>".$this->__("APP_17")."</span>":"<span class='green'>".$this->__("APP_18")."</span>")."</td></tr>
							<tr> <td>djmLeague/Media/Game</td><td>  ".($folder['Game'] != "0777" ? "<span class='red'>".$this->__("APP_17")."</span>":"<span class='green'>".$this->__("APP_18")."</span>")."</td></tr>
							<tr> <td>djmLeague/Media/Match</td><td>  ".($folder['Match'] != "0777" ? "<span class='red'>".$this->__("APP_17")."</span>":"<span class='green'>".$this->__("APP_18")."</span>")."</td></tr>
							<tr> <td>djmLeague/Media/Team</td><td>  ".($folder['Team'] != "0777" ? "<span class='red'>".$this->__("APP_17")."</span>":"<span class='green'>".$this->__("APP_18")."</span>")."</td></tr>
							</table>
						</div>";
					closetable();
				} else {
				
					if ($banned && $this->page != "admin") { 
						opentable($this->__("APPB_1"));
							djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbBan." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.ban_admin WHERE ban_user='%d' AND (ban_expiration>".time()." OR ban_permanent='YES')", $userdata['user_id']);
							$ban = djmDB::Data();
							echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>";
								echo "<tr> <td class='tbl1' colspan='2' style='padding:15px; text-align:center;'>
									<h2 style='margin:0; padding:0px;'> ".$this->__("APPB_2")."</h2>
									".$this->__("APPB_3")." ".($ban['ban_permanent'] == "YES" ? "<span class='red'>".$this->__("APPB_4")."</span>":"<span class='green'>".$this->__("APPB_5")."</span>")." ".$this->__("APPB_6")." ".$ban['user_name'].".
									<br/><br/>
									<h3> ".$this->__("APPB_7")."</h3>
									".$ban['ban_reason']."
								</td> </tr>";
								echo "<tr> <td class='tbl1' width='30%'>".$this->__("APPB_8").":</td> <td class='tbl1'><a href='".BASEDIR."profile.php?lookup=".$ban['ban_admin']."'>".$ban['user_name']."</a></td> </tr>";
								echo "<tr> <td class='tbl1'>".$this->__("APPB_9").":</td> <td class='tbl1'>".strftime("%d.%m.%Y %H:%M", $ban['ban_time'])."</td> </tr>";
								echo "<tr> <td class='tbl1'>".$this->__("APPB_10").":</td> <td class='tbl1'>".($ban['ban_permanent'] == "YES" ? "---": strftime("%d.%m.%Y %H:%M", $ban['ban_expiration']))."</td> </tr>";
								echo "<tr> <td class='tbl1'>".$this->__("APPB_11").":</td> <td class='tbl1'>".($ban['ban_permanent'] == "YES" ? $this->__("APPB_12"):$this->__("APPB_13"))."</td> </tr>";
							echo "</table>";
						closetable();
					} else {
						opentable($page['title'].($this->page == "admin" && isset($_GET['option']) ? "<a href='".urlAdminIndex."' class='right' style='padding-right: 10px;'>".$this->__("APP_13")."</a>":"").($page['help'] ? "<span class='right'>&nbsp;|&nbsp;</span> <a href='javascript:void(0);' id='toggleHelp' class='right'>".$this->__("APP_8")."</a>":""));
							if (isset($_GET['option']) && in_array($_GET['option'], $Page[$file])) {
								include pathTemplates.$file."/".ucfirst($_GET['option']).".template.php";
							} else {
								include pathTemplates.$file."/Index.template.php";
							}							
						closetable();
					}
				}
				
				if ($page['comment'] == TRUE) {	
					require_once INCLUDES."comments_include.php";
					showcomments("M", dbMatch, "match_id", $_GET['match'], urlMatch.$_GET['match']);
				}
				
				
				/* Administration logs */
				if ($this->page == "admin") {
					opentable($this->__("APP_14"));
						echo "<div id='djmScroll' class='tbl-border' style='height: 140px; overflow: auto;'>";
							$logData = $this->getAdminLog((isset($_GET['option']) ? $_GET['option'] : ""));
							if ($logData) { 
								foreach ($logData as $log) {
									echo "<div class='left tbl1' style='width: 99%; margin-bottom: 1px;'>";
										echo "<div class='left' style='width: 175px;'>".strftime("%d.%m.%Y %H:%M", $log['log_time'])."</div>";
										echo "<div class='left' style='width: 100px;'><a href='".urlProfile.$log['user_id']."'>".$log['user_name']."</a> </div>";
										echo "<div class='left'>".$log['log_value']."</div>";
									echo "</div>";
								} 
							} else {
								echo "<center>".$this->__("APP_15")."</center>";
							}
						echo "</div>";
					closetable();
				}
            } else {
				$this->setError("Core Error ## File ".$file.".php no exist!", urlDefault);
				exit;
            }
        } else {
               $this->location(urlDefault);
        }
		
		// Copyright
		$this->Copyright();
		
    }

    /*
     *  Run djmLeague
     */
    public function Run() {
        echo $this->Parse();
    }
}