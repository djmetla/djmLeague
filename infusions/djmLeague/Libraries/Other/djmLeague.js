$(function(){
    
	/* Scroll */
	 $('#djmScroll').slimScroll({
		railVisible: true,
		height: '140px'
	});

	/* Photo gallery in match */
    $(".ImageView").fancybox({
		padding: 0,
		margin: 0 
    });	
	
	/* Admins more */
	$('a.ShowAdmin').click(function(){
		var ele = $(this), item = ele.attr('rel');
		
		if ( $('tr#Show-'+item).is(':visible') ) {
			$('tr#Show-' + item).fadeOut();
		} else {
			$('tr.AdminMore').hide();
			$('tr#Show-' + item).fadeIn();
		}
		return false;
	});
	
	
	/* Table effect */
	$('.effect tr').mouseover(function(){ $(this).find('td').removeClass('tbl1').addClass('tbl2'); });
	$('.effect tr').mouseout(function(){ $(this).find('td').removeClass('tbl2').addClass('tbl1'); });
	

	
	
	/* Help box */
	$('#toggleHelp').click(function() {
		$('#dialog').dialog({
			width: 600,
			height: "auto",
			modal: true,
			draggable: false,
			resizable: false,
			 buttons: {
					Ok: function() {
						$( this ).dialog( "close" );
					}		
			}
		});
		return false;
	});
	
	
	/* Delay function */
    var delay = (function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();	
	
	/* Auto hide  error & done message box */
	delay(function() {
		$('#djmLeagueMessageBox').fadeOut('slow');
	}, 5000);
	
	
	/* Tooltip (  (c) Tipsy) */
	$('span.tooltip').tipsy({html: true, gravity: 'se' });
	
	/* Administrator checkboxes */
	$('input[name="admin_league[]"]').click(function(){
		$('input[name="admin_league_all"]').removeAttr('checked');
	});
	$('input[name="admin_league_all"]').click(function(){
		$('input[name="admin_league[]"]').removeAttr('checked');
	});	
	
	$('input[name="admin_account[]"]').click(function(){
		$('input[name="admin_account_all"]').removeAttr('checked');
	});
	$('input[name="admin_account_all"]').click(function(){
		$('input[name="admin_account[]"]').removeAttr('checked');
	});	
	
	$('input[name="pattern_league[]"]').click(function(){
		$('input[name="pattern_league_all"]').removeAttr('checked');
	});
	$('input[name="pattern_league_all"]').click(function(){
		$('input[name="pattern_league[]"]').removeAttr('checked');
	});		
	
    /***********************************************
     *		jQTabs	Engine
     *		Created by djmetla
     *		19.02.2013
     *		http://www.djmetla.eu
     ***********************************************/
    /* Definitions */
    var hash = window.location.hash.substr(1),
            tabs_navi = $('ul.tabs-navi');

    /* Add style to tabs buttons */
    tabs_navi.find('li').addClass('tab');

	/* Additional A link */
	$('a.go_to_Tab').click(function(){
		var a_link = $(this), href_hash = a_link.attr('href');
		window.location.hash = href_hash;
		a_link.html('Loading...');
		document.location.reload(true);
		return false;
	});
	
    /* Engine hash */
    if (hash != "") {
        var hash_tab = tabs_navi.find('li[name="' + hash + '"]');
        if (hash_tab.length > 0) {
            tabs_navi.find('li').removeClass('hover');
            hash_tab.addClass('hover');
            var show_tab = hash_tab.attr('id'), tab_id = show_tab.substr(3);
            $('div.tabs-content').find('div.tab').hide();
            $('div.tabs-content').find('div#tab-' + tab_id).show();
        }
    }

    /* Engine tabs */
    tabs_navi.on("click", "li", function() {
        var ele = $(this), tab = ele.attr('id').substr(3), link = ele.attr('name');
        tabs_navi.find('li').removeClass('hover');
        $('div.tabs-content').find('div.tab').hide();
        $(ele).addClass('hover');
        $('div#tab-' + tab).show();
        window.location.hash = link;
        return false;
    });
	
    
});