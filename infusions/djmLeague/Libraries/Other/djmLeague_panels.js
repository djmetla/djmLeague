$(function(){

	/* Delay function */
    var delay = (function() {
        var timer = 0;
        return function(callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();	

	/* Event box */
	var cfgReload = $('input[name="eventBoxReload"]').val();
	setInterval(function(){
		 $.getJSON("../../infusions/djmLeague/index.php?ajax=event", function(json){
			if (parseInt(json.count) > 0) {
					var box = $('#event');
					if (!box.is(':visible')) {
						box.append('<audio id="chatSound" autoplay="autoplay"><source src="../../infusions/djmLeague/Media/Sound/'+box.attr('audio')+'" type="audio/wav" /></audio>');
						box.append("<span style='font-weight: bold; font-size: 18px;'>" + box.attr('rel') + "</span>");
						box.fadeIn();
						delay(function() { box.fadeOut('slow', function() { box.html(''); } ); }, 3000);					
					}
					
			}
		});	
	}, cfgReload);

	/* League list panel */
	$('li.show-game').click(function(){
		var button = $(this).attr('rel');
		$('ul#game-'+button).fadeToggle();
		return false;
	});
	
	$('li.show-league').click(function(){
		var button = $(this).attr('rel');
		$('ul#league-'+button).fadeToggle();
		return false;
	});
	
}); 