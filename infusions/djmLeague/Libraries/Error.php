<?php

/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Error.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

if ($_SESSION['league_error'] != "") {
	echo "<div id='djmLeagueMessageBox'>";
    opentable($this->__("ER_1"));
        echo "<div style='text-align: center; color: red;'>".$_SESSION['league_error']."</div>";
        $_SESSION['league_error'] = NULL;
    closetable();
	echo "</div>";
}

if ($_SESSION['league_done'] != "") {
	echo "<div id='djmLeagueMessageBox'>";
    opentable($this->__("ER_2"));
        echo "<div style='text-align: center; color: green;'>".$_SESSION['league_done']."</div>";
        $_SESSION['league_done'] = NULL;
    closetable();	
	echo "</div>";
}

?>
