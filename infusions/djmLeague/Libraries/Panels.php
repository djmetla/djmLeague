<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Panels.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
/* PHP-Fusion Security */
if (!defined("IN_FUSION")) { die('Access Denied'); }

/* MySQL Class */
include_once ("Classes/MySQL.php"); 

/* CSS */
?>
<style>
	.red { color: red; }
	.green { color: green; }
	.blue { color: blue; }
	
	div#event {  
		color: #1EC3FA;
		display: none;
		border: 1px solid #d8d8d8;
		border-radius: 5px; 
		max-width: 200px; 
		z-index: 10001; 
		position:fixed; 
		padding: 10px;
		font-size: 12px;
	}
	div#event:hover { background-color: #1ec3fa; color: #ffffff; border: 1px solid #1DA0CC; cursor: pointer; }
</style>
<?php

/* Path */
if (!defined("pathLeague")) {
	define("pathLeague", INFUSIONS."djmLeague/");
	define("pathMedia", pathLeague."Media/");
	define("pathLocale", pathLeague."Locale/");
}

/* Databses */
	if (!defined("dbPlayer")) {
	$config['database_layout'] = DB_PREFIX."league_";
	define("dbPlayer",  $config['database_layout']."player"); //Players database
	define("dbLeague",  $config['database_layout']."league"); // Leagues database
	define("dbGame",  $config['database_layout']."game"); // Games database
	define("dbTeamPlayer", $config['database_layout']."team_player"); // Players in teams
	define("dbTeam", $config['database_layout']."team"); // Teams database
	define("dbMatch", $config['database_layout']."match"); // Matches database
	define("dbSearch", $config['database_layout']."search"); // Search opponent
	define("dbAdmin", $config['database_layout']."admin"); // Admin database
	define("dbSettings", $config['database_layout']."settings"); // Settings database
	}

/* jQuery */
add_to_head("<script type='text/javascript' src='".pathLeague."Libraries/Other/djmLeague_panels.js'></script>");

/* App */
include_once(pathLeague."Libraries/Application.php");




?>