<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Bootloader.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/

/* Session start */
session_start();
if (function_exists('session_register')) {
	@session_register("league_error");
	@session_register("league_done");
}

/* PHP-Fusion */
require "../../maincore.php";

/* PHP-Fusion Security */
if (!defined("IN_FUSION")) { die('Access Denied'); }

/* MySQL Class */
include ("Classes/MySQL.php"); 

/* Default config */
$config['default_index'] = "league";
$config['database_layout'] = DB_PREFIX."league_";

/* Modification package version */
$config['pckgVersion'] = 1.1;
$config['Version'] = "1.1";

/* MySQL tables */
define("dbSettings", $config['database_layout']."settings");
define("dbPlayer",  $config['database_layout']."player");
define("dbLeague",  $config['database_layout']."league");
define("dbLog",  $config['database_layout']."log");
define("dbGame",  $config['database_layout']."game");
define("dbAccounts", $config['database_layout']."accounts");
define("dbAccountsPattern", $config['database_layout']."accounts_pattern");
define("dbTeam", $config['database_layout']."team");
define("dbTeamPlayer", $config['database_layout']."team_player");
define("dbMatch", $config['database_layout']."match");
define("dbMatchMedia", $config['database_layout']."match_media");
define("dbMatchLinks", $config['database_layout']."match_links");
define("dbMatchRequest", $config['database_layout']."match_request");
define("dbAdmin", $config['database_layout']."admin");
define("dbAdminCategory", $config['database_layout']."admin_category");
define("dbAdminLog",  $config['database_layout']."admin_log");
define("dbBan",  $config['database_layout']."ban");
define("dbProtest", $config['database_layout']."protest");
define("dbRequest", $config['database_layout']."request");
define("dbProtestPattern", $config['database_layout']."protest_pattern");
define("dbRequestPattern", $config['database_layout']."request_pattern");
define("dbEvent", $config['database_layout']."event");
define("dbChallenge", $config['database_layout']."challenge");
define("dbSearch", $config['database_layout']."search");
define("dbComplaint", $config['database_layout']."complaints");
define("dbElo", $config['database_layout']."elo");

/* Default config from MySQL */
djmDB::Select("SELECT * FROM ".dbSettings);
$dbConfig = djmDB::Data();

/* Pages settings */
$Pages  = array("admin","league", "player", "team", "match", "tool");

/* Subpage settings */
$Page['League'] = array("rules", "information");
$Page['Player'] = array("settings", "events");
$Page['Team'] = array("settings", "squad", "activate", "leave");
$Page['Match'] = array("media", "links", "result", "request_delete", "request_guest", "request", "protest", "server", "close"); 
$Page['Tool'] = array("create", "join","challenge", "opponent", "admins");
$Page['Admin'] = array("m_league", "m_admins", "m_accounts", "m_game", "m_modification", "m_protests", "m_requests", "m_logs", "m_elo", "m_backups", "a_accounts", "a_my_card", "a_protests", "a_statistics", "a_penalties_and_points", "a_banlist");

/* Enable logs */
$Logs = array("player", "match", "team");

/* System path */
define("pathLeague", INFUSIONS."djmLeague/");
define("pathLibs", pathLeague."Libraries/");
define("pathPages", pathLeague."Pages/");
define("pathForms", pathLeague."Form/");
define("pathTemplates", pathLeague."Templates/");
define("pathBackups", pathLeague."Backup/");
define("pathMedia", pathLeague."Media/");
define("pathLocale", pathLeague."Locale/");
define("pathDocs", pathLeague."Docs/");

/* URL Define */
$config['default_link'] = ($dbConfig['settings_link'] == "full" ? "index.php?":"?");
define("urlDefault", $config['default_link'].$config['default_index']);
define("urlLeague", $config['default_link']."league=");
define("urlTeam", $config['default_link']."team=");
define("urlPlayer", $config['default_link']."player=");
define("urlMatch", $config['default_link']."match=");
define("urlProfile", $config['default_link']."player=");
define("urlPage", $config['default_link']."page&amp;option=");
define("urlAdminIndex", $config['default_link']."admin");
define("urlAdmin", $config['default_link']."admin&amp;option=");
define("urlTool", $config['default_link']."tool&amp;option=");

/* Application core */
include (pathLibs."Application.php");

/* Other Classes */
include ('Classes/Upload.php');
include ('Classes/SelectTime.php');
include ('Classes/Update.php');
include ('Classes/Elo.php');

/* Member register */
if (iMEMBER) {
    djmDB::Select("SELECT * FROM ".dbPlayer." WHERE player_user='%d'", $userdata['user_id']);
    if (!djmDB::Num()) {
        djmDB::Insert(dbPlayer, array('player_user' => $userdata['user_id'], 'player_time' => time())); 
    }
}

/* Initialize Update system */
$update = new djmUpdate();

/* Ajax */
$Ajax = array("event", "elo", "getTeam", "dc_register", "makeBackup");
if (isset($_GET['ajax']) && !empty($_GET['ajax']) && in_array($_GET['ajax'], $Ajax)) {
	include_once pathLibs."Ajax.php";
	exit;
}

/* Call Application */
$isAdminLink = array_keys($_GET);
if ($isAdminLink[0] == "admin") { $header_file = "admin_header.php"; } else { $header_file = "header.php"; } unset($isAdminLink);
require_once THEMES."templates/".$header_file;
add_to_head("<link rel='stylesheet' href='".pathLibs."Other/djmLeague.css' type='text/css' media='screen' />");
add_to_head("<link rel='stylesheet' href='".pathLibs."Other/jQuery-ui.css' type='text/css' media='screen' />");
add_to_head("<script type='text/javascript' src='".pathLibs."Other/jQuery.js'></script>");
add_to_head("<script type='text/javascript' src='".pathLibs."Other/jQuery-UI.js'></script>");
add_to_head("<script type='text/javascript' src='".pathLibs."Other/Tipsy.js'></script>");
add_to_head("<script type='text/javascript' src='".pathLibs."Other/fancybox.js'></script>");
add_to_head("<script type='text/javascript' src='".pathLibs."Other/jquery.slimscroll.js'></script>");
add_to_head("<script type='text/javascript' src='".pathLibs."Other/djmLeague.js'></script>");
$league = new djmLeague();

/* Admin Access flags defined */
$config['admin_flags'] = array (
	'ACCESS_ACCOUNTS' => $league->__("ADF_1"),
	'ACCESS_PROTESTS' => $league->__("ADF_2"),
	'ACCESS_STATISTICS' => $league->__("ADF_3"),
	'ACCESS_PENALTIES_AND_POINTS' => $league->__("ADF_4"),
	'ACCESS_BANLIST' => $league->__("ADF_5"),
	'ACCESS_M_LEAGUE' => $league->__("ADF_8"),
	'ACCESS_M_GAME' => $league->__("ADF_9"),
	'ACCESS_M_ACCOUNTS' => $league->__("ADF_10"),
	'ACCESS_M_ADMINS' => $league->__("ADF_11"),
	'ACCESS_M_PROTESTS' => $league->__("ADF_12"),
	'ACCESS_M_REQUESTS' => $league->__("ADF_13"),
	'ACCESS_M_LOGS' => $league->__("ADF_14"),
	'ACCESS_M_ELO' => $league->__("ADF_15"),
	'ACCESS_M_BACKUPS' => $league->__("ADF_16"),
	'ACCESS_M_MODIFICATION' => $league->__("ADF_17")
);

$league->Run();
require_once THEMES."templates/footer.php";
djmDB::Debug();


