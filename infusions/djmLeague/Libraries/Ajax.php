<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Ajax.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

switch($_GET['ajax']) {
	
	case "event":
			if (!iMEMBER) { exit; }
			global $userdata;	
			djmDB::Select("SELECT event_id FROM ".dbEvent." WHERE event_user='%d' AND event_ajax='0' ORDER BY event_id DESC LIMIT 1", $userdata['user_id']);
			$count = djmDB::Num();
			$data = djmDB::Data();
			$json = array('count' => $count);
			djmDB::Update(dbEvent, array('event_ajax' => '1'), array('event_id' => $data['event_id']));
		die(json_encode($json));
	break;

	case "elo":
		if (!iMEMBER) { exit; }
		if (!isset($_GET['constant']) OR !isset($_GET['t1pts']) OR !isset($_GET['t2pts']) OR !isset($_GET['maxScore'])) { exit; }
		
		$elo = new Elo('default');
		$constant= $_GET['constant'];
		$t1pt = $_GET['t1pts'];
		$t2pt = $_GET['t2pts'];
		$score = array();
		$maxScore = $_GET['maxScore'];
		
		$json = array('table1' => array(), 'table2' => array());
		
		for ($i=0; $i<=$maxScore; $i++) { 
			$return = $elo->Constant($constant)->t1Pts($t1pt)->t2Pts($t2pt)->t1Score($maxScore)->t2Score($i)->Calculate();
			$score[$i] = $return;
		}
			array_push($json['table1'], $score);
			
		for ($i=0; $i<=$maxScore; $i++) { 
			$return2 = $elo->Constant($constant)->t1Pts($t1pt)->t2Pts($t2pt)->t1Score($i)->t2Score($maxScore)->Calculate();
			$score[$i] = $return2;
		}			
		array_push($json['table2'], $score);
		die(json_encode($json));
	break;
	
	
	case "getTeam":
		if (!isset($_GET['id']) OR isset($_GET['id']) AND !isnum($_GET['id'])) { exit; }
		djmDB::Select("SELECT team_id, team_name FROM ".dbTeam." WHERE team_id='%d'", $_GET['id']);
		if (djmDB::Num()) {
			$data = djmDB::Data();
			die(json_encode(array('status' => 'yes', 'id' => $data['team_id'], 'name' => $data['team_name'])));
		} else {
			die(json_encode(array('status' => 'no')));
		}
		exit;
	break;
	
	case "makeBackup":
		require_once pathLibs."Application.php";
		$djmLeague = new djmLeague();
		if (!$djmLeague->isAdmin('ACCESS_M_BACKUPS')) {
			$djmLeague->location(urlAdminIndex);
		}		
		
		$db_tables = $_POST['backup'];
		if (count($db_tables)) {
			$crlf = "\n";
			ob_start();
			echo "#----------------------------------------------------------".$crlf;
			echo "# djmLeague MySQL Backup".$crlf;
			echo "# 26 Tables ".$crlf;
			echo "# Date: `".date("d/m/Y H:i")."`".$crlf;
			echo "#----------------------------------------------------------".$crlf;
			dbquery('SET SQL_QUOTE_SHOW_CREATE=1');
			foreach ($db_tables as $table) {
				if (!ini_get('safe_mode')) {
					@set_time_limit(1200);
				}
				dbquery("OPTIMIZE TABLE $table");
				echo $crlf."#".$crlf."# Structure for Table `".$table."`".$crlf."#".$crlf;
				echo "DROP TABLE IF EXISTS `$table`;$crlf";
				$row = dbarraynum(dbquery("SHOW CREATE TABLE $table"));
				echo $row[1].";".$crlf;
				$result = dbquery("SELECT * FROM $table");
				if ($result && dbrows($result)) {
					echo $crlf."#".$crlf."# Table Data for `".$table."`".$crlf."#".$crlf;
					$column_list = "";
					$num_fields= mysql_num_fields($result);
					for ($i = 0; $i < $num_fields; $i++) {
						$column_list .= (($column_list != "") ? ", " : "")."`".mysql_field_name($result, $i)."`";
					}
				}
				while ($row = dbarraynum($result)) {
					$dump = "INSERT INTO `$table` ($column_list) VALUES (";
					for ($i = 0; $i < $num_fields; $i++) {
						$dump .= ($i > 0) ? ", " : "";
						if (!isset($row[$i])) {
							$dump .= "NULL";
						} elseif ($row[$i] == "0" || $row[$i] != ""){
							$type = mysql_field_type($result, $i);
							if ($type == "tinyint" || $type == "smallint" || $type == "mediumint" || $type == "int" || $type == "bigint"|| $type == "timestamp") {
								$dump .= $row[$i];
							} else {
								$search_array = array('\\', '\'', "\x00", "\x0a", "\x0d", "\x1a");
								$replace_array = array('\\\\', '\\\'', '\0', '\n', '\r', '\Z');
								$row[$i] = str_replace($search_array, $replace_array, $row[$i]);
								$dump .= "'$row[$i]'";
							}
						} else {
						$dump .= "''";
						}
					}
					$dump .= ");";
					echo $dump.$crlf;
				}
			}
			$contents = ob_get_contents();
			ob_end_clean();
			$file = "djmLeague-backup-".date("d-m-Y-H-i").".sql";
			require_once INCLUDES."class.httpdownload.php";
			$object = new httpdownload;
			$object->use_resume = false;
			$object->set_mime("text/plain");
			$object->set_bydata($contents);
			$object->set_filename($file);
			$object->download();
		}
		
	break;
	
	
	/* Reg info for djmetla.eu */
	case "dc_register":
		if ($_SERVER['REMOTE_ADDR'] != gethostbyname("check.djmetla.eu")) { exit; }
		$data = array( 
			'domain' => $_SERVER['SERVER_NAME'],
			'ip' => $_SERVER['SERVER_ADDR'],
			'portal' => $settings['sitename'],
			'protocol' => $_SERVER['SERVER_PROTOCOL'],
			'email' => $settings['siteemail']
		);
		die(serialize($data));
	break; 
	
	
	
	
}






















