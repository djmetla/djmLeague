<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: infusion.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
 if (!defined("IN_FUSION")) { die('Access Denied'); }

// Basic information
 $inf_title = "djmLeague";
 $inf_description = "Professional electronic sports league system for PHP-Fusion";
 $inf_version = "1.0";
 $inf_developer = "djmetla";
 $inf_email = "djmetla@gmail.com";
 $inf_weburl = "http://www.djmetla.eu/djmLeague/";
 $inf_folder = "djmLeague";
 
 // MySQL Tables
 $layout = DB_PREFIX."league_";
 if (!defined('dbSettings')) {
define("dbSettings", $layout."settings"); 
define("dbPlayer",  $layout."player"); 
define("dbLeague",  $layout."league"); 
define("dbLog",  $layout."log");
define("dbGame",  $layout."game"); 
define("dbAccounts", $layout."accounts"); 
define("dbAccountsPattern", $layout."accounts_pattern");
define("dbTeam", $layout."team"); 
define("dbTeamPlayer", $layout."team_player"); 
define("dbMatch", $layout."match"); 
define("dbMatchMedia", $layout."match_media"); 
define("dbMatchLinks", $layout."match_links"); 
define("dbMatchRequest", $layout."match_request"); 
define("dbAdmin", $layout."admin"); 
define("dbAdminCategory", $layout."admin_category"); 
define("dbAdminLog",  $layout."admin_log"); 
define("dbBan",  $layout."ban"); 
define("dbProtest", $layout."protest"); 
define("dbRequest", $layout."request"); 
define("dbProtestPattern", $layout."protest_pattern"); 
define("dbRequestPattern", $layout."request_pattern"); 
define("dbEvent", $layout."event"); 
define("dbChallenge", $layout."challenge"); 
define("dbSearch", $layout."search"); 
define("dbComplaint", $layout."complaints"); 
define("dbElo", $layout."elo"); 
}

/* MySQL :: Modification Settings */
$inf_newtable[1] = "`".dbSettings."` (
  `settings_link` enum('full','simple') NOT NULL COMMENT 'Tvar odkazov',
  `settings_upload_size` int(10) unsigned NOT NULL COMMENT 'Max. velkost uploadu',
  `settings_profile_link` enum('FUSION','LEAGUE') NOT NULL COMMENT 'odkaz na profil',
  `settings_team_per_page` int(10) unsigned NOT NULL default '50' COMMENT 'pocet timov na stranku',
  `panel_search_limit` int(10) unsigned NOT NULL default '10' COMMENT 'Pocet �iadost� o h�adanie',
  `panel_search_type` enum('OPENTABLE','OPENSIDE') NOT NULL default 'OPENTABLE' COMMENT 'Typ panelu',
  `panel_event_position` enum('LEFT-TOP','RIGHT-TOP','LEFT-BOTTOM','RIGHT-BOTTOM') NOT NULL default 'LEFT-BOTTOM' COMMENT 'Pozicia panelu',
  `panel_event_refresh` int(10) unsigned NOT NULL default '30000' COMMENT 'Cas obnovenia a kontroly'
) ENGINE=MyISAM;";

/* MySQL :: League Player profile */
$inf_newtable[2] = "`".dbPlayer."` (
  `player_id` int(10) unsigned NOT NULL auto_increment,
  `player_user` int(10) unsigned NOT NULL,
  `player_time` int(10) unsigned NOT NULL,
  `player_flag` varchar(150) NOT NULL,
  `player_sound` enum('1.wav','2.wav','3.wav') NOT NULL default '1.wav',
  PRIMARY KEY  (`player_id`)
) ENGINE=MyISAM;";

/* MySQL :: League tables */
$inf_newtable[3] = "`".dbLeague."` (
  `league_id` int(10) unsigned NOT NULL auto_increment,
  `league_name` varchar(50) NOT NULL,
  `league_game` int(10) unsigned NOT NULL COMMENT 'hra',
  `league_session` varchar(25) NOT NULL COMMENT 'Obdobie start stop',
  `league_enable` enum('YES','NO') NOT NULL COMMENT 'Je liga online ?',
  `league_account` enum('YES','NO') NOT NULL COMMENT 'Vyzaduje game ACC?',
  `league_account_key` varchar(20) NOT NULL COMMENT 'Ktory acc vyzaduje',
  `league_default_points` int(10) unsigned NOT NULL,
  `league_team_max` int(10) unsigned NOT NULL,
  `league_team_min` int(10) unsigned NOT NULL,
  `league_information` text NOT NULL,
  `league_rules` text NOT NULL,
  `league_information_time` int(10) unsigned NOT NULL,
  `league_rules_time` int(10) unsigned NOT NULL,
  `league_block_status` int(10) unsigned NOT NULL COMMENT 'Nastavenie blokovaneho statusu v lige',
  `league_match_use_maps` enum('YES','NO') NOT NULL COMMENT 'Povolit vyber mapy v lige',
  `league_match_maps` text NOT NULL COMMENT 'Zoznam map',
  `league_match_max_score` int(10) unsigned NOT NULL COMMENT 'Maximalne skore v zapase ',
  `league_match_elo_type` enum('default','classic','custom') NOT NULL,
  `league_match_elo` int(10) unsigned NOT NULL COMMENT 'ID elo rovnice',
  `league_match_use_server` enum('YES','NO') NOT NULL COMMENT 'Pouzivat server v zapasoch',
  `league_match_server_time` int(10) unsigned NOT NULL COMMENT 'Cas dokedy moze zadat',
  `league_match_host` enum('YES','NO') NOT NULL COMMENT 'Povoli� hos�a ',
  `league_match_host_time` int(10) unsigned NOT NULL COMMENT 'Pocet minut do podania �iadosti o hos�a',
  `league_match_host_num` int(10) unsigned NOT NULL COMMENT 'Max pocet hostov',
  `league_match_delete` enum('YES','NO') NOT NULL COMMENT 'Povoli� zmaza� z�pas',
  `league_match_delete_time` int(10) unsigned default NULL COMMENT 'Cas dokedy sa da zapas zmazat',
  `league_match_request_delay` int(10) unsigned NOT NULL COMMENT 'Start podavania ',
  `league_match_request_delay_stop` int(10) unsigned NOT NULL COMMENT 'Stop podavania',
  `league_match_protest_delay` int(10) unsigned NOT NULL COMMENT 'Start podavania',
  `league_match_protest_delay_stop` int(10) unsigned NOT NULL COMMENT 'Stop podavania',
  PRIMARY KEY  (`league_id`)
) ENGINE=MyISAM;";

/* MySQL :: Leagues LOG */
$inf_newtable[4] = "`".dbLog."` (
  `log_id` int(11) NOT NULL auto_increment,
  `log_type` enum('player','match','team') NOT NULL,
  `log_user` int(10) unsigned default NULL,
  `log_parent` int(10) unsigned NOT NULL,
  `log_value` text NOT NULL,
  `log_time` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`log_id`)
) ENGINE=MyISAM;";

/* MySQL :: League games */
$inf_newtable[5] = "`".dbGame."` (
  `game_id` int(10) unsigned NOT NULL auto_increment,
  `game_name` varchar(40) NOT NULL,
  `game_icon` varchar(70) NOT NULL,
  PRIMARY KEY  (`game_id`)
) ENGINE=MyISAM;";

/* MySQL :: Player game accounts */
$inf_newtable[6] = "`".dbAccounts."` (
  `account_id` int(10) unsigned NOT NULL auto_increment,
  `account_user` int(10) unsigned NOT NULL,
  `account_key` varchar(25) NOT NULL COMMENT 'Pattern',
  `account_value` varchar(50) NOT NULL COMMENT 'hodnota uctu',
  `account_new_value` varchar(50) NOT NULL COMMENT 'nova hodnota uctu',
  `account_last_change` int(10) unsigned NOT NULL COMMENT 'Kedy naposledy menil',
  `account_post_change` int(11) NOT NULL COMMENT 'kedy odoslal ziadost',
  PRIMARY KEY  (`account_id`)
) ENGINE=MyISAM;";

/* MySQL :: Player game accounts type */
$inf_newtable[7] = "`".dbAccountsPattern."` (
  `pattern_id` int(10) unsigned NOT NULL auto_increment,
  `pattern_name` varchar(40) NOT NULL,
  `pattern_limit` int(10) unsigned NOT NULL,
  `pattern_key` varchar(20) NOT NULL,
  `pattern_information` text NOT NULL,
  PRIMARY KEY  (`pattern_id`)
) ENGINE=MyISAM;";

/* MySQL :: Leagues team */
$inf_newtable[8] = "`".dbTeam."` (
  `team_id` int(10) unsigned NOT NULL auto_increment,
  `team_league` int(10) unsigned NOT NULL,
  `team_points` int(10) unsigned NOT NULL,
  `team_name` varchar(40) NOT NULL,
  `team_tag` varchar(5) NOT NULL,
  `team_password` varchar(8) NOT NULL,
  `team_avatar` varchar(20) NOT NULL,
  `team_flag` varchar(150) NOT NULL,
  `team_founder` int(10) unsigned NOT NULL,
  `team_created` int(10) unsigned NOT NULL,
  `team_web` varchar(50) NOT NULL,
  `team_status` enum('ACTIVE','INACTIVE') NOT NULL,
  PRIMARY KEY  (`team_id`)
) ENGINE=MyISAM;";

/* MySQL :: League teams players */
$inf_newtable[9] = "`".dbTeamPlayer."` (
  `player_id` int(10) unsigned NOT NULL auto_increment,
  `player_user` int(10) unsigned NOT NULL,
  `player_team` int(10) unsigned NOT NULL,
  `player_team_league` int(10) unsigned NOT NULL,
  `player_time` int(10) unsigned NOT NULL,
  `player_position` enum('CR','CL','PL') NOT NULL,
  PRIMARY KEY  (`player_id`)
) ENGINE=MyISAM;";

/* MySQL :: Leagues matches */
$inf_newtable[10] = "`".dbMatch."` (
  `match_id` int(10) unsigned NOT NULL auto_increment,
  `match_league` int(10) unsigned NOT NULL COMMENT 'Liga',
  `match_time` int(10) unsigned NOT NULL COMMENT 'Cas zapasu',
  `match_t1` int(10) unsigned NOT NULL COMMENT 'Team 1',
  `match_t2` int(10) unsigned NOT NULL COMMENT 'Team 2',
  `match_t1_points` int(10) unsigned NOT NULL COMMENT 'Team 1 vstupne body',
  `match_t2_points` int(10) unsigned NOT NULL COMMENT 'Team 2 vstupne body',
  `match_t1_score` int(10) unsigned NOT NULL COMMENT 'Score',
  `match_t2_score` int(10) unsigned NOT NULL COMMENT 'Score',
  `match_t1_after_pts` int(10) unsigned NOT NULL COMMENT 'Pridane body po',
  `match_t2_after_pts` int(10) unsigned NOT NULL COMMENT 'Pridane body po',
  `match_map` varchar(100) default NULL,
  `match_winner` int(10) unsigned default NULL,
  `match_losser` int(10) unsigned default NULL,
  `match_draw` enum('NO','YES') default NULL,
  `match_server` text,
  `match_status` enum('0','1','2','3') NOT NULL COMMENT '0 - vytvoreny, 1 - uz su vysledky a screens 3 - uzavrety 4 - zruseny',
  PRIMARY KEY  (`match_id`)
) ENGINE=MyISAM;";

/* MySQL :: Match Media for match in league */
$inf_newtable[11] = "`".dbMatchMedia."` (
  `media_id` int(10) unsigned NOT NULL auto_increment,
  `media_match` int(10) unsigned NOT NULL,
  `media_user` int(10) unsigned NOT NULL,
  `media_time` int(10) unsigned NOT NULL,
  `media_item` varchar(100) NOT NULL,
  `media_type` varchar(10) NOT NULL,
  PRIMARY KEY  (`media_id`)
) ENGINE=MyISAM;";

/* MySQL :: Match links for match in league */
$inf_newtable[12] = "`".dbMatchLinks."` (
  `link_id` int(10) unsigned NOT NULL auto_increment,
  `link_match` int(10) unsigned NOT NULL,
  `link_user` int(10) unsigned NOT NULL,
  `link_time` int(10) unsigned NOT NULL,
  `link_target` text NOT NULL,
  `link_type` enum('demo','screenshot','other') NOT NULL,
  PRIMARY KEY  (`link_id`)
) ENGINE=MyISAM;";

/* MySQL :: Match requests for match in league */
$inf_newtable[13] = "`".dbMatchRequest."` (
  `request_id` int(10) unsigned NOT NULL auto_increment,
  `request_match` int(10) unsigned NOT NULL,
  `request_from` int(10) unsigned NOT NULL,
  `request_from_team` int(10) unsigned NOT NULL,
  `request_type` enum('guest','delete') NOT NULL,
  `request_type_guest` int(10) unsigned default NULL,
  `request_time` int(10) unsigned NOT NULL,
  `request_accept` int(10) unsigned NOT NULL,
  `request_accept_time` int(10) unsigned NOT NULL,
  `request_status` enum('WAIT','ACCEPT','REJECT') NOT NULL,
  PRIMARY KEY  (`request_id`)
) ENGINE=MyISAM;";

/* MySQL :: Administrators database */
$inf_newtable[14] = "`".dbAdmin."` (
  `admin_id` int(10) unsigned NOT NULL auto_increment,
  `admin_user` int(10) unsigned NOT NULL,
  `admin_category` int(10) unsigned NOT NULL,
  `admin_flag` text NOT NULL,
  `admin_league` text NOT NULL,
  `admin_account` text NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_surname` varchar(100) NOT NULL,
  `admin_description` varchar(250) NOT NULL,
  PRIMARY KEY  (`admin_id`)
) ENGINE=MyISAM;";

/* MySQL :: Administrators category */
$inf_newtable[15] = "`".dbAdminCategory."` (
  `category_id` int(10) NOT NULL auto_increment,
  `category_name` varchar(100) NOT NULL,
  PRIMARY KEY  (`category_id`)
) ENGINE=MyISAM;";

/* MySQL :: Administrators log in administration */
$inf_newtable[16] = "`".dbAdminLog."` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `log_page` varchar(50) NOT NULL,
  `log_user` int(10) unsigned NOT NULL,
  `log_time` int(10) unsigned NOT NULL,
  `log_value` text NOT NULL,
  PRIMARY KEY  (`log_id`)
) ENGINE=MyISAM;";

/* MySQL :: Banlist for modification */
$inf_newtable[17] = "`".dbBan."` (
  `ban_id` int(10) unsigned NOT NULL auto_increment,
  `ban_user` int(10) unsigned NOT NULL,
  `ban_time` int(10) unsigned NOT NULL,
  `ban_expiration` int(10) unsigned NOT NULL,
  `ban_permanent` enum('NO','YES') NOT NULL,
  `ban_admin` int(10) unsigned NOT NULL,
  `ban_reason` text NOT NULL,
  PRIMARY KEY  (`ban_id`)
) ENGINE=MyISAM;";

/* MySQL :: Protests for match in leagues */
$inf_newtable[18] = "`".dbProtest."` (
  `protest_id` int(10) unsigned NOT NULL auto_increment,
  `protest_pattern` int(10) unsigned NOT NULL,
  `protest_match` int(10) unsigned NOT NULL,
  `protest_time` int(10) unsigned NOT NULL,
  `protest_from` int(10) unsigned NOT NULL,
  `protest_from_team` int(10) unsigned NOT NULL,
  `protest_admin` int(10) unsigned default NULL,
  `protest_description` text NOT NULL,
  `protest_admin_description` text NOT NULL,
  `protest_status` enum('0','1','2') NOT NULL,
  `protest_time_done` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`protest_id`)
) ENGINE=MyISAM;";

/* MySQL :: Requests for match in leagues */
$inf_newtable[19] = "`".dbRequest."` (
  `request_id` int(10) unsigned NOT NULL auto_increment,
  `request_match` int(10) unsigned NOT NULL,
  `request_pattern` int(10) unsigned NOT NULL,
  `request_author` int(10) unsigned NOT NULL,
  `request_author_team` int(10) unsigned NOT NULL,
  `request_to` int(10) unsigned default NULL,
  `request_time` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`request_id`)
) ENGINE=MyISAM;";

/* MySQL :: Requests pattern for match in leagues */
$inf_newtable[20] = "`".dbRequestPattern."` (
  `pattern_id` int(10) unsigned NOT NULL auto_increment,
  `pattern_name` varchar(100) NOT NULL,
  `pattern_preddefined` text NOT NULL,
  `pattern_league` text NOT NULL,
  PRIMARY KEY  (`pattern_id`)	
) ENGINE=MyISAM;";

/* MySQL :: Protests pattern for match in leagues */
$inf_newtable[21] = "`".dbProtestPattern."` (
  `pattern_id` int(10) unsigned NOT NULL auto_increment,
  `pattern_name` varchar(100) NOT NULL,
  `pattern_preddefined` text NOT NULL,
  `pattern_use_custom_description` enum('0','1') NOT NULL,
  `pattern_league` text NOT NULL,
  PRIMARY KEY  (`pattern_id`)
) ENGINE=MyISAM;";

/* MySQL :: Events in modification */
$inf_newtable[22] = "`".dbEvent."` (
  `event_id` int(10) unsigned NOT NULL auto_increment,
  `event_user` int(10) unsigned NOT NULL,
  `event_time` int(10) unsigned NOT NULL,
  `event_subject` varchar(100) NOT NULL,
  `event_text` text NOT NULL,
  `event_read` enum('0','1') NOT NULL default '0',
  `event_ajax` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`event_id`)
) ENGINE=MyISAM;";

/* MySQL :: Challenges in league */
$inf_newtable[23] = "`".dbChallenge."` (
  `challenge_id` int(10) unsigned NOT NULL auto_increment,
  `challenge_league` int(10) unsigned NOT NULL,
  `challenge_from` int(10) unsigned NOT NULL,
  `challenge_to` int(10) unsigned NOT NULL,
  `challenge_time` int(10) unsigned NOT NULL,
  `challenge_map` text,
  `challenge_time_match` int(10) unsigned NOT NULL,
  `challenge_server` enum('YES','NO') default NULL,
  PRIMARY KEY  (`challenge_id`)
) ENGINE=MyISAM;";

/* MySQL :: Search opponent in league */
$inf_newtable[24] = "`".dbSearch."` (
  `search_id` int(10) unsigned NOT NULL auto_increment,
  `search_team` int(10) unsigned NOT NULL,
  `search_league` int(10) unsigned NOT NULL,
  `search_time` int(10) unsigned NOT NULL,
  `search_server` enum('YES','NO') default NULL,
  `search_map` text,
  `search_limit_positive` int(10) unsigned default NULL,
  `search_limit_negative` int(10) unsigned default NULL,
  PRIMARY KEY  (`search_id`)
) ENGINE=MyISAM;";

/* MySQL :: Complaints for team in leagues */
$inf_newtable[25] = "`".dbComplaint."` (
  `complaint_id` int(10) unsigned NOT NULL auto_increment,
  `complaint_time` int(10) unsigned NOT NULL,
  `complaint_team` int(10) unsigned NOT NULL,
  `complaint_type` enum('+','-') NOT NULL,
  `complaint_value` int(10) unsigned NOT NULL,
  `complaint_admin` int(10) unsigned NOT NULL,
  `complaint_reason` varchar(100) NOT NULL,
  PRIMARY KEY  (`complaint_id`)
) ENGINE=MyISAM;";

/* MySQL :: ELO System for modification */
$inf_newtable[26] = "`".dbElo."` (
  `elo_id` int(10) NOT NULL auto_increment,
  `elo_name` varchar(100) NOT NULL,
  `elo_value` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`elo_id`)
) ENGINE=MyISAM;";

/* Insert Lines */
$inf_insertdbrow[1] = dbSettings." (settings_link, settings_upload_size, settings_profile_link, settings_team_per_page, panel_search_limit, panel_search_type, panel_event_position,panel_event_refresh) 
													VALUES 
													('full','2', 'FUSION', '50', '10', 'OPENTABLE', 'LEFT-BOTTOM','30000') ";


/* Delete rules for MySQL */
$inf_droptable[1] = dbSettings; 					$inf_droptable[2] = dbPlayer; 				$inf_droptable[3] = dbLeague;
$inf_droptable[4] = dbLog; 						$inf_droptable[5] = dbGame; 				$inf_droptable[6] = dbAccounts;
$inf_droptable[7] = dbAccountsPattern;	 	$inf_droptable[8] = dbTeam; 				$inf_droptable[9] = dbTeamPlayer;
$inf_droptable[10] = dbMatch; 					$inf_droptable[11] = dbMatchMedia; 		$inf_droptable[12] = dbMatchLinks;
$inf_droptable[13] = dbMatchRequest; 		$inf_droptable[14] = dbAdmin; 				$inf_droptable[15] = dbAdminCategory;
$inf_droptable[16] = dbAdminLog; 			$inf_droptable[17] = dbBan; 					$inf_droptable[18] = dbProtest;
$inf_droptable[19] = dbRequest; 				$inf_droptable[20] = dbProtestPattern; 	$inf_droptable[21] = dbRequestPattern;
$inf_droptable[22] = dbEvent; 					$inf_droptable[23] = dbChallenge; 			$inf_droptable[24] = dbSearch;
$inf_droptable[25] = dbComplaint; 			$inf_droptable[26] = dbElo;

$inf_adminpanel[1] = array("title" => "djmLeague", "image" => "images.gif", "panel" => "admin.php", "rights" => "LEA");