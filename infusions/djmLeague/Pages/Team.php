<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Team.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (!isset($_GET['team']) || !isnum($_GET['team'])) { $this->location(urlDefault); }
$page['location'] = urlTeam.$_GET['team'];

//User Status
function func_UserStatus($time, $Limit) {
	$c = time(); $u = $time; $l = $Limit;
	$s = $u+$l;
	$dni_s =  $s - time();
	$dni =  round($dni_s /60 /60 /24); 
	if ($s > time()) {
		$return = array('status' => false, 'day' => $dni);
	} else { $return = array('status' => true, 'day' => ''); } 
	return $return;
}


switch($_GET['option']) {

	/* Index */
	default:
		djmDB::Select("SELECT * 
								FROM ".dbTeam." as t1 
								LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league 
								LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game 
								LEFT JOIN ".DB_USERS." as t4 ON t4.user_id=t1.team_founder
								WHERE team_id='%d'", $_GET['team']);
		if (!djmDB::Num()) { $this->setError($this->__("PT_1", $_GET['team']), urlDefault); }
		$team = djmDB::Data();
		$page['title'] = "#".$team['team_id']." ".$team['team_name'];
		$isMember = $this->isTeamPlayer($team['team_id']);
	break;

	/* Team Settings */
	case "settings":
		djmDB::Select("SELECT * 
								FROM ".dbTeam." as t1 
								LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league 
								LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game 
								LEFT JOIN ".DB_USERS." as t4 ON t4.user_id=t1.team_founder
								WHERE team_id='%d'", $_GET['team']);
		if (!djmDB::Num()) { $this->setError($this->__("PT_1", $_GET['team']), urlDefault); }
		$team = djmDB::Data();
		$page['title'] = "#".$team['team_id']." ".$team['team_name']." - ".$this->__("PT_2");
		$isMember = $this->isTeamPlayer($team['team_id']);	
	    if (!$isMember) { $this->setError($this->__("PT_3"), $page['location']); }
		if ($isMember == "PL") { $this->setError($this->__("PT_4"), $page['location']); }
	break;

	/* Leave */
	case "leave":
		djmDB::Select("SELECT * 
								FROM ".dbTeam." as t1 
								LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league 
								LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game 
								LEFT JOIN ".DB_USERS." as t4 ON t4.user_id=t1.team_founder
								WHERE team_id='%d'", $_GET['team']);
		if (!djmDB::Num()) { $this->setError($this->__("PT_1", $_GET['team']), urlDefault); }
		$team = djmDB::Data();
		$page['title'] = "#".$team['team_id']." ".$team['team_name']." - ".$this->__("PT_5");
		$isMember = $this->isTeamPlayer($team['team_id']);	
	    if (!$isMember) { $this->setError($this->__("PT_3"), $page['location']); }	
	break;
	
	
	/* Activate */
	case "activate":
		djmDB::Select("SELECT * 
								FROM ".dbTeam." as t1 
								LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league 
								LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game 
								LEFT JOIN ".DB_USERS." as t4 ON t4.user_id=t1.team_founder
								WHERE team_id='%d'", $_GET['team']);
		if (!djmDB::Num()) { $this->setError($this->__("PT_1", $_GET['team']), urlDefault); }
		$team = djmDB::Data();
		$page['title'] = "#".$team['team_id']." ".$team['team_name']." - ".$this->__("PT_6");
		$isMember = $this->isTeamPlayer($team['team_id']);	
	    if (!$isMember) { $this->setError($this->__("PT_3"), $page['location']); }
		if ($isMember != "CR") { $this->setError($this->__("PT_4"), $page['location']); }		
	break;
	
	/* Squad settings */
	case "squad":
		global $userdata;
		djmDB::Select("SELECT * 
								FROM ".dbTeam." as t1 
								LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league 
								LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game 
								LEFT JOIN ".DB_USERS." as t4 ON t4.user_id=t1.team_founder
								WHERE team_id='%d'", $_GET['team']);
		if (!djmDB::Num()) { $this->setError($this->__("PT_1", $_GET['team']), urlDefault); }
		$team = djmDB::Data();
		$page['title'] = "#".$team['team_id']." ".$team['team_name']." - ".$this->__("PT_7");
		$isMember = $this->isTeamPlayer($team['team_id']);	
	    if (!$isMember) { $this->setError($this->__("PT_3"), $page['location']); }
		if ($isMember != "CR") { $this->setError($this->__("PT_8"), $page['location']); }
		$this->Help("team_squad.html");
		
			if (iMEMBER && $isMemeber == "CR" && isset($_GET['kick']) && isnum($_GET['kick'])) {
				djmDB::Select("SELECT * FROM ".dbTeamPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user WHERE player_id='%d' AND player_team='%d'", array($_GET['kick'], $team['team_id']));
				if (djmDB::Num()) {
					$data = djmDB::Data();
					if ($userdata['user_id'] == $data['player_user']) { $this->setError($this->__("PT_9"), urlTeam.$_GET['team']."&amp;option=".$_GET['option']); }
					djmDB::Delete(dbTeamPlayer, array('player_id' => $data['player_id']));
					$this->setLog($this->__("PT_10")." ".$data['user_name'], $team['team_id']);
					$this->setDone($this->__("PT_11", $data['user_name']), urlTeam.$_GET['team']."&amp;option=".$_GET['option']);
				} else {
					$this->setError($this->__("PT_12", $_GET['kick']), urlTeam.$_GET['team']."&amp;option=".$_GET['option']);
				}
			}
	break;
}