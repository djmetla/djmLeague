<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Tool.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (!isset($_GET['option'])) { $this->location(urlDefault); }
$page['location'] = urlTool.$_GET['option'];
$page['list'] = false;

switch($_GET['option']) {

	/*
	* Page : Admins
	*/
	case "admins":
		$page['title'] = $this->__("PTO_1");
	break;


	/*
	* Page : Opponent
	*/
	case "opponent":
				if (!iMEMBER) { $this->location(urlDefault); }
				global $userdata;
				if (!isset($_GET['task'])) { $this->location(urlDefault); }
				$task = explode("[", $_GET['task']);
				
				switch($task[0]) {
				
					/* Task create */
					case "create":
						$page['list'] = "create";
						$page['title'] = $this->__("PTO_2");
						
						if (isset($_GET['myteam']) && isnum($_GET['myteam'])) {
							djmDB::Select("SELECT * 
													FROM ".dbTeam." as t1
													LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.team_league
													LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game
													WHERE team_id='%d'
													", $_GET['myteam']);
							if (!djmDB::Num()) { $this->setError($this->__("PTO_3"), $page['location']."&amp;task=create");	}				
							$team = djmDB::Data();
							$mteam = $this->isTeamPlayer($team['team_id']);
							
							if (!$mteam) {
								$this->setError($this->__("PTO_4"), $page['location']."&amp;task=create");
							}
							
							if ($mteam == "PL") {
								$this->setError($this->__("PTO_5"), $page['location']."&amp;task=create");
							}
							
							if ($team['league_enable'] == "NO") {
								$this->setError($this->__("PTO_6"), $page['location']."&amp;task=create");
							}
							
							if ($team['team_status'] == "INACTIVE") {
								$this->setError($this->__("PTO_7"), $page['location']."&amp;task=create");
							}
							
							
							
						} else {
							
							djmDB::Select("SELECT * 
													FROM ".dbTeamPlayer." as t1
													LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team
													LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.player_team_league
													LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
													WHERE player_user='%d' AND player_position!='PL' AND t2.team_status='ACTIVE'
													", $userdata['user_id']);
							$count = djmDB::Num();
							$data = djmDB::fullData();
						}						
					break;
				
					/* Task ALL List */
					case "all":
						$page['list'] = "all";
						$page['title'] = $this->__("PTO_8");
					break;
					
					/* Task Accept match */
					case "accept":
						if ($task[1] == "") { $this->location(urlDefault); }
						$id = str_replace("]", "", $task[1]);
						$page['list'] = "accept";
						
						djmDB::Select("SELECT *
												FROM ".dbSearch." as t1
												LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.search_team
												LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.search_league
												LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
												WHERE search_id='%d'", $id);
						if(!djmDB::Num()) { $this->setError($this->__("PTO_9"), $page['location']."&amp;task=all"); }
						$data = djmDB::Data();
						
						if ($data['league_enable'] == "NO") {
							$this->setError($this->__("PTO_10"), $page['location']."&amp;task=all");
						}
						
						if ($data['search_time'] < time()) {
							$this->setError($this->__("PTO_11"),  $page['location']."&amp;task=all");
						}
	
						$page['title'] = $this->__("PTO_12")." ".$data['team_name'];
						
						djmDB::Select("SELECT t1.*, t2.*, t3.league_id 
												FROM ".dbTeamPlayer." as t1 
												LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team
												LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.player_team_league
												WHERE player_user='%d' AND player_position != 'PL' AND player_team_league='%d'
						", array($userdata['user_id'], $data['league_id']));
						$myteam = djmDB::Data();
						
						if (!djmDB::Num()) { 
							$this->setError($this->__("PTO_13"), $page['location']."&amp;task=all");
						}
						
						$iAmPlayer = $this->isTeamPlayer($data['team_id']);
						if ($iAmPlayer) { 
							$this->setError($this->__("PTO_14"), $page['location']."&amp;task=all");
						}
					break;
				
					/* Task Reject match */
					case "reject":
						if ($task[1] == "") { $this->location(urlDefault); }
						$id = str_replace("]", "", $task[1]);
						djmDB::Select("SELECT * FROM ".dbSearch." WHERE search_id='%d'", $id);
						if (!djmDB::Num()) { $this->location($page['location']."&amp;task=all"); }
						$data = djmDB::Data();
						if (!$this->isTeamPlayer($data['search_team'])) { $this->setError("Nem��ete zru�i� cudziu v�zvu", $page['location']."&amp;task=all"); }
						djmDB::Delete(dbSearch, array('search_id' => $data['search_id']));
						$this->setLog($this->__("PTO_15"), $data['search_team'], "team");
						$this->setDone($this->__("PTO_16"), $page['location']."&amp;task=all");
					break;
				
				}
	break;

	/*
	* Page : Join team
	*/
	case "join":
		$page['title'] = $this->__("PTO_17");
		if (!iMEMBER) { $this->location(BASEDIR."login.php"); }
	break;

	/*
	* Page : Create team 
	*/
	case "create":
		$page['title'] = $this->__("PTO_18");
		if (!iMEMBER) { $this->location(BASEDIR."login.php"); }
		
		// Select leagues
		djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game");
		$league_count = djmDB::Num();
		$leagues = djmDB::fullData();
		
		$_ENV['T'] = array(
			$this->__("PTO_19"),
			$this->__("PTO_20"),
			$this->__("PTO_21"),
			$this->__("PTO_22"),
			$this->__("PTO_23")
		);
		
		// League status
		function func_leagueStatus($league) {
			global $userdata;
			$status = ""; $status_text = ""; $enable = TRUE;
			djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $league);
			if (djmDB::Num()) {
				$data = djmDB::Data();
					if ($data['league_enable'] == "YES") {
						if ($data['league_account'] == "YES") { 
							if (dbcount("(*)", dbAccounts, "account_key='".$data['league_account_key']."' AND account_user='".$userdata['user_id']."' AND account_value!=''")) {
								if (!dbcount("(*)", dbTeamPlayer, "player_user='".$userdata['user_id']."' AND player_team_league='".$league."'")) {
									$status = "<span class='green'>".$_ENV['T'][0]."</span>"; $status_text = ""; $enable = TRUE;
								} else {
									$status = "<span class='red'>".$_ENV['T'][1]."</span>"; $status_text = $_ENV['T'][2]; $enable = FALSE;
								}
							} else {
								$status = "<span class='red'>".$_ENV['T'][1]."</span>"; $status_text = $_ENV['T'][3]; $enable = FALSE;
							}
						} else {
							if (!dbcount("(*)", dbTeamPlayer, "player_user='".$userdata['user_id']."' AND player_team_league='".$league."'")) {
								$status = "<span class='green'>".$_ENV['T'][0]."</span>"; $status_text = ""; $enable = TRUE;
							} else {
								$status = "<span class='red'>".$_ENV['T'][1]."</span>"; $status_text = $_ENV['T'][2]; $enable = FALSE;
							}
						}
					} else {
						$status = "<span class='red'>".$_ENV['T'][1]."</span>"; $status_text = $_ENV['T'][4]; $enable = FALSE;
					}
					return array('status' => $status, 'text' => $status_text, 'enable' => $enable);		
			} else {
				return false;
			}
		}
	break;


	/*
	* Page : Challenge
	*/
	case "challenge":
		global $userdata;
		if (!iMEMBER) { $this->location(urlDefault); }

		if (isset($_GET['opponent_team']) && isnum($_GET['opponent_team'])) {
		
				djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $_GET['opponent_team']);
				if (!djmDB::Num()) { 
					$this->setError($this->__("PTO_24", $_GET['opponent_team']), urlDefault); 
				}
				$oponent = djmDB::Data();
			
				djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_id='%d'", $oponent['team_league']);
				$league = djmDB::Data();
				
				if ($league['league_enable'] == "NO") {
					$this->setError($this->__("PTO_25", $league['league_name']), urlTeam.$oponent['team_id']);
				}
				
				djmDB::Select("SELECT * FROM ".dbTeamPlayer." as t1 LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team WHERE player_user='%d' AND player_team_league='%d'", array($userdata['user_id'], $league['league_id']));
				if (!djmDB::Num()) { $this->setError($this->__("PTO_26")." ".$league['league_name'], urlTeam.$oponent['team_id']); } 
				$me = djmDB::Data();
				
				if ($oponent['team_status'] == "INACTIVE") {
					$this->setError($this->__("PTO_27", $oponent['team_name']), urlTeam.$oponent['team_id']);
				}
				
				if ($me['team_status'] == "INACTIVE") {
					$this->setError($this->__("PTO_28", $me['team_name']), urlTeam.$oponent['team_id']);
				} 
				
				if ($me['team_id'] == $oponent['team_id']) {
					$this->setError($this->__("PTO_29"), urlTeam.$me['team_id']);
				}

				if ($me['player_position'] == "PL") {
					$this->setError($this->__("PTO_30", $oponent['team_name'])." ".$me['team_name'], urlTeam.$oponent['team_id']);
				}
			
				$page['title'] = $this->__("PTO_31")." ".$oponent['team_name'];
		
		/* Challenge view */
		} elseif (isset($_GET['view']) && isnum($_GET['view'])) {
			
			djmDB::Select("SELECT * FROM ".dbChallenge." as t1 LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.challenge_league WHERE challenge_id='%d'", $_GET['view']);
			if (!djmDB::Num()) { 
				$this->setError($this->__("PTO_32", $_GET['view']), urlDefault);
			}
			$challenge = djmDB::Data();
			
			djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $challenge['challenge_from']);
			$t1 = djmDB::Data();
			
			djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $challenge['challenge_to']);
			$t2 = djmDB::Data();
			
			$page['title'] = $this->__("PTO_33")." ".$t1['team_name']." vs. ".$t2['team_name'];
			
			$pos_1 = $this->isTeamPlayer($t1['team_id']);
			$pos_2 = $this->isTeamPlayer($t2['team_id']);
			
			if (!$pos_1 && !$pos_2) { $this->location(urlDefault); }
			
			// Reject challenge
			if (isset($_GET['reject'])) {
				if ($pos_1 && $pos_1 != "PL") {
					djmDB::Delete(dbChallenge, array('challenge_id' => $challenge['challenge_id']));
					djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $t2['team_id']);
					foreach(djmDB::fullData() as $player) { 
						$this->setEvent($this->__("PTO_33"), $this->__("PTO_33")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("PTO_34"), $player['player_user']);
					}					
					$this->setDone($this->__("PTO_35"), urlTeam.$t1['team_id']);
				} elseif ($pos_2 && $pos_2 != "PL") {
					djmDB::Delete(dbChallenge, array('challenge_id' => $challenge['challenge_id']));
					djmDB::Select("SELECT * FROM ".dbTeamPlayer." WHERE player_team='%d' AND (player_position='CR' OR player_position='CL')", $t1['team_id']);
					foreach(djmDB::fullData() as $player) { 
						$this->setEvent($this->__("PTO_33"), $this->__("PTO_33")." ".$t1['team_name']." vs. ".$t2['team_name']." ".$this->__("PTO_36"), $player['player_user']);
					}					
					$this->setDone($this->__("PTO_37"), urlTeam.$t2['team_id']);				
				} 
			}
		} else {
			$this->location(urlDefault);
		}
	break;
}