<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Match.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (empty($_GET['match']) || !isnum($_GET['match'])) { $this->location(urlDefault); }
$page['location'] = urlMatch.$_GET['match'];

djmDB::Select("SELECT * 
						FROM ".dbMatch." as t1 
						LEFT JOIN ".dbLeague." as t2 ON t2.league_id=t1.match_league 
						LEFT JOIN ".dbGame." as t3 ON t3.game_id=t2.league_game
						WHERE match_id='%d'", $_GET['match']);
if (!djmDB::Num()) { $this->setError($this->__("PM_1", $_GET['match']), urlDefault); }
$match = djmDB::Data();

djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t1']);
$t1 = djmDB::Data();

djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", $match['match_t2']);
$t2 = djmDB::Data();	

if ($this->isTeamPlayer($t1['team_id'])) {
	$myTeam = $t1['team_id'];
	$position = $this->isTeamPlayer($t1['team_id']);
} elseif ($this->isTeamPlayer($t2['team_id'])) {
	$myTeam = $t2['team_id'];
	$position = $this->isTeamPlayer($t2['team_id']);
} else {
	$myTeam = FALSE;
	$position = FALSE;
}

$_ENV['mS'] = array($this->__("PM_2"), $this->__("PM_3"), $this->__("PM_4"), $this->__("PM_5"));
function func_MatchStatus($status) { 
	if ($status == 0) { 
		$return = array('status' => $status, 'text' => '<span class="blue">'.$_ENV['mS'][0].'</span>');
	} elseif ($status == 1) {
		$return = array('status' => $status, 'text' => '<span class="orange">'.$_ENV['mS'][1].'</span>');
	} elseif ($status == 2) {
		$return = array('status' => $status, 'text' => '<span class="green">'.$_ENV['mS'][2].'</span>');
	} elseif ($status == 3) {
		$return = array('status' => $status, 'text' => '<span class="red">'.$_ENV['mS'][3].'</span>');
	}
	return $return;
}


switch ($_GET['option']) {
	/* Option :: Close match */
	case "close":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_7");
		if ($match['match_status'] != 1) { $this->setError($this->__("PM_8"), urlMatch.$_GET['match']); }
	break;

	/* Option :: Server */
	case "server":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_9");
		if (($match['match_time']+$match['league_match_server_time']) < time()) {
			$this->setError($this->__("PM_10"), urlMatch.$_GET['match']);
		}
		if (!iMEMBER OR !$position OR $position == "PL") { $this->setError($this->__("PM_11"), urlMatch.$_GET['match']); }	
	break;

	/* Option :: protest */
	case "protest":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_12");
		if (($match['match_time'] + $match['league_match_protest_delay']) > time() || ($match['match_time'] + $match['league_match_protest_delay_stop']) < time()) {
			$this->setError($this->__("PM_13"));
		} 			
		if (!iMEMBER OR !$position OR $position == "PL") { $this->setError($this->__("PM_14"), urlMatch.$_GET['match']); }	
	break;

	/* Option :: request */
	case "request":
		if (($match['match_time'] + $match['league_match_request_delay']) > time() || ($match['match_time'] + $match['league_match_request_delay_stop']) < time()) {
			$this->setError($this->__("PM_15"), urlMatch.$_GET['match']);
		} 
		
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_16");
		if (!iMEMBER OR !$position OR $position == "PL") { $this->setError($this->__("PM_17"), urlMatch.$_GET['match']); }	
	break;

	/* Option :: request guest */
	case "request_guest":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_18");
		if (!iMEMBER OR !$position OR $position == "PL") { $this->setError($this->__("PM_19"), urlMatch.$_GET['match']); }
		
		if ($match['league_match_host'] == "NO") {
			$this->setError($this->__("PM_20"), urlMatch.$_GET['match']);
		}		
		
		if (($match['match_time'] + $match['league_match_host_time']) < time()) {
			$this->setError($this->__("PM_21"), urlMatch.$_GET['match']);
		}		
	break;

	/* Option :: request delete */
	case "request_delete":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_22");
		if (!iMEMBER OR !$position OR $position == "PL") { $this->setError($this->__("PM_23"), urlMatch.$_GET['match']); }
		
		if ($match['league_match_delete'] == "NO") {
			$this->setError($this->__("PM_24"), urlMatch.$_GET['match']);
		}
		
		if (($match['match_time'] + $match['league_match_delete_time']) < time()) {
			$this->setError($this->__("PM_25"), urlMatch.$_GET['match']);
		}
		
	break;

	/* Option :: add result */
	case "result":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_26");
		if (!iMEMBER OR !$position) { $this->setError($this->__("PM_27"), urlMatch.$_GET['match']); }
		if ($match['match_status'] == 2 OR $match['match_status'] == 3) { $this->setError($this->__("PM_28"), urlMatch.$_GET['match']); }
	break;

	/* Option :: add links */
	case "links":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_29");
		if (!iMEMBER OR !$position) { $this->setError($this->__("PM_30"), urlMatch.$_GET['match']); }
	break;

	/* Option :: upload images */
	case "media":
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name']." - ".$this->__("PM_31");
		if (!iMEMBER OR !$position) { $this->setError($this->__("PM_32"), urlMatch.$_GET['match']); }
	break;


	/* Index */ 
	default:
		global $userdata;
		if (dbcount("(*)", dbMatchRequest, "request_match='".$match['match_id']."' AND request_type='delete' AND request_status='WAIT' ")) {
			$status = array('status' => $match['match_status'], 'text' => '<span class="red bold">'.$this->__("PM_33").'</span>');
		} else {
			$status =  func_MatchStatus($match['match_status']);
		}
		$page['comment'] = TRUE;
		$page['title'] = "#".$match['match_id']." ".$this->__("PM_6")." ".$t1['team_name']." vs. ".$t2['team_name'];
		
		// Host match request
		if (iMEMBER && isset($_GET['request_guest']) && !empty($_GET['request_guest'])) {
			$explode = explode("-", $_GET['request_guest']);
			$id = $explode[0];
			$task = $explode[1];
			
			djmDB::Select("SELECT * FROM ".dbMatchRequest." WHERE request_id='%d' AND request_match='%d' AND request_type='guest' AND request_status='WAIT'", array($id, $match['match_id']));
			if (!djmDB::Num()) { $this->location($page['location']); }
			$request = djmDB::Data();
			if ($myTeam == $request['request_from_team']) { $this->location($page['location']); }
			if ($position == "PL") { $this->location($page['location']); }
			
			if ($task == "accept") {
				djmDB::Update(dbMatchRequest, array(
					'request_accept' => $userdata['user_id'],
					'request_accept_time' => time(),
					'request_status' => 'ACCEPT'
				), array('request_id' => $id));
				$this->setLog($this->__("PM_34"), $match['match_id']);
				$this->setDone($this->__("PM_35"));
			} 
			
			if ($task == "reject") {
				djmDB::Update(dbMatchRequest, array(
					'request_accept' => $userdata['user_id'],
					'request_accept_time' => time(),
					'request_status' => 'REJECT'
				), array('request_id' => $id));
				$this->setLog($this->__("PM_36"), $match['match_id']);
				$this->setDone($this->__("PM_37"));				
			}
		}		
		
		// Delete match Request
		if (iMEMBER && isset($_GET['request_delete']) && !empty($_GET['request_delete'])) {
			djmDB::Select("SELECT * FROM ".dbMatchRequest." WHERE request_match='%d' AND request_type='delete' AND request_status='WAIT'", $match['match_id']);
			if (!djmDB::Num()) { $this->location($page['location']); }
			$request = djmDB::Data();
			if ($myTeam == $request['request_from_team']) { $this->location($page['location']); }
			if ($position == "PL") { $this->location($page['location']); }
			
			if ($_GET['request_delete'] == "accept") {
				djmDB::Update(dbMatch, array('match_status' => 3), array('match_id' => $match['match_id']));
				djmDB::Update(dbMatchRequest, array(
					'request_accept' => $userdata['user_id'],
					'request_accept_time' => time(),
					'request_status' => 'ACCEPT'
				), array('request_id' => $request['request_id']));
				$this->setLog($this->__("PM_38"), $match['match_id']);
				$this->setLog($this->__("PM_39"), $match['match_id']);
				$this->setDone($this->__("PM_40"), $page['location']);				
			} 
			
			if ($_GET['request_delete'] == "reject") {
				djmDB::Update(dbMatchRequest, array(
					'request_accept' => $userdata['user_id'],
					'request_accept_time' => time(),
					'request_status' => 'REJECT'
				), array('request_id' => $request['request_id']));
				$this->setLog($this->__("PM_41"), $match['match_id']);
				$this->setDone($this->__("PM_42"), $page['location']);
			}
		}
	break;
}