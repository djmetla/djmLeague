<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Player.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$page['location'] = (!empty($_GET['player']) ?  urlPlayer.$_GET['player'] :  str_replace("=", "", urlPlayer));


switch($_GET['option']) {

		/* Profile Index */
		default:
			if (iMEMBER) { 
				if (!empty($_GET['player']) && isnum($_GET['player'])) {
					$data = $this->isPlayer($_GET['player']);
					if (!$data) { $this->setError($this->__("PP_1"), urlDefault); }
				} else {	
					$data = $this->isPlayer();
				}
			} else {
				if (empty($_GET['player'])) { 
					$this->setError($this->__("PP_2"), urlDefault); 
				} else {
					$data = $this->isPlayer($_GET['player']);
					if (!$data) { $this->setError($this->__("PP_1"), urlDefault); }
				}
			}
			
			$page['title'] = $this->__("PP_3")." ".$data['user_name'];
			
			// Accounts pattern 
			djmDB::Select("SELECT * FROM ".dbAccountsPattern);
			$accountsPattern = djmDB::fullData();		

			
			$_ENV['locale'] = array($this->__("PP_4"), $this->__("PP_5"), $this->__("PP_6"), $this->__("PP_7"), $this->__("PP_8"), $this->__("PP_9") );
			
			
			// User Accounts
			function UserAccount($key, $user) { 
				djmDB::Select("SELECT * FROM ".dbAccounts." WHERE account_key='%s' AND account_user='%d'", array($key, $user));
				if (djmDB::Num()) {
					$aData = djmDB::Data();
						if ($aData['account_value'] == "") {
							$return = array(
								'account' => '<span class="red">'.$_ENV['locale'][0].'</span>',
								'tooltip' => $_ENV['locale'][1].' <strong>'.$aData['account_new_value'].'</strong>. '.$_ENV['locale'][2].' <strong>'.strftime("%d.%m.%Y %H:%M", $aData['account_post_change']).'</strong>'
							);
						} else {
							if ($aData['account_new_value'] == "") {
									$return = array(
										'account' => $aData['account_value'],
										'tooltip' => $_ENV['locale'][3].' <strong>'.strftime("%d.%m.%Y %H:%M", $aData['account_last_change']).'</strong>'
									);
							} else {
								$return = array(
									'account' => $aData['account_value'].' <em><span class="small">('.$_ENV['locale'][4].')</span></em>',
									'tooltip' => $_ENV['locale'][5].' <strong>'.strftime("%d.%m.%Y %H:%M", $aData['account_last_change']).'</strong>'
								);
							}
						}
				} else {
					$return = array('account' => '', 'tooltip' => '');
				}
				
				return $return;
			}
		break;
		
		/* Settings */
		case "settings":
			if (!iMEMBER) { $this->location(BASEDIR."login.php"); }
			$data = $this->isPlayer();
			if (!empty($_GET['player'])) { $this->location(urlDefault); }
			if (!$data) { $this->location(BASEDIR."login.php"); }
			$page['title'] = $this->__("PP_10", $data['user_name']);
		break;
		
		/* Events */
		case "events":
			global $userdata;
			if (!iMEMBER) { $this->location(BASEDIR."login.php"); }
			$page['title'] = $this->__("PP_11", $userdata['user_name']);
			
			if (!isset($_GET['rowstart']) || !isnum($_GET['rowstart'])) { $_GET['rowstart'] = 0; }
			$event_per_page = 15;
			$events_count_all = dbcount("(*)", dbEvent, "event_user='".$userdata['user_id']."'");
			
			djmDB::Select("SELECT * FROM ".dbEvent." WHERE event_user='%d' ORDER BY event_id DESC LIMIT ".$_GET['rowstart'].", ".$event_per_page."", $userdata['user_id']);
			$events_count = djmDB::Num();
			$events = djmDB::fullData();
			djmDB::Update(dbEvent, array('event_read' => 1, 'event_ajax' => 1), array('event_read' => 0, 'event_user' => $userdata['user_id']));
			
			if (isset($_GET['delete_all'])) {
				djmDB::Delete(dbEvent, array('event_user' => $userdata['user_id']));
				$this->setDone($this->__("PP_12"), $page['location']."&amp;option=events");
			}
			
		break;



}
?>
