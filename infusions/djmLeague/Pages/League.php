<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: League.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

	$page['title'] = $this->__("PL_1");
	$page['location'] = (!empty($_GET['league']) ? urlLeague.$_GET['league'] : urlDefault);



	switch($_GET['option']) {
	
		/*
		* Index 
		*/
		default:
			if (isset($_GET['league']) && isnum($_GET['league'])) {
				djmDB::Select("SELECT * 
										FROM ".dbLeague." as t1 
										LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game 
										LEFT JOIN ".dbAccountsPattern." as t3 ON t3.pattern_key=t1.league_account_key
										WHERE league_id='%d'", $_GET['league']);
				if (!djmDB::Num()) { $this->setError("Liga s ID #".$_GET['league']." neexistuje!", urlDefault); }
				$league = djmDB::Data();
				$session = explode("|", $league['league_session']);
				$page['title'] = $league['league_name']." - ".$this->__("PL_2");	
				
				
				if (!isset($_GET['rowstart']) || !isnum($_GET['rowstart'])) { $_GET['rowstart'] = 0; }
				$team_per_page = $dbConfig['settings_team_per_page'];
				$rows_count = dbcount("(*)", dbTeam, "team_league='".$league['league_id']."'");
				djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_league='%d' ORDER BY  (team_status='ACTIVE') DESC, team_points DESC LIMIT ".$_GET['rowstart'].", ".$team_per_page, $league['league_id']);
				$team_count = djmDB::Num();
				$teams = djmDB::fullData();
				
			
				
			}
		break;
	
		/*
		* Section information
		*/
		case "information" :
			djmDB::Select("SELECT league_name, league_information, league_information_time FROM ".dbLeague." WHERE league_id='%d'", $_GET['league']);
			if (!djmDB::Num()) { $this->location(urlDefault); }
			$information = djmDB::Data();
			$page['title'] = $information['league_name']." - ".$this->__("PL_3")." ".($information['league_information_time'] != 0 ? strftime("%d.%m.%Y %H:%M", $information['league_information_time']) : "---").")";
		break;
		
		/*
		* Section rules
		*/
		case "rules" :
			djmDB::Select("SELECT league_name, league_rules, league_rules_time FROM ".dbLeague." WHERE league_id='%d'", $_GET['league']);
			if (!djmDB::Num()) { $this->location(urlDefault); }
			$rules = djmDB::Data();
			$page['title'] = $rules['league_name']." - ".$this->__("PL_4")." ".($rules['league_rules_time'] != 0 ? strftime("%d.%m.%Y %H:%M", $rules['league_rules_time']) : "---").")";				
		break;
	
	}

    
    
     
?>
