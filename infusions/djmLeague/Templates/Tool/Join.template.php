<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Squad.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (!iMEMBER) { $this->location(BASEDIR."login.php"); }
$this->Form("JoinTeam"); 
?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TTOJ_1")?>: #</td> <td class='tbl1'> <?=$this->Input("text", "team_id", (isset($_GET['team']) && isnum($_GET['team']) ? $_GET['team']:null), $this->__("TTOJ_2"))?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TTOJ_3")?>: </td> <td class='tbl1'> <?=$this->Input("password", "team_password", null, $this->__("TTOJ_4"))?> </td> </tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TTOJ_5"))?> </td> </tr>
</table>
</form>