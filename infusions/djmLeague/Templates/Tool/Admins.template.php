<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Admins.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr>
		<td class='tbl1' width='200px;' valign='top'> 
			<ul class='tabs-navi' style='margin: 15px;'>
				<li class='bold'> <?=$this->__("TTOA_1")?>: </li> <br/>
				<?php
					djmDB::Select("SELECT * FROM ".dbAdminCategory); $i = 0;
					foreach(djmDB::fullData() as $cat) {
						$i++;
						$hash = str_replace(" ", "-", $cat['category_name']);
				?>
				<li id='tab<?=$cat['category_id']?>' name='<?=$hash?>' <?=($i == 1 ? "class='hover'":"")?>> <?=THEME_BULLET?> <?=$cat['category_name']?> </li> <br/>
				<?php } ?>
				
				<br/> <li class='bold'> <?=$this->__("TTOA_2")?>: </li> <br/>
				<?php 
					djmDB::Select("SELECT league_id, league_name FROM ".dbLeague);
					foreach(djmDB::fullData() as $league) {
						$hash = str_replace(" ", "-", $league['league_name']);
				?>
				<li id='tabl<?=$league['league_id']?>' name='<?=$hash?>'> <?=THEME_BULLET?> <?=$league['league_name']?> </li> <br/>	
				<?php } ?>
			</ul>		
		</td>
		
		<td class='tbl1' valign='top'> 
			<div class='tabs-content'>
				<?php
					djmDB::Select("SELECT * FROM ".dbAdminCategory); $i = 0;
					foreach (djmDB::fullData() as $cat) {
					$i++;
				?>
					<div id='tab-<?=$cat['category_id']?>' class='tab <?=($i == 1 ? "":"hide")?>'>
							<h3><?=$cat['category_name']?></h3>
							
								<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
									<?php 
										djmDB::Select("SELECT t1.*, t2.user_name, t3.player_flag
																FROM ".dbAdmin." as t1 
																LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.admin_user 
																LEFT JOIN ".dbPlayer." as t3 ON t3.player_user=t1.admin_user
																WHERE admin_category='%d' ORDER BY admin_user ASC", $cat['category_id']);
										if (djmDB::Num()) {
										foreach (djmDB::fullData() as $admin) {
									?>
									<tr class='tbl-border'>
										<td class='tbl2'> <?=($admin['player_flag'] != "" ? $this->Icon($admin['player_flag'], "Flags"):"")?> <?=$admin['admin_name']?> <?=$this->Profile($admin['admin_user'], $admin['user_name'])?> <?=$admin['admin_surname']?> </td>
										<td class='tbl2' align='center'> <a href='<?=BASEDIR?>messages.php?msg_send=<?=$admin['admin_user']?>'><?=$this->__("TTOA_3")?></a> </td>
										<td class='tbl2' align='center'> <a href='javascript:void(0)' class='ShowAdmin' rel='<?=$admin['admin_id']?>'><?=$this->Icon("more.png")?></a> </td>
									</tr>
									<tr class='hide AdminMore' id='Show-<?=$admin['admin_id']?>'>
										<td class='tbl2 small' colspan='3'>
											<?=($admin['admin_description'] ? $admin['admin_description'] : "<em>".$this->__("TTOA_4")."</em>")?>
										</td>
									</tr>
									<?php } } else { ?>
										<tr> <td class='tbl1' align='center'> <?=$this->__("TTOA_5")?> </td> </tr>
									<?php } ?>
								</table>
							
							
					</div>
				<?php
					}
				?>
			
				<?php 
					djmDB::Select("SELECT * FROM ".dbLeague); 
					foreach (djmDB::fullData() as $league) {
				?>
					<div id='tab-l<?=$league['league_id']?>' class='tab'>
						<h3><?=$league['league_name']?></h3>
						
							<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
							<?php
								djmDB::Select("SELECT t1.*, t2.user_name, t3.player_flag FROM ".dbAdmin." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.admin_user LEFT JOIN ".dbPlayer." as t3 ON t3.player_user=t1.admin_user");
								foreach (djmDB::fullData() as $admin) {
									if (!empty($admin['admin_league'])) {
										$leagues = explode(".",$admin['admin_league']);
										if (in_array($league['league_id'], $leagues)) {
											$isAdmin = true;
										?>	
											<tr class='tbl-border'>
												<td class='tbl2'> <?=($admin['player_flag'] != "" ? $this->Icon($admin['player_flag'], "Flags"):"")?> <?=$admin['admin_name']?> <?=$this->Profile($admin['admin_user'], $admin['user_name'])?> <?=$admin['admin_surname']?> </td>
												<td class='tbl2' align='center'> <a href='<?=BASEDIR?>messages.php?msg_send=<?=$admin['admin_user']?>'><?=$this->__("TTOA_3")?></a> </td>
												<td class='tbl2' align='center'> <a href='javascript:void(0)' class='ShowAdmin' rel='<?=$admin['admin_id']?>'><?=$this->Icon("more.png")?></a> </td>
											</tr>
											<tr class='hide AdminMore' id='Show-<?=$admin['admin_id']?>'>
												<td class='tbl2 small' colspan='3'>
													<?=($admin['admin_description'] ? $admin['admin_description'] : "<em>".$this->__("TTOA_4")."</em>")?>
												</td>
											</tr>												
										<?php	
										} else { $isAdmin = false; }
									} 
								}
								 if (!$isAdmin) { echo "<tr> <td class='tbl2' align='center'> ".$this->__("TTOA_6")." </td> </tr>"; } ?>
								</table>
					</div>	
				<?php } ?>
			</div>
		</td>
	</tr>
</table>