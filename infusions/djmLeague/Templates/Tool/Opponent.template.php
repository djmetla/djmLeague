<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Opponent.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

if ($page['list'] == "all") {
	echo "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
		$count = dbcount("(search_id)", dbSearch);
		djmDB::Select("SELECT t1.*,
											t2.team_id, t2.team_name, t2.team_points, t2.team_flag,
											t3.league_name, t3.league_match_use_server, t3.league_id, t3.league_match_use_maps,
											t4.game_name, t4.game_icon
								FROM ".dbSearch." as t1
								LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.search_team
								LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.search_league
								LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
								WHERE search_time>'".time()."' ORDER BY search_time ASC
		");
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $v) {
			
			if (iMEMBER) {
			$iHaveTeam = dbcount("(player_id)", dbTeamPlayer, "player_user='".$userdata['user_id']."' AND player_position !='PL' AND player_team_league='".$v['league_id']."'");
			} else {
			$iHaveTeam = false;
			}
			
			// This is my request
			$player = $this->isTeamPlayer($v['team_id']);
			
			// Show action
			if (!$player && $iHaveTeam && iMEMBER) {
				$action = "<a href='".INFUSIONS."djmLeague/?tool&amp;option=opponent&amp;task=accept[".$v['search_id']."]'>";
				$action .= "<img src='".INFUSIONS."djmLeague/Media/System/accept.png' alt='Accept' title='".$this->__("TTOO_1")."'/>";
				$action .= "</a>";
			} elseif ($player && $player != "PL") {
				$action = "<a href='".INFUSIONS."djmLeague/?tool&amp;option=opponent&amp;task=reject[".$v['search_id']."]'>";
				$action .= "<img src='".INFUSIONS."djmLeague/Media/System/decline.png' alt='Cancel' title='".$this->__("TTOO_2")."'/>";
				$action .= "</a>";
			} else {
				$action = false;
			}
			
			// Show request server
			if ($v['search_server'] != "" && $v['league_match_use_server'] == "YES") { 
				if ($v['search_server'] == "YES") { 
					$server = "Server: <span class='green'>".$this->__("TTOO_3")."</span>";
				} else {
					$server = "Server: <span class='red'>".$this->__("TTOO_4")."</span>";
				}
			} else {
				$server = "";
			}
			
			$additional = "<strong>".$this->__("TTOO_5").":</strong><br/></br>";
			if ($v['search_map'] != "" && $v['league_match_use_maps'] == "YES") {
				$maps = explode(".", $v['search_map']);
				$additional .= "<strong>".$this->__("TTOO_6").": </strong>"; $map_i = 0;
				foreach($maps as $map) { $map_i++; $additional .= $map.(count($maps) == $map_i ? "":", "); }
				$additional .= "<br/>";
			}	
			
			if ($v['search_limit_positive'] != 0) { 
				$additional .= "<strong>".$this->__("TTOO_7").": </strong> ".$v['search_limit_positive']." ".$this->__("TTOO_8")."<br/>";
			}
			
			if ($v['search_limit_negative'] != 0) { 
				$additional .= "<strong>".$this->__("TTOO_9").": </strong> ".$v['search_limit_negative']." ".$this->__("TTOO_8")."<br/>";
			}			
		
		echo "<tr>";
			echo "<td class='tbl2'>"; $this->Icon($v['game_icon'], "Game"); echo " ".$v['league_name']."</td>";
			echo "<td class='tbl2'>"; $this->Icon($v['team_flag'], "Flags"); echo " <a href='".INFUSIONS."djmLeague/?team=".$v['team_id']."' style='position:relative; top:-2px;'>".$v['team_name']."</a></td>";
			echo "<td class='tbl2'>".strftime("%d.%m.%Y %H:%M", $v['search_time'])."</td>";
			echo "<td class='tbl2'>".$server." "; $this->Tooltip($additional); echo "</td>";
			echo "<td class='tbl2'>".$action."</td>";
		echo "</tr>";
		} } else {
			echo "<tr> <td class='tbl2' align='center'> ".$this->__("TTOO_10")." </td> </tr>";
		}
	echo "</table>";

	
} elseif ($page['list'] == "accept") {
	$this->Form("AcceptSearchOpponent");
	$this->Input("hidden", "parent", $data['search_id']);
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>";
		
		echo "<tr> 
					<td class='tbl1' align='right' width='30%'>".$this->__("TTOO_11").":</td> 
					<td class='tbl1'>"; 
						$this->Icon($data['team_flag'], "Flags"); 
						echo " <a href='".urlTeam.$data['team_id']."' style='position:relative; top: -2px;'>".$data['team_name']."</a> 
						<span class='right' style='position: relative; top: 2px;'>
						 (".$this->__("TTOO_12").": <strong>"; $this->TeamRank($data['team_id'], $data['league_id']); echo"</strong>, ".$this->__("TTOO_13").": <strong>"; $this->TeamPoints($data['team_id'], $data['team_points']); echo "</strong>)
						</span>
					</td> 
				</tr>";
		
		echo "<tr> 
					<td class='tbl1' align='right' width='30%'>".$this->__("TTOO_14").":</td> 
					<td class='tbl1'>"; 
						$this->Icon($myteam['team_flag'], "Flags"); 
						echo " <a href='".urlTeam.$myteam['team_id']."' style='position:relative; top: -2px;'>".$myteam['team_name']."</a> 
						<span class='right'>
						 (".$this->__("TTOO_12").": <strong>"; $this->TeamRank($myteam['team_id'], $myteam['league_id']); echo"</strong>, ".$this->__("TTOO_13").": <strong>"; $this->TeamPoints($myteam['team_id'], $myteam['team_points']); echo "</strong>)
						</span>
					</td> 
				</tr>";		
		
		echo "<tr> <td class='tbl1' align='right' width='30%'>".$this->__("TTOO_15").":</td> <td class='tbl1'>"; $this->Icon($data['game_icon'], "Game"); echo " <a href='".urlLeague.$data['league_id']."' style='position:relative; top:-1px;'>".$data['league_name']."</a> </td> </tr>";
		echo "<tr> <td class='tbl1' align='right' width='30%'>".$this->__("TTOO_16").":</td> <td class='tbl1'>".strftime("%d.%m.%Y %H:%M", $data['search_time'])." </td> </tr>";
		
		echo "<tr> <td class='tbl1' align='right' width='30%'>".$this->__("TTOO_17").":</td> <td class='tbl1'>";
			if ($data['search_limit_positive'] != "") { 
				echo $this->__("TTOO_18", $data['search_limit_positive'])."<br/>";
				if ($myteam['team_points'] > $data['search_limit_positive']) { $control = "<span class='red bold'>".$this->__("TTOO_19")."</span>"; } else { $control = "<span class='green bold'>".$this->__("TTOO_20")."</span>"; }
				echo $this->__("TTOO_21").": ".$control."<br/><br/>"; unset($control);				
			}
			
			if ($data['search_limit_negative'] != "") { 
				echo $this->__("TTOO_22", $data['search_limit_negative'])."<br/>";
				if ($myteam['team_points'] < $data['search_limit_negative']) { $control = "<span class='red bold'>".$this->__("TTOO_19")."</span>"; } else { $control = "<span class='green bold'>".$this->__("TTOO_20")."</span>"; }
				echo "Kontrola: ".$control; unset($control);
			}		
		echo "</td></tr>";
		
		echo "<tr> <td class='tbl1' align='right' width='30%'>Server:</td> <td class='tbl1'>"; 
			if($data['league_match_use_server'] == "YES") { 
				if ($data['search_server'] == "YES") {
					echo "<span class='green'>".$this->__("TTOO_23")."</span>";
				} else {
					echo "<span class='red'>".$this->__("TTOO_24")."</span>";
				}
			} else {
				echo $this->__("TTOO_25");
			}
		echo "</td> </tr>";
		
		echo "<tr> <td class='tbl1' align='right' width='30%'>".$this->__("TTOO_26").":</td> <td class='tbl1'>"; 
			if ($data['league_match_use_maps'] == "YES") { 
				$maps = explode(".", $data['search_map']);
				foreach($maps as $map) { 
					$this->Input("radio", "match_map", $map); echo "<span style='position:relative;top: -2px;'>".$map."</span><br/>";
				}
			} else {
				echo $this->__("TTOO_27");
			}
		echo "</td> </tr>";
		echo "<tr> <td class='tbl1' colspan='2'>"; $this->Input("submit", "submit", $this->__("TTOO_28")); echo $this->__("TTOO_29")." <a href='".$page['location']."&amp;task=all'>".$this->__("TTOO_30")."</a></td></tr>";
	echo "</table> </form>";

} elseif ($page['list'] == "create") {
		if (isset($_GET['myteam']) && isnum($_GET['myteam'])) {
		$this->Form("CreateSearchOpponent");
		$this->Input("hidden", "parent", $team['team_id']);
		?>
			<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
				<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOO_31")?>:</td> <td class='tbl1'> <?=$this->Icon($team['team_flag'], "Flags")?> <?=$team['team_name']?> <span class='right'><?=$this->TeamPoints($team['team_id'], $team['team_points'])?> (Rank: <?=$this->TeamRank($team['team_id'], $team['league_id'])?>)</span> </td> </tr>
				<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOO_32")?>:</td> <td class='tbl1'> <?=$this->Icon($team['game_icon'], "Game")?> <?=$team['league_name']?> </td> </tr>
				<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOO_33")?>:</td> <td class='tbl1'> <?=SelectTime::Create("search_time", "d|m|y|h|i", 1200)?> </td> </tr>
				<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOO_34")?>:</td> <td class='tbl1'> <?=$this->Input("radio", "match_server", "YES")?><?=$this->__("TTOO_35")?> <?=$this->Input("radio", "match_server", "NO", null, true)?><?=$this->__("TTOO_36")?> </td> </tr>
				<tr> <td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TTOO_37")?>:</td> <td class='tbl1'>
						<?php 
							if ($team['league_match_use_maps'] == "YES") { 
								$maps = explode(".", $team['league_match_maps']);
								foreach($maps as $map) {
									$this->Input("checkbox", "maps[]", $map); echo "<span style='position:relative;top:-2px;'>".$map."</span> <br/>";
								}
							} else {
								echo $this->__("TTOO_38");
							}
						?>
				</td> </tr>
				<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOO_39")?>:</td> 
					<td class='tbl1'> 
					
						<fieldset>
							<legend class='green'><?=$this->__("TTOO_40")?></legend>
								<?=$this->__("TTOO_41")?>
								<p style='padding: 5px;'>
								<?=$this->Input("checkbox", "limit_positive")?><span style='position: relative; top: -2px; font-weight: bold;'> <?=$this->__("TTOO_42")?></span> <br/>
								<?php
									$plus100 = $team['team_points'] + 100;
									$plus50 = $team['team_points'] + 50;
									$plus25 = $team['team_points'] + 25;
								?>
								<select name='limit_positive_points' class='textbox'> 
									<option value='100'><?=$this->__("TTOO_43", $plus100)?></option>
									<option value='50'><?=$this->__("TTOO_44", $plus50)?></option>
									<option value='25'><?=$this->__("TTOO_45", $plus25)?></option>
								</select>
								</p>
						</fieldset>
					
						<fieldset>
							<legend class='green'><?=$this->__("TTOO_46")?></legend>
								<?=$this->__("TTOO_47")?>
								<p style='padding: 5px;'>
								<?=$this->Input("checkbox", "limit_negative")?><span style='position: relative; top: -2px; font-weight: bold;'> <?=$this->__("TTOO_48")?></span> <br/>
								<?php
									$minus100 = $team['team_points'] - 100;
									$minus50 = $team['team_points'] - 50;
									$minus25 = $team['team_points'] - 25;
								?>
								<select name='limit_negative_points' class='textbox'> 
									<option value='100'><?=$this->__("TTOO_49", $minus100)?></option>
									<option value='50'><?=$this->__("TTOO_50", $minus50)?></option>
									<option value='25'><?=$this->__("TTOO_51", $minus25)?></option>
								</select>
								</p>
						</fieldset>					
					
					</td> </tr>
					<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TTOO_52"))?> <?=$this->__("TTOO_53")?> <a href='<?=urlTool?>opponent&amp;task=create'><?=$this->__("TTOO_54")?></a> </td> </tr>
				
			</table>
		<?php
		} else {
			$this->Form("RedirectCreateSearchOpponent");
			echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>";
				echo "<tr> <td class='tbl1' colspan='5' align='center'>".$this->__("TTOO_55")."</td> </tr>";
			
				foreach ($data as $team) { 
					
					if ($team['league_enable'] == "NO") {
						$control =	$this->Tooltip($this->__("TTOO_56"));
						$control_bin = FALSE;
					} else {
						$control = "<span class='green'>".$this->__("TTOO_57")."</span>";
						$control_bin = TRUE;
					}
				
				?>
					<tr>
						<td class='tbl1'> <?=$this->Icon($team['game_icon'], "Game")?> <span style='position: relative; top: -1px;'><?=$team['league_name']?></span> </td>
						<td class='tbl1'> <?=$this->Icon($team['team_flag'], "Flags")?> <a href='<?=urlTeam.$team['team_id']?>' style='position: relative; top: -1px;'><?=$team['team_name']?></a> </td>
						<td class='tbl1' align='center'> <?=$this->TeamPoints($team['team_id'], $team['team_points'])?> (Rank: <?=$this->TeamRank($team['team_id'], $team['league_id'])?>) </td>
						<td class='tbl1' align='center'> <?=$control?> </td>
						<td class='tbl1' align='center'> <?=($control_bin ? $this->Input("radio", "my_team", $team['team_id'], null):"")?> </td>
					</tr>	
				<?php }
				echo "<tr> <td class='tbl1' colspan='5'>"; $this->Input("submit", "submit", $this->__("TTOO_58"));  echo $this->__("TTOO_53")." <a href='".urlDefault."'>".$this->__("TTOO_59")."</a></td> </tr>";
			echo "</table></form>";
		}
}
?>