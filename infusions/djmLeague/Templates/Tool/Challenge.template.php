<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Challenge.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (isset($_GET['opponent_team']) && isnum($_GET['opponent_team'])) {
	$this->Form("CreateChallenge");
	$this->Input("hidden", "parent", $oponent['team_id']);
	?>
	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
		<tr> <td class='forum-caption' colspan='2'><?=$this->__("TTOC_1")?></td> </tr>
		<tr> 
			<td class='tbl1' colspan='2' align='center' style='padding: 15px;'> 
					<?=$this->Icon($oponent['team_flag'], "Flags")?> <span style='position: relative; top: -2px;'> <?=$oponent['team_name']?> </span>
					<span style='opacity: 0.3; position:relative; top: -2px;'>vs.</span>
					<span style='position: relative; top: -2px;'> <?=$me['team_name']?> </span> <?=$this->Icon($me['team_flag'], "Flags")?> 
					<br/>
					<span style='opacity: 0.5;'><?=$league['league_name']?></span>
			</td>
		</tr>
		<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOC_2")?>:</td> <td class='tbl1'> <?=SelectTime::Create("challenge_time", "d|m|y|h|i", 600)?> </td> </tr>
		<?php if ($league['league_match_use_maps'] == "YES") { ?>
			<tr> 
				<td class='tbl1' width='30%' align='right'> <?=$this->__("TTOC_3")?>:</td> 
				<td class='tbl1'> 
					<?php 
						$maps = explode(".", $league['league_match_maps']);
						foreach($maps as $map) {
							$this->Input("checkbox", "challenge_maps[]", $map); echo "<span style='position: relative; top: -3px;'>".$map."</span><br/>";
						}
					?>
				</td> 
			</tr>
		<?php } ?>
		<?php if ($league['league_match_use_server'] == "YES") { ?>
		<tr> 
			<td class='tbl1' width='30%' align='right'> <?=$this->__("TTOC_4")?>:</td> 
			<td class='tbl1'>
				<?=$this->Input("radio", "challenge_server", "YES")?><span style='position: relative; top: -2px;'><?=$this->__("TTOC_5")?></span>  &nbsp;&nbsp;
				<?=$this->Input("radio", "challenge_server", "NO", null, true)?><span style='position: relative; top: -2px;'><?=$this->__("TTOC_6")?></span> 
			</td> 
		</tr>
		<?php } ?>
		<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TTOC_7"))?> <?=$this->__("TTOC_8")?> <a href='<?=urlTeam.$oponent['team_id']?>'> <?=$this->__("TTOC_9")?> <?=$oponent['team_name']?></a> </td> </tr>
	</table>
	</form>

<? } elseif (isset($_GET['view']) && isnum($_GET['view'])) { ?>
	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
		<tr> <td class='forum-caption' colspan='2'><?=$this->__("TTOC_10")?></td> </tr>
		<tr> 
			<td class='tbl1' colspan='2' align='center' style='padding: 15px;'> 
					<?=$this->Icon($t1['team_flag'], "Flags")?> <span style='position: relative; top: -2px;'> <?=$t1['team_name']?> </span>
					<span style='opacity: 0.3; position:relative; top: -2px;'>vs.</span>
					<span style='position: relative; top: -2px;'> <?=$t2['team_name']?> </span> <?=$this->Icon($t2['team_flag'], "Flags")?> 
			</td>
		</tr>

		<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TTOC_11")?>:</td>  <td class='tbl1'> <?=strftime("%d.%m.%Y %H:%M", $challenge['challenge_time_match'])?> </td> </tr>
		<?php if ($challenge['league_match_use_maps'] == "YES") { ?>
		<tr> <td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TTOC_12")?>:</td>  <td class='tbl1'> <?php $maps = explode(".", $challenge['challenge_map']); foreach ($maps as $map) { echo $map."<br/>"; } ?> </td> </tr>
		<?php } ?>
		<?php if ($challenge['league_match_use_server'] == "YES") { ?>
		<tr> <td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TTOC_13")?>: </td>  <td class='tbl1'> <?=($challenge['challenge_server'] == "YES" ? "<span class='green'>".$this->__("TTOC_14")."</span>":"<span class='red'>".$this->__("TTOC_15")."</a>")?> </td> </tr>
		<?php } ?>
		
		<?php if ($pos_1 && $pos_1 != "PL") { ?>
			<tr> <td class='tbl1' align='right' colspan='2'> <a href='javascript:void(0);' class='red' title='<?=$this->__("TTOC_16")?>' onclick="if (confirm('<?=$this->__("TTOC_17")?>')) { window.location.href='<?=urlTool?>challenge&amp;view=<?=$_GET['view']?>&amp;reject'; }"> <?=$this->__("TTOC_18")?> </a> </td> </tr>
		<?php } elseif ($pos_2 && $pos_2 != "PL") { ?>
			<?=$this->Form("AcceptChallenge")?>
			<?=$this->Input("hidden", "parent", $challenge['challenge_id'])?>
			<tr> <td class='tbl1' align='right'><?=$this->__("TTOC_19")?>: </td>
				<td class='tbl1'>
					<?php $i = 0; foreach($maps as $map) {  $i++;?>
						<?=$this->Input("radio", "challenge_map", $map, null, ($i == 1 ? true:false))?> <?=$map?><br/>
					<?php } ?>
				</td>
			</tr>
			<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TTOC_20"))?> <?=$this->__("TTOC_8")?> <a href='javascript:void(0);' class='red' title='<?=$this->__("TTOC_16")?>' onclick="if (confirm('<?=$this->__("TTOC_17")?>')) { window.location.href='<?=urlTool?>challenge&amp;view=<?=$_GET['view']?>&amp;reject'; }"><?=$this->__("TTOC_18")?></a>  <a href='javascript:history.go(-1)' class='right'><?=$this->__("TTOC_21")?></a>  </td> </tr> 
			</form>
		<?php } ?>
	</table>
<?php } else { $this->location(urlDefault); } ?>