<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Create.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
?>

<?=$this->Form("CreateTeam")?>
<table border='0' align='center' cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TTOCR_1")?>:</td> <td class='tbl1' colspan='3'> <?=$this->Input("text", "team_name", null, $this->__("TTOCR_2"))?> <?=$this->Tooltip($this->__("TTOCR_3"))?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TTOCR_4")?>:</td> <td class='tbl1' colspan='3'> <?=$this->Flags("team_flag")?> <?=$this->Tooltip($this->__("TTOCR_5"))?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TTOCR_6")?>:</td> <td class='tbl1' colspan='3'> <?=$this->Input("text", "team_tag", null, $this->__("TTOCR_7"))?> <?=$this->Tooltip($this->__("TTOCR_8"))?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TTOCR_9")?>:</td> <td class='tbl1' colspan='3'> <?=$this->Input("password", "team_password", null, $this->__("TTOCR_10"))?> <?=$this->Tooltip($this->__("TTOCR_11"))?> </td> </tr>
</table>

<table border='0' align='center' cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
	<tr> <td colspan='4' class='forum-caption'> <?=$this->__("TTOCR_12")?> </td> </tr>
	<?php 
		if ($league_count) {
		foreach ($leagues as $league) { 
			$status = func_leagueStatus($league['league_id']);
	?>
	<tr> 
		<td class='tbl1'> <?=$this->Icon($league['game_icon'], "Game")?> <a href='<?=urlLeague.$league['league_id']?>'><?=$league['league_name']?></a> </td>
		<td class='tbl1' align='center'> <?=($league['league_enable'] == "YES" ? "<span class='green'>".$this->__("TTOCR_13")."</span>":"<span class='red'>".$this->__("TTOCR_14")."</span>")?> </td>
		<td class='tbl1' align='center'> <?=$status['status']?> <?=($status['text'] != "" ? $this->Tooltip($status['text']) : "")?> </td> 
		<td class='tbl1' align='center' width='40'> <input type='radio' name='team_league' value='<?=$league['league_id']?>' <?=($status['enable'] ? "":"disabled")?>/> </td>
	</tr>
	<?php } } else { ?> 
		<tr> <td class='tbl1' align='center' colspan='4'> <?=$this->__("TTOCR_15")?> </td> </tr>
	<?php } ?>
	<tr> <td class='tbl1' colspan='4'> <?=$this->Input("submit","submit",$this->__("TTOCR_16"))?> </td> </tr>
</table>
