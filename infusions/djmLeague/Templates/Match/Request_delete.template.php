<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Request_delete.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("RequestDelete");
$this->Input("hidden", "parent", $match['match_id']);
?>
<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr> <td class='tbl1' align='center' style='padding: 15px;'> <?=$this->__("TMRD_1")?> </td> </tr>
	<tr> <td class='tbl1' align='center'> <?=$this->Input("submit", "submit", $this->__("TMRD_2"))?> <br/> <a href='<?=$page['location']?>'><?=$this->__("TMRD_3")?></a> </td> </tr>
</table>
</form>
