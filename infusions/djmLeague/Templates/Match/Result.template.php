<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Result.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("AddResult");
$this->Input("hidden", "parent", $match['match_id']);
?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> 
		<td class='tbl1' align='center' width='200' style='font-weight: bold; font-size: 10pt;'>
				<?=$this->Icon($t1['team_flag'], "Flags")?> <br/>
				<a href='<?=urlTeam.$t1['team_id']?>'><?=$t1['team_name']?></a>
		</td>
		
		<td class='tbl1' align='center'> 
			<select name='t1_score' class='textbox'>
				<option value=''></option>
				<?php for($i=0; $i<=$match['league_match_max_score']; $i++) { ?>
					<option value='<?=$i?>' <?=($match['match_status'] == 1 && $match['match_t1_score'] == $i ? "selected":"")?>><?=$i?></option>
				<?php } ?>
			</select>
			:
			<select name='t2_score' class='textbox'>
				<option value=''></option>
				<?php for($i=0; $i<=$match['league_match_max_score']; $i++) { ?>
					<option value='<?=$i?>' <?=($match['match_status'] == 1 && $match['match_t2_score'] == $i ? "selected":"")?>><?=$i?></option>
				<?php } ?>			
			</select>
		</td>
		
		<td class='tbl1' align='center' width='200' style='font-weight: bold; font-size: 10pt;'>
				<?=$this->Icon($t2['team_flag'], "Flags")?> <br/>
				<a href='<?=urlTeam.$t2['team_id']?>'><?=$t2['team_name']?></a>
		</td>	
	</tr>
	<tr> <td class='tbl1' align='center' colspan='3'> <?=$this->Input("submit", "submit", ($match['match_status'] == 1 ? 	$this->__("TMRE_1"):$this->__("TMRE_2"))." ".$this->__("TMRE_3"))?> <br/> <a href='<?=urlMatch.$match['match_id']?>'><?=$this->__("TMRE_4")?></a> </td> </tr>
</table>