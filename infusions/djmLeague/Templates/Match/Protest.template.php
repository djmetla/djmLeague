<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Protest.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("CreateProtest");
$this->Input("hidden", "parent", $match['match_id']);
?>

<script type='text/javascript'>
	$(function(){
		$('select[name="protest_type"]').change(function(){
			var select = $(this); var val = select.find(":selected").val();
			if (val != "") { var find = $('input[name="setting_'+val+'"]').val();
			} else { var find = "NO"; }
			if (find == "YES") { $('#Auto').fadeOut(function() { $('#Custom').fadeIn() });
			} else { $('#Custom').fadeOut(function(){ $('#Auto').fadeIn(); }); $ }
		});
	});
</script>
<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr>
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TMP_1")?>: </td>
		<td class='tbl1'>
		<?php
			djmDB::Select("SELECT * FROM ".dbProtestPattern);
			$protests = djmDB::fullData();
			foreach($protests as $protest) {
				$this->Input("hidden", "setting_".$protest['pattern_id'], ($protest['pattern_use_custom_description'] == 0 ? "NO":"YES"));
			}
			
		?>
			<select name='protest_type' class='textbox' style='width: 220px;'>
				<option value=''></option>
				<?php foreach($protests as $protest) { ?>
					<?php if ($protest['pattern_league'] == "") { ?>
						<option value='<?=$protest['pattern_id']?>'><?=$protest['pattern_name']?></option>
					<?php 
						} else { 
							$league = explode(".", $protest['pattern_league']);
							if (in_array($match['league_id'], $league)) {
					?>
						<option value='<?=$protest['pattern_id']?>'><?=$protest['pattern_name']?></option>
					<?php } } ?>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr id='Custom' class='hide'>
		<td class='tbl1' align='right'> <?=$this->__("TMP_2")?>: </td>
		<td class='tbl1'> <textarea name='protest_custom' class='textbox' style='width: 450px; height: 70px;' placeholder="<?=$this->__("TMP_3")?>"></textarea> </td>
	</tr>
	<tr id='Auto'>
		<td class='tbl1' align='right'> <?=$this->__("TMP_2")?>: </td>
		<td class='tbl1'> <?=$this->__("TMP_4")?> </td>
	</tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMP_5"))?> <?=$this->__("TMP_6")?> <a href='<?=$page['location']?>'><?=$this->__("TMP_7")?></a> </td> </tr>
</table>	
</form>