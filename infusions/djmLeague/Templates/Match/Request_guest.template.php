<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Request_guest.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("RequestHost");
$this->Input("hidden", "parent", $match['match_id']);
?>
 <table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr>
		<td class='tbl1' style='padding: 15px;' colspan='2' align='center'>
			<?=$this->__("TMRG_1", $match['league_match_host_num'])?>
		</td>
	</tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TMRG_2")?>: </td> <td class='tbl1'> <?=dbcount("(*)", dbMatchRequest, "request_type='guest' AND request_from_team='".$myTeam."' AND request_status!='REJECT'")?>/<?=$match['league_match_host_num']?>
	<?php if (dbcount("(*)", dbMatchRequest, "request_type='guest' AND request_from_team='".$myTeam."' AND request_status!='REJECT'") == $match['league_match_host_num']) { ?>
		<tr>
			<td class='tbl1' colspan='2' align='center'> <?=$this->__("TMRG_3")?> <br/> <a href='<?=$page['location']?>'><?=$this->__("TMRG_4")?></a></td> 
		</tr>
	<?php } else { ?>
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TMRG_5")?>:</td> 
		<td class='tbl1'> 
				<select name='match_host' class='textbox' style='width: 220px;'>	
				<option value=''></option>
				<?php
					djmDB::Select("SELECT t1.*, t2.user_name, t2.user_id FROM ".dbPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user");
					foreach(djmDB::fullData() as $player) {
						if (!$this->isTeamPlayer($t1['team_id'], $player['user_id']) && !$this->isTeamPlayer($t2['team_id'], $player['user_id'])) {
							if ($match['league_account'] == "YES") {
								if (dbcount("(*)", dbAccounts, "account_user='".$player['user_id']."' AND account_key='".$match['league_account_key']."' AND account_value!=''")) {
									echo "<option value='".$player['user_id']."'>".$player['user_name']."</option>";
								}
							} else {
								echo "<option value='".$player['user_id']."'>".$player['user_name']."</option>";
							}
						}
					}
				?>
				</select>
		</td> 
	</tr>
	<tr> <td class='tbl1' colspan='2'><?=$this->Input("submit", "submit", $this->__("TMRG_6"))?> <?=$this->__("TMRG_7")?> <a href='<?=$page['location']?>'><?=$this->__("TMRG_8")?></a> </td></tr>
	<?php } ?>
</table>	