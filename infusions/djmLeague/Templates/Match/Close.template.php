<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Server.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("CloseMatch");
$this->Input("hidden", "parent", $match['match_id']);
?>
<center>
	<?=$this->__("TMC_1")?> <br/>
	<strong><?php echo $t1['team_name']." (".$match['match_t1_score'].":".$match['match_t2_score'].") ".$t2['team_name'].""; ?></strong>
	<br/><br/>
	<?=$this->Input("submit", "submit", $this->__("TMC_2"))?>
	<br/>
	<a href='<?=$page['location']?>'><?=$this->__("TMC_3")?></a>
</center>	
</form>