<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Server.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("EditServer");
$this->Input("hidden", "parent", $match['match_id']); 	
?>
<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr>
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TMS_1")?>: </td>
		<td class='tbl1'> <?=$this->Input("text", "match_server", $match['match_server'], null, "IP: xx.xx.xx.xx, heslo: xxxx")?> </td>
	</tr>
	<tr>
		<td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMS_2"))?> <?=$this->__("TMS_3")?> <a href='<?=$page['location']?>'><?=$this->__("TMS_4")?></a> </td>
	</tr>
</table>
</form>