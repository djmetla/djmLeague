<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Links.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("AddExternLink");
$this->Input("hidden", "parent", $match['match_id']);
?>
<table border='0' width='100%' align='center' class='tbl-border' cellpadding='0' cellspacing='1'>
	<tr> <td class='tbl1' colspan='2' align='center' style='padding: 15px;'> <?=$this->__("TML_1")?> </td> </tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TML_2")?>:</td> 
		<td class='tbl1'> 
			<select name='link_type' class='textbox'>
				<option value=''></option>
				<option value='demo'><?=$this->__("TML_3")?></option>
				<option value='screenshot'><?=$this->__("TML_4")?></option>
				<option value='other'><?=$this->__("TML_5")?></option>
			</select>
		</td> 
	</tr>
	
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TML_6")?>:</td> <td class='tbl1'> <?=$this->Input("text", "link_link", null, "http://")?> </td> </tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TML_7"))?> <?=$this->__("TML_8")?> <a href='<?=urlMatch.$_GET['match']?>'><?=$this->__("TML_9")?></a> </td> </tr>
</table>