<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Index.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>
 
 <table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr> <td colspan='3' class='forum-caption'><?=$this->__("TMI_1")?> </td> </tr>
	<tr> 
		<td class='tbl1' align='right' width='20%'> <?=$this->__("TMI_2")?> (#<?=$match['match_id']?>): </td> 
		<td class='tbl1'> <?=$this->Icon($t1['team_flag'], "Flags")?> <?=$t1['team_name']?> <span style='opacity: 0.5;'>vs.</span> <?=$this->Icon($t2['team_flag'], "Flags")?> <?=$t2['team_name']?> </td> 
		
		<?php if(iMEMBER && $position && $match['match_status'] != 3) { ?>
		<td class='tbl1' rowspan='6' valign='top' width='150'>
			<!-- Add score -->
			<?php if (dbcount("(*)", dbMatchMedia, "media_match='".$match['match_id']."'") && $position != "PL" && $match['match_status'] != 2) { ?>
				<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=result'><?=$this->__("TMI_3")?></a> <br/> 
			<?php }?>
			
			<!-- Close match -->
			<?php if ($match['match_status'] == 1 && $position != "PL") { ?>
				<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=close'><?=$this->__("TMI_4")?></a> <br/>
			<?php } ?>			
			
			<?php if ($match['match_status'] == 0) { ?>
				<?php if ($match['league_match_delete'] == "YES" && ($match['match_time'] + $match['league_match_delete_time']) > time()) { ?>
					<!-- Delete Request-->
					<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=request_delete'><?=$this->__("TMI_5")?></a> <br/>
				<?php } ?>
				
				<?php if ($match['league_match_host'] == "YES" && ($match['match_time'] + $match['league_match_host_time']) > time()) {?>
					<!--Guest Request-->
					<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=request_guest'><?=$this->__("TMI_6")?></a> <br/>
				<?php } ?>
			<?php } ?>
			
			
			<?php if ($match['match_status'] != 2 || $match['match_status'] != 3) { ?>
				<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=media'><?=$this->__("TMI_7")?></a> <br/> 
				<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=links'><?=$this->__("TMI_8")?></a> <br/> 
			<?php } ?>
			

			<?php if (($match['match_time'] + $match['league_match_request_delay']) < time() && ($match['match_time'] + $match['league_match_request_delay_stop']) > time()) { ?> 
				<!--Add Request-->
				<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=request'><?=$this->__("TMI_9")?></a> <br/> 
			<?php } ?>
			
			<?php if (($match['match_time'] + $match['league_match_protest_delay']) < time() && ($match['match_time'] + $match['league_match_protest_delay_stop']) > time()) { ?>
				<!--Add Protest-->
				<?=THEME_BULLET?> <a href='<?=urlMatch.$match['match_id']?>&amp;option=protest'><?=$this->__("TMI_10")?></a> <br/> 
			<?php } ?>

		</td>
		<?php } ?>
		
	</tr>
	<tr> <td class='tbl1' align='right' width='20%'> <?=$this->__("TMI_11")?>: </td> <td class='tbl1'> <?=$this->Icon($match['game_icon'], "Game")?> <?=$match['league_name']?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='20%'> <?=$this->__("TMI_12")?>: </td> <td class='tbl1'> <?=strftime("%d.%m.%Y %H:%M", $match['match_time'])?>  </td> </tr>
	<tr> <td class='tbl1' align='right' width='20%'> <?=$this->__("TMI_13")?>: </td> <td class='tbl1'>  <?=$match['match_map']?> </td> </tr> <!-- Ak je v lige enable -->
	<?php if ($match['league_match_use_server'] == "YES" && $match['match_status'] != 3) { ?>
	<tr> 
		<td class='tbl1' align='right' width='20%'> <?=$this->__("TMI_14")?>: </td> 
		<td class='tbl1'> 
			<?php 
				if ($match['match_server'] != "") { 
					echo $match['match_server'].(iMEMBER &&  $position != "PL" && ($match['match_time']+$match['league_match_server_time']) > time()  ? "<a href='".$page['location']."&amp;option=server' class='right'>".$this->__("TMI_15")."</a>":"");
				} else {
					echo "<em>".$this->__("TMI_56")."</em> ".(iMEMBER &&  $position != "PL" && ($match['match_time']+$match['league_match_server_time']) > time()  ? "<a href='".$page['location']."&amp;option=server' class='right'>".$this->__("TMI_15")."</a>":"")."";
				}
			?>
		</td> 
	</tr>
	<?php } ?>
	<tr> <td class='tbl1' align='right' width='20%'> <?=$this->__("TMI_16")?>: </td> <td class='tbl1'> <?=$status['text']?> </td> </tr>
 </table>
 <br/>
 
 <!-- Match result -->
 <table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr> <td class='forum-caption' colspan='3'> <?=$this->__("TMI_17")?> </td> </tr>
	<tr> 
		<td class='tbl1' align='center' width='150'>  
			<?=$this->Icon($t1['team_flag'], "Flags")?> <a href='<?=urlTeam.$t1['team_id']?>'><?=$t1['team_name']?></a> <br/>
			<?php if ($t1['team_avatar'] != "") { ?>
				<img src='<?=pathMedia?>Team/<?=$t1['team_avatar']?>' alt='Avatar' width='100' height='100' />
			<?php } else { ?>
				<img src='<?=IMAGES?>avatars/noavatar100.png' alt='NO-AVATAR' width='100' height='100' />
			<?php } ?>
			
		</td>
		<td class='tbl1' align='center'> 
			<span style='font-size: 30pt'>
				<?php
					if ($match['match_status'] == 0) { 
						echo "TBA : TBA";
					} elseif ($match['match_status'] == 3) {
						echo "<span style='text-decoration: line-through;'>TBA</span> : <span style='text-decoration: line-through;'>TBA</span>";
					} else {
						$s1 = $match['match_t1_score'];
						$s2 = $match['match_t2_score'];
						if ($s1 > $s2) {		
							echo "<span class='green'>$s1</span> : <span class='red'>$s2</span>";
						} elseif ($s1 < $s2) { 
							echo "<span class='red'>$s1</span> : <span class='green'>$s2</span>";
						} elseif ($s1 == $s2) {
							echo "<span class='blue'>$s1</span> : <span class='blue'>$s2</span>";
						}
					}
				?>	
			</span> <br/>
				<?php
					if ($match['match_status'] == 0) {
						echo "---";
					} elseif ($match['match_status'] == 3) {	
						echo $this->__("TMI_18");
					} else {
						
						if ($match['league_match_elo_type'] == "default") {
							$elo = new Elo("default");
							$score = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
						} elseif ($match['league_match_elo_type'] == "classic") {
							$elo = new Elo("classic");
							$score = $elo->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();				
						} elseif ($match['league_match_elo_type'] == "custom") {
							djmDB::Select("SELECT elo_value FROM ".dbElo." WHERE elo_id='%d'", $match['league_match_elo']);
							$elod = djmDB::Data();
							$elo = new Elo("default");
							$score = $elo->Constant($elod['elo_value'])->t1Pts($match['match_t1_points'])->t2Pts($match['match_t2_points'])->t1Score($match['match_t1_score'])->t2Score($match['match_t2_score'])->Calculate();
						}
					

						if($score[0] > $score[1]) {
							echo "<span class='green'>(+".$score[0].")</span> : <span class='red'>(".$score[1].")</span>";
						} elseif ($score[0] < $score[1]) {
							echo "<span class='red'>(".$score[0].")</span> : <span class='green'>(+".$score[1].")</span>";
						} elseif ($score[0] == $score[1]) {
							echo "<span class='blue'>(+".$score[0].")</span> : <span class='blue'>(+".$score[1].")</span>";
						}
					}
				?>
		</td>
		<td class='tbl1' align='center' width='150'>
			<?=$this->Icon($t2['team_flag'], "Flags")?> <a href='<?=urlTeam.$t2['team_id']?>'><?=$t2['team_name']?></a> <br/>
			<?php if ($t2['team_avatar'] != "") { ?>
				<img src='<?=pathMedia?>Team/<?=$t2['team_avatar']?>' alt='Avatar' width='100' height='100' />
			<?php } else { ?>
				<img src='<?=IMAGES?>avatars/noavatar100.png' alt='NO-AVATAR' width='100' height='100' />
			<?php } ?>
		</td>
	</tr>
 </table>
 <br/>
 
 <ul class='tabs-navi'>
	<li id='tab1' name='Index' class='hover forum-caption tbl-border'> <?=$this->__("TMI_19")?> </li>
	<li id='tab2' name='Requests' class='forum-caption tbl-border'> <?=$this->__("TMI_20")?> </li>
	<li id='tab3' name='Protests' class='forum-caption tbl-border'> <?=$this->__("TMI_21")?> </li>
	<li id='tab4' name='Logs' class='forum-caption tbl-border'> <?=$this->__("TMI_22")?> </li>
</ul>
<div class='tabs-content tbl-border'>
	
	<!-- Index -->
	<div id='tab-1' class='tab'>
	
		 <!-- Match media -->
		 <?php 
			djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbMatchMedia." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.media_user WHERE media_match='%d'", $match['match_id']); 
			$media_count = djmDB::Num();
		?>
		 <table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='tbl1 bold' colspan='1'> <?=$this->__("TMI_23")?> (<?=$media_count?>) </td> </tr>
			<?php if ($media_count) { ?>
			<tr> 
				<td class='tbl1'>
					<?php foreach(djmDB::fullData() as $img) { ?>
						<div class='tbl-border' style='float: left; margin-right:10px; text-align: center; width: 130px;'>
							<a href='<?=pathMedia?>Match/<?=$match['match_league']?>/<?=$match['match_id']?>/<?=$img['media_item']?>' class='ImageView' title='<?=$this->__("TMI_24")?> <?=$img['user_name']?>, <?=$this->__("TMI_25")?> <?=$img['media_type']?>, <?=strftime("%d.%m.%Y %H:%M", $img['media_time'])?>' rel='gallery'>
								<img src='<?=pathMedia?>Match/<?=$match['match_league']?>/<?=$match['match_id']?>/thumb_<?=$img['media_item']?>' alt='Image - <?=$img['media_id']?>' width='130' height='70' />
							</a>
							<span style='font-size: 10px;'>
							<?=$this->Profile($img['media_user'], $img['user_name'])?> <br/>
							<?=strftime("%d.%m.%Y %H:%M", $img['media_time'])?>
							</span>
							<br class='clear'/>
						</div>

					<?php } ?>
				</td>
			</tr>	
			<?php } else { ?>
				<tr> <td class='tbl1' align='center'> <?=$this->__("TMI_26")?> </td> </tr>
			<?php } ?>	
		 </table>
		 <br/>
		 
		 <!-- Extern links -->
		 <?php
			djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbMatchLinks." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.link_user WHERE link_match='%d'", $match['match_id']); 
			$link_count = djmDB::Num();
		 ?>
		 <table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='tbl1 bold' colspan='3'> <?=$this->__("TMI_27")?> (<?=$link_count?>)</td> </tr>
			<?php
				if ($link_count) { 
				foreach (djmDB::fullData() as $link) { 
					if ($link['link_type'] == "demo") { $typ = $this->__("TMI_28"); } 
					elseif ($link['link_type'] == "screenshot") { $typ = $this->__("TMI_29"); }
					else { $typ = $this->__("TMI_30"); }
				?>
				<tr> 
					<td class= 'tbl1'> 
						<a href='<?=$link['link_target']?>' target='_blank'><?=$typ?></a>  <?=$this->__("TMI_31")?> <a href='<?=urlProfile.$link['link_user']?>'><?=$link['user_name']?></a> 
						<span class='right'> <?=strftime("%d.%m.%Y %H:%M", $link['link_time'])?> </span>
					</td>
				</tr>	
			<?php } } else { ?>
				<tr> <td class='tbl1' align='center'> <?=$this->__("TMI_32")?> </td> </tr>
			<?php } ?>
		 </table>
		 <br/>		 
		 
	</div>
	
	<!-- Requests -->
	<div id='tab-2' class='tab hide'>
		<?php
			djmDB::Select("SELECT t1.*, 
												t2.user_name as Author, 
												t3.team_name as AuthorTeam, 
												t4.*,
												t5.user_name as Target
									FROM ".dbRequest." as t1
									LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.request_author
									LEFT JOIN ".dbTeam." as t3 ON t3.team_id=t1.request_author_team
									LEFT JOIN ".dbRequestPattern." as t4 ON t4.pattern_id=t1.request_pattern
									LEFT JOIN ".DB_USERS." as t5 ON t5.user_id=t1.request_to
									WHERE request_match='%d' ORDER BY request_id DESC", $match['match_id']);
			$r_count = djmDB::Num();
			if ($r_count) {
			foreach (djmDB::fullData() as $r) {
				$text1 = str_replace("%name%", "<strong>".$r['Target']."</strong>", $r['pattern_preddefined']);
				$text = str_replace("%id%", "<strong>".$r['request_to']."</strong>", $text1);
		?>
			<div class='request'>
				<strong><?=$r['pattern_name']?></strong>
				<p><?=$text?><hr/> <?=$this->__("TMI_33", $r['Author'])?> <?=$r['AuthorTeam']?>, <em><?=strftime("%d.%m.%Y %H:%M", $r['request_time'])?></em> </p> 
			</div>	
		<?php
			}
			}
			djmDB::Select("SELECT t1.*, t2.team_name, t3.user_name FROM ".dbMatchRequest." as t1 LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.request_from_team LEFT JOIN ".DB_USERS." as t3 ON t3.user_id=t1.request_from WHERE request_match='%d' ORDER BY request_id DESC", $match['match_id']);
			$sr_count = djmDB::Num();
			if ($sr_count) {
			foreach (djmDB::fullData() as $r) {
			if ($r['request_type'] == "delete") {
		?>
			<div class='request'>
				<strong><?=$this->__("TMI_34")?></strong>
				<p> <?=$this->__("TMI_35")?> <?=$r['team_name']?> (<?=$r['user_name']?>) <?=$this->__("TMI_36")?>. <br/> <em><?=strftime("%d.%m.%Y %H:%M", $r['request_time'])?></em> </p> 
				<?php if ($r['request_status'] == "WAIT" && $myTeam != $r['request_from_team'] && $position != "PL") { ?>
					<a href='<?=$page['location']?>&amp;request_delete=accept' class='red'><?=$this->__("TMI_37")?></a> - <a href='<?=$page['location']?>&amp;request_delete=reject' class='red'><?=$this->__("TMI_38")?></a>
				<?php } ?>
				<?php if ($r['request_status'] == "ACCEPT") { ?>
					<div><hr/> <?=$this->__("TMI_39")?> <span class='green bold'><?=$this->__("TMI_40")?></span> <?=$this->__("TMI_42")?> <?=strftime("%d.%m.%Y %H:%M", $r['request_accept_time'])?> </div>
				<?php } elseif ($r['request_status'] == "REJECT") { ?>
					<div><hr/> <?=$this->__("TMI_39")?> <span class='red bold'><?=$this->__("TMI_41")?></span> <?=$this->__("TMI_42")?> <?=strftime("%d.%m.%Y %H:%M", $r['request_accept_time'])?> </div>
				<?php } ?>
			</div>
		<?php } else { 
			djmDB::Select("SELECT user_name FROM ".DB_USERS." WHERE user_id='%d'", $r['request_type_guest']);
			$host = djmDB::Data();
		?>
			<div class='request'>
				<strong><?=$this->__("TMI_43")?></strong>
				<p> <?=$this->__("TMI_44")?> <?=$r['team_name']?> (<?=$r['user_name']?>) <?=$this->__("TMI_45")?> <?=$host['user_name']?> <br/> <em><?=strftime("%d.%m.%Y %H:%M", $r['request_time'])?></em> </p> 
				<?php if ($r['request_status'] == "WAIT" && $myTeam != $r['request_from_team'] && $position != "PL") { ?>
					<a href='<?=$page['location']?>&amp;request_guest=<?=$r['request_id']?>-accept' class='red'><?=$this->__("TMI_46")?></a> - <a href='<?=$page['location']?>&amp;request_guest=<?=$r['request_id']?>-reject' class='red'><?=$this->__("TMI_47")?></a>
				<?php } ?>
				<?php if ($r['request_status'] == "ACCEPT") { ?>
					<div><hr/><?=$this->__("TMI_39")?> <span class='green bold'><?=$this->__("TMI_40")?></span> <?=$this->__("TMI_42")?> <?=strftime("%d.%m.%Y %H:%M", $r['request_accept_time'])?> </div>
				<?php } elseif ($r['request_status'] == "REJECT") { ?>
					<div><hr/><?=$this->__("TMI_39")?> <span class='red bold'><?=$this->__("TMI_41")?></span> <?=$this->__("TMI_42")?> <?=strftime("%d.%m.%Y %H:%M", $r['request_accept_time'])?> </div>
				<?php } ?>				
			</div>		
		<?php } } } ?>
			
		<?php if ($r_count+$sr_count == 0) { ?>
			<center> <?=$this->__("TMI_48")?> </center>
		<?php } ?>
			
	</div>
	
	<!-- Protests -->
	<div id='tab-3' class='tab hide'>
		<?php 
			djmDB::Select("SELECT t1.*,
												t2.*,
												t3.user_name as User,
												t4.team_name as UserTeam,
												t5.user_name as Boss
									FROM ".dbProtest." as t1
									LEFT JOIN ".dbProtestPattern." as t2 ON t2.pattern_id=t1.protest_pattern
									LEFT JOIN ".DB_USERS." as t3 ON t3.user_id=t1.protest_from
									LEFT JOIN ".dbTeam." as t4 ON t4.team_id=t1.protest_from_team
									LEFT JOIN ".DB_USERS." as t5 ON t5.user_id=t1.protest_admin
									WHERE protest_match='%d' ORDER BY protest_id DESC", $match['match_id']);
			if (djmDB::Num()) {
			foreach (djmDB::fullData() as $protest) { 
			
		?>
			<div class='protest'>
				<strong> <?=$protest['pattern_name']?> </strong>
				<p><?=($protest['pattern_use_custom_description'] == 1 ? $protest['protest_description'] : $protest['pattern_preddefined'])?> <br/>
					 <em><?=$this->__("TMI_49", $protest['User'])?> <?=$protest['UserTeam']?>, <?=strftime("%d.%m.%Y %H:%M", $protest['protest_time'])?></em>
					 <hr/>
					 <?php if ($protest['protest_status'] == 0) { ?>
						<?=$this->__("TMI_50")?>
					 <?php } elseif ($protest['protest_status'] == 1) { ?>
						<?=$this->__("TMI_51")?> <?=$protest['Boss']?>
					 <?php } elseif ($protest['protest_status'] == 2) { ?>
						<?=$this->__("TMI_52")?> <?=$protest['Boss']?> <br/><br/>
						<em><?=$this->__("TMI_53")?></em> <br/> <?=$protest['protest_admin_description']?>
					 <?php } ?>
					 
				</p>
			</div>	
		<?php } } else { ?>
			<center> <?=$this->__("TMI_54")?> </center>
		<?php } ?>
	</div>	
	
	<!-- Logs -->
	<div id='tab-4' class='tab hide'>
		 <table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='tbl1 bold' colspan='3'> <?=$this->__("TMI_55")?> </td> </tr>	
			<?php
				djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbLog." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.log_user WHERE log_type='match' AND log_parent='%d' ORDER BY log_id DESC", $match['match_id']);
				foreach (djmDB::fullData() as $log) {
			?>
				<tr> 
					<td class='tbl1' width='120'> <?=strftime("%d.%m.%Y %H:%M", $log['log_time'])?> </td>
					<td class='tbl1' align='center'> <a href='<?=urlProfile.$log['log_user']?>'><?=$log['user_name']?></a> </td>
					<td class='tbl1'> <?=$log['log_value']?> </td>
				</tr>
			<?php } ?>
		</table>
	</div>
	
</div>	