<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Request.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("CreateRequest");
$this->Input("hidden", "parent", $match['match_id']);
?>
<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr>
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TMR_1")?>: </td>
		<td class='tbl1'>
		<?php
			djmDB::Select("SELECT * FROM ".dbRequestPattern);
		?>
			<select name='request_type' class='textbox' style='width: 220px;'>
				<option value=''></option>
				<?php foreach(djmDB::fullData() as $request) { ?>
					<?php if ($request['pattern_league'] == "") { ?>
						<option value='<?=$request['pattern_id']?>'><?=$request['pattern_name']?></option>
					<?php 
						} else { 
							$league = explode(".", $request['pattern_league']);
							if (in_array($match['league_id'], $league)) {
					?>
						<option value='<?=$request['pattern_id']?>'><?=$request['pattern_name']?></option>
					<?php } } ?>
				<?php } ?>
			</select>
		</td>
	</tr>
	<tr>
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TMR_2")?>: </td>
		<td class='tbl1'>
		<?php
			if ($myTeam == $t1['team_id']) { 
				djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbTeamPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user WHERE player_team='%d'", $t2['team_id']);
			} else {
				djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbTeamPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user WHERE player_team='%d'", $t1['team_id']);
			}
		?>
			<select name='request_user' class='textbox' style='width: 220px;'>
				<option value=''></option>
				<?php foreach(djmDB::fullData() as $player) { ?>
					<option value='<?=$player['player_user']?>'><?=$player['user_name']?></option>
				<?php }
				
					djmDB::Select("SELECT t1.*, t2.user_name 
											FROM ".dbMatchRequest." as t1 
											LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.request_type_guest 
											WHERE request_from_team='%d' AND request_type='guest' AND request_status='ACCEPT'", ($myTeam == $t1['team_id'] ? $t2['team_id'] : $t1['team_id']));
											
					foreach (djmDB::fullData() as $host) { 
						echo "<option value='".$host['request_type_guest']."'>".$host['user_name']." - ".$this->__("TMR_3")."</a>";
					}
				?>
			</select>
		</td>
	</tr>	
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMR_4"))?> <?=$this->__("TMR_5")?> <a href='<?=$page['location']?>'><?=$this->__("TMR_6")?></a> </td> </tr>
</table>
</form>