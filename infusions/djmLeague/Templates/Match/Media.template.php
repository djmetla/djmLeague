<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Media.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
	
	if (!$position) { 
		$this->setError($this->__("TMM_1"), urlMatch.$_GET['match']); 
	}

	if ($match['match_status'] == 3 || $match['match_status'] == 4) { 
			$this->setError($this->__("TMM_2"), urlMatch.$_GET['match']);
	}

	$this->Form("UploadMedia", "POST", null, true);	
	$this->Input("hidden", "parent", $match['match_id']);	
?>
<script type='text/javascript'>
	$(function(){
		$('input[name="media[]"]').tipsy({html: true, gravity: 'w' });
	});
</script>

<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> 
		<td class='tbl1' style='padding: 15px;' colspan='2'>
			<?=$this->__("TMM_3")?> <strong><?=$dbConfig['settings_upload_size']?> MB</strong>. 
		</td>
	</tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TMM_4")?>:</td> <td class='tbl1'> <input type='file' multiple name='media[]' class='textbox' title='<?=$this->__("TMM_5")?>' /> </td> </tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMM_6"))?> <?=$this->__("TMM_7")?> <a href='<?=urlMatch.$_GET['match']?>'><?=$this->__("TMM_8")?></a> </td> </tr>
</table>
</form>