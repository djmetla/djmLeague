<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Index.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

if (isset($_GET['league']) && isnum($_GET['league'])) {
 ?>
<!--League Information-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0' class='tbl-border'>
	<tr> <td class='bold tbl2' colspan='4'> <h2 style='padding:0; margin:0;'><?=$league['league_name']?></h2> </td> </tr>
	<tr> <td class='tbl2'>
		<p style='text-align:justify'><?=$this->__("TLI_1",$league['league_name'])?> <?=($league['league_enable'] == "YES" ? "<span class='green'>".$this->__("TLI_2")."</span>":"<span class='red'>".$this->__("TLI_3")."</span>")?> 
		<?=$this->__("TLI_4")?> <?=$this->Icon($league['game_icon'], "Game")?> <strong><?=$league['game_name']?></strong>. <?=$this->__("TLI_5")?> (<?=$this->__("TLI_6")?> <strong><?=strftime("%d.%m.%Y", $session[0])?></strong> <?=$this->__("TLI_7")?> <strong><?=strftime("%d.%m.%Y", $session[1])?></strong>)
		<?=$this->__("TLI_8")?> <strong><?=dbcount("(*)", dbTeam, "team_league='".$league['league_id']."'")?></strong> <?=$this->__("TLI_9")?> <a href='<?=$page['location']?>&amp;option=information'><?=$this->__("TLI_10")?></a>.
		<?=$this->__("TLI_11")?> <a href='<?=$page['location']?>&amp;option=rules'><?=$this->__("TLI_12")?></a>. <?php if ($league['league_account'] == "YES") { ?> <?=$this->__("TLI_13")?> <strong><?=$league['pattern_name']?></strong>. <?php } ?></p>
	</td></tr>
</table>
<br/>

<!--Team list-->
<?php echo "<div align='right' style=';margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $team_per_page, $rows_count, 3, $page['location']."&amp;")."\n</div>\n"; ?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border effect'>
		<tr> 
			<td class='forum-caption' align='center' width='20'> <?=$this->__("TLI_14")?> </td>
			<td class='forum-caption' align='center' width='250'> <?=$this->__("TLI_15")?> </td>
			<td class='forum-caption' align='center'> <?=$this->__("TLI_16")?> </td>
			<td class='forum-caption' align='center'> <?=$this->__("TLI_17")?> </td>
			<td class='forum-caption' align='center'> <?=$this->__("TLI_18")?> </td>
			<td class='forum-caption' align='center'> <?=$this->__("TLI_19")?> </td>
		</tr>	
		<?php 
			if ($team_count) {
			foreach ($teams as $team) {
		?>
		<tr>
			<td class='tbl1' align='center'> <?=$this->teamRank($team['team_id'], $league['league_id'])?> </td> 
			<td class='tbl1'> <?=$this->Icon($team['team_flag'], "Flags")?> <a href='<?=urlTeam.$team['team_id']?>' style='position:relative; top:-2px;'><?=$team['team_name']?></a> </td>
			<td class='tbl1' align='center'> <?=dbcount("(match_id)", dbMatch, "match_status !='3' AND (match_t1='".$team['team_id']."' OR match_t2='".$team['team_id']."')")?> </td>
			<td class='tbl1' align='center'> 
				<span class='green'><?=dbcount("(match_id)", dbMatch, "match_winner='".$team['team_id']."'")?></span> - 
				<span class='blue'><?=dbcount("(match_id)", dbMatch, "(match_t1='".$team['team_id']."' OR match_t2='".$team['team_id']."') AND match_draw='YES'")?></span> - 
				<span class='red'><?=dbcount("(match_id)", dbMatch, "match_losser='".$team['team_id']."'")?></span>
			</td>
			<td class='tbl1' align='center'> <?=$this->TeamPoints($team['team_id'], $team['team_points'])?> </td>
			<td class='tbl1' align='center'> <?=$this->TeamActivity($team['team_id'])?> </td>
		</tr>	
		<?php } } else { ?>
			<tr> <td class='tbl1' align='center' colspan='6'> <?=$this->__("TLI_20")?> </td> </tr>
		<?php } ?>
</table>
<?php echo "<div align='right' style=';margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $team_per_page, $rows_count, 3, $page['location']."&amp;")."\n</div>\n"; ?>


<?php }else{ ?>
<!--Games & Leagues-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<?php
		djmDB::Select("SELECT * FROM ".dbGame." ORDER BY game_id ASC");
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $game) {
	?>
		<tr> <td class='forum-caption' colspan='5'> <?=$this->Icon($game['game_icon'], "Game")?> <?=$game['game_name']?> </td> </tr>
		<tr> 
			<td class='tbl1 bold' align='center'> <?=$this->__("TLI_21")?> </td>
			<td class='tbl1 bold' align='center'> <?=$this->__("TLI_22")?> </td>
			<td class='tbl1 bold' align='center'> <?=$this->__("TLI_23")?> </td>
			<td class='tbl1 bold' align='center'> <?=$this->__("TLI_24")?> </td>
			<td class='tbl1 bold' align='center'> <?=$this->__("TLI_25")?> </td>
		</tr>	
		<?php
			djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_game='%d'", $game['game_id']);
			if (djmDB::Num()) {
			foreach (djmDB::fullData() as $league) {
		?>
		<tr> 
			<td class='tbl1' width='200'> <?=$this->Icon($game['game_icon'], "Game")?> <a href='<?=urlLeague.$league['league_id']?>'><?=$league['league_name']?></a></td> 
			<td class='tbl1' align='center'> <?php $session = explode("|", $league['league_session']); echo strftime("%d.%m.%Y", $session[0])." - ".strftime("%d.%m.%Y", $session[1]); ?> </td> 
			<td class='tbl1' align='center'> </td> 
			<td class='tbl1' align='center'> <?=($league['league_enable'] == "YES" ? "<span class='green'>".$this->__("TLI_26")."</span>":"<span class='red'>".$this->__("TLI_27")."</span>")?> </td>
			<td class='tbl1' align='center'> <a href='<?=$page['location']?>=<?=$league['league_id']?>&amp;option=rules'><?=$this->__("TLI_12")?></a> - <a href='<?=$page['location']?>=<?=$league['league_id']?>&amp;option=information'><?=$this->__("TLI_10")?></a> </td>
		</tr>
		<?php } } else { ?>
			<tr> <td class='tbl1' align='center' colspan='5'> <?=$this->__("TLI_28")?> </td> </tr>
		<?php } ?>
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center' colspan='5'> <?=$this->__("TLI_29")?> </td> </tr>
	<?php } ?>
</table>	
<?php } ?>