<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  Information.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
echo "<div style='padding:5px; text-align: right;'> <a href='".$page['location']."'>".$this->__("TLII_1")."</a> </div><hr/>";
echo parseubb(nl2br($information['league_information']));
echo "<hr/><div style='padding:5px; text-align: right;''> <a href='".$page['location']."'>".$this->__("TLII_1")."</a> </div>";
 ?>