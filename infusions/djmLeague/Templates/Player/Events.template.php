<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  Events.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<?php 
		if ($events_count) {
		foreach ($events as $event) {
	?>
		<tr> 
			<td class='tbl1 bold' align='center' width='100' height='50'> <?=$event['event_subject']?> <?=($event['event_read'] == 0 ? "<br/><span class='eventMark'>!</span>":"")?>  </td> 
			<td class='tbl1' valign='top'><span style='opacity: 0.5;'><?=strftime("%d.%m.%Y %H:%M", $event['event_time'])?></span> <br/> <?=$event['event_text']?> </td>
		</tr>
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center'><?=$this->__("TPE_1")?> </td> </tr>
	<?php } ?>
</table>
<?php echo "<div align='left' style='float:left;margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $event_per_page, $events_count_all, 6, $page['location']."&amp;option=events&amp;")."\n</div>\n"; ?>
 
<a href='<?=$page['location']?>&amp;option=events&amp;delete_all' style='float: right;margin: 10px 5px 0 0;'><?=$this->__("TPE_2")?></a>