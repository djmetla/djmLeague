<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Index.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>
 
<?php 
	djmDB::Select("SELECT * FROM ".dbBan." WHERE ban_user='%d' AND (ban_expiration>".time()." OR ban_permanent='YES')", $data['user_id']);
	if (djmDB::Num()) {
	$ban = djmDB::Data();
?>
 <!-- Bans -->
 <br/>
 <div style='border: 1px solid #ff0000; margin: 0 auto; padding: 10px; background-color: #ff0000; color: #ffffff; border-radius: 7px; text-align:center; width: 90%;'>
	<strong><?=$this->__("TPI_15")?></strong> <br/>
	<?=$this->__("TPI_16")?> <?=($ban['ban_permanent'] == "YES" ? $this->__("TPI_17"):$this->__("TPI_18"))?> <?=$this->__("TPI_19")?>: <?=$ban['ban_reason']?>.
 </div> <br/>
 <?php } ?>

 

<!--Information-->
<table cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
    <tr> <td class='forum-caption' colspan='2'> <?=$this->__("TPI_1")?> </td> </tr>
    <tr> <td class='tbl1' width='30%'> <?=$this->__("TPI_2")?>: </td> <td class='tbl1'> <?=$this->Icon($data['player_flag'], "Flags")?> <a href='<?=urlProfile.$data['user_id']?>'><?=$data['user_name']?></a> </tr> </tr>
    <tr> <td class='tbl1' width='30%'> <?=$this->__("TPI_3")?>: </td> <td class='tbl1'> <?=showdate("longdate", $data['player_time'])?> </tr> </tr>
    <tr> <td class='tbl1' width='30%'> <?=$this->__("TPI_4")?>: </td> <td class='tbl1'> <?=dbcount("(*)", dbTeamPlayer, "player_user='".$data['user_id']."'")?> </tr> </tr>
</table> 
<br/>

<!--Game Accounts-->
<table cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
    <tr> <td class='forum-caption' colspan='2'> <?=$this->__("TPI_5")?> </td> </tr>
	<?php 
		foreach($accountsPattern as $pattern) { 
		$GID = UserAccount($pattern['pattern_key'], $data['user_id']);
	?>
	
    <tr> 
		<td class='tbl1' width='25%'><?=$pattern['pattern_name']?>:</td>
		<td class='tbl1'>
			<?=($GID['account'] != "" ? $GID['account'] : $this->__("TPI_6"))?>
			<?=($GID['tooltip'] != "" ? $this->Tooltip($GID['tooltip'], true) : "")?>
		</td> 
		</tr>
	<?php } ?>
</table>    
<br/>

<!--My teams-->
<table cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
    <tr> <td class='forum-caption' colspan='4'><?=$this->__("TPI_7")?></td> </tr>
    <tr> 
        <td class='tbl1 bold'> <?=$this->__("TPI_8")?>  </td> 
        <td class='tbl1 bold' align='center'> <?=$this->__("TPI_9")?> </td> 
        <td class='tbl1 bold' width='20%' align='center'> <?=$this->__("TPI_10")?>  </td> 
        <td class='tbl1 bold' width='20%' align='center'> <?=$this->__("TPI_11")?> </td> 
    </tr>
    <?php
		djmDB::Select("SELECT * 
								FROM ".dbTeamPlayer." as t1 
								LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team
								LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t2.team_league
								LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
								WHERE player_user='%d'", $data['user_id']);
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $team) { 
	?>
    <tr>
        <td class='tbl1'> <?=$this->Icon($team['team_flag'], "Flags")?> <a href='<?=urlTeam.$team['team_id']?>'><?=$team['team_name']?></a> </td>
        <td class='tbl1' align='center'> <?=$this->Icon($team['game_icon'], "Game")?> <a href='<?=urlLeague.$team['league_id']?>'><?=$team['league_name']?></a> </td>
        <td class='tbl1' align='center'> <?=$team['team_points']?> </td>
        <td class='tbl1' align='center'> [<?=$team['player_position']?>] </td>
    </tr>
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center' colspan='4'> <?=$this->__("TPI_12")?></td> </tr>
	<?php } ?>
</table>   
<br/>

<!--Player logs -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'><?=$this->__("TPI_13")?></td> </tr>
	<tr> 
		<td>
			<?php
				echo "<div class='tbl-border' style='height: 140px; overflow: auto;'>";
					djmDB::Select("SELECT t1.*, t2.user_id, t2.user_name FROM ".dbLog." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.log_user WHERE log_type='player' AND log_parent='%d' ORDER BY log_id DESC", $data['user_id']);
					if (djmDB::Num()) { 
						foreach (djmDB::fullData() as $log) {
							echo "<div class='left tbl1' style='width: 98.5%; margin-bottom: 1px;'>";
								echo "<div class='left' style='width: 120px;'>".strftime("%d.%m.%Y %H:%M", $log['log_time'])."</div>";
								echo "<div class='left' style='width: 100px;'><a href='".urlProfile.$log['user_id']."'>".$log['user_name']."</a></div>";
								echo "<div class='left'>".$log['log_value']."</div>";
							echo "</div>";
						} 
					} else {
						echo "<center>".$this->__("TPI_14")."</center>";
					}
				echo "</div>";
			?>	
		</td>
	</tr>
</table>	