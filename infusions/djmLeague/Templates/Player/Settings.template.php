<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  Settings.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 $this->Form("National");
 ?>
 <fieldset style='border: 1px solid silver'>
	<legend><?=$this->__("TPS_1")?></legend>
	<table width='100%' cellspacing='1'>
		<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPS_2")?>:</td> <td class='tbl1'> <?=$this->Flags("player_national", $data['player_flag'])?> <?=$this->Icon($data['player_flag'], "Flags")?> </td> </tr>
		<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TPS_3"))?> </td> </tr>
	</table>
</fieldset>
</form>
<br/>

 
 <?=$this->Form("GameID")?>
 <fieldset style='border: 1px solid silver'>
	<legend><?=$this->__("TPS_4")?></legend>
	<table width='100%' cellspacing='1'>
		<?php
			djmDB::Select("SELECT * FROM ".dbAccountsPattern);
			if (djmDB::Num()) { 
			foreach (djmDB::fullData() as $pattern) {
				
		?>
			<tr>
				<td class='tbl1' align='right' width='30%'> <?=$pattern['pattern_name']?>:</td>
				<td class='tbl1'> 
				
					<?php 
						djmDB::Select("SELECT * FROM ".dbAccounts." WHERE account_key='%s' AND account_user='%d'", array($pattern['pattern_key'], $data['user_id']));
						if (djmDB::Num()) {
							$userAcc = djmDB::Data();
							if ($userAcc['account_new_value'] != "") {
								// Waiting for accept 
								echo "<span class='red'>".$this->__("TPS_5")."</span>";
							} else {
								if (($userAcc['account_last_change'] + $pattern['pattern_limit']) > time()) {
									// Change disabled reason low time for change
									echo $userAcc['account_value'];
									$this->Tooltip($this->__("TPS_6", round($pattern['pattern_limit'] / 60 /60)), true);
								} else {
									// Change enabled
									$this->Input("hidden", "game_ids[]", $pattern['pattern_key']);
									$this->Input("text", "id_".$pattern['pattern_key'], $userAcc['account_value'], $this->__("TPS_7", $pattern['pattern_name']));
									$this->Tooltip("<strong>".$this->__("TPS_8", $pattern['pattern_name']).":</strong> <br/><br/> ".($pattern['pattern_information'] != "" ? $pattern['pattern_information'] : $this->__("TPS_9")), true);								
								}
							}
						} else {
						?>
							<?=$this->Input("hidden", "game_ids[]", $pattern['pattern_key'])?>
							<?=$this->Input("text", "id_".$pattern['pattern_key'], null, $this->__("TPS_7", $pattern['pattern_name']))?>
							<?=$this->Tooltip("<strong>".$this->__("TPS_8", $pattern['pattern_name']).":</strong> <br/><br/> ".($pattern['pattern_information'] != "" ? $pattern['pattern_information'] : $this->__("TPS_9")), true)?>
							
						<?php } ?>
				</td>
			</tr>
		<?php } ?>
		<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TPS_10"))?> </td> </tr>
		<?php  } else { ?>
			<tr> <td class='tbl1' align='center'> <?=$this->__("TPS_11")?> </td> </tr>
		<?php } ?>
		
	</table>	
</fieldset>
</form>
<br/>

<?=$this->Form("ChangeSound")?>
 <fieldset style='border: 1px solid silver'>
	<legend><?=$this->__("TPS_17")?></legend>
	<table width='100%' cellspacing='1'>
		<tr> 
			<td class='tbl1' width='30%' align='right'> <?=$this->__("TPS_13")?>:</td> 
			<td class='tbl1'>
				<select name='player_sound' class='textbox'>
					<option value='1.wav' <?=($data['player_sound'] == "1.wav" ? "selected='selected'":"")?>><?=$this->__("TPS_14")?></option>
					<option value='2.wav' <?=($data['player_sound'] == "2.wav" ? "selected='selected'":"")?>><?=$this->__("TPS_15")?></option>
					<option value='3.wav' <?=($data['player_sound'] == "3.wav" ? "selected='selected'":"")?>><?=$this->__("TPS_16")?></option>
				</select>
				<?=$this->Tooltip($this->__("TPS_19"), true)?>
			</td> 
		</tr>
		<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TPS_18"))?> </td> </tr>
	</table>
</fieldset>
</form>
<br/>

<div style='padding: 5px;'> <a href='<?=$page['location']?>'><?=$this->__("TPS_12")?></a> </div>