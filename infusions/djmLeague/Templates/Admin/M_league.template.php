<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_league.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>

<?php if (isset($_GET['manage_league']) && isnum($_GET['manage_league'])) { ?>

<ul class='tabs-navi'>
	<li id='tab1' name='Statistics' class='hover button'> <?=$this->__("TLEA_1")?> </li>
	<li id='tab2' name='Edit' class='button'> <?=$this->__("TLEA_2")?> </li>
	<li id='tab3' name='Rules' class='button'> <?=$this->__("TLEA_3")?> </li>
	<li id='tab4' name='Information' class='button'> <?=$this->__("TLEA_4")?> </li>
	<li id='tab5' name='Tools' class='button'> <?=$this->__("TLEA_5")?> </li>
</ul>
<div class='tabs-content tbl-border'>
	
	<!-- Index -->
	<div id='tab-1' class='tab'>
		<h3> <?=$this->__("TLEA_6")?> </h3>
		<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TLEA_7")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_8")?>: </td> <td class='tbl1'> <?=$league['league_name']?> <a href='#Edit' class='go_to_Tab right'><?=$this->__("TLEA_9")?></a> </td> </tr> 
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_10")?>: </td> <td class='tbl1'> <?=$this->__("TLEA_11")?> <?=($league['league_enable'] == "YES" ? "<span class='green bold'>".$this->__("TLEA_12")."</span>":"<span class='red bold'>".$this->__("TLEA_13")."</span>")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_14")?>: </td> <td class='tbl1'> <?=$this->Icon($league['game_icon'], "Game")?> <?=$league['game_name']?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_15")?>: </td> 
					<td class='tbl1'>
						<?php $session = explode("|", $league['league_session']); ?>
						<?=$this->__("TLEA_16")?>: <strong><?=strftime("%d.%m.%Y", $session[0])?></strong> 
						<?=$this->__("TLEA_17")?>: <strong><?=strftime("%d.%m.%Y", $session[1])?></strong> <br/><br/>
						<?php if ($session[1] > time()) { ?>
						<?php
							$differents = $session[1] - $session[0];
							$total_days = round((($differents / 60) / 60) / 24) + 1;
							$differents_cday = time() - $session[0];
							$current_day = round((($differents_cday / 60) / 60) / 24)+1;						
						?>
						<?=$this->__("TLEA_18")?>: <strong><?=$total_days?></strong> <br/>
						<?=$this->__("TLEA_19")?>: <strong><?=$current_day?>.</strong><br/>
						<?=$this->__("TLEA_20")?>: <span class='red bold'><?=$total_days-$current_day?></span> <?=$this->__("TLEA_21")?>
						<?php } ?>
					</td> 
			</tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_22")?>: </td>
					<td class='tbl1'> 
					<?php 
						if ($league['league_account'] == "YES") {
							djmDB::Select("SELECT * FROM ".dbAccountsPattern." WHERE pattern_key='%s'", $league['league_account_key']);
							$account = djmDB::Data();
							echo "<span class='green'>".$this->__("TLEA_23")."</span> ".$this->__("TLEA_24")." <strong>".$account['pattern_name']."</strong>.";
						} else {
							echo $this->__("TLEA_25");
						}
					?>
					</td> 
			</tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_26")?>: </td> <td class='tbl1'> <strong><?=$league['league_default_points']?></strong> <?=$this->__("TLEA_27")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_28")?>: </td> 
					<td class='tbl1'>
						<?=$this->__("TLEA_29")?> <strong><?=$league['league_team_min']?></strong>. <?=$this->__("TLEA_30")?> <strong><?=$league['league_team_max']?></strong>.
					</td> 
			</tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_31")?>: </td> 
					<td class='tbl1'> 
						<?php
							if ($league['league_information'] != "") {
								echo "<span class='green'>".$this->__("TLEA_32")."</span>. ".$this->__("TLEA_33")." ".strftime("%d.%m.%Y %H:%M", $league['league_information_time']);
							} else {
								echo "<span class='red'>".$this->__("TLEA_34")."</span>";
							}
						?>
					</td> 
			</tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_35")?>: </td> 
					<td class='tbl1'>
						<?php
							if ($league['league_rules'] != "") {
								echo "<span class='green'>".$this->__("TLEA_32")."</span>. ".$this->__("TLEA_33")." ".strftime("%d.%m.%Y %H:%M", $league['league_rules_time']);
							} else {
								echo "<span class='red'>".$this->__("TLEA_36")."</span>";
							}
						?>					
					</td> 
			</tr>
		</table>
		<br/>
		
		<?php
			/* Current day start */
			$cday = mktime(0,0,0, date("m"), date("d"), date("Y"));
			$cweek = time()-604800;
			$cmonth =  time()-2678000;
			
			/* STATISTICS */
			$id = $league['league_id'];
			$teams = dbcount("(team_id)", dbTeam, "team_league='".$id."'");
			$teams_up_1000 = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_points>1000");
			$teams_do_1000 = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_points<1000");
			$teams_active = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_status='ACTIVE'");
			$teams_inactive = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_status='INACTIVE'");
			@$teams_active_ratio = round(($teams_active / $teams) * 100);
			$teams_cday = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_created BETWEEN ".$cday." AND ".time()."");
			$teams_cweek = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_created BETWEEN ".$cweek." AND ".time()."");
			$teams_cmonth = dbcount("(team_id)", dbTeam, "team_league='".$id."' AND team_created BETWEEN ".$cmonth." AND ".time()."");
		
		
			$match = dbcount("(match_id)", dbMatch, "match_league='".$id."'");
			$match_close = dbcount("(match_id)", dbMatch, "match_league='".$id."' AND match_status='3'");
			$match_unclose = dbcount("(match_id)", dbMatch, "match_league='".$id."' AND (match_status!='3' OR match_status!='4')");
			@$match_close_ratio = round(($match_close/ $match) * 100);
			$match_cday = dbcount("(match_id)", dbMatch, "match_league='".$id."' AND match_time BETWEEN ".$cday." AND ".time()."");
			$match_cweek = dbcount("(match_id)", dbMatch, "match_league='".$id."' AND match_time BETWEEN ".$cweek." AND ".time()."");
			$match_cmonth = dbcount("(match_id)", dbMatch, "match_league='".$id."' AND match_time BETWEEN ".$cmonth." AND ".time()."");
			
			djmDB::Select("SELECT COUNT(t1.protest_id) as num FROM ".dbProtest." as t1, (SELECT team_id FROM ".dbTeam." WHERE team_league='%d') as t2 WHERE protest_from_team=t2.team_id", $id);
			$protest = djmDB::Data('num');
			djmDB::Select("SELECT COUNT(t1.protest_id) as num FROM ".dbProtest." as t1, (SELECT team_id FROM ".dbTeam." WHERE team_league='%d') as t2 WHERE protest_from_team=t2.team_id AND protest_status='2'", $id);
			$protest_close = djmDB::Data('num');		
			djmDB::Select("SELECT COUNT(t1.protest_id) as num FROM ".dbProtest." as t1, (SELECT team_id FROM ".dbTeam." WHERE team_league='%d') as t2 WHERE protest_from_team=t2.team_id AND protest_status!='2'", $id);
			$protest_unclose = djmDB::Data('num');		
			@$protest_close_ratio = round(($protest_close / $protest) * 100);
			
			$admins = dbcount("(admin_id)", dbAdmin, "admin_league IN ('".$id."')");
			
			djmDB::Select("SELECT COUNT(t1.request_id) as num FROM ".dbRequest." as t1, (SELECT team_id FROM ".dbTeam." WHERE team_league='%d') as t2 WHERE request_author_team=t2.team_id", $id);
			$request = djmDB::Data('num');
						
		?>
		
		
		<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TLEA_37")?> </td> </tr>
			<tr> <td class='tbl1 bold' colspan='2'><?=$this->__("TLEA_38")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_39")?>: </td> <td class='tbl1'><?=$teams?></td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_40")?>: </td> <td class='tbl1'><?=$teams_up_1000?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_41")?>: </td> <td class='tbl1'><?=$teams_do_1000?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_42")?>: </td> <td class='tbl1'><span class='green'><?=$teams_active?></span>/<span class='red'><?=$teams_inactive?></span> (<?=$teams_active_ratio?>%) </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_43")?>: </td> <td class='tbl1'><?=$teams_cday?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_44")?>: </td> <td class='tbl1'><?=$teams_cweek?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_45")?>: </td> <td class='tbl1'><?=$teams_cmonth?> </td> </tr>
			
			<tr> <td class='tbl1 bold' colspan='2'> <?=$this->__("TLEA_46")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_47")?>: </td> <td class='tbl1'><?=$match?> </td> </tr>			
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_48")?>: </td> <td class='tbl1'><span class='green'><?=$match_close?></span>/<span class='red'><?=$match_unclose?></span> (<?=$match_close_ratio?>%) </td> </tr>			
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_49")?>: </td> <td class='tbl1'><?=$match_cday?> </td> </tr>			
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_50")?>: </td> <td class='tbl1'><?=$match_cweek?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_51")?>: </td> <td class='tbl1'><?=$match_cmonth?> </td> </tr>
			
			<tr> <td class='tbl1 bold' colspan='2'> <?=$this->__("TLEA_52")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_53")?>: </td> <td class='tbl1'><?=$protest?> </td> </tr>		
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_54")?>: </td> <td class='tbl1'><span class='green'><?=$protest_close?></span>/<span class='red'><?=$protest_unclose?></span> (<?=$protest_close_ratio?>%) </td> </tr>		
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_55")?>: </td> <td class='tbl1'><?=$admins?> </td> </tr>		
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_56")?>: </td> <td class='tbl1'><?=$request?> </td> </tr>	

			<tr> <td class='tbl1 bold' colspan='2'> <?=$this->__("TLEA_57")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_58")?>: </td> <td class='tbl1'><?=$folder?> </td> </tr>				
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_59")?>: </td> <td class='tbl1'><?=sizeFormat($dir['size'])?> </td> </tr>				
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_60")?>: </td> <td class='tbl1'><?=$dir['dircount']?> </td> </tr>				
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_61")?>: </td> <td class='tbl1'><?=$dir['count']?> </td> </tr>						
		</table>			
	</div>
	
	<!-- Edit -->
	<div id='tab-2' class='tab hide'>
		<h3><?=$this->__("TLEA_62")?></h3>
		<?php $this->Form("EditLeague");  $this->Input("hidden", "parent", $league['league_id']); $this->Input("hidden", "league_name", $league['league_name']);?>
		<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TLEA_63")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_64")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_name", $league['league_name'], $this->__("TLEA_64"))?> <?=$this->Tooltip($this->__("TLEA_65"),true)?></td> </tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_66")?>:</td> 
				<td class='tbl1'> 
					<?php
						djmDB::Select("SELECT * FROM ".dbGame);
						if (djmDB::Num()) {
					?>
					<select name='league_game' class='textbox'>
						<?php foreach (djmDB::fullData() as $game) { ?>
							<option value='<?=$game['game_id']?>' <?=($league['league_game'] == $game['game_id'] ? "selected='selected'":"")?>><?=$game['game_name']?></option>
						<?php } ?>
					</select>
					<?php } else { ?>
						<span class='red'> <?=$this->__("TLEA_67")?> </span>
					<?php }?>
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_68")?>:</td> 
				<td class='tbl1'> 
					<?=$this->__("TLEA_69")?>
				</td> 
			</tr>
			
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_70")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_default_points", $league['league_default_points'], $this->__("TLEA_71"))?> <?=$this->Tooltip($this->__("TLEA_72"), true)?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_73")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_max_player", $league['league_team_max'], $this->__("TLEA_71"))?> <?=$this->Tooltip($this->__("TLEA_74"), true)?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_75")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_min_player", $league['league_team_min'], $this->__("TLEA_71"))?> <?=$this->Tooltip($this->__("TLEA_76"), true)?> </td> </tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_77")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_block_status", ($league['league_block_status'] /60 /60 /24), $this->__("TLEA_71"))?> <?=$this->__("TLEA_78")?>
					<?=$this->Tooltip($this->__("TLEA_79"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_80")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("radio", "league_use_maps", "YES", null, ($league['league_match_use_maps'] == "YES" ? true:false))?><?=$this->__("TLEA_81")?> <?=$this->Input("radio", "league_use_maps", "NO", null, ($league['league_match_use_maps'] == "NO" ? true:false))?><?=$this->__("TLEA_82")?>
					<?=$this->Tooltip($this->__("TLEA_83"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_84")?>:</td> 
				<td class='tbl1'>
					<textarea name='league_maps' class='textbox' placeholder='<?=$this->__("TLEA_85")?>' style='width:435px;height:50px;'><?=str_replace(".",", ", $league['league_match_maps'])?></textarea><br/> <?=$this->__("TLEA_86")?>
					<?=$this->Tooltip($this->__("TLEA_87"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_88")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_match_max_score", $league['league_match_max_score'], $this->__("TLEA_71"))?> 
					<?=$this->Tooltip($this->__("TLEA_89"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> (ELO) <?=$this->__("TLEA_90")?>:</td> 
				<td class='tbl1'> 
					<select name='league_elo' class='textbox' style='width:240px;'>
						<option value='default' <?=($league['league_match_elo_type'] == "default" ? "selected='selected'":"")?>> <?=$this->__("TLEA_91")?> </option>
						<option value='classic' <?=($league['league_match_elo_type'] == "classic" ? "selected='selected'":"")?>> <?=$this->__("TLEA_92")?> </option>
						<?php 
							djmDB::Select("SELECT * FROM ".dbElo." ORDER BY elo_id ASC"); 
							foreach(djmDB::fullData() as $elo) {
								echo "<option value='".$elo['elo_id']."' ".($league['league_match_elo'] == $elo['elo_id'] ? "selected='selected'":"").">".$this->__("TLEA_93").": ".$elo['elo_name']." (".$this->__("TLEA_94").": ".$elo['elo_value'].")</option>";
							}
						?>
					</select>
					<?=$this->Tooltip($this->__("TLEA_95"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_96")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("radio", "league_match_use_server", "YES", null, ($league['league_match_use_server'] == "YES" ? true:false))?><?=$this->__("TLEA_81")?>  <?=$this->Input("radio", "league_match_use_server", "NO", null, ($league['league_match_use_server'] == "NO" ? true:false))?><?=$this->__("TLEA_82")?>
					<?=$this->Tooltip($this->__("TLEA_97"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_98")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_match_server_time", ($league['league_match_server_time'] /60), $this->__("TLEA_99"))?> 
					<select class='textbox' name='league_match_server_time_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>
					<?=$this->Tooltip($this->__("TLEA_102"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_103")?>:</td> 
				<td class='tbl1'>
					<?=$this->Input("radio", "league_match_use_host", "YES", null, ($league['league_match_host'] == "YES" ? true:false))?><?=$this->__("TLEA_104")?>  <?=$this->Input("radio", "league_match_use_host", "NO", null, ($league['league_match_host'] == "NO" ? true:false))?><?=$this->__("TLEA_105")?>
					<?=$this->Tooltip($this->__("TLEA_106"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_107")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_match_host_time", ($league['league_match_host_time'] / 60), $this->__("TLEA_71"))?> (<?=$this->__("TLEA_108")?>)
					<?=$this->Tooltip($this->__("TLEA_109"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_110")?>:</td>
				<td class='tbl1'> 
					<select name='league_match_host_num' class='textbox'> <?php for($i=1; $i<=10; $i++) { ?> <option value='<?=$i?>' <?=($league['league_match_host_num'] == $i ? "selected='selected'":"")?>><?=$i?></option><?php } ?></select>
					<?=$this->Tooltip($this->__("TLEA_111"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_112")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("radio", "league_match_delete", "YES", null, ($league['league_match_delete'] == "YES" ? true:false))?><?=$this->__("TLEA_104")?> <?=$this->Input("radio", "league_match_delete", "NO", null, ($league['league_match_delete'] == "NO" ? true:false))?><?=$this->__("TLEA_105")?>
					<?=$this->Tooltip($this->__("TLEA_113"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_114")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_match_delete_time", ($league['league_match_delete_time'] / 60), $this->__("TLEA_71"))?> (<?=$this->__("TLEA_108")?>)
					<?=$this->Tooltip($this->__("TLEA_115"), true)?> 
				</td> 
			</tr>
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_116")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_match_request_delay", ($league['league_match_request_delay'] / 60), $this->__("TLEA_99"))?> 
					<select class='textbox' name='league_match_request_delay_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>			
					<?=$this->Tooltip($this->__("TLEA_117"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_118")?>:</td> 
				<td class='tbl1'>
					<?=$this->Input("text", "league_match_request_delay_stop",  ($league['league_match_request_delay_stop'] / 60), $this->__("TLEA_99"))?> 
					<select class='textbox' name='league_match_request_delay_stop_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>	
					<?=$this->Tooltip($this->__("TLEA_119"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_120")?>:</td> 
				<td class='tbl1'>
					<?=$this->Input("text", "league_match_protest_delay",  ($league['league_match_protest_delay'] / 60), $this->__("TLEA_99"))?> 
					<select class='textbox' name='league_match_protest_delay_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>		
					<?=$this->Tooltip($this->__("TLEA_121"), true)?> 
				</td> 
			</tr>
			
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_122")?>:</td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "league_match_protest_delay_stop",  ($league['league_match_protest_delay_stop'] / 60), $this->__("TLEA_99"))?> 
					<select class='textbox' name='league_match_protest_delay_stop_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>			
					<?=$this->Tooltip($this->__("TLEA_123"), true)?> 
				</td> 
			</tr>			
					
			<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TLEA_124"))?> </td> </tr>	
		</table>
		</form>
	</div>
	
	
	<!-- Edit rules -->
	<div id='tab-3' class='tab hide'>
		<h3> <?=$this->__("TLEA_125")?> </h3>
		<?php
			$this->form("EditRules", "POST", "EditRules");
			$this->Input("hidden", "parent", $league['league_id']);
			$this->Input("hidden", "league_name", $league['league_name']);
		?>
		<textarea name='league_rules' class='textbox' style='width: 100%; height: 400px;' placeholder='<?=$this->__("TLEA_126")?>'><?=$league['league_rules']?></textarea> <br/>
		<?php 
			include_once(INCLUDES."bbcode_include.php");
			echo "<div class='right'>".display_bbcodes("330px", "league_rules", "EditRules")."</div>";
		?>
		<?=$this->Input("submit", "submit", $this->__("TLEA_127"))?>
		</form>
	</div>	
	
	
	<!-- Edit information -->
	<div id='tab-4' class='tab hide'>
		<h3> <?=$this->__("TLEA_128")?> </h3>
		<?php
			$this->form("EditInformation", "POST", "EditInformation");
			$this->Input("hidden", "parent", $league['league_id']);
			$this->Input("hidden", "league_name", $league['league_name']);
		?>
		<textarea name='league_information' class='textbox' style='width: 100%; height: 400px;' placeholder='<?=$this->__("TLEA_129")?>'><?=$league['league_information']?></textarea> <br/>
		<?php 
			echo "<div class='right'>".display_bbcodes("330px", "league_information", "EditInformation")."</div>";
		?>
		<?=$this->Input("submit", "submit", $this->__("TLEA_130"))?>
		</form>
	</div>	
	
	<!-- Tools -->
	<div id='tab-5' class='tab hide'>
		<h3><?=$this->__("TLEA_131")?></h3>
		<div style='color: #fff; margin: 0 auto; width: 650px; border-radius: 5px; padding: 10px; background: #ff0000;'>
			<strong><?=$this->__("TLEA_132")?></strong><br/> 
			<?=$this->__("TLEA_133")?>
		</div>
	
		<ul style='list-style: none; margin: 2px; padding: 0px;'>
			<li class='tbl1' style='margin: 15px; border-radius: 3px;'> 
				<strong> <?=$this->__("TLEA_134")?> </strong>
				<?php if ($league['league_enable'] == "YES") { ?>
				<p><?=$this->__("TLEA_135")?></p>
					<?=$this->Form("DeactivateLeague")?>
					<?=$this->Input("hidden", "parent", $league['league_id'])?>
					<?=$this->Input("hidden", "name", $league['league_name'])?>
					<?=$this->Input("submit", "submit", $this->__("TLEA_136"))?>
					</form>
				<?php } else { ?>
				<p><?=$this->__("TLEA_137")?></p>
					<?=$this->Form("ActivateLeague")?>
					<?=$this->Input("hidden", "parent", $league['league_id'])?>
					<?=$this->Input("hidden", "name", $league['league_name'])?>
					<?=$this->Input("submit", "submit", $this->__("TLEA_138"))?>
					</form>
				<?php } ?>
			</li>
			
			<li class='tbl1' style='margin: 15px; border-radius: 3px;'> 
				<strong> <?=$this->__("TLEA_139")?> </strong>
				<p> <?=$this->__("TLEA_140")?> </p>
					
					<ul>
						<li> <?=$this->__("TLEA_141")?>: <strong><?=$folder?></strong> </li>
						<li> <?=$this->__("TLEA_142")?>: <strong><?=sizeFormat($dir['size'])?></strong> </li>
						<li> <?=$this->__("TLEA_143")?>: <strong><?=$dir['dircount']?></strong> <em><?=$this->__("TLEA_144")?></em> </li>
						<li> <?=$this->__("TLEA_145")?>: <strong><?=$dir['count']?></strong>  </li>
					</ul>					
				</p>
				<p> <?=$this->__("TLEA_146")?> <span class='red bold'><?=$this->__("TLEA_147")?></span> </p>
					<?=$this->Form("ClearMedia")?>
					<?=$this->Input("hidden", "parent", $league['league_id'])?>
					<?=$this->Input("submit", "submit", $this->__("TLEA_148"))?>
					</form>
			</li>			
			
			<li class='tbl1' style='margin: 15px; border-radius: 3px;'> 
				<strong> <?=$this->__("TLEA_149")?> </strong>
				<p><?=$this->__("TLEA_150")?></p>
				
					<?php
						$this->Form("ChangeGameID");
						$this->Input("hidden", "parent", $league['league_id']);
						djmDB::Select("SELECT * FROM ".dbAccountsPattern."");
						echo "<select name='gameID_id' class='textbox' style='width: 225px;'>";
							foreach (djmDB::fullData() as $gid) { echo "<option value='".$gid['pattern_id']."'>".$gid['pattern_name']."</option>"; }
						echo "</select> <br/>";
						echo "<textarea name='gameID_message' style='width: 440px; height: 130px;' class='textbox' placeholder='".$this->__("TLEA_151")."'></textarea><br/>";
						$this->Input("submit", "submit", $this->__("TLEA_149"));
						echo "</form>";
					?>				
			</li>
			
			<li class='tbl1' style='margin: 15px; border-radius: 3px;'> 
				<strong> <?=$this->__("TLEA_152")?> </strong>
				<p> <?=$this->__("TLEA_153")?>: (<?=$league['league_default_points']?>). 
				
					<ul>
						<li class='bold'> <?=$this->__("TLEA_154")?></li>
						<li> <?=$this->__("TLEA_155")?> </li>
						<li> <?=$this->__("TLEA_156")?> </li>
					</ul>
					<br/>
					<ul>
						<li class='bold'> <?=$this->__("TLEA_157")?> </li>
						<li> <?=$this->__("TLEA_158")?> </li>
						<li> <?=$this->__("TLEA_159")?> </li>
						<li> <?=$this->__("TLEA_160")?> </li>
						<li> <?=$this->__("TLEA_161")?> </li>
						<li> <?=$this->__("TLEA_162")?> </li>
						<li> <?=$this->__("TLEA_163")?> </li>
					</ul>	
					<br/>
					<span class='bold red'><?=$this->__("TLEA_164")?></span>
				</p>
					<?=$this->Form("RestartLeague")?>
					<?=$this->Input("hidden", "parent", $league['league_id'])?>
					<?=$this->Input("submit", "submit", $this->__("TLEA_152"))?>
					</form>
			</li>		
			
			<li class='tbl1' style='margin: 15px; border-radius: 3px;'> 
				<strong> <?=$this->__("TLEA_165")?> </strong>
				<p class='bold red'><?=$this->__("TLEA_166")?></p>
					<?=$this->Form("DeleteLeague")?>
					<?=$this->Input("hidden", "parent", $league['league_id'])?>
					<?=$this->Input("submit", "submit", $this->__("TLEA_167"))?>				
					</form>
			</li>					
		</ul>	
	</div>
	
</div>

<?php } else { ?>
<!--League list-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
	<tr> <td colspan='4' class='forum-caption'> <?=$this->__("TLEA_168")?> </td> </tr>
	<?php 
		djmDB::Select("SELECT * FROM ".dbGame);
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $game) {
	?>
	<tr> <td colspan='4' class='tbl2 bold'> <?=$this->Icon($game['game_icon'], "Game")?> <?=$game['game_name']?></td> </tr>
		<?php
			djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_game='%d'", $game['game_id']);
			if (djmDB::Num()) {
			foreach(djmDB::fullData() as $league) { 
		?>	
			<tr> 
				<td class='tbl1 bold' width='350'> &nbsp;<?=THEME_BULLET?>&nbsp;<?=$league['league_name']?> </td>
				<td class='tbl1' align='center'> 
					<?php 
					$session = explode("|", $league['league_session']); 
					echo strftime("%d.%m.%Y", $session[0])." - ".strftime("%d.%m.%Y", $session[1]); 
					
					$differents = $session[1] - $session[0];
					$total_days = round((($differents / 60) / 60) / 24) + 1;
					$differents_cday = time() - $session[0];
					$current_day = round((($differents_cday / 60) / 60) / 24)+1;

					$this->Tooltip("<strong>".$this->__("TLEA_169")."</strong> <br/>
											".$this->__("TLEA_170").": <strong>".$current_day.".</strong> <br/>
											".$this->__("TLEA_171").": <strong>".$total_days." </strong> <br/>
											".$this->__("TLEA_172").": <strong>".strftime("%d.%m.%Y", $session[0])."</strong> <br/> 
											".$this->__("TLEA_173").": <strong>".strftime("%d.%m.%Y", $session[1])."</strong> <br/> 
					"); ?> 
				</td>
				<td class='tbl1' align='center'> <?=($league['league_enable'] == "YES" ? "<span class='green'>".$this->__("TLEA_174")."</span>":"<span class='red'>".$this->__("TLEA_175")."</span>")?> <?=$this->Tooltip($this->__("TLEA_176"))?> </td>
				<td class='tbl1' align='center' width='100'> <a href='<?=$page['location']?>&amp;manage_league=<?=$league['league_id']?>'><?=$this->__("TLEA_177")?></a> </td>
			</tr>
		<?php } } else { ?>
			<tr> <td class='tbl1' align='center' colspan='4'> <?=$this->__("TLEA_178")?> </td> </tr>
		<?php } ?>
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center' colspan='4'> <?=$this->__("TLEA_179")?> <?=($this->isAdmin("ACCESS_M_GAME") ? "<br/> <a href='".urlAdmin."m_game'>".$this->__("TLEA_180")."</a>":"")?> </td> </tr>
	<?php } ?>
</table>	
<br/>


<?php $this->Form("CreateLeague"); ?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TLEA_180")?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_64")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_name", null, $this->__("TLEA_64"))?> <?=$this->Tooltip($this->__("TLEA_65"),true)?></td> </tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_66")?>:</td> 
		<td class='tbl1'> 
			<?php
				djmDB::Select("SELECT * FROM ".dbGame);
				if (djmDB::Num()) {
			?>
			<select name='league_game' class='textbox'>
				<option value=''></option>
				<?php foreach (djmDB::fullData() as $game) { ?>
					<option value='<?=$game['game_id']?>'><?=$game['game_name']?></option>
				<?php } ?>
			</select>
			<?php } else { ?>
				<span class='red'> <?=$this->__("TLEA_67")?> </span>
			<?php }?>
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_182")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Tooltip($this->__("TLEA_183"), true)?>
			<strong><?=$this->__("TLEA_16")?>:</strong> <?=SelectTime::Create("league_session_start", "d|m|y")?> <br/>
			<strong><?=$this->__("TLEA_17")?>:</strong> <?=SelectTime::Create("league_session_end", "d|m|y", 7948800)?>
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_68")?>:</td> 
		<td class='tbl1'> 
			<select name='league_account' class='textbox'>
				<option value='NO'><?=$this->__("TLEA_184")?></option>
				<?php
					djmDB::Select("SELECT * FROM ".dbAccountsPattern);
					foreach (djmDB::fullData() as $account) {
				?>
					<option value='<?=$account['pattern_key']?>'><?=$account['pattern_name']?></option>
				<?php } ?>
			</select> <?=$this->Tooltip($this->__("TLEA_185"), true)?>
		</td> 
	</tr>
	
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_70")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_default_points", 1000, $this->__("TLEA_71"))?> <?=$this->Tooltip($this->__("TLEA_72"), true)?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_73")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_max_player", null, $this->__("TLEA_71"))?> <?=$this->Tooltip($this->__("TLEA_74"), true)?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_75")?>:</td> <td class='tbl1'> <?=$this->Input("text", "league_min_player", null, $this->__("TLEA_71"))?> <?=$this->Tooltip($this->__("TLEA_76"), true)?> </td> </tr>

	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_77")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_block_status", null, $this->__("TLEA_71"))?> (<?=$this->__("TLEA_78")?>)
			<?=$this->Tooltip($this->__("TLEA_79"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_80")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("radio", "league_use_maps", "YES", null, true)?><?=$this->__("TLEA_81")?> <?=$this->Input("radio", "league_use_maps", "NO")?><?=$this->__("TLEA_82")?>
			<?=$this->Tooltip($this->__("TLEA_83"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_84")?>:</td> 
		<td class='tbl1'>
			<textarea name='league_maps' class='textbox' placeholder='<?=$this->__("TLEA_85")?>' style='width:435px;height:50px;'></textarea><br/> (<?=$this->__("TLEA_86")?>)
			<?=$this->Tooltip($this->__("TLEA_87"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_88")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_match_max_score", null, $this->__("TLEA_71"))?> 
			<?=$this->Tooltip($this->__("TLEA_89"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> (ELO) <?=$this->__("TLEA_90")?>:</td> 
		<td class='tbl1'> 
			<select name='league_elo' class='textbox' style='width:220px;'>
				<option value='default'> <?=$this->__("TLEA_91")?> </option>
				<option value='classic'> <?=$this->__("TLEA_92")?> </option>
				<?php 
					djmDB::Select("SELECT * FROM ".dbElo." ORDER BY elo_id ASC"); 
					foreach(djmDB::fullData() as $elo) {
						echo "<option value='".$elo['elo_id']."'>".$this->__("TLEA_93").": ".$elo['elo_name']." (".$this->__("TLEA_94").": ".$elo['elo_value'].")</option>";
					}
				?>
			</select>
			<?=$this->Tooltip($this->__("TLEA_95"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_96")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("radio", "league_match_use_server", "YES", null, true)?><?=$this->__("TLEA_81")?>  <?=$this->Input("radio", "league_match_use_server", "NO")?><?=$this->__("TLEA_82")?>
			<?=$this->Tooltip($this->__("TLEA_97"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_98")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_match_server_time", null, $this->__("TLEA_99"))?> 
			<select class='textbox' name='league_match_server_time_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>
			<?=$this->Tooltip($this->__("TLEA_102"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_103")?>:</td> 
		<td class='tbl1'>
			<?=$this->Input("radio", "league_match_use_host", "YES", null, true)?><?=$this->__("TLEA_104")?> <?=$this->Input("radio", "league_match_use_host", "NO")?><?=$this->__("TLEA_105")?>
			<?=$this->Tooltip($this->__("TLEA_106"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_107")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_match_host_time", null, $this->__("TLEA_71"))?> (<?=$this->__("TLEA_108")?>)
			<?=$this->Tooltip($this->__("TLEA_109"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_110")?>:</td>
		<td class='tbl1'> 
			<select name='league_match_host_num' class='textbox'> <?php for($i=1; $i<=10; $i++) { ?> <option value='<?=$i?>'><?=$i?></option><?php } ?></select>
			<?=$this->Tooltip($this->__("TLEA_111"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_112")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("radio", "league_match_delete", "YES", null, true)?><?=$this->__("TLEA_104")?>  <?=$this->Input("radio", "league_match_delete", "NO")?><?=$this->__("TLEA_105")?>
			<?=$this->Tooltip($this->__("TLEA_113"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_114")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_match_delete_time", null, $this->__("TLEA_71"))?> (<?=$this->__("TLEA_108")?>)
			<?=$this->Tooltip($this->__("TLEA_115"), true)?> 
		</td> 
	</tr>
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_116")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_match_request_delay", null, $this->__("TLEA_99"))?> 
			<select class='textbox' name='league_match_request_delay_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>			
			<?=$this->Tooltip($this->__("TLEA_117"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_118")?>:</td> 
		<td class='tbl1'>
			<?=$this->Input("text", "league_match_request_delay_stop", null, $this->__("TLEA_99"))?> 
			<select class='textbox' name='league_match_request_delay_stop_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>	
			<?=$this->Tooltip($this->__("TLEA_119"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_120")?>:</td> 
		<td class='tbl1'>
			<?=$this->Input("text", "league_match_protest_delay", null, $this->__("TLEA_99"))?> 
			<select class='textbox' name='league_match_protest_delay_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>		
			<?=$this->Tooltip($this->__("TLEA_121"), true)?> 
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TLEA_122")?>:</td> 
		<td class='tbl1'> 
			<?=$this->Input("text", "league_match_protest_delay_stop", null, $this->__("TLEA_99"))?> 
			<select class='textbox' name='league_match_protest_delay_stop_type'> <option value='min'><?=$this->__("TLEA_100")?></option> <option value='hour'><?=$this->__("TLEA_101")?></option> </select>			
			<?=$this->Tooltip($this->__("TLEA_123"), true)?> 
		</td> 
	</tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TLEA_181"))?> </td> </tr>	
</table>
</form>
<?php } ?>