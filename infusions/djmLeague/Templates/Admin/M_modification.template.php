<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_modification.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?> 
<ul class='tabs-navi'>
	<li id='tab1' name='GlobalSettings' class='hover button'> <?=$this->__("TMOD_1")?> </li>
	<li id='tab2' name='Panels' class='button'> <?=$this->__("TMOD_2")?> </li>
	<li id='tab3' name='Updates' class='button'> <?=$this->__("TMOD_3")?> </li>
</ul>

<div class='tabs-content tbl-border'>
	
	<!-- Index -->
	<div id='tab-1' class='tab'>
		<?=$this->Form("EditModificationSettings")?>
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TMOD_5")?> </td> </tr>
			<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_6")?>:</td> 
					<td class='tbl1'> 
						<?=$this->Input("radio", "modification_link", "full", null , ($dbConfig['settings_link'] == "full" ? true:false))?> <strong><?=$this->__("TMOD_7")?></strong> (djmLeague/index.php?league=1) <br/>
						<?=$this->Input("radio", "modification_link", "simple", null ,  ($dbConfig['settings_link'] == "simple" ? true:false))?> <strong><?=$this->__("TMOD_8")?></strong> (djmLeague/?league=1) <br/>
					</td> 
			</tr>
			<tr> 
				<td class='tbl1' align='right'><?=$this->Tooltip($this->__("TMOD_9"))?> <?=$this->__("TMOD_10")?>:  </td> 
				<td class='tbl1' style='padding: 10px;'> 
					<script type='text/javascript'>
						$(function() {	
							var value = $('input[name="modification_upload"]').val();
							var limit = $('input[name="upload_limit"]').val();
							$('#upload_slider').slider({
								value: value,
								min: 1,
								max: limit,
								slide: function ( event, ui ) {
									$('#value').html( ui.value );
									$('input[name="modification_upload"]').val( ui.value );
								}
							});
						});
					</script>
					<?php 
						$post_max_size = return_bytes(ini_get('post_max_size'));
						$upload_max_filesize = return_bytes(ini_get('upload_max_filesize'));
						
						$r1 = $post_max_size / 1024 / 1024;
						$r2 = $upload_max_filesize / 1024 / 1024;
						
						if ($r1 > $r2) {
							$limit = $r2;
						} elseif ($r1 < $r2) {
							$limit = $r1;
						} else { 
							$limit = $r1;
						}
					?>					
					
					<?=$this->Input("hidden", "upload_limit", $limit)?>
					<?=$this->Input("hidden", "modification_upload", $dbConfig['settings_upload_size'])?>
					<div class='left'>
						<?=$this->__("TMOD_11")?>: <span class='bold' id='value'><?=$dbConfig['settings_upload_size']?></span> MB
					</div>
					
					<div class='right'>
					<div class='left' style='padding-right: 15px;'>1MB</div> 
						<div id='upload_slider' class='left' style='width: 150px; position: relative;top: 1px;'></div> 
					<div class='left' style='padding-left:15px;'><?=$limit?>MB</div>	
					</div>
	
				</td> 
			</tr>
			<tr> <td class='tbl1' align='right'><?=$this->__("TMOD_12")?>:</td> <td class='tbl1'> 
				<?=$this->Tooltip($this->__("TMOD_13"), true)?>
				<?=$this->Input("radio", "modification_profile_link", "FUSION", null , ($dbConfig['settings_profile_link'] == "FUSION" ? true:false))?> <strong><?=$this->__("TMOD_14")?></strong> (profile.php?lookup_id=1) <br/>
				<?=$this->Input("radio", "modification_profile_link", "LEAGUE", null ,  ($dbConfig['settings_profile_link'] == "LEAGUE" ? true:false))?> <strong><?=$this->__("TMOD_15")?></strong> (djmLeague/?player=1) <br/>			
			</td></tr>
			<tr> 
				<td class='tbl1' align='right'><?=$this->__("TMOD_16")?>:</td> 
				<td class='tbl1'>
					<?=$this->Input("text", "modification_team_per_page", $dbConfig['settings_team_per_page'])?> <?=$this->__("TMOD_17")?>
					<?=$this->Tooltip($this->__("TMOD_18"), true)?>
				</td>
			</tr>
			<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMOD_19"))?> </td> </tr> 
		</table>	
		</form>
	</div>
	
	<!-- Panels-->
	<div id='tab-2' class='tab hide'>
		<?php
			$this->Form("PanelSearch");
			$panel = "include(INFUSIONS.\"djmLeague/Panels/Opponent_panel.php\");";
			$installed = dbcount("(*)", DB_PREFIX."panels", "panel_content='".$panel."'");	
			if ($installed) { 
				$status = "<span class='green'>".$this->__("TMOD_20")."</span>";
			} else {
				$status = "<a href='".$page['location']."&amp;install_panel=search_opponent' class='button'>".$this->__("TMOD_21")."</a> <span class='red'>".$this->__("TMOD_22")."</span>";
			}
		?>
		
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TMOD_23")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_24")?>: </td> <td class='tbl1'> <?=$status?> </td> </tr>
			<?php if ($installed) {?>
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_25")?>: </td> 
				<td class='tbl1'> 
					<?=$this->Input("text", "panel_search_limit", $dbConfig['panel_search_limit'], $this->__("TMOD_17"))?>  
					<?=$this->Tooltip($this->__("TMOD_26"), true)?>
				</td> 
			</tr>
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_27")?>: </td> 
				<td class='tbl1'>
					<?=$this->Input("radio", "search_panel_type", "OPENSIDE", null, ($dbConfig['panel_search_type'] == "OPENSIDE" ? true:false))?> <span style='position:relative;top:-2px;'><?=$this->__("TMOD_28")?> (Openside)</span> <br/>
					<?=$this->Input("radio", "search_panel_type", "OPENTABLE", null, ($dbConfig['panel_search_type'] == "OPENTABLE" ? true:false))?> <span style='position:relative;top:-2px;'><?=$this->__("TMOD_29")?> (Opentable)</span> <br/>
				</td> 
			</tr>
			<?php } ?>
			<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_30")?>: </td> <td class='tbl1'> <a href='<?=ADMIN?>panels.php?aid=<?=iAUTH?>'><?=$this->__("TMOD_31")?></a> </td> </tr>
			<?php if ($installed) { ?> <tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMOD_32"))?> </td> </tr> <?php } ?>
		</table>
		</form>
		<br/>
		
		<?php
			unset($panel, $installed, $status);
			$panel = "include(INFUSIONS.\"djmLeague/Panels/User_panel.php\");";
			$installed = dbcount("(*)", DB_PREFIX."panels", "panel_content='".$panel."'");	
			if ($installed) { 
				$status = "<span class='green'>".$this->__("TMOD_20")."</span>";
			} else {
				$status = "<a href='".$page['location']."&amp;install_panel=user_panel' class='button'>".$this->__("TMOD_21")."</a> <span class='red'>".$this->__("TMOD_22")."</span>";
			}		
		?>
		<?php if ($installed) {  $this->Form("PanelEvent"); ?>
			<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
				<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TMOD_33")?> </td> </tr>		
				<tr> 
					<td class='tbl1' colspan='2'>
						<?=$this->__("TMOD_34")?>
					</td>
				</tr>	
				<tr> 
					<td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_35")?>:</td> 
					<td class='tbl1'> 
						<?=$this->Tooltip($this->__("TMOD_36"),true)?>
						<?=$this->Input("radio", "event_pos", "LEFT-TOP", null, ($dbConfig['panel_event_position'] == "LEFT-TOP" ? true:false))?> <span style='position:relative;top:-2px;'><?=$this->__("TMOD_37")?></span><br/>
						<?=$this->Input("radio", "event_pos", "RIGHT-TOP", null, ($dbConfig['panel_event_position'] == "RIGHT-TOP" ? true:false))?> <span style='position:relative;top:-2px;'><?=$this->__("TMOD_38")?></span><br/>
						<?=$this->Input("radio", "event_pos", "LEFT-BOTTOM", null, ($dbConfig['panel_event_position'] == "LEFT-BOTTOM" ? true:false))?> <span style='position:relative;top:-2px;'><?=$this->__("TMOD_39")?></span><br/>
						<?=$this->Input("radio", "event_pos", "RIGHT-BOTTOM", null, ($dbConfig['panel_event_position'] == "RIGHT-BOTTOM" ? true:false))?> <span style='position:relative;top:-2px;'><?=$this->__("TMOD_40")?></span><br/>
					</td>
				</tr>
				<tr> 
					<td class='tbl1' align='right'> <?=$this->__("TMOD_41")?>: </td> 
					<td class='tbl1'> 
						<?=$this->Input("text", "event_interval", ($dbConfig['panel_event_refresh']/1000))?> (<?=$this->__("TMOD_42")?>) 
						<?=$this->Tooltip($this->__("TMOD_43"), true)?>
					</td> 
				</tr>
				<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMOD_32"))?> </form> </td></tr>
			</table>	
			<br/>
		<?php } ?>
		
		
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TMOD_44")?> </td> </tr>		
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_24")?>: </td> <td class='tbl1'> <?=$status?> </td> </tr>
			<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_30")?>: </td> <td class='tbl1'> <a href='<?=ADMIN?>panels.php?aid=<?=iAUTH?>'><?=$this->__("TMOD_31")?></a> </td> </tr>
		</table>
		<br/>
		
		<?php
			unset($panel, $installed, $status);
			$panel = "include(INFUSIONS.\"djmLeague/Panels/League_panel.php\");";
			$installed = dbcount("(*)", DB_PREFIX."panels", "panel_content='".$panel."'");	
			if ($installed) { 
				$status = "<span class='green'>".$this->__("TMOD_20")."</span>";
			} else {
				$status = "<a href='".$page['location']."&amp;install_panel=league_panel' class='button'>".$this->__("TMOD_21")."</a> <span class='red'>".$this->__("TMOD_22")."</span>";
			}		
		?>
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TMOD_45")?> </td> </tr>		
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_24")?>: </td> <td class='tbl1'> <?=$status?> </td> </tr>
			<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_30")?>: </td> <td class='tbl1'> <a href='<?=ADMIN?>panels.php?aid=<?=iAUTH?>'><?=$this->__("TMOD_31")?></a> </td> </tr>
		</table>
		<br/>
		
	</div>		
	
	
	<!-- Updates -->
	<div id='tab-3' class='tab hide'>
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TMOD_46")?> </td> </tr>
			<?php if ($update->checkPHPSettings()) { ?>
				
				<?php if ($update->getPckg() > $config['pckgVersion']) { ?>
					<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TMOD_47")?>:</td> <td class='tbl1'> <strong><?=$update->getVersion()?></strong> (<?=$config['Version']?>) </td> </tr>
					<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_48")?>:</td> <td class='tbl1'> <a href='<?=$update->getChangelog()?>' target='_blank'>Changelog</a> </td> </tr>
					<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_49")?>:</td> <td class='tbl1'> <input onclick="location.href='<?=$update->getDownload()?>'" type='submit' class='button' value='<?=$this->__("TMOD_50")?>'/> </td> </tr>
					<tr> <td class='tbl1' align='right'> <?=$this->__("TMOD_51")?>:</td> 
							<td class='tbl1'>  
								<?php echo $update->showInstallation(); ?>	
							</td> 
					</tr>
				<?php } else { ?>
				<tr>
					<td class='tbl1'> 
						<img src='<?=pathMedia?>System/update.png' alt='Update' align='left' style='margin: 5px 35px 0 0'/> <span style='font-weight: bold; font-size: 17px;'><?=$this->__("TMOD_52")?></span>
						<br/><p style='text-align: left'> <?=$this->__("TMOD_53")?> </p> <br/>
					</td> 
				</tr>
				<?php } ?>
			<?php } else { ?>
				<tr>
					<td class='tbl1' align='center'> 
						<img src='<?=pathMedia?>System/update.png' alt='Update' align='left' style='margin: 35px 35px 0 0'/> <span style='font-weight: bold; font-size: 17px; color: red;'><?=$this->__("TMOD_54")?></span>
						<br/><p style='text-align: justify'><?=$this->__("TMOD_55")?> </p> <br/>
					</td> 
				</tr>
			<?php } ?>
		</table>
	</div>

</div>