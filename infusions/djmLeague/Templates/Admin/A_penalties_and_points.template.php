<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: A_penalties_and_points.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (isset($_GET['id']) && isnum($_GET['id'])) { 
	$this->Form("MakePenalties");
	$this->Input("hidden", "parent", $data['team_id']);
	echo "<table align='center' width='100%' border='0' class='tbl-border' cellpadding='0' cellspacing='1'>";
		echo "<tr> <td class='tbl1' width='35%'>".$this->__("TPAP_1").":</td> <td class='tbl1'>";
			echo "<strong>".$data['team_name']."</strong> (".$this->__("TPAP_2").": ".$data['team_points'].")";
		echo "</td></tr>";
		echo "<tr> <td class='tbl1'>".$this->__("TPAP_3").":</td> <td class='tbl1'>";
			$this->Input("radio", "complaint_type","+", null); echo "<span class='bold' style='position:relative;top:-2px;'>+</span>&nbsp;&nbsp;&nbsp;"; 
			$this->Input("radio", "complaint_type","-", null); echo "<span class='bold' style='position:relative;top:-2px;'>-</span>";
		echo "</td></tr>";
		echo "<tr> <td class='tbl1'>".$this->__("TPAP_4").":</td> <td class='tbl1'>";
			$this->Input("text", "complaint_value", null, $this->__("TPAP_5"));
		echo "</td></tr>";												
		echo "<tr><td class='tbl1'> ".$this->__("TPAP_6").": </td> <td class='tbl1'>";
			echo "<textarea name='complaint_message' class='textbox' style='width: 350px; height: 75px;' placeholder='".$this->__("TPAP_7")."'></textarea>";
		echo "</td> </tr>";
		echo "<tr> <td class='tbl1' colspan='2'>"; $this->Input("submit", "submit", $this->__("TPAP_8")); echo "</td></tr>";
	echo "</table>";
	echo "</form>";
 } else { ?>
	<script type='text/javascript'>
		$(function(){
				/* Delay function */
				var delay = (function() {
					var timer = 0;
					return function(callback, ms) {
						clearTimeout(timer);
						timer = setTimeout(callback, ms);
					};
				})();		
					
				/* Ajax Query */	
				$('input[name="team_id"]').keyup(function(){
						var value = $(this).val();
						if (value != "") {
							$('#AjaxResult').html('');
							$('#AjaxResult').show();
							delay(function() {
								$.getJSON( "../../infusions/djmLeague/index.php?ajax=getTeam", { id: value },
								function( json ) {	
									if (json.status == "yes") {
										$('#AjaxResult').html(json.name+' (#'+json.id+')');
									}
								});							
							}, 100);
						} else {
							$('#AjaxResult').hide();
						}
				});	
		});
	</script>

	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
		<tr> <td class='tbl1' align='center'>
				<?=$this->__("TPAP_9")?>: <br/>
				<?=$this->Form("goPenalties")?>
				<?=$this->Input("text", "team_id", null, $this->__("TPAP_10"))?><br/><br/>
				<span class='hide' id='AjaxResult'></span>	
				<br/><br/>
				<?=$this->Input("submit", "submit", $this->__("TPAP_11"))?>
				</form>
		</td></tr>
	</table>	
<?php } ?>