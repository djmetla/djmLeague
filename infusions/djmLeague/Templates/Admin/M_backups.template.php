<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_backup.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 
 $prefix = DB_PREFIX."league_";
 $tables = array(
	$prefix."accounts", $prefix."accounts_pattern",
	$prefix."admin", $prefix."admin_category",
	$prefix."admin_log", $prefix."ban",
	$prefix."challenge", $prefix."complaints",
	$prefix."elo", $prefix."event",
	$prefix."game", $prefix."league",
	$prefix."log", $prefix."match",
	$prefix."match_links", $prefix."match_media",
	$prefix."match_request", $prefix."player",
	$prefix."protest", $prefix."protest_pattern",
	$prefix."request", $prefix."request_pattern",
	$prefix."search", $prefix."settings",
	$prefix."team", $prefix."team_player"
 );
 
$query = mysql_query("SHOW TABLE STATUS");  

echo "<form action='".INFUSIONS."djmLeague/?ajax=makeBackup' method='post'>";
echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>";
	echo "<tr>";
		echo "<td class='forum-caption' align='center'> ".$this->__("TBCK_1")." </td>";
		echo "<td class='forum-caption' align='center'> ".$this->__("TBCK_2")." </td>";
		echo "<td class='forum-caption' align='center'> ".$this->__("TBCK_3")." </td>";
		echo "<td class='forum-caption' align='center'> ".$this->__("TBCK_4")." </td>";
	echo "</tr>";
	$allsize = 0;
	while ($t = mysql_fetch_array($query)) {
		if (in_array($t['Name'], $tables)) {
			$size = $t['Data_length']+$t['Index_length'];
			$allsize += $t['Data_length']+$t['Index_length'];
			echo "<tr>";
				echo "<td class='tbl1'> ".$t['Name']." </td>";
				echo "<td class='tbl1' align='center'> ".number_format($size/1024,2)." KB </td>";
				echo "<td class='tbl1' align='center'> ".$t['Rows']." </td>";
				echo "<td class='tbl1' align='center'> <input type='checkbox' name='backup[]' value='".$t['Name']."' checked='checked'/> </td>";
		}
	}
	echo "<tr> <td class='tbl1' align='right' colspan='4'> ".$this->__("TBCK_5").": <strong>".number_format($allsize/(1024*1024),2)."</strong> MB </td> </tr>";
	echo "<tr> <td class='tbl1' align='left' colspan='4'>"; $this->Input("submit", "submit", $this->__("TBCK_6")); echo "</td></tr>";
echo "</table>";
echo "</form>";

