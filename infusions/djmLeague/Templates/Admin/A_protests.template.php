<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  A_protests.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

/* Navigation */
echo "<div>";
	echo "<a href='".$page['location']."' class='button' style='text-decoration:none;'>".$this->__("TPR_1")."</a>&nbsp;";
	echo "<a href='".$page['location']."&amp;page=my' class='button' style='text-decoration:none;'>".$this->__("TPR_2")."</a>&nbsp;";
	echo "<a href='".$page['location']."&amp;page=history' class='button' style='text-decoration:none;'>".$this->__("TPR_3")."</a>&nbsp;";
echo "</div><br/>";	


switch ($_GET['page']) {

	default:
	echo "<div align='right' style=';margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $protest_per_page, $protest_count, 3, $page['location']."&amp;")."\n</div>\n"; 
	$this->Form("TakeProtests");
	
	?>
		<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='7'> <?=$this->__("TPR_4")?> (<?=$protest_count?>) </td> </tr>
			<tr> 
				<td class='tbl1 bold' align='center' width='30'></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_5")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_6")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_7")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_8")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_9")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_10")?></td>
			</tr>
			<?php if (!$protest_count) { echo "<tr> <td colspan='7' align='center' class='tbl1'>".$this->__("TPR_11")."</td> </tr>"; } ?>
			<?php foreach ($protests as $protest) { if (iSUPERADMIN || in_array($protest['LeagueID'], $access)) { ?>
				<tr> 
					<td class='tbl1' align='center'> <?php if ($protest['protest_status'] == 0) { ?> <?=$this->Input("checkbox", "take_protest[]", $protest['protest_id'])?> <?php } ?> </td>
					<td class='tbl1'> <?=$protest['pattern_name']?> </td>
					<td class='tbl1'> <a href='<?=urlMatch.$protest['protest_match']?>'>#<?=$protest['protest_match']?></a>, <a href='<?=urlProfile.$protest['protest_from']?>'><?=$protest['User']?></a> (<?=$protest['Team']?>) </td>
					<td class='tbl1' align='center'><?=$this->Icon($protest['Game'],"Game")?> <?=$protest['League']?> </td>
					<td class='tbl1' align='center'><?=strftime("%d.%m.%Y %H:%M", $protest['protest_time'])?> </td>
					<td class='tbl1' align='center'><?=($protest['protest_status'] == 0 ? "---":$protest['Admin'])?> </td>
					<td class='tbl1' align='center'><?php if ($protest['protest_status'] == 0) { ?> <a href='<?=$page['location']?>&amp;take=<?=$protest['protest_id']?>' title='<?=$this->__("TPR_12")?>'><?=$this->Icon("accept.png")?></a> <?php } ?> </td>
				</tr>
			<?php } } ?>
		</table>
	<?php
	if ($protest_count) { echo "<div align='left' style='float: left; margin-top:5px;'>"; $this->Input("submit", "submit", $this->__("TPR_13")); echo "</div>"; }
	echo "<div align='right' style='float:right; margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $protest_per_page, $protest_count, 3, $page['location']."&amp;")."\n</div>\n"; 
	echo "</form>";
	break;

	case "history":
	echo "<div align='right' style=';margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $protest_per_page, $protest_count, 3, $page['location']."&amp;")."\n</div>\n"; 
	?>
		<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='7'><?=$this->__("TPR_3")?> (<?=$protest_count?>) </td> </tr>
			<tr> 
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_5")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_6")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_7")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_8")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_14")?></td>
			</tr>
			<?php if (!$protest_count) { echo "<tr> <td colspan='7' align='center' class='tbl1'>".$this->__("TPR_11")."</td> </tr>"; } ?>
			<?php foreach ($protests as $protest) { if (iSUPERADMIN || in_array($protest['LeagueID'], $access)) { ?>
				<tr> 
					<td class='tbl1'> <?=$protest['pattern_name']?> </td>
					<td class='tbl1'> <a href='<?=urlMatch.$protest['protest_match']?>'>#<?=$protest['protest_match']?></a>, <a href='<?=urlProfile.$protest['protest_from']?>'><?=$protest['User']?></a> (<?=$protest['Team']?>) </td>
					<td class='tbl1' align='center'><?=$this->Icon($protest['Game'],"Game")?> <?=$protest['League']?> </td>
					<td class='tbl1' align='center'><?=strftime("%d.%m.%Y %H:%M", $protest['protest_time'])?> </td>
					<td class='tbl1' align='center'><?=($protest['protest_status'] == 0 ? "---":$protest['Admin'])?> </td>
				</tr>
			<?php } } ?>
		</table>
	<?php
	echo "<div align='right' style='float:right; margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $protest_per_page, $protest_count, 3, $page['location']."&amp;")."\n</div>\n"; 	
	break;
	
	/* My protests */
	case "my":
	echo "<div align='right' style='float:right; margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $protest_per_page, $protest_count, 3, $page['location']."&amp;page=my&amp;")."\n</div>\n"; 	
	?>
		<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> <td class='forum-caption' colspan='7'> <?=$this->__("TPR_2")?> </td> </tr>
			<tr> 
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_5")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_6")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_7")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_8")?></td>
				<td class='tbl1 bold' align='center'><?=$this->__("TPR_10")?></td>
			</tr>
			<?php if (!$protest_count) { echo "<tr> <td colspan='7' align='center' class='tbl1'>".$this->__("TPR_11")."</td> </tr>"; } ?>
			<?php foreach ($protests as $protest) { if (iSUPERADMIN || in_array($protest['LeagueID'], $access)) { ?>
				<tr> 
					<td class='tbl1'> <?=$protest['pattern_name']?> </td>
					<td class='tbl1'> <a href='<?=urlMatch.$protest['protest_match']?>'>#<?=$protest['protest_match']?></a>, <a href='<?=urlProfile.$protest['protest_from']?>'><?=$protest['User']?></a> (<?=$protest['Team']?>) </td>
					<td class='tbl1' align='center'><?=$this->Icon($protest['Game'],"Game")?> <?=$protest['League']?> </td>
					<td class='tbl1' align='center'><?=strftime("%d.%m.%Y %H:%M", $protest['protest_time'])?> </td>
					<td class='tbl1' align='center'> 
						<a href='<?=$page['location']?>&amp;page=protest&amp;id=<?=$protest['protest_id']?>'> <?=$this->Icon("more.png")?> </a>
					</td>
				</tr>
			<?php } } ?>
		</table>		
	<?php
	echo "<div align='right' style='float:right; margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $protest_per_page, $protest_count, 3, $page['location']."&amp;page=my&amp;")."\n</div>\n"; 	
	break;
	
	/* Protest */
	case "protest": 
	$this->Form("CloseProtest");
	$this->Input("hidden", "parent", $data['protest_id']);
	?>
	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
		<tr> 
			<td class='forum-caption' colspan='2'> <?=$this->__("TPR_15")?> #<?=$data['protest_id']?> (<?=$t1['team_name']?> vs. <?=$t2['team_name']?>) </td> 
			<td class='forum-caption'> <?=$this->__("TPR_16")?> </td>
		</tr>
		
		<tr>
			<td class='tbl1' width='30%' align='right'><?=$this->__("TPR_17")?>: </td> <td class='tbl1'> <a href='<?=urlProfile.$data['protest_from']?>'><?=$data['Applicant']?></a> (<?=$data['ApplicantTeam']?>) </td> 
			<td class='tbl1' rowspan='6' valign='top' width='200'>
				<ul style='margin: 5px 5px 5px 15px; padding: 5px;'>
					<li class='bold'><?=$this->__("TPR_18")?></li>
					<?php if ($data['match_status'] == 2) { ?> <li> <a href='<?=$page['location']?>&amp;page=protest&amp;id=<?=$data['protest_id']?>&amp;task=refresh'><?=$this->__("TPR_19")?></a> </li> <?php } ?>
					<?php if ($data['match_status'] == 0 OR $data['match_status'] == 1) { ?> <li> <a href='<?=$page['location']?>&amp;page=protest&amp;id=<?=$data['protest_id']?>&amp;task=close'><?=$this->__("TPR_20")?></a> </li> <?php } ?>
					<li> <a href='<?=$page['location']?>&amp;page=protest&amp;id=<?=$data['protest_id']?>&amp;task=edit_score'><?=$this->__("TPR_21")?></a> </li>
					<li> <a href='<?=$page['location']?>&amp;page=protest&amp;id=<?=$data['protest_id']?>&amp;task=complaints'><?=$this->__("TPR_22")?></a> </li>
					<br/>
					
					<li class='bold'> <?=$this->__("TPR_23")?> </li>
					<li> <a href='<?=urlMatch.$data['match_id']?>' target='_blank'> <?=$this->__("TPR_24")?> </a> </li>				
					<li> <a href='<?=urlLeague.$data['league_id']?>&amp;option=rules' target='_blank'> <?=$this->__("TPR_25")?> </a> </li>
					<li> <a href='<?=urlTeam.$t1['team_id']?>' target='_blank'> <?=$this->__("TPR_26")?> <?=$t1['team_name']?> </a> </li>
					<li> <a href='<?=urlTeam.$t2['team_id']?>' target='_blank'> <?=$this->__("TPR_26")?> <?=$t2['team_name']?> </a> </li>
					
				</ul>					
			</td>
		</tr>
		
		<tr><td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_27")?>: </td> <td class='tbl1'> <?=strftime("%d.%m.%Y %H:%M", $data['protest_time'])?> </td> </tr>
		<tr><td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_28")?>: </td> <td class='tbl1 bold'> <?=$data['pattern_name']?> </td> </tr>
		<tr><td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TPR_29")?>: </td> <td class='tbl1'> <?php if ($data['pattern_use_custom_description'] == 0) { echo $data['pattern_preddefined']; } else { echo $data['protest_description']."<br/><em>(".$this->__("TPR_30").")</em>"; } ?>  </td> </tr>
		<tr><td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TPR_58")?>: </td> <td class='tbl1'> <textarea name='protest_admin_description' class='textbox' style='width: 335px; height: 100px;'></textarea></td> </tr>
		<tr><td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TPR_31"))?> </form> </td> </tr>
	</table>
	
	
	<br/>
	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
		<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TPR_32")?> </td> </tr>
		<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_33")?>: </td> <td class='tbl1'> <?=$this->Icon($t1['team_flag'], "Flags")?> <?=$t1['team_name']?> vs. <?=$this->Icon($t2['team_flag'], "Flags")?> <?=$t2['team_name']?> </td> </tr>
		
		<tr> 
			<td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_34")?>: </td> 
			<td class='tbl1 bold'> 
				<?php 
					if ($data['match_status'] == 0) { 
						echo $this->__("TPR_35");
					} elseif ($data['match_status'] == 1) {
						echo $this->__("TPR_36");
					} elseif ($data['match_status'] == 2) {
						echo $this->__("TPR_37");
					} elseif ($data['match_status'] == 3) {
						echo $this->__("TPR_38");
					}
				?>
			</td> 
		</tr>
		
		<tr>
			<td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_39")?>: </td> 
			<td class='tbl1'> 
				<?php
				if ($data['league_match_elo_type'] == "default") {
					$elo = new Elo("default");
					$calc = $elo->t1Pts($data['match_t1_points'])->t2Pts($data['match_t2_points'])->t1Score($data['match_t1_score'])->t2Score($data['match_t2_score'])->Calculate();
				} elseif ($data['league_match_elo_type'] == "classic") {
					$elo = new Elo("classic");
					$calc = $elo->t1Pts($data['match_t1_points'])->t2Pts($data['match_t2_points'])->t1Score($data['match_t1_score'])->t2Score($data['match_t2_score'])->Calculate();				
				} elseif ($data['league_match_elo_type'] == "custom") {
					djmDB::Select("SELECT elo_value FROM ".dbElo." WHERE elo_id='%d'", $data['league_match_elo']);
					$elod = djmDB::Data();
					$elo = new Elo("default");
					$calc = $elo->Constant($elod['elo_value'])->t1Pts($data['match_t1_points'])->t2Pts($data['match_t2_points'])->t1Score($data['match_t1_score'])->t2Score($data['match_t2_score'])->Calculate();
				}
				
				if($calc[0] > $calc[1]) {
					$score = "<span class='green'>(+".$calc[0].")</span> : <span class='red'>(".$calc[1].")</span>";
				} elseif ($calc[0] < $calc[1]) {
					$score = "<span class='red'>(".$calc[0].")</span> : <span class='green'>(+".$calc[1].")</span>";
				} elseif ($calc[0] == $calc[1]) {
					$score = "<span class='blue'>(+".$calc[0].")</span> : <span class='blue'>(+".$calc[1].")</span>";
				}
				
				echo $t1['team_name']." <span class='bold'>[".($data['match_status'] != 0 ? $data['match_t1_score']:"TBA")."]</span> ".($data['match_status'] != 0 ? $score:"")." <span class='bold'>[".($data['match_status'] != 0 ? $data['match_t2_score']:"TBA")."]</span> ".$t2['team_name'];
				?>
			</td> 
		</tr>
		
		<tr> 
			<td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_40")?>: </td> 
			<td class='tbl1'> 
				<?php
					djmDB::Select("SELECT * FROM ".dbMatchRequest." WHERE request_match='%d' AND request_type='guest'", $data['match_id']);
					if (djmDB::Num()) {
						echo "<span class='red'>".$this->__("TPR_41").".</span> <a href='".urlMatch.$data['match_id']."#Requests' target='_blank'>".$this->__("TPR_42")."</a>";
					} else {
						echo "<span class='green'>".$this->__("TPR_43")."</span>";
					}
				?>
			</td> 
		</tr>
		
		<tr> 
			<td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_44")?>: </td> 
			<td class='tbl1'>
				<?php
					djmDB::Select("SELECT * FROM ".dbMatchRequest." WHERE request_match='%d' AND request_type='delete'", $data['match_id']);
					if (djmDB::Num()) {
						echo "<span class='red'>".$this->__("TPR_45").".</span> <a href='".urlMatch.$data['match_id']."#Requests' target='_blank'>".$this->__("TPR_42")."</a>";
					} else {
						echo "<span class='green'>".$this->__("TPR_46")."</span>";
					}
				?>

			</td> 
		</tr>
		
		<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPR_47")?>: </td> <td class='tbl1'> <?=strftime("%d.%m.%Y %H:%M", $data['match_time'])?> </td> </tr>
		
		<tr> 
			<td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TPR_48")?>: </td> 
			<td class='tbl1'> 
				<?php
					djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbMatchMedia." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.media_user WHERE media_match='%d'", $data['match_id']); 
					if (djmDB::Num()) {
					
					 foreach(djmDB::fullData() as $img) { ?>
						<div class='tbl-border' style='float: left; margin-right:10px; margin-bottom:5px; text-align: center; width: 120px;'>
							<a href='<?=pathMedia?>Match/<?=$data['match_league']?>/<?=$data['match_id']?>/<?=$img['media_item']?>' class='ImageView' title='<?=$this->__("TPR_49")?>: <?=$img['user_name']?>, <?=$this->__("TPR_50")?>: <?=$img['media_type']?>, <?=strftime("%d.%m.%Y %H:%M", $img['media_time'])?>' rel='gallery'>
								<img src='<?=pathMedia?>Match/<?=$data['match_league']?>/<?=$data['match_id']?>/thumb_<?=$img['media_item']?>' alt='Image - <?=$img['media_id']?>' width='120' height='70' />
							</a>
							<span style='font-size: 10px;'>
							<?=$this->Profile($img['media_user'], $img['user_name'])?> <br/>
							<?=strftime("%d.%m.%Y %H:%M", $img['media_time'])?>
							</span>
							<br class='clear'/>
						</div>

					<?php } 					
					} else { echo $this->__("TPR_51"); }
				?>
			</td> 
		</tr>
		
		<tr> 
			<td class='tbl1' width='30%' align='right' valign='top'> <?=$this->__("TPR_52")?>: </td> 
			<td class='tbl1'>
				 <table align='center' width='100%' cellpadding='0' cellspacing='1'>
					<?php
						djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbMatchLinks." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.link_user WHERE link_match='%d'", $data['match_id']); 
						$link_count = djmDB::Num();					
						if ($link_count) { 
						foreach (djmDB::fullData() as $link) { 
							if ($link['link_type'] == "demo") { $typ = $this->__("TPR_53"); } 
							elseif ($link['link_type'] == "screenshot") { $typ = $this->__("TPR_54"); }
							else { $typ = $this->__("TPR_55"); }
						?>
						<tr> 
							<td class= 'tbl2'> 
								<a href='<?=$link['link_target']?>' target='_blank'><?=$typ?></a>  <?=$this->__("TPR_56")?> <a href='<?=urlProfile.$link['link_user']?>'><?=$link['user_name']?></a> 
								<span class='right'> <?=strftime("%d.%m.%Y %H:%M", $link['link_time'])?> </span>
							</td>
						</tr>	
					<?php } } else { ?>
						<tr> <td class='tbl2' align='center'><?=$this->__("TPR_57")?></td> </tr>
					<?php } ?>
				 </table>			
			</td> 
		</tr>
	</table>
	<?php
	break;
}
?>
