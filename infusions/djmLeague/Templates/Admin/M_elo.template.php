<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_elo.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
?>
<script type='text/javascript'>
$(function(){
		var constant = $('input[name="elo_constant"]').val();
		$('#Value').html(constant);
		$('#slider').slider({
			value: constant,
			min: 30,
			max: 90,
			 slide: function( event, ui ) {
				$( "#Value" ).html( ui.value );
				$('input[name="elo_constant"]').val( ui.value );
			}			
		});
		
		var delay = (function() {
			var timer = 0;
			return function(callback, ms) {
				clearTimeout(timer);
				timer = setTimeout(callback, ms);
			};
		})();	
		
		$('#Simulateslider').slider({
			value: constant,
			min: 30,
			max: 90,
			 slide: function( event, ui ) {
				$('#Message').fadeOut();
				$('#Result').fadeIn();
				$('#Table').fadeIn();			 
				$( "#SimulateValue" ).html( ui.value );
				$('#Table tr[class="val"]').remove();
				
				if (!$('#Table tr[class="loading"]').length ) {
					$('#Table').append("<tr class='loading'> <td class='tbl2' colspan='7' align='center'> <img src='../../infusions/djmLeague/Media/System/loader.gif' alt='Loading'/> </td> </tr>");
				}
					
				delay(function() {
						var _maxScore = parseInt($('input[name="maxScore"]').val());
						var _t1Points = parseInt($('input[name="t1Points"]').val());
						var _t2Points =  parseInt($('input[name="t2Points"]').val());
						var _constant = ui.value;					
				
						$.getJSON( "../../infusions/djmLeague/index.php?ajax=elo", {
							maxScore: _maxScore,
							t1pts: _t1Points,
							t2pts: _t2Points,
							constant: _constant	
						},
						function( json ) {
							var table = $('#Table');
							$('#Table tr[class="loading"]').remove();
							var limit = _maxScore - 2;
							for(i=0; i<= _maxScore; i++) {
								if (i < 10) { a = '0'+i; } else { a = i; }
								if (i <= limit) {
									table.append('<tr class="val"> <td class="tbl2" align="center">'+_maxScore+':'+a+'</td> <td class="tbl2" align="center">'+json.table1[0][i][0]+'</td> <td class="tbl2" align="center">'+json.table1[0][i][1]+'</td> <td clas="tbl1">&nbsp;</td> <td class="tbl2" align="center">'+a+':'+_maxScore+'</td> <td class="tbl2" align="center">'+json.table2[0][i][0]+'</td> <td class="tbl2" align="center">'+json.table2[0][i][1]+'</td>  </tr>');
								}
							}
						});					
				}, 150);
			}			
		});		
});	
</script>

<table border='0' align='center' width='100%' class='tbl-border' cellpadding='0' cellspacing='1'>
	<tr> <td colspan='4' class='forum-caption'> <?=$this->__("TELO_1")?> </td> </tr>
	
	<tr>
		<td class='bold tbl1' align='center'> <?=$this->__("TELO_2")?> </td>
		<td class='bold tbl1' align='center'> <?=$this->__("TELO_3")?> </td>
		<td class='bold tbl1' align='center'> <?=$this->__("TELO_4")?> </td>
		<td class='bold tbl1' align='center'> <?=$this->__("TELO_5")?> </td>
	</tr>
	
	<tr>
		<td class='tbl1'> ELO <em>(<?=$this->__("TELO_6")?>)</em> </td>
		<td class='tbl1 bold' align='center'> 30 </td> 
		<td class='tbl1' align='center'> <?=$this->Tooltip($this->__("TELO_7"))?> </td> 
		<td class='tbl1' align='center'> --- </td> 
	</tr>	
	
	<tr>
		<td class='tbl1'> <?=$this->__("TELO_8")?> </td>
		<td class='tbl1' align='center'> --- </td> 
		<td class='tbl1' align='center'> <?=$this->Tooltip($this->__("TELO_9"))?> </td> 
		<td class='tbl1' align='center'> --- </td> 
	</tr>
	
	<?php foreach($data as $elo) { ?>
		<tr>
			<td class='tbl1'> <?=$elo['elo_name']?> </td>
			<td class='tbl1 bold' align='center'> <?=$elo['elo_value']?> </td>
			<td class='tbl1' align='center'> <?=$this->Tooltip($this->__("TELO_10").": <strong>".$elo['elo_value']."</strong>")?> </td>
			<td class='tbl1' align='center'> <a href='<?=$page['location']?>&amp;edit_elo=<?=$elo['elo_id']?>'><?=$this->__("TELO_11")?></a> - <a href='<?=$page['location']?>&amp;delete_elo=<?=$elo['elo_id']?>'><?=$this->__("TELO_12")?></a> </td>
		</tr>
	<?php } ?>
	
</table>
<br/>

<ul class='tabs-navi'>
   <li id='tab1' class='button' name='Create'> <?=$this->__("TELO_13")?> </li>
   <li id='tab2' class='button' name='Simulation'> <?=$this->__("TELO_14")?> </li>
</ul>


<div class='tabs-content tbl-border'>
   <div id='tab-1' class='tab'>
		  <?php if (isset($_GET['edit_elo']) && isnum($_GET['edit_elo'])) { ?>
			  <?=$this->Form("EditElo")?>
			  <?=$this->Input("hidden", "parent", $edit['elo_id'])?>
			  <table border='0' align='center' width='100%' class='tbl-border' cellpadding='0' cellspacing='1'>
				<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TELO_15")?> </td> </tr>
				<?php if (dbcount("(league_id)", dbLeague, "league_match_elo='".$edit['elo_id']."'")) { ?>
				<tr> 
					<td colspan='2' class='tbl1 red' align='center'>
							<?=$this->__("TELO_16")?>
					</td>
				</tr>	
				<?php } ?>
				<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TELO_17")?>: </td> <td class='tbl1' align='left'> <?=$this->Input("text", "elo_name", $edit['elo_name'], $this->__("TELO_18"))?> <?=$this->Tooltip($this->__("TELO_19"), true)?> </td> </tr>
				<tr> 
					<td class='tbl1' align='right' width='30%'> <?=$this->__("TELO_20")?>: </td> 
					<td class='tbl1' align='right'>  
						<input type='hidden' name='elo_constant' value='<?=$edit['elo_value']?>' />
						<span class='left'><?=$this->__("TELO_20")?>:</span> <span class='left bold' id='Value'></span>  
						<?=$this->Tooltip($this->__("TELO_21"), true)?> 
						<div class='right' style='margin-right:15px;'>
							<div class='left' style='margin-right: 15px;'>30</div>
							<div id='slider' class='left' style='width: 200px; position:relative;top:3px;'> </div>
							<div class='left' style='margin-left: 15px;'>90</div>
						</div>
					</td> 
				</tr>
				<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TELO_22"))?> <?=$this->__("TELO_23")?> <a href='<?=$page['location']?>'><?=$this->__("TELO_24")?></a> </td> </tr>
			  </table>
			  </form>
		  <?php } else { ?>
			  <?=$this->Form("CreateElo")?>
			  <table border='0' align='center' width='100%' class='tbl-border' cellpadding='0' cellspacing='1'>
				<tr> <td class='forum-caption' colspan='2'> <?=$this->__("TELO_25")?> </td> 
				<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TELO_17")?>: </td> <td class='tbl1' align='left'> <?=$this->Input("text", "elo_name", null, $this->__("TELO_18"))?> <?=$this->Tooltip($this->__("TELO_19"), true)?> </td> </tr>
				<tr> 
					<td class='tbl1' align='right' width='30%'> <?=$this->__("TELO_20")?>: </td> 
					<td class='tbl1' align='right'>  
						<input type='hidden' name='elo_constant' value='30' />
						<span class='left'><?=$this->__("TELO_20")?>:</span> <span class='left bold' id='Value'></span>  
						<?=$this->Tooltip($this->__("TELO_21"), true)?> 
						<div class='right' style='margin-right:15px;'>
							<div class='left' style='margin-right: 15px;'>30</div>
							<div id='slider' class='left' style='width: 200px; position:relative;top:3px;'> </div>
							<div class='left' style='margin-left: 15px;'>90</div>
						</div>
					</td> 
				</tr>
				<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TELO_25"))?> </td> </tr>
			  </table>
			  </form>		  
		  <?php } ?>
	</div>

        
	<div id='tab-2' class='tab' style='display:none;'>
		<table border='0' align='center' width='100%' class='tbl-border' cellpadding='0' cellspacing='1'>
			<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TELO_26")?> </td> </tr>
			<tr> <td class='tbl1'> <?=$this->__("TELO_27")?>: </td> <td class='tbl1' align='right'> <input type='text' class='textbox' name='maxScore' value='16'/>  <?=$this->Tooltip($this->__("TELO_28"))?> </td> </tr>
			<tr> <td class='tbl1'> <?=$this->__("TELO_29")?> #1: </td> <td class='tbl1' align='right'> <input type='text' class='textbox' name='t1Points' value='1000'/> <?=$this->Tooltip($this->__("TELO_30"))?> </td> </tr>
			<tr> <td class='tbl1'> <?=$this->__("TELO_29")?> #2: </td> <td class='tbl1' align='right'> <input type='text' class='textbox' name='t2Points' value='1000'/> <?=$this->Tooltip($this->__("TELO_31"))?> </td> </tr>
				
			<tr>
				<td class='tbl1' colspan='2'>
							<input type='hidden' name='elo_constant' value='30'/>
							<span class='left'><?=$this->__("TELO_20")?>:</span> <span id='SimulateValue' class='left bold' style='margin-left: 5px;'>30</span> 
							<div class='right'> <div class='left' style='margin-right:10px;'>30</div> <div id='Simulateslider' class='left' style='position:relative; top:2px; width: 400px;'> </div> <div class='left' style='margin-left:10px;'>90</div> </div>			
				</td>
			</tr>
			
			<tr>
				<td class='tbl1 hide' id='Result' colspan='2' style='padding: 0px; margin: 0px;'>
				
						<table border='0' align='center' width='100%' class='tbl-border hide' cellpadding='0' cellspacing='1' id='Table'>
							<tr> 
								<td class='tbl2 bold' width='30' align='center'> <?=$this->__("TELO_32")?></td> 
								<td class='tbl2 bold' align='center'><?=$this->__("TELO_33")?></td> 
								<td class='tbl2 bold' align='center'><?=$this->__("TELO_34")?></td>
								<td class='tbl1 bold' align='center'><?=$this->__("TELO_35")?></td>
								<td class='tbl2 bold' width='30' align='center'> <?=$this->__("TELO_32")?> </td>
								<td class='tbl2 bold' align='center'><?=$this->__("TELO_36")?></td>
								<td class='tbl2 bold' align='center'><?=$this->__("TELO_38")?></td>
							</tr>
						</table>
				</td>
			</tr>	
			<tr id='Message'> <td class='tbl1' style='text-align:center;' colspan='2'><?=$this->__("TELO_38")?></td> </tr>
		</table>
	</div>
</div>