<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_protests.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?> 
<!--Preddefined protests-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='6' class='forum-caption'><?=$this->__("TPPR_1")?></td></tr>
	<tr>
		<td class='tbl1 bold'> <?=$this->__("TPPR_2")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPPR_3")?> <?=$this->Tooltip($this->__("TPPR_4"))?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPPR_5")?> <?=$this->Tooltip($this->__("TPPR_6"))?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPPR_7")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPPR_8")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPPR_9")?> </td>
	</tr>
		<?php 
			djmDB::Select("SELECT * FROM ".dbProtestPattern);
			if (djmDB::Num()) {
			foreach (djmDB::fullData() as $protest) {
					$league_name = "";
					$array_league = explode(".", $protest['pattern_league']);
					for($i=0; $i<=count($array_league); $i++) {
						djmDB::Select("SELECT league_name FROM ".dbLeague." WHERE league_id='%d'", $array_league[$i]);
						$league_name .= djmDB::Data('league_name')."<br/>";
					}
		?>
			<tr>
				<td class='tbl1' width='200'> <?=$protest['pattern_name']?> </td>
				<td class='tbl1' align='center'> <?=($protest['pattern_preddefined'] != "" ? "<span class='green'>".$this->__("TPPR_10")."</span>":"<span class='red'>".$this->__("TPPR_11")."</span>")?> </td>
				<td class='tbl1' align='center'> <?=($protest['pattern_use_custom_description'] == 1 ? "<span class='green'>".$this->__("TPPR_10")."</span>":"<span class='red'>".$this->__("TPPR_11")."</span>")?> </td>
				<td class='tbl1' align='center'> <?php echo count(explode(".", $protest['pattern_league'])); ?> <?=$this->Tooltip("<strong>".$this->__("TPPR_12").":</strong> <br/><br/> ".$league_name)?> </td>
				<td class='tbl1' align='center'> <?=$this->__("TPPR_13")?>: <?=dbcount("(protest_id)", dbProtest, "protest_pattern='".$protest['pattern_id']."'")?> </td>
				<td class='tbl1' align='center' width='110'> <a href='<?=$page['location']?>&amp;edit_protest=<?=$protest['pattern_id']?>'><?=$this->__("TPPR_14")?></a> - <a href='<?=$page['location']?>&amp;delete_protest=<?=$protest['pattern_id']?>'><?=$this->__("TPPR_15")?></a> </td>
			</tr>
		<?php } } else { ?>
			<tr> <td colspan='6' class='tbl1' align='center'> <?=$this->__("TPPR_16")?> </td> </tr>
		<?php } ?>
</table>
<br/>

<?php
if (isset($_GET['edit_protest']) && isnum($_GET['edit_protest'])) {
$this->Form("EditProtestPattern");
$this->Input("hidden", "parent", $edit['protest']['pattern_id']);
?>
<table border='0' align='center' cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
	<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TPPR_17")?></td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_18")?>:</td> <td class='tbl1'> <?=$this->Input("text", "pattern_name", $edit['protest']['pattern_name'], $this->__("TPPR_19"))?> <?=$this->Tooltip($this->__("TPPR_20"), true)?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_21")?>:</td> <td class='tbl1'> <textarea name='pattern_preddefined' class='textbox' style='width: 380px; height: 100px;'><?=$edit['protest']['pattern_preddefined']?></textarea> <?=$this->Tooltip($this->__("TPPR_22"), true)?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_23")?>:</td> <td class='tbl1'> <?=$this->Input("checkbox", "pattern_custom_desc", 1, ($edit['protest']['pattern_use_custom_description'] == 1 ? true:false))?> <?=$this->Tooltip($this->__("TPPR_24"), true)?> </td> </tr>
	<tr> 
		<td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_25")?>:</td> <td class='tbl1'> 
				<?php
					$this->Tooltip($this->__("TPPR_26"), true);
					$this->input("checkbox", "pattern_league_all", 1, null, ($edit['protest']['pattern_league'] == "" ? true:false)); echo "<strong>".$this->__("TPPR_27")."</strong><br/>";
					djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game ORDER BY league_game ASC");
					if (djmDB::Num()) {
					foreach (djmDB::fullData() as $league) {
					$leagues = explode(".", $edit['protest']['pattern_league']);
					if (in_array($league['league_id'], $leagues)) { $checked = true; } else { $checked = false; }
				?>
					<?=$this->Input("checkbox", "pattern_league[]", $league['league_id'], null, $checked)?> <?=$this->Icon($league['game_icon'], "Game")?> <?=$league['league_name']?> <br/>
				<?php } } else { ?>
					<?=$this->Input("hidden", "pattern_league_all", 1)?>
					<span class='red'><?=$this->__("TPPR_28")?></span>
				<?php } ?>
		</td> 
	</tr>
	<tr> <td colspan='2' class='tbl1'> <?=$this->Input("submit", "submit", $this->__("TPPR_29"))?> <?=$this->__("TPPR_30")?> <a href='<?=$page['location']?>'><?=$this->__("TPPR_31")?></a> </td> </tr>
</table>
<?php }else{ ?>
<!--Create protest-->
<?=$this->Form("CreateProtestPattern")?>
<table border='0' align='center' cellpadding='0' cellspacing='1' class='tbl-border' width='100%'>
	<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TPPR_32")?></td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_18")?>:</td> <td class='tbl1'> <?=$this->Input("text", "pattern_name", null, $this->__("TPPR_19"))?> <?=$this->Tooltip($this->__("TPPR_20"), true)?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_21")?>:</td> <td class='tbl1'> <textarea name='pattern_preddefined' class='textbox' style='width: 380px; height: 100px;'></textarea> <?=$this->Tooltip($this->__("TPPR_22"), true)?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_23")?>:</td> <td class='tbl1'> <?=$this->Input("checkbox", "pattern_custom_desc", 1)?> <?=$this->Tooltip($this->__("TPPR_24"), true)?> </td> </tr>
	<tr> 
		<td class='tbl1' width='30%' align='right'> <?=$this->__("TPPR_25")?>:</td> <td class='tbl1'> 
				<?php
					$this->Tooltip($this->__("TPPR_26"), true);
					$this->input("checkbox", "pattern_league_all", 1, null, true); echo "<strong>".$this->__("TPPR_27")."</strong><br/>";
					djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game ORDER BY league_game ASC");
					if (djmDB::Num()) {
					foreach (djmDB::fullData() as $league) {
				?>
					<?=$this->Input("checkbox", "pattern_league[]", $league['league_id'])?> <?=$this->Icon($league['game_icon'], "Game")?> <?=$league['league_name']?> <br/>
				<?php } } else { ?>
					<?=$this->Input("hidden", "pattern_league_all", 1)?>
					<span class='red'><?=$this->__("TPPR_28")?></span>
				<?php } ?>
		</td> 
	</tr>
	<tr> <td colspan='2' class='tbl1'> <?=$this->Input("submit", "submit", $this->__("TPPR_33"))?> </td> </tr>
</table>
<?php } ?>