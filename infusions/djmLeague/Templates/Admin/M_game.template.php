<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_game.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?> 
<!-- Games -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='forum-caption' colspan='3'> <?=$this->__("TGAM_1")?> </td> </tr>
	<?php
		djmDB::Select("SELECT * FROM ".dbGame);
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $game) {
	?>
	<tr>
		<td class='tbl1'> <?=$this->Icon($game['game_icon'], "Game")?> <?=$game['game_name']?> </td>
		<td class='tbl1' width='150' align='center'> <?=$this->__("TGAM_2")?>: <strong><?=dbcount("(*)", dbLeague, "league_game='".$game['game_id']."'")?></strong> <?=$this->Tooltip($this->__("TGAM_3"))?> </td>
		<td class='tbl1' width='120' align='center'> <a href='<?=$page['location']?>&amp;edit_game=<?=$game['game_id']?>'><?=$this->__("TGAM_4")?></a> - <a href='<?=$page['location']?>&amp;delete_game=<?=$game['game_id']?>'><?=$this->__("TGAM_5")?></a> </td>
	</tr>
	<?php } } else { ?>
	<tr>
		<td class='tbl1' align='center' colspan='4'><?=$this->__("TGAM_6")?></td>
	</tr>
	<?php } ?>
</table>
<br/>

<?php
	if (isset($_GET['edit_game']) && isnum($_GET['edit_game'])) {
	$this->Form("EditGame", "POST", null, true);
	$this->Input("hidden", "parent", $edit['game']['game_id']);
?>
<!-- Edit game -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='forum-caption' colspan='4'> <?=$this->__("TGAM_7")?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TGAM_8")?>: </td> <td class='tbl1'> <?=$this->Input("text", "game_name", $edit['game']['game_name'], $this->__("TGAM_8"))?> <?=$this->Tooltip($this->__("TGAM_9"), true)?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TGAM_10")?>: </td> <td class='tbl1'> <?=$this->Input("file", "game_icon")?> (<?=$this->__("TGAM_11")?>) <?=$this->Tooltip($this->__("TGAM_12"), true)?> </td> </tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TGAM_13"))?> <?=$this->__("TGAM_14")?> <a href='<?=$page['location']?>'><?=$this->__("TGAM_15")?></a> </td> </tr>
</table>
</form>

<?php } else { ?>
<!-- Create game -->
<?=$this->Form("CreateGame", "POST", null, true)?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='forum-caption' colspan='4'> <?=$this->__("TGAM_16")?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TGAM_8")?>: </td> <td class='tbl1'> <?=$this->Input("text", "game_name", null, $this->__("TGAM_8"))?> <?=$this->Tooltip($this->__("TGAM_9"), true)?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TGAM_10")?>: </td> <td class='tbl1'> <?=$this->Input("file", "game_icon")?> <?=$this->Tooltip($this->__("TGAM_12"), true)?> </td> </tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submi", $this->__("TGAM_16"))?> </td> </tr>
</table>
</form>
<?php } ?>