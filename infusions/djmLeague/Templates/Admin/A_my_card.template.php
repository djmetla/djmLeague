<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: A_my_card.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>
<!-- Games --> 
<?=$this->Form("AdminVisitCard")?>
<?=$this->Input("hidden", "parent", $admin['admin_id'])?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TMC_1")?>:</td> <td class='tbl1'> <?=$this->Input("text", "admin_name", $admin['admin_name'], $this->__("TMC_2"))?> </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TMC_3")?>:</td> <td class='tbl1'> <?=$this->Input("text", "admin_surname", $admin['admin_surname'], $this->__("TMC_4"))?>  </td> </tr>
	<tr> <td class='tbl1' width='30%' align='right'> <?=$this->__("TMC_5")?>:</td> <td class='tbl1'> <textarea name='admin_description' class='textbox' placeholder='<?=$this->__("TMC_6")?>' maxlength='250' style='width: 500px; height: 50px;'><?=$admin['admin_description']?></textarea> </td> </tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TMC_7"))?> </td> </tr>
</table>