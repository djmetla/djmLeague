<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  M_logs.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }

$sday = array($this->__("TLOG_1"), $this->__("TLOG_2"), $this->__("TLOG_3"), $this->__("TLOG_4"), $this->__("TLOG_5"), $this->__("TLOG_6"), $this->__("TLOG_7"));
$day = array($this->__("TLOG_8"), $this->__("TLOG_9"), $this->__("TLOG_10"), $this->__("TLOG_11"), $this->__("TLOG_12"), $this->__("TLOG_13"), $this->__("TLOG_14"));
$month = array($this->__("TLOG_15"), $this->__("TLOG_16"), $this->__("TLOG_17"), $this->__("TLOG_18"), $this->__("TLOG_19"), $this->__("TLOG_20"), $this->__("TLOG_21"), $this->__("TLOG_22"), $this->__("TLOG_23"), $this->__("TLOG_24"), $this->__("TLOG_25"), $this->__("TLOG_26"));


$filter = array(
	'a_accounts' => $this->__("TLOG_27"),
	'a_protests' => $this->__("TLOG_28"),
	'a_statistics' => $this->__("TLOG_29"),
	'a_penalties_and_points' => $this->__("TLOG_30"),
	'a_my_card' => $this->__("TLOG_31"),
	'a_banlist' => $this->__("TLOG_32"),
	'm_league' => $this->__("TLOG_33"),
	'm_game' => $this->__("TLOG_34"),
	'm_accounts' => $this->__("TLOG_35"),
	'm_admins' => $this->__("TLOG_36"),
	'm_protests' => $this->__("TLOG_37"),
	'm_requests' => $this->__("TLOG_38"),
	'm_logs' => $this->__("TLOG_39"),
	'm_elo' => $this->__("TLOG_40"),
	'm_backups' => $this->__("TLOG_41"),
	'm_modification' => $this->__("TLOG_42")
);

 ?>
 <script type='text/javascript'>
 $(function(){
	
	$('input[name="filter_start"]').datepicker({
		firstDay: 1,
		maxDate: "-1d",
		dateFormat: "dd.mm.yy",
		dayNames: [ "<?=$day[6]?>", "<?=$day[0]?>", "<?=$day[1]?>", "<?=$day[2]?>", "<?=$day[3]?>", "<?=$day[4]?>", "<?=$day[5]?>" ],
		dayNamesMin: [ "<?=$sday[6]?>", "<?=$sday[0]?>", "<?=$sday[1]?>", "<?=$sday[2]?>", "<?=$sday[3]?>", "<?=$sday[4]?>", "<?=$sday[5]?>" ],
		dayNamesShort: [ "<?=$sday[6]?>", "<?=$sday[0]?>", "<?=$sday[1]?>", "<?=$sday[2]?>", "<?=$sday[3]?>", "<?=$sday[4]?>", "<?=$sday[5]?>"  ],
		monthNames: [ "<?=$month[0]?>", "<?=$month[1]?>", "<?=$month[2]?>", "<?=$month[3]?>", "<?=$month[4]?>", "<?=$month[5]?>", "<?=$month[6]?>", "<?=$month[7]?>", "<?=$month[8]?>", "<?=$month[9]?>", "<?=$month[10]?>", "<?=$month[11]?>" ]
	});
	
	$('input[name="filter_stop"]').datepicker({
		firstDay: 1,
		maxDate: 0,
		dateFormat: "dd.mm.yy",
		dayNames: [ "<?=$day[6]?>", "<?=$day[0]?>", "<?=$day[1]?>", "<?=$day[2]?>", "<?=$day[3]?>", "<?=$day[4]?>", "<?=$day[5]?>" ],
		dayNamesMin: [ "<?=$sday[6]?>", "<?=$sday[0]?>", "<?=$sday[1]?>", "<?=$sday[2]?>", "<?=$sday[3]?>", "<?=$sday[4]?>", "<?=$sday[5]?>" ],
		dayNamesShort: [ "<?=$sday[6]?>", "<?=$sday[0]?>", "<?=$sday[1]?>", "<?=$sday[2]?>", "<?=$sday[3]?>", "<?=$sday[4]?>", "<?=$sday[5]?>"  ],
		monthNames: [ "<?=$month[0]?>", "<?=$month[1]?>", "<?=$month[2]?>", "<?=$month[3]?>", "<?=$month[4]?>", "<?=$month[5]?>", "<?=$month[6]?>", "<?=$month[7]?>", "<?=$month[8]?>", "<?=$month[9]?>", "<?=$month[10]?>", "<?=$month[11]?>" ]
	});
 
 });
 </script>
 
 <?php
 if (isset($_POST['make_filter'])) {
	
	// Time
	if (!empty($_POST['filter_start']) && !empty($_POST['filter_stop'])) {
		$start = explode(".", $_POST['filter_start']);
		$start_time = SelectTime::CreateTime(array($start[0], $start[1], $start[2]));
		$stop = explode(".", $_POST['filter_stop']);
		$stop_time = SelectTime::CreateTime(array($stop[0], $stop[1], $stop[2]));
		$time = "&amp;from=".$start_time."&amp;to=".$stop_time;
	} else {
		$time = "";
	}
	// Section
	if (!empty($_POST['filter_section'])) {
		$section = "";
		for($i=0; $i < count($_POST['filter_section']); $i++) {
			$section .= ($section != "" ? ".":"").stripinput($_POST['filter_section'][$i]); 
		}				
	} else {
		$section = "";
	}
	
	$buildLink = "";
	if($section != "") { $buildLink .= "&amp;filter_page=".$section.""; } 
	if ($time != "") { $buildLink .= $time; }
	$this->location($page['location'].$buildLink);
 }
 ?>
 
 <!-- Logs filter -->
 <form action='' method='post'>
 <table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='forum-caption'> <?=$this->__("TLOG_43")?> </td> </tr>
	<tr> <td class='tbl1'> <strong><?=$this->__("TLOG_44")?></strong> <?=$this->__("TLOG_45")?>: <?=$this->Input("text", "filter_start", (isset($_GET['from']) ? strftime("%d.%m.%Y", $_GET['from']):null), $this->__("TLOG_46"))?><?=$this->__("TLOG_47")?> <?=$this->Input("text", "filter_stop", (isset($_GET['to']) ? strftime("%d.%m.%Y", $_GET['to']):null), $this->__("TLOG_46"))?> </td> </tr>
	<tr> <td class='tbl1'> 
			<table>
				<tr> 
				<?php 
					$i=0; 
					foreach($filter as $id => $v) { 
					$i++; 
					if (isset($_GET['filter_page']) && !empty($_GET['filter_page'])) {
						$cua = explode(".", $_GET['filter_page']);
						if (in_array($id, $cua)) { $checked = true; } else { $checked = false; }
					} 
				?> 
					<td width='200'> <input type='checkbox' name='filter_section[]' value='<?=$id?>' <?=($checked ? "checked='checked'":"")?> /><span style='position: relative; top:-2px;'><?=$v?></span></td>  
				<?php if(in_array($i, array(4,8,12,16))) { echo "</tr><tr>"; } } ?> 
				</tr>
			</table>
			</td> 
	</tr>
	<tr> <td class='tbl1'> <input type='submit' class='button' name='make_filter' value='<?=$this->__("TLOG_48")?>' /> <?=(isset($_GET['filter_page']) || isset($_GET['from']) ? $this->__("TLOG_49")." <a href='".$page['location']."'>".$this->__("TLOG_50")."</a>.":"")?></td> </tr>
</table>
</form>
 <br/>
 	
 <!--Logs -->
 <?php 
	$select_additional = "";	
	if (isset($_GET['filter_page']) && !empty($_GET['filter_page'])) {
		$val = explode(".", $_GET['filter_page']);
		$secure = array('a_accounts','a_protests','a_statistics','a_penalties_and_points','a_my_card','a_banlist','m_league','m_game','m_accounts','m_admins','m_protests','m_requests','m_complaint','m_logs','m_elo','m_backups','m_modification');
		
		$select_additional .= " (";
		for($i=0; $i<count($val); $i++) {
			if (in_array($val[$i], $secure)) {
				$select_additional .= " ".($i == 0 ? "":"OR")." log_page='".$val[$i]."'";
			}
		}
		$select_additional .= ")";
	} else {
		$select_additional = "";
	}
	
	if (isset($_GET['from']) && isnum($_GET['from']) && isset($_GET['to']) && isnum($_GET['to'])) {
		$s = $_GET['from']; $e = $_GET['to'];
		if ($s >= $e) {
			$select_additional .= "";
		} else {
			$select_additional .= " ".($select_additional != "" ? "AND":"")." (log_time BETWEEN ".$s." AND ".$e.")";
		}
	}
	
	if (!isset($_GET['rowstart']) || !isnum($_GET['rowstart'])) { $_GET['rowstart'] = 0; }
	$logs_per_page = 50;
	$rows_count = dbcount("(*)", dbAdminLog, ($select_additional != "" ? $select_additional:""));
	djmDB::Select("SELECT * FROM ".dbAdminLog." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.log_user ".($select_additional != "" ? "WHERE":"")." ".$select_additional." ORDER BY log_id DESC LIMIT ".$_GET['rowstart'].", ".$logs_per_page.""); $count = djmDB::Num(); 
 
		if (isset($_GET['filter_page']) && isset($_GET['from'])) {
			$target = $page['location']."&amp;filter_page=".$_GET['filter_page']."&amp;from=".$_GET['from']."&amp;".$_GET['to'];
		} elseif (isset($_GET['filter_page'])) {
			$target = $page['location']."&amp;filter_page=".$_GET['filter_page'];
		} elseif (isset($_GET['from'])) {
			$target = $page['location']."&amp;from=".$_GET['from']."&amp;".$_GET['to'];
		} else {
			$target = $page['location'];
		}
		
		echo "<div align='right' style=';margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $logs_per_page, $rows_count, 3, $target."&amp;")."\n</div>\n"; 
  ?>

  <table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td class='forum-caption' colspan='3'> <?=$this->__("TLOG_51")?> (<?=$count?>) </td> </tr>
	<?php 
		if ($count) {
		foreach (djmDB::fullData() as $log) {
	?>
		<tr>
			<td class='tbl1' width='120' align='center'> <?=strftime("%d.%m.%Y %H:%M", $log['log_time'])?></td>
			<td class='tbl1' align='center' width='120'> <a href='<?=urlProfile.$log['user_id']?>'><?=$log['user_name']?></a> </td>
			<td class='tbl1'><span class='bold small2'><?=$filter[$log['log_page']]?></span><br/> <?=$log['log_value']?> </td>
		</tr>	
	<?php } } else { ?>
		<tr> <td colspan='3' align='center'> <?=$this->__("TLOG_52")?> </td> </tr>
	<?php } ?>
  </table>
  <?php echo "<div align='right' style=';margin-top:5px;'>\n".makepagenav($_GET['rowstart'], $logs_per_page, $rows_count, 3, $target."&amp;")."\n</div>\n"; ?>