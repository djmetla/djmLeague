<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  M_admins.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?> 

<!-- Admin list -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TADM_1")?> </td> </tr>
	<?php 
		djmDB::Select("SELECT * FROM ".dbAdminCategory);
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $cat) {
	?>
		<tr>
			<td class='tbl1'> <?=THEME_BULLET." ".$cat['category_name']?> (<?=dbcount("(*)", dbAdmin, "admin_category='".$cat['category_id']."'")?>)</td>
			<td class='tbl1' align='center'>	<a href='<?=$page['location']?>&amp;edit_cat=<?=$cat['category_id']?>'><?=$this->__("TADM_2")?></a> - <a href='<?=$page['location']?>&amp;delete_cat=<?=$cat['category_id']?>'><?=$this->__("TADM_3")?></a> </td>
		</tr>	
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center' colspan='2'> <?=$this->__("TADM_4")?> </td> </tr>
	<?php } ?>

<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TADM_5")?> </td> </tr>
<tr> 
	<td class='tbl1' colspan='2'>
		<?php 
			if (isset($_GET['edit_cat']) && isnum($_GET['edit_cat'])) {
				$this->Form("EditAdminCategory");
				$this->Input("hidden", "parent", $edit['category']['category_id']);
				$this->Input("text", "category_name", $edit['category']['category_name'], $this->__("TADM_6"));
				$this->Input("submit", "submit", $this->__("TADM_7"));			
				echo $this->__("TADM_8")." <a href='".$page['location']."'>".$this->__("TADM_9")."</a>";
			} else {
				$this->Form("CreateAdminCategory");
				$this->Input("text", "category_name", null, $this->__("TADM_6"));
				$this->Input("submit", "submit", $this->__("TADM_10"));				
			}
		?>	
		</form>
	</td> 
</tr>
</table>
<br/>


<!-- Admin list -->
<table border='0' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='3' class='forum-caption'> <?=$this->__("TADM_11")?> </td> </tr>
	<?php
		djmDB::Select("SELECT * FROM ".dbAdmin." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.admin_user LEFT JOIN ".dbAdminCategory." as t3 ON t3.category_id=t1.admin_category ORDER BY admin_category ASC");
		if (djmDB::Num()) {
		foreach(djmDB::fullData() as $admin) {
	?>
	<tr>
		<td class='tbl1'> <a href='<?=urlProfile.$admin['user_id']?>'><?=$admin['user_name']?></a> (AID #<?=$admin['admin_id']?>) </td>
		<td class='tbl1' width='200' align='center'> <?=$admin['category_name']?> </td>
		<td class='tbl1' width='150' align='center'> <a href='<?=$page['location']?>&amp;edit_admin=<?=$admin['admin_id']?>'><?=$this->__("TADM_2")?></a> - <a href='<?=$page['location']?>&amp;delete_admin=<?=$admin['admin_id']?>'><?=$this->__("TADM_3")?></a> </td>
	</tr>
	<?php } } else { ?>
		<tr> <td class='tbl1' colspan='3' align='center'> <?=$this->__("TADM_12")?> </td> </tr>
	<?php } ?>
</table>
<br/>		

<!-- Create & update admin -->
<?php
	/* EDIT ADMIN */
	if (isset($_GET['edit_admin']) && isnum($_GET['edit_admin'])) {
	$this->Form("EditAdmin"); $this->Input("hidden", "parent", $edit['admin']['admin_id']);
?>
<table border='0' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='2' class='forum-caption'><?=$this->__("TADM_13")?></td> </tr>
	
	<tr> 
		<td class='tbl1' align='right'> <?=$this->__("TADM_14")?>:</td> 
		<td class='tbl1'> 
				<a href='<?=urlProfile.$edit['admin']['user_id']?>'><?=$edit['admin']['user_name']?></a>
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right'> <?=$this->__("TADM_15")?>:</td> 
		<td class='tbl1'> 
				<?php
					djmDB::Select("SELECT * FROM ".dbAdminCategory);
					if (djmDB::Num()) {
				?>		
				<select name='admin_category' class='textbox' style='width: 250px;'>
					<?php foreach(djmDB::fullData() as $cat) { ?>
						<option value='<?=$cat['category_id']?>' <?=($cat['category_id'] == $edit['admin']['admin_category'] ? "selected='selected'":"")?>><?=$cat['category_name']?></option>
					<?php } ?>
				</select>
				<?php } else { ?>
					<span class='red'><?=$this->__("TADM_4")?></span>
				<?php } ?>
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' valign='top'><?=$this->__("TADM_16")?>:</td> 
		<td class='tbl1'> 
				<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
					<!-- Leagues-->
					<tr> <td class='forum-caption'><?=$this->__("TADM_17")?> </td> </tr>
					<tr> <td class='tbl2'> <input type='checkbox' name='admin_league_all' value='1' <?=($edit['admin']['admin_league'] == "" ? "checked='checked'":"")?> /> <span class='bold' style='position:relative; top: -2px;'><?=$this->__("TADM_18")?></span> </td> </tr>
					<?php
						djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game");
						if (djmDB::Num()) {
						foreach (djmDB::fullData() as $league) {
						$league_flags = explode(".", $edit['admin']['admin_league']);
						if (in_array($league['league_id'], $league_flags)) { $checked = TRUE; } else { $checked = FALSE; }
					?>
						<tr> <td class='tbl2'>
						<?php echo "<input type='checkbox' name='admin_league[]' value='".$league['league_id']."' ".($checked ? "checked='checked'":"")." /> "; $this->Icon($league['game_icon'], "Game"); echo " <span style='position:relative; top: -2px;'>".$league['league_name']."</span>"; ?>
						</td> </tr>
					<?php } } else { ?>
						<tr> <td class='tbl1' align='center'> <?=$this->__("TADM_19")?> </td> </tr>
					<?php } ?>
					
					<!-- Game accounts-->
					<tr> <td>&nbsp;</td> </tr>
					<tr> <td class='forum-caption'><?=$this->__("TADM_20")?> </td> </tr>
					<tr> <td class='tbl2'> <input type='checkbox' name='admin_account_all' value='1' <?=($edit['admin']['admin_account'] == "" ? "checked='checked'":"")?> /> <span class='bold' style='position:relative; top: -2px;'><?=$this->__("TADM_21")?></span> </td> </tr>
					<?php
						djmDB::Select("SELECT * FROM ".dbAccountsPattern);
						if (djmDB::Num()) {
						foreach (djmDB::fullData() as $acc) {
						$acc_flags = explode(".", $edit['admin']['admin_account']);
						if (in_array($acc['pattern_key'], $acc_flags)) { $checked = TRUE; } else { $checked = FALSE; }
					?>
						<tr> <td class='tbl2'>
						<?php echo "<input type='checkbox' name='admin_account[]' value='".$acc['pattern_key']."' ".($checked ? "checked='checked'":"")." /> "; echo "<span style='position:relative; top: -2px;'>".$acc['pattern_name']."</span>"; ?>
						</td> </tr>
					<?php } } else { ?>
						<tr> <td class='tbl1' align='center'> <?=$this->__("TADM_22")?> </td> </tr>
					<?php } ?>					
				
				
					<!--Access-->
					<tr> <td>&nbsp;</td> </tr>
					<tr> <td class='forum-caption'><?=$this->__("TADM_23")?></td> </tr>
					<tr> <td class='tbl1 bold'> <?=$this->__("TADM_24")?> <?=$this->Tooltip($this->__("TADM_25"))?></td> </tr>
					<?php 
					foreach ($config['admin_flags'] as $flag => $desc) { 
					if ($flag == "ACCESS_M_LEAGUE") { ?> <tr> <td class='tbl1 bold'> <?php echo $this->__("TADM_26"); $this->Tooltip($this->__("TADM_27")); ?> </td> </tr> <?php }
					if (in_array($flag, $edit['flags'])) { $checked = TRUE; } else { $checked = FALSE; }
					?>
						<tr> <td class='tbl2'>
						<?php $this->Input("checkbox", "admin_flag[]", $flag, null, $checked); echo "<span style='position:relative; top: -2px;'>".$desc."</span>"; ?>
						</td></tr>
					<?php }  ?>
				</table> 
		</td> 
	</tr>
	
	<tr>
		<td class='tbl1' align='left' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TADM_28"))?> <?=$this->__("TADM_8")?> <a href='<?=$page['location']?>'><?=$this->__("TADM_29")?></a> </td> 
	</tr>
</table>
</form>

<?php 
	/* ADD ADMIN */ 
	} else { 
?>

<?=$this->Form("AddAdmin")?>
<table border='0' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='2' class='forum-caption'><?=$this->__("TADM_30")?></td> </tr>
	
	<tr> 
		<td class='tbl1' align='right'> <?=$this->__("TADM_31")?>:</td> 
		<td class='tbl1'> 
				<select name='admin_user' class='textbox' style='width: 250px;'>
					<option value=''></option>
					<?php 
						djmDB::Select("SELECT * FROM ".DB_USERS." ORDER BY user_name ASC");
						foreach(djmDB::fullData() as $user) {
					?>
						<option value='<?=$user['user_id']?>'><?=$user['user_name']?></option>
					<?php } ?>
				</select>
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right'> <?=$this->__("TADM_15")?>:</td> 
		<td class='tbl1'> 
				<?php
					djmDB::Select("SELECT * FROM ".dbAdminCategory);
					if (djmDB::Num()) {
				?>		
				<select name='admin_category' class='textbox' style='width: 250px;'>
					<option value=''></option>
					<?php foreach(djmDB::fullData() as $cat) { ?>
						<option value='<?=$cat['category_id']?>'><?=$cat['category_name']?></option>
					<?php } ?>
				</select>
				<?php } else { ?>
					<span class='red'><?=$this->__("TADM_4")?></span>
				<?php } ?>
		</td> 
	</tr>
	
	<tr> 
		<td class='tbl1' align='right' valign='top'> <?=$this->__("TADM_16")?>:</td> 
		<td class='tbl1'> 
				<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>
					<!-- Leagues-->
					<tr> <td class='forum-caption'><?=$this->__("TADM_17")?></td> </tr>
					<tr> <td class='tbl2'> <input type='checkbox' name='admin_league_all' value='1' checked='checked' /> <span class='bold' style='position:relative; top: -2px;'><?=$this->__("TADM_18")?></span> </td> </tr>
					<?php
						djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game");
						if (djmDB::Num()) {
						foreach (djmDB::fullData() as $league) {
					?>
						<tr> <td class='tbl2'>
						<?php echo "<input type='checkbox' name='admin_league[]' value='".$league['league_id']."' /> "; $this->Icon($league['game_icon'], "Game"); echo " <span style='position:relative; top: -2px;'>".$league['league_name']."</span>"; ?>
						</td> </tr>
					<?php } } else { ?>
						<tr> <td class='tbl1' align='center'> <?=$this->__("TADM_19")?> </td> </tr>
					<?php } ?>
					
					<!-- Game accounts-->
					<tr> <td>&nbsp;</td> </tr>
					<tr> <td class='forum-caption'><?=$this->__("TADM_20")?> </td> </tr>
					<tr> <td class='tbl2'> <input type='checkbox' name='admin_account_all' value='1' checked='checked' /> <span class='bold' style='position:relative; top: -2px;'><?=$this->__("TADM_21")?></span> </td> </tr>
					<?php
						djmDB::Select("SELECT * FROM ".dbAccountsPattern);
						if (djmDB::Num()) {
						foreach (djmDB::fullData() as $acc) {
					?>
						<tr> <td class='tbl2'>
						<?php echo "<input type='checkbox' name='admin_account[]' value='".$acc['pattern_key']."' /> "; echo "<span style='position:relative; top: -2px;'>".$acc['pattern_name']."</span>"; ?>
						</td> </tr>
					<?php } } else { ?>
						<tr> <td class='tbl1' align='center'> <?=$this->__("TADM_22")?> </td> </tr>
					<?php } ?>					
				
				
					<!--Access-->
					<tr> <td>&nbsp;</td> </tr>
					<tr> <td class='forum-caption'><?=$this->__("TADM_23")?></td> </tr>
					<tr> <td class='tbl1 bold'> <?=$this->__("TADM_24")?> <?=$this->Tooltip($this->__("TADM_25"))?></td> </tr>
					<?php 
					foreach ($config['admin_flags'] as $flag => $desc) { 
					if ($flag == "ACCESS_M_LEAGUE") { ?> <tr> <td class='tbl1 bold'> <?php echo $this->__("TADM_26"); $this->Tooltip($this->__("TADM_27")); ?> </td> </tr> <?php }
					?>
						<tr> <td class='tbl2'>
						<?php $this->Input("checkbox", "admin_flag[]", $flag, null); echo "<span style='position:relative; top: -2px;'>".$desc."</span>"; ?>
						</td></tr>
					<?php }  ?>
				</table> 
		</td> 
	</tr>
	
	<tr>
		<td class='tbl1' align='left' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TADM_30"))?> </td> 
	</tr>
</table>
</form>
<?php } ?>