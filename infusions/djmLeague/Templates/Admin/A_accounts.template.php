<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: A_accounts.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); } 
?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='7' class='forum-caption'> <?=$this->__("TAC_1")?> (<?=$count?>) </td> </tr>
	<tr>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_2")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_3")?></td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_4")?></td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_5")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_6")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_7")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TAC_8")?> </td>
	</tr>	
	
	<?php 
		if ($count) { 
		foreach($accounts AS $acc) {
			$only_acc = explode(".", $access['admin_account']);
			if (iSUPERADMIN OR in_array($acc['pattern_key'], $only_acc)) {
				$new = str_replace(" ", "<span class='red'>_</span>", $acc['account_new_value']);
				$collision = Collision($acc['account_new_value'], $acc['pattern_key'], $acc['account_user']);
	?>
	<tr>
		<td class='tbl1'> <a href='<?=urlProfile.$acc['user_id']?>'><?=$acc['user_name']?></a></td>
		<td class='tbl1 bold' align='center'> <?=$acc['pattern_name']?> </td>
		<td class='tbl1' align='center'> <?=$new?> </td>
		<td class='tbl1' align='center'> <?=($acc['account_value'] == "" ? "<em>".$this->__("TAC_9")."</em>":$acc['account_value'])?> </td>
		<td class='tbl1' align='center'> <?=($collision['status'] ? "<span class='red'>".$this->__("TAC_10")."</span>".$this->Tooltip($collision['tooltip']):"<span class='green'>".$this->__("TAC_11")."</span>")?> </td>
		<td class='tbl1' align='center'> <?=($acc['account_value'] == "" ? "-":strftime("%d.%m.%Y %H:%M", $acc['account_last_change']))?> </td>
		<td class='tbl1' align='center'> 
			<a href='javascript:void(0)' onclick="if (confirm('<?=$this->__("TAC_12")?>')) { location.href='<?=$page['location']?>&amp;accept=<?=$acc['account_id']?>'; }"><?=$this->Icon("accept.png")?></a> 
			<a href='javascript:void(0)' onclick="if (confirm('<?=$this->__("TAC_13")?>')) { location.href='<?=$page['location']?>&amp;decline=<?=$acc['account_id']?>'; }"><?=$this->Icon("decline.png")?></a>
		</td>
	</tr>	
	<?php } } } else {?>
	  <tr> <td class='tbl1' align='center' colspan='7'> <?=$this->__("TAC_14")?> </td> </tr>
	<?php } ?>
</table>