<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: M_requests.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>
 <!-- Preddefined requests list -->
 <table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'><?=$this->__("TPRQ_1")?></td></tr>
	<tr> 
		<td class='tbl1 bold' align='center'> <?=$this->__("TPRQ_2")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPRQ_3")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPRQ_4")?> </td>
		<td class='tbl1 bold' align='center'> <?=$this->__("TPRQ_5")?> </td>
	</tr>
	
	<?php
		djmDB::Select("SELECT * FROM ".dbRequestPattern);
		if (djmDB::Num()) {
		foreach(djmDB::fullData() as $request) {
		$league_name = "";
		$array_league = explode(".", $request['pattern_league']);
		for($i=0; $i<=count($array_league); $i++) {
			djmDB::Select("SELECT league_name FROM ".dbLeague." WHERE league_id='%d'", $array_league[$i]);
			$league_name .= djmDB::Data('league_name')."<br/>";
		}		
	?>
		<tr>
			<td class='tbl1'> <?=$request['pattern_name']?> </td>
			<td class='tbl1' align='center'> <?=count($array_league)?> <?=$this->Tooltip("<strong>".$this->__("TPRQ_6").":</strong> <br/><br/> ".$league_name)?> </td>
			<td class='tbl1' align='center'> <?=$this->__("TPRQ_7")?>: <?=dbcount("(*)", dbRequest, "request_pattern='".$request['pattern_id']."'")?> </td>
			<td class='tbl1' align='center' width='120'> <a href='<?=$page['location']?>&amp;edit_request=<?=$request['pattern_id']?>'><?=$this->__("TPRQ_8")?></a> - <a href='<?=$page['location']?>&amp;delete_request=<?=$request['pattern_id']?>'><?=$this->__("TPRQ_9")?></a> </td>
		</tr>
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center' colspan='4'> <?=$this->__("TPRQ_10")?> </td> </tr>
	<?php } ?>
</table>
 <br/>
 
 <?php
	if (isset($_GET['edit_request']) && isnum($_GET['edit_request'])) {
	$this->Form("EditRequestPattern");
	$this->Input("hidden", "parent", $edit['request']['pattern_id']);
?>
 <table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='2' class='forum-caption'><?=$this->__("TPRQ_11")?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TPRQ_12")?>:</td> <td class='tbl1'> <?=$this->Input("text", "pattern_name", $edit['request']['pattern_name'], $this->__("TPRQ_13"))?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%' valign='top'> <?=$this->__("TPRQ_14")?>:</td> 
			<td class='tbl1'> 
				<textarea name='pattern_text' class='textbox' style='width:380px; height: 100px;'><?=$edit['request']['pattern_preddefined']?></textarea> <?=$this->Tooltip($this->__("TPRQ_15"), true)?> <br/>
				<strong>%name%</strong> = <?=$this->__("TPRQ_16")?><br/>
				<strong>%id%</strong> = <?=$this->__("TPRQ_17")?> <br/>
			</td> 
	</tr>
	<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TPRQ_18")?>:</td> 
			<td class='tbl1'>
				<?php
				$this->Tooltip($this->__("TPRQ_19"), true);
				$this->input("checkbox", "pattern_league_all", 1, null, ($edit['request']['pattern_league'] == "" ? true:false)); echo "<strong>".$this->__("TPRQ_20")."</strong><br/>";
				djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game ORDER BY league_game ASC");
				if (djmDB::Num()) {
				foreach (djmDB::fullData() as $league) {
					$leagues = explode(".", $edit['request']['pattern_league']);
					if (in_array($league['league_id'], $leagues)) { $checked = true; } else { $checked = false; }
				?>
					<?=$this->Input("checkbox", "pattern_league[]", $league['league_id'], null, $checked)?> <?=$this->Icon($league['game_icon'], "Game")?> <?=$league['league_name']?> <br/>
				<?php } } else { ?>
					<?=$this->Input("hidden", "pattern_league_all", 1)?>
					<span class='red'><?=$this->__("TPRQ_21")?></span>				
				<?php } ?>
			</td> 
	</tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TPRQ_22"))?> <?=$this->__("TPRQ_23")?> <a href='<?=$page['location']?>'><?=$this->__("TPRQ_24")?></a> </td> </tr>
</table>  
 
 <?php } else { ?>
 <!-- Create request -->
 <?=$this->Form("CreateRequestPattern")?>
 <table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='2' class='forum-caption'><?=$this->__("TPRQ_25")?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TPRQ_12")?>:</td> <td class='tbl1'> <?=$this->Input("text", "pattern_name", null, $this->__("TPRQ_13"))?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%' valign='top'> <?=$this->__("TPRQ_14")?>:</td> 
			<td class='tbl1'> 
				<textarea name='pattern_text' class='textbox' style='width:380px; height: 100px;'></textarea> <?=$this->Tooltip($this->__("TPRQ_15"), true)?> <br/>
				<strong>%name%</strong> = <?=$this->__("TPRQ_16")?> <br/>
				<strong>%id%</strong> = <?=$this->__("TPRQ_17")?> <br/>
			</td> 
	</tr>
	<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TPRQ_18")?>:</td> 
			<td class='tbl1'>
				<?php
				$this->Tooltip($this->__("TPRQ_19"), true);
				$this->input("checkbox", "pattern_league_all", 1, null, true); echo "<strong>".$this->__("TPRQ_20")."</strong><br/>";
				djmDB::Select("SELECT * FROM ".dbLeague." as t1 LEFT JOIN ".dbGame." as t2 ON t2.game_id=t1.league_game ORDER BY league_game ASC");
				if (djmDB::Num()) {
				foreach (djmDB::fullData() as $league) {
				?>
					<?=$this->Input("checkbox", "pattern_league[]", $league['league_id'])?> <?=$this->Icon($league['game_icon'], "Game")?> <?=$league['league_name']?> <br/>
				<?php } } else { ?>
					<?=$this->Input("hidden", "pattern_league_all", 1)?>
					<span class='red'><?=$this->__("TPRQ_21")?></span>				
				<?php } ?>
			</td> 
	</tr>
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TPRQ_26"))?> </td> </tr>
</table> 
 <?php } ?>
 