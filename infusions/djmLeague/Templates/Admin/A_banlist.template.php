<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: A_banlist.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
if (isset($_GET['ban_id']) && isnum($_GET['ban_id'])) { 
?>
	<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_1")?>:</td> <td class='tbl1'> <a href='<?=urlProfile.$data['ban_user']?>'><?=$data['user']?></a></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_2")?>:</td> <td class='tbl1'><?=strftime("%d.%m.%Y %H:%M", $data['ban_time'])?></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_3")?>:</td> <td class='tbl1'><?=strftime("%d.%m.%Y %H:%M", $data['ban_expiration'])?></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_4")?>:</td> <td class='tbl1'><?=(($data['ban_expiration']>time()) || ($data['ban_permanent'] == "YES") ? "<strong>".$this->__("TBA_9")."</strong>":"<strong>".$this->__("TBA_10")."</strong>")?></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_5")?>:</td> <td class='tbl1'><?=($data['ban_permanent'] == "YES" ? $this->__("TBA_11"):$this->__("TBA_12"))?></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_6")?>:</td> <td class='tbl1'><a href='<?=urlProfile.$data['ban_admin']?>'><?=$data['admin']?></a></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_7")?>:</td> <td class='tbl1'><?=$data['ban_reason']?></td></tr>
		<tr> <td class='tbl1' align='right' width='30%'><?=$this->__("TBA_8")?>:</td> <td class='tbl1'> <a href='<?=$page['location']?>&amp;unban=<?=$data['ban_id']?>' class='red'><?=$this->__("TBA_13")?></a></td></tr>
	</table>
<?php } else { ?>
<ul class='tabs-navi'>
	<li id='tab1' name='ActiveBans' class='hover button'> <?=$this->__("TBA_14")?> </li>
	<li id='tab2' name='History' class='button'> <?=$this->__("TBA_15")?> </li>
	<li id='tab3' name='NewBan' class='button'> <?=$this->__("TBA_16")?> </li>
</ul>

<div class='tabs-content tbl-border'>
	
	<!-- Active bans -->
	<div id='tab-1' class='tab'>
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_17")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_18")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_19")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_20")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_21")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_22")?> </td>
			</tr>
		
			<?php
				djmDB::Select("SELECT t1.*, t2.user_name AS user, t3.user_name AS admin 
										FROM ".dbBan." as t1 
										LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.ban_user
										LEFT JOIN ".DB_USERS." as t3 ON t3.user_id=t1.ban_admin
										WHERE ban_expiration>".time()." OR ban_permanent='YES'
										ORDER BY ban_expiration ASC
										");
				if (djmDB::Num()) {						
				foreach(djmDB::fullData() as $b) { 
			?>
				<tr>
					<td class='tbl1'> <a href='<?=urlProfile.$b['ban_user']?>'><?=$b['user']?></a> </td>
					<td class='tbl1' align='center'> <?=strftime("%d.%m.%Y %H:%M", $b['ban_time'])?> </td>
					<td class='tbl1' align='center'> <?=($b['ban_permanent'] == "YES" ? "---": strftime("%d.%m.%Y %H:%M", $b['ban_expiration']))?> </td>
					<td class='tbl1' align='center'> <?=($b['ban_permanent'] == "YES" ? $this->__("TBA_11"):$this->__("TBA_12"))?></td>
					<td class='tbl1' align='center'> <a href='<?=urlProfile.$b['ban_admin']?>'><?=$b['admin']?></a> </td>
					<td class='tbl1' align='center'> <a href='<?=$page['location']?>&amp;ban_id=<?=$b['ban_id']?>'><?=$this->Icon("more.png")?></a> </td>
				</tr>	
			<?php } } else { ?>
				<tr> <td colspan='6' class='tbl1' align='center'> <?=$this->__("TBA_23")?> </td> </tr>
			<?php } ?>
		</table>
	</div>
	
	<!-- Bans history -->
	<div id='tab-2' class='tab hide'>
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_17")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_18")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_20")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_21")?> </td>
				<td class='forum-caption' align='center'> <?=$this->__("TBA_22")?> </td>
			</tr>
		
			<?php
				djmDB::Select("SELECT t1.*, t2.user_name AS user, t3.user_name AS admin 
										FROM ".dbBan." as t1 
										LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.ban_user
										LEFT JOIN ".DB_USERS." as t3 ON t3.user_id=t1.ban_admin
										WHERE ban_expiration<".time()." AND ban_permanent='NO'
										ORDER BY ban_expiration ASC
										");
				if (djmDB::Num()) {						
				foreach(djmDB::fullData() as $b) { 
			?>
				<tr>
					<td class='tbl1'> <a href='<?=urlProfile.$b['ban_user']?>'><?=$b['user']?></a> </td>
					<td class='tbl1' align='center'> <?=strftime("%d.%m.%Y %H:%M", $b['ban_time'])?> </td>
					<td class='tbl1' align='center'> <?=($b['ban_permanent'] == "YES" ? "---": strftime("%d.%m.%Y %H:%M", $b['ban_expiration']))?> </td>
					<td class='tbl1' align='center'> <a href='<?=urlProfile.$b['ban_admin']?>'><?=$b['admin']?></a> </td>
					<td class='tbl1' align='center'> <a href='<?=$page['location']?>&amp;ban_id=<?=$b['ban_id']?>'><?=$this->Icon("more.png")?></a> </td>
				</tr>
			<?php } } else { ?>
				<tr> <td colspan='6' class='tbl1' align='center'> <?=$this->__("TBA_24")?> </td> </tr>
			<?php } ?>
		</table>	
	</div>
	
	<!-- New ban -->
	<div id='tab-3' class='tab hide'>
		<?=$this->Form("NewBan")?>
		<table class='tbl-border' align='center' width='100%' cellpadding='0' cellspacing='1'>
			<tr> 
				<td class='tbl1' align='right' width='30%'> <?=$this->__("TBA_25")?>:</td> 
				<td class='tbl1'> 
					<select name='ban_user' class='textbox' style='width: 250px;'> 
						<option value=''></option>
					<?php 
						djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user");
						foreach (djmDB::fullData() as $v) {
					?>
						<option value='<?=$v['player_user']?>'><?=$v['user_name']?></option>
					<?php } ?>
					</select>
				</td>
			</tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TBA_26")?>:</td> <td class='tbl1'> <?=SelectTime::Create("ban_expiration", "d|m|y|h|i", 3600)?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TBA_27")?>:</td> <td class='tbl1'> <?=$this->Input("checkbox", "ban_permanent", "YES")?> </td> </tr>
			<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TBA_28")?>:</td> <td class='tbl1'> <textarea name='ban_reason' class='textbox' style='width:350px;height:65px;' placeholder='<?=$this->__("TBA_29")?>'></textarea> </td> </tr>
			<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TBA_30"))?> </td> </tr>
		</table>
		</form>
	</div>	
</div>
<?php } ?>