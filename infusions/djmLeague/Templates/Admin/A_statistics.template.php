<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: A_statistics.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$cday = mktime(0,0,0, date("m"), date("d"), date("Y"));
$cweek = time()-604800;
$cmonth =  time()-2678000;
$ctime = time();

echo "<table border='0' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>";
	echo "<tr>";
		echo "<td class='tbl1 bold' align='center'> ".$this->__("TAST_1")." </td>";
		echo "<td class='tbl1 bold' align='center'> ".$this->__("TAST_2")." </td>";
		echo "<td class='tbl1 bold' align='center'> ".$this->__("TAST_3")." </td>";
		echo "<td class='tbl1 bold' align='center'> ".$this->__("TAST_4")." </td>";
		echo "<td class='tbl1 bold' align='center'> ".$this->__("TAST_5")." </td>";
		echo "<td class='tbl1 bold' align='center'> ".$this->__("TAST_6")." </td>";
	echo "</tr>";

	foreach ($admins as $a) {
		echo "<tr>";
			echo "<td class='tbl1' width='150'> <a href='".urlProfile.$a['admin_user']."'>".$a['user_name']."</a></td>";
			echo "<td class='tbl1' align='center'> ".dbcount("(protest_id)", dbProtest, "protest_admin='".$a['admin_user']."' AND protest_status='2' AND protest_time_done BETWEEN ".$cday." AND ".$ctime)." </td>";
			echo "<td class='tbl1' align='center'> ".dbcount("(protest_id)", dbProtest, "protest_admin='".$a['admin_user']."' AND protest_status='2' AND protest_time_done BETWEEN ".$cweek." AND ".$ctime)." </td>";
			echo "<td class='tbl1' align='center'> ".dbcount("(protest_id)", dbProtest, "protest_admin='".$a['admin_user']."' AND protest_status='2' AND protest_time_done BETWEEN ".$cmonth." AND ".$ctime)." </td>";
			echo "<td class='tbl1' align='center'> ".dbcount("(protest_id)", dbProtest, "protest_admin='".$a['admin_user']."' AND protest_status='2'")." </td>";
			echo "<td class='tbl1' align='center'> ".dbcount("(protest_id)", dbProtest, "protest_admin='".$a['admin_user']."' AND protest_status='1'")." </td>";
		echo "</tr>";
	}
	
echo "</table>";

