<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  M_accounts.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?> 
<!-- Game Accounts list -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'> <?=$this->__("TACP_1")?> </td> </tr>
	<?php
	djmDB::Select("SELECT * FROM ".dbAccountsPattern);
	if (djmDB::Num()) {
	foreach(djmDB::fullData() as $account) {
	
			/* Define text to pattern_limit */
			if ($account['pattern_limit'] == 43200) {	$limit = $this->__("TACP_2"); }
			elseif ($account['pattern_limit'] == 86400) { $limit = $this->__("TACP_3"); }
			elseif ($account['pattern_limit'] == 172800) { $limit = $this->__("TACP_4"); }
			elseif ($account['pattern_limit'] == 259200) { $limit = $this->__("TACP_5"); }
			elseif ($account['pattern_limit'] == 604800) { $limit = $this->__("TACP_6"); }
	?>
	<tr> 
		<td class='tbl1 bold'> <?=$account['pattern_name']?> </td>
		<td class='tbl1' width='180'> <?=$this->__("TACP_7")?>: <strong><?=$limit?></strong> <?=$this->Tooltip($this->__("TACP_8"))?> </td>
		<td class='tbl1' width='160'> <?=$this->__("TACP_9")?>:  <strong><?=dbcount("(*)", dbAccounts, "account_key='".$account['pattern_key']."'")?></strong> <?=$this->Tooltip($this->__("TACP_10"))?> </td>
		<td class='tbl1' align='center' width='120'> <a href='<?=$page['location']?>&amp;edit_account=<?=$account['pattern_id']?>'><?=$this->__("TACP_11")?></a> - <a href='<?=$page['location']?>&amp;delete_account=<?=$account['pattern_id']?>'><?=$this->__("TACP_12")?></a> </td>
	</tr>
	<?php } } else { ?>
		<tr> <td class='tbl1' align='center' colspan='2'> <?=$this->__("TACP_13")?> </td> </tr>
	<?php } ?>
</table> 
<br/>

<?php
	if (isset($_GET['edit_account']) && isnum($_GET['edit_account'])) { 
	$this->Form("EditAccount", "POST", "acc_edit");
	$this->Input("hidden", "parent", $edit['account']['pattern_id']);
?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TACP_14")?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TACP_15")?>: </td> <td class='tbl1'> <?=$this->Input("text", "acc_name", $edit['account']['pattern_name'], $this->__("TACP_15"))?> <?=$this->Tooltip($this->__("TACP_16"), true)?> </td> </tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TACP_17")?>: </td> 
		<td class='tbl1'> 
			<select name='acc_limit' class='textbox'>
				<option value='43200' <?=($edit['account']['pattern_limit'] == 43200 ? "selected='selected'":"")?>><?=$this->__("TACP_2")?></option>
				<option value='86400' <?=($edit['account']['pattern_limit'] == 86400 ? "selected='selected'":"")?>><?=$this->__("TACP_3")?></option>
				<option value='172800' <?=($edit['account']['pattern_limit'] == 172800 ? "selected='selected'":"")?>><?=$this->__("TACP_4")?></option>
				<option value='259200' <?=($edit['account']['pattern_limit'] == 259200 ? "selected='selected'":"")?>><?=$this->__("TACP_5")?></option>
				<option value='604800' <?=($edit['account']['pattern_limit'] == 604800 ? "selected='selected'":"")?>><?=$this->__("TACP_6")?></option>
			</select>
			<?=$this->Tooltip($this->__("TACP_8"), true)?>
		</td>
	</tr>
	
	<tr>
		<td class='tbl1' align='right' width='30%' valign='top'> <?=$this->__("TACP_18")?>: </td>
		<td class='tbl1'> 
			<?=$this->Tooltip($this->__("TACP_19"), true)?> <br/>
			<textarea name='acc_tutorial' class='textbox' style='width: 360px; height: 110px;'><?=$edit['account']['pattern_information']?></textarea>
			<?php 
				include_once(INCLUDES."bbcode_include.php");
				echo display_bbcodes("370px", "acc_tutorial", "acc_edit");
			?>
			<br/>
		</td>
	</tr>
	
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TACP_20"))?> <?=$this->__("TACP_21")?> <a href='<?=$page['location']?>'><?=$this->__("TACP_22")?></a> </td> </tr>
</table>
</form>

<?php } else { ?>

<!-- Create Account -->
<?php $this->Form("CreateAccount", "POST", "acc_create"); ?>
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='2' class='forum-caption'> <?=$this->__("TACP_23")?> </td> </tr>
	<tr> <td class='tbl1' align='right' width='30%'> <?=$this->__("TACP_15")?>: </td> <td class='tbl1'> <?=$this->Input("text", "acc_name", null, $this->__("TACP_15"))?> <?=$this->Tooltip($this->__("TACP_16"), true)?> </td> </tr>
	
	<tr> 
		<td class='tbl1' align='right' width='30%'> <?=$this->__("TACP_17")?>: </td> 
		<td class='tbl1'> 
			<select name='acc_limit' class='textbox'>
				<option value=''></option>
				<option value='43200'><?=$this->__("TACP_2")?></option>
				<option value='86400'><?=$this->__("TACP_3")?></option>
				<option value='172800'><?=$this->__("TACP_4")?></option>
				<option value='259200'><?=$this->__("TACP_5")?></option>
				<option value='604800'><?=$this->__("TACP_6")?></option>
			</select>
			<?=$this->Tooltip($this->__("TACP_8"), true)?>
		</td>
	</tr>
	
	<tr>
		<td class='tbl1' align='right' width='30%' valign='top'> <?=$this->__("TACP_18")?>: </td>
		<td class='tbl1'> 
			<?=$this->Tooltip($this->__("TACP_19"), true)?> <br/>
			<textarea name='acc_tutorial' class='textbox' style='width: 360px; height: 110px;'></textarea>
			<?php 
				include_once(INCLUDES."bbcode_include.php");
				echo display_bbcodes("370px", "acc_tutorial", "acc_create");
			?>
			<br/>
		</td>
	</tr>
	
	<tr> <td class='tbl1' colspan='2'> <?=$this->Input("submit", "submit", $this->__("TACP_24"))?> </td> </tr>
</table>
</form>
<?php } ?>