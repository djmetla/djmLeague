<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Index.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>

<?php
	foreach ($admin_index_pages as $v => $k) {
		echo "<div class='left tbl1 bold tbl-border' style='width: 99%; margin: 5px 0px 5px 0px;'>".$v."</div><br/>";
		echo "<div>";
		foreach ($k as $d => $c) {
			if ($this->isAdmin($c['flag'])) {
			echo "<div onclick=\"location.href='".urlAdmin.$d."'\" class='left tbl1 tbl-border' style='cursor:pointer; text-align:center; width: 115px; height: 65px; margin: 0px 3px 3px 0px; padding-top: 15px;'>";
				$this->Icon($c['image']); echo"<br/><br/";
				echo "<strong>".$c['name']."</strong><br/>";
			echo "</div>";
			}
		}
		echo "</div>";
	}
?>