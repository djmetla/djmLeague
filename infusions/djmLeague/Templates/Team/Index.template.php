<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:  Index.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
 ?>
<!--Team-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr>
		<?php if ($team['team_avatar'] != "") { ?>
		<td class='tbl1' rowspan='6' width='110' valign='top' align='center'> 
			<img src='<?=pathMedia?>Team/<?=$team['team_avatar']?>' width='100' height='100' alt='Avatar' /> <br/>
		</td>
		<?php } ?>
		<td class='tbl1' colspan='2'> <?=$this->Icon($team['team_flag'], "Flags")?> <span class='bold' style='position:relative; top:-2px;'><?=$team['team_name']?> (<?=$team['team_tag']?>)</span> <span class='right'>#<?=$team['team_id']?></span> </td>

		<td class='tbl1' rowspan='6' width='110' valign='top'>
			<strong><?=$this->__("TTI_1")?>: </strong> <br/><br/>
			<?php if ($team['team_web'] != "") { ?> <?=THEME_BULLET?> <a href='<?=$team['team_web']?>' target='_blank' rel='nofollow'><?=$this->__("TTI_2")?></a> </br> <?php } ?>
			<?php if (!$isMember) { ?> <?=THEME_BULLET?> <a href='<?=urlTool?>join&amp;team=<?=$team['team_id']?>'><?=$this->__("TTI_3")?></a> <br/> <?php } ?>
			<?php if (iMEMBER && !$isMember) { ?> <?=THEME_BULLET?> <a href='<?=urlTool?>challenge&amp;opponent_team=<?=$team['team_id']?>'><?=$this->__("TTI_4")?></a> <br/> <?php } ?>
			<?php if ($isMember && ($isMember == "CR" || $isMember == "CL")) { ?>  <?=THEME_BULLET?> <a href='<?=$page['location']?>&amp;option=settings'><?=$this->__("TTI_5")?></a> <br/> <?php } ?>	
			<?php if ($isMember && ($isMember == "CR")) { ?>  <?=THEME_BULLET?> <a href='<?=$page['location']?>&amp;option=squad'><?=$this->__("TTI_6")?></a> <br/> <?php } ?>	
			<?php if ($isMember && ($isMember == "CR")) { ?>  <?=THEME_BULLET?> <a href='<?=$page['location']?>&amp;option=activate'><?=($team['team_status'] == "INACTIVE" ? $this->__("TTI_7"):$this->__("TTI_8"))?></a> <br/> <?php } ?>	
			<?php if ($isMember) { ?>  <?=THEME_BULLET?> <a href='<?=$page['location']?>&amp;option=leave'><?=$this->__("TTI_9")?></a> <br/> <?php } ?>
			
		</td>		
	
	</tr>
	<tr> <td class='tbl1'> <?=$this->__("TTI_10")?>:</td> <td class='tbl1'> <?=$this->Icon($team['game_icon'], "Game")?> <a href='<?=urlLeague.$team['team_league']?>'><?=$team['league_name']?></a> </td> </tr>
	<tr> <td class='tbl1'> <?=$this->__("TTI_11")?>:</td> <td class='tbl1'> <?=$this->TeamPoints($team['team_id'], $team['team_points'])?> <?=($team['team_status'] == "ACTIVE" ? "<span class='right green'>".$this->__("TTI_12")."</span>":"<span class='right red'>".$this->__("TTI_13")."</span>")?> </td> </tr>
	<tr> <td class='tbl1'> <?=$this->__("TTI_14")?>:</td> <td class='tbl1'> <?=$this->teamRank($team['team_id'], $team['league_id'])?> </td> </tr>
	<tr> <td class='tbl1'> <?=$this->__("TTI_15")?>:</td> <td class='tbl1'> <?=$this->Profile($team['team_founder'], $team['user_name'])?> <span class='right'><?=strftime("%d.%m.%Y %H:%M", $team['team_created'])?></span> </td> </tr>
	<tr> <td class='tbl1'> <?=$this->__("TTI_16")?>:</td> <td class='tbl1'>  <span class='left' style='position: relative; top:-1px;'><?=$this->__("TTI_17")?>:</span> <span class='left' style='text-align: center;'><?=$this->TeamActivity($team['team_id'])?></span></td> </tr>
</table>	
<br/>

<!--Team Squad-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'><?=$this->__("TTI_18")?></td> </tr>
	<?php 
		djmDB::Select("SELECT t1.*, t2.*, t3.player_flag FROM ".dbTeamPlayer." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.player_user LEFT JOIN ".dbPlayer." as t3 ON t3.player_user=t1.player_user WHERE player_team='%d'", $team['team_id']);
		if (djmDB::Num()) {
		foreach(djmDB::fullData() as $player) {
			if ($player['player_position'] == "CR") { $pos = "<span class='orange'>[CR]</span>"; }
			elseif ($player['player_position'] == "CL") { $pos = "<span class='blue'>[CL]</span>"; }
			else { $pos = "[PL]"; }
			$status = func_UserStatus($player['player_time'], $team['league_block_status']);
	?>
	<tr>
		<td class='tbl1 bold' align='center' width='20'> <?=$pos?> </td>
		<td class='tbl1'> <?=$this->Icon($player['player_flag'], "Flags")?> <?=$this->Profile($player['user_id'], $player['user_name'], "style='position:relative;top:-2px;'")?> </td>
		<td class='tbl1' align='center' width='170'> <?=$this->GameID($player['user_id'], $team['team_league'])?> </td>
		<td class='tbl1' align='center' width='120'> <?=($status['status'] ? "<span class='green'>".$this->__("TTI_19")."</span>":"<span class='red'>".$this->__("TTI_20")."</span> ".$this->Tooltip($this->__("TTI_21", $status['day'])))?> </td>
	</tr>	
	<?php } } else { ?>
		<tr> <td colspan='4' class='tbl1' align='center'> <?=$this->__("TTI_22")?></td> </tr> 
	<?php } ?>
</table>
<br/>


<?php
	if ($isMember) { 
	djmDB::Select("SELECT t1.*, t2.team_name as t1Name, t2.team_flag as t1Flag, t3.team_name as t2Name, t3.team_flag as t2Flag 
								FROM ".dbChallenge." as t1 
								LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.challenge_to
								LEFT JOIN ".dbTeam." as t3 ON t3.team_id=t1.challenge_from
								WHERE challenge_to='%d' OR challenge_from='%d'", array($team['team_id'], $team['team_id']));
	if (djmDB::Num()) {
?>
<!--Team Challenge-->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'><?=$this->__("TTI_23")?></td> </tr>
	<?php 
		foreach (djmDB::fullData() as $challenge) { 
			if ($challenge['challenge_to'] == $team['team_id']) {
				$enemy['flag'] = $challenge['t2Flag'];
				$enemy['name'] = "<a href='".urlTeam.$challenge['challenge_from']."' style='position: relative; top:-2px;'>".$challenge['t2Name']."</a>";
			} else {
				$enemy['flag'] = $challenge['t1Flag'];
				$enemy['name'] = "<a href='".urlTeam.$challenge['challenge_to']."' style='position: relative; top:-2px;'>".$challenge['t1Name']."</a>";		
			}
	?>
	<tr>
		<td class='tbl1' width='20' align='center' title='<?=($challenge['challenge_to'] == $team['team_id'] ? $this->__("TTI_24"):$this->__("TTI_25"))?>'> <?=$this->Icon(($challenge['challenge_to'] == $team['team_id'] ? "in.png":"out.png"))?> </td>
		<td class='tbl1'><?=$this->Icon($enemy['flag'], "Flags")?> <?=$enemy['name']?> </td>
		<td class='tbl1' width='120' align='center'> <?=strftime("%d.%m.%Y %H:%M", $challenge['challenge_time'])?> </td>
		<td class='tbl1' align='center' width='75'> 
			<a href='<?=urlTool?>challenge&amp;view=<?=$challenge['challenge_id']?>' title='<?=$this->__("TTI_26")?>'><?=$this->Icon("more.png")?></a> 
		</td>
	</tr>
	<?php } ?>
</table>	
<br/>
<?php } } ?>

<!--Team match -->
<table border='0' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='6' class='forum-caption'><?=$this->__("TTI_27")?></td> </tr>
	<?php 
	djmDB::Select("SELECT * FROM ".dbMatch." WHERE (match_t1='%d' OR match_t2='%d') AND match_status !='3' ORDER BY match_id DESC ".(!isset($_GET['All_Match']) ? "LIMIT 10":"")."", array($team['team_id'], $team['team_id']));
	if (djmDB::Num()) {
	foreach(djmDB::fullData() as $match) {
			
		djmDB::Select("SELECT * FROM ".dbTeam." WHERE team_id='%d'", ($match['match_t1'] == $team['team_id'] ? $match['match_t2']:$match['match_t1']));
		$enemy = djmDB::Data();
		
		if ($match['match_t1'] == $team['team_id']) {
			if ($match['match_t1_score'] > $match['match_t2_score']) {
				$color = "green";
			} elseif ($match['match_t1_score'] < $match['match_t2_score']) {
				$color = "red";
			} else { 
				$color = "blue";
			}
		} else {
			if ($match['match_t1_score'] > $match['match_t2_score']) {
				$color = "red";
			} elseif ($match['match_t1_score'] < $match['match_t2_score']) {
				$color = "green";
			} else { 
				$color = "blue";
			}		
		}
	?>
		<tr>
			<td width='2' class='tbl1' style='background-color: <?=($match['match_status'] == 2 ? $color : "")?>; width: 2px;'>&nbsp;</td>
			<td class='tbl1'> 
				<span style='opacity:0.5; position: relative; top:-2px;'>vs.</span>&nbsp;&nbsp;
				<?=$this->Icon($enemy['team_flag'], "Flags")?> <a href='<?=urlTeam.$enemy['team_id']?>' style='position: relative; top:-2px;'><?=$enemy['team_name']?></a> 
				<span class='right'>
					<?php
						if (dbcount("(*)", dbProtest, "protest_match='".$match['match_id']."' AND protest_from_team!='".$team['team_id']."' AND protest_status!='2'")) {
							echo "<span title='".$this->__("TTI_28")."'>"; $this->Icon("protest.png"); echo "</span> ";
						}
						if (dbcount("(*)", dbRequest, "request_match='".$match['match_id']."' AND request_author_team!='".$team['team_id']."'")) {
							echo "<span title='".$this->__("TTI_29")."'>"; $this->Icon("request.png"); echo "</span>";
						}						
					?>
				</span> 
			</td>
			<td class='tbl1' align='center' width='120'> <?=strftime("%d.%m.%Y %H:%M", $match['match_time'])?></td>
			<?php if ($team['league_match_use_maps'] == "YES") { ?> <td class='tbl1' align='center' width='75'> <?=$match['match_map']?> </td> <?php } ?>
			<td class='tbl1' align='center' width='50'>  
				<?php
					if ($match['match_status'] == 0) {
						echo "<span title='".$this->__("TTI_30")."'>"; $this->Icon("loader.gif"); echo "</span>";
					} else {
						if ($match['match_t1_score'] > $match['match_t2_score']) {
							$return = "<span class='green'>".$match['match_t1_score']."</span> : <span class='red'>".$match['match_t2_score']."</span>";
						} elseif ($match['match_t1_score'] < $match['match_t2_score']) {
							$return = "<span class='red'>".$match['match_t1_score']."</span> : <span class='green'>".$match['match_t2_score']."</span>";
						} else {
							$return = "<span class='blue'>".$match['match_t1_score']."</span> : <span class='blue'>".$match['match_t2_score']."</span>";
						}
						echo $return;
					}
				?>
			</td>
			<td class='tbl1' align='center'> <a href='<?=urlMatch.$match['match_id']?>'><?=$this->Icon("more.png")?></a> </td>
		</tr>
	<?php } ?>
		<tr>
			<td class='tbl1' colspan='6'><?=THEME_BULLET?> <?=(isset($_GET['All_Match']) ? "<a href='".$page['location']."'>".$this->__("TTI_31")."</a>":"<a href='".$page['location']."&amp;All_Match'>".$this->__("TTI_32")."</a>")?> </td>
		</tr>
	<?php } else { ?> 
		<tr> <td class='tbl1' align='center'> <?=$this->__("TTI_33")?> </td> </tr>
	<?php } ?>
</table>	
<br/>


<?php
	djmDB::Select("SELECT t1.*, t2.user_name FROM ".dbComplaint." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.complaint_admin WHERE complaint_team='%d'", $team['team_id']);
	if (djmDB::Num()) {
?>
<!--Team complaints -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'><?=$this->__("TTI_34")?></td> </tr>
	<?php 	foreach(djmDB::fullData() as $c) {  
		if ($c['complaint_type'] == "+") { $c_color = "green"; $c_type = "+"; } else { $c_color = "red"; $c_type = "-"; }
		$string = $c['complaint_reason'];		
		$reason = preg_replace("^#([0-9]+)^", "<a href='".urlMatch."$1'>#$1</a>",$string);
	?>
	<tr>
		<td class='tbl1'><?=$this->Profile($c['complaint_admin'], $c['user_name'])?> </td>
		<td class='tbl1 bold' align='center'> <span class='<?=$c_color?>'><?=$c_type.$c['complaint_value']?></span>   </td>
		<td class='tbl1'> <?=$reason?> </td>
	</tr>
	<?php } ?>
</table>	
<br/>
<?php } ?>

<!--Team logs -->
<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
	<tr> <td colspan='4' class='forum-caption'><?=$this->__("TTI_35")?></td> </tr>
	<tr> 
		<td>
			<?php
				echo "<div id='djmScroll' class='tbl-border'>";
					djmDB::Select("SELECT t1.*, t2.user_id, t2.user_name FROM ".dbLog." as t1 LEFT JOIN ".DB_USERS." as t2 ON t2.user_id=t1.log_user WHERE log_type='team' AND log_parent='%d' ORDER BY log_id DESC", $team['team_id']);
					if (djmDB::Num()) { 
						foreach (djmDB::fullData() as $log) {
							echo "<div class='left tbl1' style='width: 98%; margin-bottom: 1px;'>";
								echo "<div class='left' style='width: 120px;'>".strftime("%d.%m.%Y %H:%M", $log['log_time'])."</div>";
								echo "<div class='left' style='width: 100px;'> ".$this->profile($log['user_id'], $log['user_name'])."</div>";
								echo "<div class='left'>".$log['log_value']."</div>";
							echo "</div>";
						} 
					} else {
						echo "<center>".$this->__("TTI_36")."</center>"; 
					}
				echo "</div>";
			?>	
		</td>
	</tr>
</table>	
<br/>	