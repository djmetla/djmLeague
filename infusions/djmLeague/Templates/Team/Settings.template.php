<?php 
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Settings.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
$this->Form("TeamSettings", "POST", null, true);
$this->Input("hidden", "parent", $team['team_id']);
?>

<fieldset style='border: 1px solid silver;'>
	<legend><?=$this->__("TTS_1")?></legend>
	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
		<tr>
			<td class='tbl1' align='right' width='30%'> <?=$this->__("TTS_2")?>:</td> 
			<td class='tbl1'>
				<?=$this->Input("text", "team_name", $team['team_name'], $this->__("TTS_3"))?> 
				<?=$this->Tooltip($this->__("TTS_4"),true)?> 
			</td> 
		</tr>

		<tr>
			<td class='tbl1' align='right' width='30%'> <?=$this->__("TTS_5")?>:</td> 
			<td class='tbl1'>
				<?=$this->Input("text", "team_tag", $team['team_tag'], $this->__("TTS_6"))?> 
				<?=$this->Tooltip($this->__("TTS_7"),true)?> 
			</td> 
		</tr>	
		
		<tr>
			<td class='tbl1' align='right' width='30%'> <?=$this->__("TTS_8")?>:</td> 
			<td class='tbl1'>
				<?=$this->Input("text", "team_password", $team['team_password'], $this->__("TTS_9"))?> 
				<?=$this->Tooltip($this->__("TTS_10"),true)?> 
			</td> 
		</tr>	
	</table>	
</fieldset>
<br/>

<fieldset style='border: 1px solid silver'>
	<legend><?=$this->__("TTS_11")?></legend>
	<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1' class='tbl-border'>
		<tr>
			<td class='tbl1' align='right' width='30%'> <?=$this->__("TTS_12")?>:</td> 
			<td class='tbl1'>
				<?=$this->Flags("team_national", $team['team_flag'])?>
				<?=$this->Tooltip($this->__("TTS_13"), true)?>
			</td> 
		</tr>

		<tr>
			<td class='tbl1' align='right' width='30%'> <?=$this->__("TTS_14")?>:</td> 
			<td class='tbl1'>
				<?=$this->Input("text", "team_web", $team['team_web'], "http://")?> 
				<?=$this->Tooltip($this->__("TTS_15"),true)?> 
			</td> 
		</tr>	
		
		<tr>
			<td class='tbl1' align='right' width='30%'> <?=$this->__("TTS_16")?>:</td> 
			<td class='tbl1'>
			<?php if ($team['team_avatar'] != "") { ?>
				<?=$this->Input("checkbox", "team_avatar_del", $team['team_avatar'])?><span style='position:relative;top:-2px;'><?=$this->__("TTS_21")?></span> 
			<?php } else { ?>
				<?=$this->Input("file", "team_avatar")?> 
				<?=$this->Tooltip($this->__("TTS_17"),true)?> 
			<?php } ?>
			</td> 
		</tr>
	</table>	
</fieldset>

<div style='padding: 5px;'> <?=$this->Input("submit", "submit", $this->__("TTS_18"))?> <?=$this->__("TTS_19")?> <a href='<?=$page['location']?>'><?=$this->__("TTS_20")?></a> </div>
</form>