<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Leave.template.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die("Access Denied!"); }
?>

<p style='width: 100%; text-align:center;'>
	<?=$this->__("TTL_1")?> <?=$team['team_name']?>. <?=$this->__("TTL_2")?> <?=$team['team_name']?>?
	<br/>
	<center>
	<?php
		$this->Form("LeaveTeam");
		$this->Input("hidden", "parent", $team['team_id']);
		$this->Input("submit", "submit", $this->__("TTL_3")." ".$team['team_name']);
	?>
	<br/> <br/> <a href='<?=$page['location']?>'><?=$this->__("TTL_")?></a>
	</center>
	</form>
</p>