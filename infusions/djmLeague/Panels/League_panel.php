<?php
/* -------------------------------------------------------+
  | PHP-Fusion Content Management System
  | Copyright (C) 2002 - 2011 Nick Jones
  | http://www.php-fusion.co.uk/
  +--------------------------------------------------------+
  | Filename: 
  | Author: Patrik Hoffmann (djmetla)
  +--------------------------------------------------------+
  | This program is released as free software under the
  | Affero GPL license. You can redistribute it and/or
  | modify it under the terms of this license which you
  | can read by viewing the included agpl.txt or online
  | at www.gnu.org/licenses/agpl.html. Removal of this
  | copyright header is strictly prohibited without
  | written permission from the original author(s).
  +-------------------------------------------------------- */

  include_once INFUSIONS."djmLeague/Libraries/Panels.php";
  $djmLeague = new djmLeague();
  openside(  $djmLeague->__("PLEAGUE_1"));
	echo "<ul id='leagueList' style='list-style: none; margin: 0; padding: 0;'>";
		djmDB::Select("SELECT * FROM ".dbGame);
		foreach (djmDB::fullData() as $game) {
			echo "<li class='show-game' rel='".$game['game_id']."' style='cursor: pointer; font-weight: bold; margin-bottom: 5px;'><img src='".pathMedia."Game/".$game['game_icon']."' alt='?' style='position: relative; top: 2px;' /> ".$game['game_name']." </li>";
				djmDB::Select("SELECT * FROM ".dbLeague." WHERE league_game='%d'", $game['game_id']);
				if (djmDB::Num()) {
					echo "<ul id='game-".$game['game_id']."' style='display:none; list-style: none; margin: 0px 0px 0px 10px; padding: 0;'>";
						foreach(djmDB::fullData() as $league) {
							echo "<li class='show-league' rel='".$league['league_id']."' style='cursor: pointer; margin-bottom: 5px;'> ".THEME_BULLET." ".$league['league_name']." </li>";
								echo "<ul id='league-".$league['league_id']."'  style='display: none; list-style: none; margin: 0px 0px 0px 10px; padding: 0;'>";
									echo "<li style='margin-bottom: 2px;'> ".THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/index.php?league=".$league['league_id']."'>".$djmLeague->__("PLEAGUE_2")."</a> </a>";
									echo "<li style='margin-bottom: 2px;'> ".THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/index.php?league=".$league['league_id']."&amp;option=information'>".$djmLeague->__("PLEAGUE_3")."</a> </a>";
									echo "<li style='margin-bottom: 2px;'> ".THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/index.php?league=".$league['league_id']."&amp;option=rules'>".$djmLeague->__("PLEAGUE_4")."</a> </a>";
									echo "<li style='margin-bottom: 2px;'> ".THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/index.php?tool&amp;option=admins#".str_replace(" ", "-", $league['league_name'])."'>".$djmLeague->__("PLEAGUE_5")."</a> </a>";
								echo "</ul>";
						}
					echo "</ul>";
				}	
		}
	echo "</ul>";
  closeside();
  unset($djmLeague);
  ?>