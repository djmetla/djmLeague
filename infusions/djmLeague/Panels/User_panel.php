<?php
/* -------------------------------------------------------+
  | PHP-Fusion Content Management System
  | Copyright (C) 2002 - 2011 Nick Jones
  | http://www.php-fusion.co.uk/
  +--------------------------------------------------------+
  | Filename: admin_panel.php
  | Author: Patrik Hoffmann (djmetla)
  +--------------------------------------------------------+
  | This program is released as free software under the
  | Affero GPL license. You can redistribute it and/or
  | modify it under the terms of this license which you
  | can read by viewing the included agpl.txt or online
  | at www.gnu.org/licenses/agpl.html. Removal of this
  | copyright header is strictly prohibited without
  | written permission from the original author(s).
  +-------------------------------------------------------- */
  include_once INFUSIONS."djmLeague/Libraries/Panels.php"; 
  $djmLeague = new djmLeague();
  if (iMEMBER) { 
	$u = $djmLeague->isPlayer();
	djmDB::Select("SELECT * FROM ".dbSettings);
	$panelCfg = djmDB::Data();
	$djmLeague->Input("hidden", "eventBoxReload", $panelCfg['panel_event_refresh']);
	
	$pos = $panelCfg['panel_event_position'];
	if ($pos == "LEFT-BOTTOM") {
		$style = "bottom: 10px; left: 10px"; /* LEFT-BOTTOM */
	} elseif ($pos == "LEFT-TOP") {
		$style = "top: 10px; left: 10px;"; /* LEFT-TOP */
	} elseif ($pos == "RIGHT-TOP") {
		$style = "top: 10px; right: 10px;"; /* RIGHT-TOP */
	} elseif ($pos == "RIGHT-BOTTOM") {
		$style = "bottom: 10px; right: 10px;"; /* RIGHT-BOTTOM */
	}
  ?> <div style='<?=$style?>' id='event' class='tbl-border tbl2' audio="<?=$u['player_sound']?>" onclick="location.href='<?=BASEDIR?>infusions/djmLeague/?player&amp;option=events'" rel='<?=$djmLeague->__("PUSER_10")?>'></div><?php 
  }
  
if (iMEMBER) {
	
	openside($djmLeague->__("PUSER_1"));
		echo "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
		djmDB::Select("SELECT t2.team_tag, t2.team_id, t2.team_points, t4.game_icon, t3.league_name, t4.game_name 
								FROM ".dbTeamPlayer." as t1 
								LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.player_team 
								LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t2.team_league
								LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
								WHERE player_user='%d'", $userdata['user_id']);
		if (djmDB::Num()) {
			foreach(djmDB::fullData() as $team) { 
				echo "<tr> <td class='tbl1' title='".$team['game_name']." - ".$team['league_name']."'>"; $djmLeague->Icon($team['game_icon'], "Game"); echo" <a class='side' href='".INFUSIONS."djmLeague/?team=".$team['team_id']."' style='position:relative; top: -1px;'>".$team['team_tag']."</a></td> <td class='tbl1' align='right'> <span style='position: relative; top:-1px;'>"; $djmLeague->TeamPoints($team['team_id'], $team['team_points']); echo"</span> </td> </tr>";
			}
		} else {
			echo "<tr> <td class='tbl1' align='center'> ".$djmLeague->__("PUSER_2")." </td> </tr>";
		}
		echo "</table>";
	closeside();

	$events = dbcount("(*)", DB_PREFIX."league_event", "event_user='".$userdata['user_id']."' AND event_read='0'");
    openside($djmLeague->__("PUSER_11"));
		echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?player'>".$djmLeague->__("PUSER_3")."</a> <br/>";
		echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?player&amp;option=events'>".$djmLeague->__("PUSER_4")."</a> ".($events ? "(<span style='color: #ff0000;'>".$events."</span>)":"")." <br/>";
		echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?player&amp;option=settings'>".$djmLeague->__("PUSER_5")."</a> <br/>";
		echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?tool&amp;option=create'>".$djmLeague->__("PUSER_6")."</a> <br/>";
		echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?tool&amp;option=join'>".$djmLeague->__("PUSER_7")."</a> <br/>";
		echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?tool&amp;option=opponent&amp;task=create'>".$djmLeague->__("PUSER_8")."</a> <br/>";
		if ($djmLeague->isAdmin()) { echo THEME_BULLET." <a class='side' href='".INFUSIONS."djmLeague/?admin'>".$djmLeague->__("PUSER_9")."</a> <br/>"; }
	closeside(); 
	unset($djmLeague);
}
