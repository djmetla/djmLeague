<?php
/* -------------------------------------------------------+
  | PHP-Fusion Content Management System
  | Copyright (C) 2002 - 2011 Nick Jones
  | http://www.php-fusion.co.uk/
  +--------------------------------------------------------+
  | Filename: Opponent_panel.php
  | Author: Patrik Hoffmann (djmetla)
  +--------------------------------------------------------+
  | This program is released as free software under the
  | Affero GPL license. You can redistribute it and/or
  | modify it under the terms of this license which you
  | can read by viewing the included agpl.txt or online
  | at www.gnu.org/licenses/agpl.html. Removal of this
  | copyright header is strictly prohibited without
  | written permission from the original author(s).
  +-------------------------------------------------------- */
  include_once INFUSIONS."djmLeague/Libraries/Panels.php";
  $djmLeague = new djmLeague();  
  djmDB::Select("SELECT * FROM ".dbSettings);
  $panelCfg = djmDB::Data();
  
  
  if ($panelCfg['panel_search_type'] == "OPENTABLE") {
  opentable($djmLeague->__("POPPONENT_1"));
  } else {
  openside($djmLeague->__("POPPONENT_1"));
  }
  
	echo "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
	
		$count = dbcount("(search_id)", dbSearch);
		djmDB::Select("SELECT t1.*,
											t2.team_id, t2.team_name, t2.team_points, t2.team_flag,
											t3.league_name, t3.league_match_use_server, t3.league_id, t3.league_match_use_maps,
											t4.game_name, t4.game_icon
								FROM ".dbSearch." as t1
								LEFT JOIN ".dbTeam." as t2 ON t2.team_id=t1.search_team
								LEFT JOIN ".dbLeague." as t3 ON t3.league_id=t1.search_league
								LEFT JOIN ".dbGame." as t4 ON t4.game_id=t3.league_game
								WHERE search_time>'".time()."' ORDER BY search_time ASC LIMIT ".$panelCfg['panel_search_limit']."
		");
		if (djmDB::Num()) {
		foreach (djmDB::fullData() as $v) {
			
			if (iMEMBER) {
			$iHaveTeam = dbcount("(player_id)", dbTeamPlayer, "player_user='".$userdata['user_id']."' AND player_position !='PL' AND player_team_league='".$v['league_id']."'");
			} else {
			$iHaveTeam = false;
			}
			
			// This is my request
			$player = $djmLeague->isTeamPlayer($v['team_id']);
			
			// Show action
			if (!$player && $iHaveTeam && iMEMBER) {
				$action = "<a href='".INFUSIONS."djmLeague/?tool&amp;option=opponent&amp;task=accept[".$v['search_id']."]'>";
				$action .= "<img src='".INFUSIONS."djmLeague/Media/System/accept.png' alt='Accept' title='".$djmLeague->__("POPPONENT_2")."'/>";
				$action .= "</a>";
			} elseif ($player && $player != "PL") {
				$action = "<a href='".INFUSIONS."djmLeague/?tool&amp;option=opponent&amp;task=reject[".$v['search_id']."]'>";
				$action .= "<img src='".INFUSIONS."djmLeague/Media/System/decline.png' alt='Cancel' title='".$djmLeague->__("POPPONENT_3")."'/>";
				$action .= "</a>";
			} else {
				$action = false;
			}
			
			// Show request server
			if ($v['search_server'] != "" && $v['league_match_use_server'] == "YES") { 
				if ($v['search_server'] == "YES") { 
					$server = "Server: <span class='green'>".$djmLeague->__("POPPONENT_4")."</span> /";
				} else {
					$server = "Server: <span class='red'>".$djmLeague->__("POPPONENT_5")."</span> /";
				}
			} else {
				$server = "";
			}
			
			$additional = "";
			if ($v['search_map'] != "" && $v['league_match_use_maps'] == "YES") {
				$maps = explode(".", $v['search_map']);
				$additional .= "[".$djmLeague->__("POPPONENT_6").":"; $map_i = 0;
				foreach($maps as $map) { $map_i++; $additional .= $map.(count($maps) == $map_i ? "":", "); }
				$additional .= "] ";
			}	
			
			if ($v['search_limit_positive'] != 0) { 
				$additional .= "[Max: ".$v['search_limit_positive']." ".$djmLeague->__("POPPONENT_7")."] ";
			}
			
			if ($v['search_limit_negative'] != 0) { 
				$additional .= "[Min: ".$v['search_limit_negative']." ".$djmLeague->__("POPPONENT_7")."]";
			}			
		
		echo "<tr>";
			echo "<td class='tbl2'>"; $djmLeague->Icon($v['game_icon'], "Game"); echo " ".$v['league_name']."</td>";
			echo "<td class='tbl2'>"; $djmLeague->Icon($v['team_flag'], "Flags"); echo " <a href='".INFUSIONS."djmLeague/?team=".$v['team_id']."' style='position:relative; top:-2px;'>".$v['team_name']."</a></td>";
			echo "<td class='tbl2'>".strftime("%d.%m.%Y %H:%M", $v['search_time'])."</td>";
			echo "<td class='tbl2'>".$server." <span title='".$additional."' style='color: blue; cursor:help;'>".$djmLeague->__("POPPONENT_8")."</a></td>";
			echo "<td class='tbl2'>".$action."</td>";
		echo "</tr>";
		} } else {
			echo "<tr> <td class='tbl2' align='center'> ".$djmLeague->__("POPPONENT_9")." </td> </tr>";
		}
		if ($count > $panelCfg['panel_search_limit']) { 
			echo "<tr> <td colspan='5' class='tbl2' align='center'><a href='".INFUSIONS."djmLeague/?tool&amp;option=opponent&amp;task=all'>".$djmLeague->__("POPPONENT_10")." (".($count-10).")</a> </td> </tr>";  
		}
	echo "</table>";
  if ($panelCfg['panel_search_type'] == "OPENTABLE") {
  closetable();
  } else {
  closeside();
  }
  
  dbquery("DELETE FROM ".dbSearch." WHERE search_time<".time() );
  unset($djmLeague, $panelCfg);